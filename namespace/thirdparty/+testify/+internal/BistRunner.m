% Copyright (C) 2019 Andrew Janke
%
% This file is part of Octave.
%
% Octave is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Octave is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Octave; see the file COPYING.  If not, see
% <https://www.gnu.org/licenses/>.

classdef BistRunner < handle
  %BISTRUNNER Runs BISTs for a single source file
  %
  % This is basically the implementation for the test2() function.

  properties
    file  % The source code file the tests are drawn from. This may be an absolute or relative path.
    run_demo = false;  % Whether to run demo blocks when running tests    
    fail_fast = false;  % If true, will abort the test run immediately upon any failure
    show_failure_details = false;  % If true, when a test block fails, will print out detailed info about it to the console 
    
    % Whether the test blocks should be shuffled.
    %  false:   no shuffle (default)
    %  true:    shuffle using a seed that BistRunner picks
    %  numeric: shuffle using this seed
    shuffle = false;  
    save_workspace_on_failure = false;  % Whether to save workspaces for failed tests
  end

  properties (SetAccess = private)
    run_tmp_dir;  % Temp dir for BistRunner's data for a particular run (for this whole file)
  end  

    methods (Static)
        function file = locate_test_file( name, verbose, fid );
        % Locates file to run tests on for a name.
        % If not found, emits a diagnostic message about tests-not-found.
        %
        % inputs:
        %   name - file to search for, loosely defined. The name is the string
        %          that is passed in to the test2 function.
        %   verbose - whether to print diagnostic messages to fid when file is not found.
        %          Defaults to false.
        %   fid - file id to write progress messages to. Uses stdout if omitted.
        % outputs:
        %   file - full path to located file, including extension (charvec). Empty
        %     if file was not found.
        if( nargin < 2 || isempty(verbose) );
            verbose = false; end;
        if( nargin < 3 || isempty(fid) );
            fid = stdout; end

        file = file_in_loadpath (name, 'all');
        if( ~isempty(file) );
            file = file{1};
            return; end;
        file = file_in_loadpath ([name '.m'], 'all');
        if( ~isempty(file) );
            file = file{1};
            return; end;
        file = file_in_loadpath ([name '.cc'], 'all');
        if( ~isempty (file) );
            file = file{1};
            return; end;
        testsdir = octave_config_info__ ('octtestsdir');
        candidates = {
            fullfile(testsdir, name);
            fullfile(testsdir, [name '-tst']);
            fullfile(testsdir, [name '.cc-tst']);
            fullfile(testsdir, [name '.in.yy-tst']);
        };
        for i = 1:numel( candidates );
            if( exist(candidates{i}, 'file') );
                file = candidates{i};
                break; end; end
        if( iscell (file) );
            if( isempty (file) );
                file = '';
            else;
                file = file{1}; end; end;  % If there are duplicates, pick the first in path. 
            
        if( ~isempty (file) );
            return; end;
        
        if( verbose );
            ftype = exist( name );  %#ok<EXIST>
            if( ftype == 3 );
                fprintf (fid, '%s%s source code with tests for dynamically linked function not found\n', '????? ', name);
            elseif( ftype == 5 );
                fprintf (fid, '%s%s is a built-in function\n', '????? ', name);
            elseif( any (strcmp (operators__ (), name)) );
                fprintf (fid, '%s%s is an operator\n', '????? ', name);
            else;
                fprintf (fid, '%s%s does not exist in path\n', '????? ', name); end
            fflush (fid); end; 
        end
    end

    methods
        function this = BistRunner (file)
          %BISTRUNNER Construct a new BistRunner
            if( nargin == 0 );
                return; end;
            assert( ischar(file), 'BistRunner: file must be char; got a %s', class(file) );
            assert( exist(file, 'file')==2, 'BistRunner: file does not exist: %s', file );
            this.file = file;
        end

    function emit (this, fmt, varargin)
        fprintf (fmt, varargin{:});
    end

    function out = extract_demo_code (this)
      test_code = this.extract_test_code;
      blocks = this.parse_test_code (test_code);
      demo_blocks_txt = {};
      demo_blocks_ix = [];
      for i = 1:numel (blocks)
        block = blocks(i);
        if isequal (block.type, 'demo')
          demo_blocks_txt{end+1} = block.code; %#ok<AGROW>
          demo_blocks_ix(end+1) = numel (block.code) + 1; %#ok<AGROW>
        end
      end
      out.code = strjoin (demo_blocks_txt, '');
      out.ixs = demo_blocks_ix;
    end

    function out = maybe_shuffle_blocks (this, blocks)
      if this.shuffle
        if isnumeric (this.shuffle)
          shuffle_seed = this.shuffle;
        else
          shuffle_seed = now;
        end
        this.emit ('Shuffling test blocks with rand seed %.15f\n', shuffle_seed);
        out = testify.internal.Util.shuffle (blocks, shuffle_seed);
      else
        out = blocks;
      end
    end

    function pick_and_create_run_tmp_dir (this)
      tmp_dir_base = sprintf ('bist-run-%s', datestr(now, 'yyyy-mm-dd_HH-MM-SS'));
      tmp_dir_base = [tmp_dir_base '_' num2str(randi(10000000)) '_pid' num2str(getpid)];
      tmp_dir = fullfile (tempdir, 'octave/testify/BistRunner', tmp_dir_base);
      mkdir (tmp_dir);
      this.run_tmp_dir = tmp_dir;
    end

    function out = stashed_workspace_file (this)
      ws_dir = fullfile (this.run_tmp_dir, 'workspaces');
      out = fullfile (ws_dir, 'test-workspaces.mat');
    end

    function stash_test_workspace (this, tag, data)
      % Stashing the workspace must be done to a file, and not to appdata or somewhere
      % else in Octave memory, to avoid keeping live references to handle objects and
      % interfering with object lifecycle and cleanup, which could affect test behavior.
      ws_file = this.stashed_workspace_file;
      mkdir (fileparts (ws_file));
      if exist (ws_file, 'file')
        stash_data = load (ws_file);
      else
        stash_data = struct;
      end
      stash_data.(tag) = data;
      save (ws_file, '-struct', 'stash_data');
    end

    function clear_stashed_workspace (this)
      ws_file = this.stashed_workspace_file;
      if exist (ws_file, 'file')
        delete (ws_file);
      end
    end

    function [ out ] = run_tests( this )
      %RUN_TESTS Run the tests found in this file
      persistent signal_fail;
      if( isempty(signal_fail) ); 
          signal_fail  = '~~~~~'; end;
      persistent signal_empty;
      if( isempty(signal_empty) ); 
          signal_empty = '?????'; end;
      persistent signal_block;
      if( isempty(signal_block) ); 
          signal_block = '*****'; end;
      persistent signal_file;
      if( isempty(signal_file) ); 
          signal_file  = '>>>>>'; end;
      persistent signal_skip;
      if( isempty(signal_skip) ); 
          signal_skip  = '-----'; end;

      this.pick_and_create_run_tmp_dir;
      saved_files = false;

      test_code = this.extract_test_code;
      if isempty (test_code)
        this.emit ('%s %s has no tests\n', signal_empty, this.file);
        out = testify.internal.BistRunResult;
        out.files_processed{end+1} = this.file;
        return; end;
    
      this.emit ('\n');
      this.emit ('%s %s\n', signal_file, this.file);
      blocks = this.parse_test_code( test_code );

      blocks = this.maybe_shuffle_blocks( blocks );

      % Get initial state for tracking and cleanup
      fid_list_orig = fopen( 'all' );
      base_variables_orig = evalin('base', 'who');
      base_variables_orig{end+1} = 'ans';
      global_variables_orig = who ('global');
      orig_wstate = warning ();
      clean_warning = onCleanup( @() warning( orig_wstate ) );
      
      all_success = true;

      workspace = testify.internal.BistWorkspace;
      % rslt is a running tally of all the results
      rslt = testify.internal.BistRunResult;
      rslt.files_processed{end+1} = this.file;
      if( ~isempty(blocks) );
        rslt.files_with_tests{end+1} = this.file; end;
      functions_to_clear = {};

      try;
        for i_block = 1:numel (blocks)
          block = blocks(i_block);
          success = true;
          msg = '';

          this.emit ('%s %s \n', signal_block, block.dispstr);

          if this.save_workspace_on_failure
            this.clear_stashed_workspace;
            this.stash_test_workspace ('before', workspace.workspace);
            after_workspace = workspace.workspace;
          end
          t0 = tic;

          try;
            switch block.type

              case { 'test', 'xtest', 'assert', 'fail' }
                [success, rslt, msg, after_workspace] = run_test_code (this, block, workspace, rslt);

              case 'testif'
                have_feature = have_feature__ (block.feature);
                if have_feature
                  if isempty (block.runtime_feature_test) || eval (block.runtime_feature_test)
                    [success, rslt, msg, after_workspace] = run_test_code (this, block, workspace, rslt);
                  else
                    rslt.n_skip_runtime = rslt.n_skip_runtime + 1;
                    msg = [signal_skip 'skipped test (runtime test)'];
                  end
                else
                  rslt.n_skip_feature = rslt.n_skip_feature + 1;
                  msg = [signal_skip 'skipped test (missing feature)'];
                end

              case 'shared'
                workspace = testify.internal.BistWorkspace (block.vars);
                try;
                  workspace.eval (block.code);
                end;
                
                after_workspace = workspace.last_seen_workspace;
                workspace.clear_last_seen_workspace;
                

              case 'function'
                try
                  eval (block.code);
                  functions_to_clear{end+1} = block.function_name;
                catch err
                  success = false;
                  msg = [signal_fail 'test failed: syntax error in function definition\n' err.message];
                end

              case 'end'
                % NOP

              case 'demo'
                % Each demo gets evaled in its own workspace, with no shared variables
                demo_ws = testify.internal.BistWorkspace;
                if this.run_demo
                  try
                    demo_ws.eval (block.code);
                  catch err
                    success = false;
                    msg = [signal_fail 'demo failed\n' err.message];
                  end
                else
                  msg = [signal_skip 'demo skipped\n'];
                end

              case 'error'
                try
                  try;
                    workspace.eval (block.code);
                  end;
                    after_workspace = workspace.last_seen_workspace;
                    workspace.clear_last_seen_workspace;
                  
                  % No error raised - that's a test failure
                  success = false;
                  msg = [signal_fail 'no error raised.'];
                catch err
                  msg = '';
                  [ok, diagnostic] = this.error_matches_expected (err, block);
                  if ~ok
                    success = false;
                    msg = [signal_fail 'Incorrect error raised: ' diagnostic];
                  end
                end

              case 'warning'
                lastwarn ('');
                orig_warn_state = warning ('query', 'quiet');
                warning ('on', 'quiet');
                try;
                  try;
                    try;
                      workspace.eval (block.code);
                    catch me; %#ok<NASGU>
                        end;
                    after_workspace = workspace.last_seen_workspace;
                    workspace.clear_last_seen_workspace;
                    
                    [warn_msg, warn_id] = lastwarn;
                    [ok, diagnostic] = this.warning_matches_expected (warn_msg, warn_id, block);
                    if( ~ok );
                      success = false;
                      msg = [signal_fail 'Incorrect warning raised: ' diagnostic]; end;
                  catch me;
                    success = false;
                    msg = [signal_fail 'error raised: ' me.message]; end; 
                catch me; %#ok<NASGU>
                  end;
                lastwarn ('');
                warning (orig_warn_state.state, 'quiet');
                

              case 'comment';
                % NOP

                otherwise;
                % Unknown block type
                msg = [signal_skip 'skipped test (unknown BIST block type: ' block.type ')\n']; end;
          catch me; %#ok<NASGU>
            end;
       

          te = toc (t0);
          rslt.elapsed_wall_time = rslt.elapsed_wall_time + te;
          this.emit ('--> success=%d, msg=%s\n', success, msg);

          if block.is_test
            % This logic is a little weird: run_test_code and its ilk update all
            % the counters _except for_ n_test and n_pass, so they can be handled
            % in a single point here based on success.
            rslt.n_test = rslt.n_test + 1;
            rslt.n_pass = rslt.n_pass + success;
          end
          if (~success && ~block.is_xtest)
            % Got a real failure
            rslt = rslt.add_failed_file (this.file);
            if this.show_failure_details
              fprintf ('----- Failure details: block %d -----\n', i_block);
              fprintf ('%s %s \n', signal_block, block.dispstr);
              fprintf ('--> success=%d, msg=%s\n', success, msg);
            end
            if this.save_workspace_on_failure
              this.stash_test_workspace ('after', after_workspace);
              fprintf ('\n'); % in case prior output got cut off mid-line
              fprintf ('Saved test workspace is available in: %s\n', this.stashed_workspace_file);
            end
            saved_files = true;
            if this.fail_fast
              fprintf ('Aborted test run due to test failure.\n');
              break
            end
          else
            this.clear_stashed_workspace;
          end
          % TODO: Should probably test for (success && block.is_xtest) and count
          % the unexpected PASSes.
        end
      catch me; %#ok<NASGU>
        end;
      % Cleanup
      for i = 1:numel (functions_to_clear)
        clear (functions_to_clear{i}); end;

      

      %% Verify test file did not leak resources
      if( ~isempty(setdiff (fopen ('all'), fid_list_orig)) );
        this.emit('test2: test file %s leaked file descriptors\n', file); end;
      leaked_base_vars = setdiff (evalin ('base', 'who'), base_variables_orig);
      if (~isempty (leaked_base_vars))
        this.emit('test2: test file %s leaked variables to base workspace:%s\n', this.file, sprintf (' %s', leaked_base_vars{:})); end;
      leaked_global_vars = setdiff (who ('global'), global_variables_orig);
      if( ~isempty(leaked_global_vars) );
        this.emit ('test2: test file %s leaked global variables:%s\n', this.file, sprintf (' %s', leaked_global_vars{:})); end;
      %% TODO: Verify that test did not leave file droppings

      if( ~saved_files );
        testify.internal.Util.rm_rf (this.run_tmp_dir); end;

      out = rslt;
    end

    function [success, rslt, msg, after_workspace] = run_test_code (this, block, workspace, rslt)
      persistent signal_fail;
      if( isempty(signal_fail) ); signal_fail  = '~~~~~'; end;
      msg = '';
      try
        try
          workspace.eval (block.code);
        end
        after_workspace = workspace.last_seen_workspace;
        workspace.clear_last_seen_workspace;
        
        success = true;
      catch err
        if isempty (lasterr ())
          error ('test: empty error text, probably Ctrl-C --- aborting');
        end
        success = false;
        if block.is_xtest
          if isempty (block.bug_id)
            if (block.fixed_bug)
              rslt.n_regression = rslt.n_regression + 1;
              msg = 'regression';
            else
              rslt.n_xfail = rslt.n_xfail + 1;
              msg = 'known failure';
            end
          else
            bug_id_display = block.bug_id;
            if (all (isdigit (block.bug_id)))
              bug_id_display = ['https://octave.org/testfailure/?' block.bug_id];
            end
            if (block.fixed_bug)
              rslt.n_regression = rslt.n_regression + 1;
              msg = ['regression: ' bug_id_display];
            else
              rslt.n_xfail_bug = rslt.n_xfail_bug + 1;
              msg = ['known bug: ' bug_id_display];
            end
          end
        else
          msg = sprintf ('test failed: raised error: %s', err.message);
        end
      end
    end

    function [out, diagnostic] = error_matches_expected (this, err, block)
      diagnostic = [];
      if ~isempty (block.error_id)
        out = isequal (err.identifier, block.error_id);
        if ~out
          diagnostic = sprintf ('expected id %s, but got %s', block.error_id, err.identifier);
        end
      else
        out = ~isempty (regexp (err.message, block.pattern, 'once'));
        if ~out
          diagnostic = sprintf ('expected error message matching /%s/, but got ''%s''', block.pattern, err.message);
        end
      end
    end

    function [out, diagnostic] = warning_matches_expected (this, warn_msg, warn_id, block)
      diagnostic = [];
      if ~isempty (block.error_id)
        out = isequal (warn_id, block.error_id);
        if ~out
          diagnostic = sprintf ('expected id %s, but got %s', block.error_id, warn_id);
        end
      else
        out = ~isempty (regexp (warn_msg, block.pattern, 'once'));
        if ~out
          diagnostic = sprintf ('expected warning message matching /%s/, but got ''%s''', block.pattern, warn_msg);
        end
      end
    end

    function out = extract_test_code (this)
      %EXTRACT_TEST_CODE Extracts '%!' embedded test code from file as a single block
      % Returns multi-line char vector
      [fid, msg] = fopen (this.file, 'rt');
      assert( fid >= 0, 'BistRunner: Could not open source code file for reading: %s: %s', this.file, msg );
      
      test_code = {};
      while (true)
        line = fgets (fid);
        if( ~ischar (line) );
            break; end;
        if (strncmp (line, '%!', 2))
          test_code{end+1} = line(3:end);
        end
      end
      fclose (fid);
      out = strjoin (test_code, '');
    end

    function out = split_test_code_into_blocks (this, test_code)
      %% Add a dummy comment block to the end for ease of indexing.
      if (test_code(end) == newline)
        test_code = [newline test_code '%'];
      else
        test_code = [newline test_code newline '%'];
      end
      %% Chop it up into blocks for evaluation.
      out = {};
      ix_line = find (test_code == newline );
      ix_block = ix_line(find (~isspace (test_code(ix_line + 1)))) + 1;
      for i = 1:numel (ix_block) - 1
        out{end+1} = test_code(ix_block(i):ix_block(i + 1) - 2);
      end
    end

    function out = parse_test_code (this, test_code)
      % Parses the test code, returning BistBlock array
      block_txts = this.split_test_code_into_blocks (test_code);
      for i = 1:numel (block_txts)
        out(i) = this.parse_test_code_block (block_txts{i});
        out(i).index = i;
      end
    end

    function out = parse_test_code_block (this, block)
      out = testify.internal.BistBlock;

      ix = find (~isletter (block));
      if isempty (ix)
        out.type = block;
        contents = '';
      else
        out.type = block(1:ix(1)-1);
        contents = block(ix(1):end);
      end
      out.contents = contents;
      out.is_valid = true;
      out.error_message = '';
      out.is_test = false;

      % Type-specific parsing
      switch out.type
        case 'demo'
          out.code = contents;

        case 'shared'
          % Separate initialization code from variables
          % vars are the first line; code is the remaining lines
          ix = find (contents == '\n');
          if isempty (ix)
            vars_line = contents;
            code = '';
          else
            vars_line = contents(1:ix(1)-1);
            code = contents(ix(1):end);
          end

          % Strip comments from variables line
          ix = find (vars_line == '%' | vars_line == '%');
          if ~isempty (ix)
            vars_line = vars_line(1:ix(1)-1);
          end
          vars_line = regexprep (vars_line, '\s+$', '');
          vars_line = regexprep (vars_line, '^\s+', '');

          if isempty (vars_line)
            vars = {};
          else
            vars = regexp (vars_line, '\s*,\s*', 'split');
            vars = regexprep (vars, '\s+$', '');
            vars = regexprep (vars, '^\s+', '');
          end
          out.vars = vars;
          out.code = code;

        case 'function'
          ix_fcn_name = this.find_function_name (contents);
          if (isempty (ix_fcn_name))
            out.is_valid = false;
            out.error_message = 'missing function name';
          else
            out.function_name = contents(ix_fcn_name(1):ix_fcn_name(2));
            out.code = ['function ' contents];
          end

        case 'end'
          % No additional contents

        case {'assert', 'fail'}
          [bug_id, rest, fixed] = this.find_bugid_in_assert (contents);
          %TODO: What to do with 'fixed' here?
          out.is_test = true;
          out.is_xtest = ~isempty (bug_id);
          out.bug_id = bug_id;
          out.code = [out.type contents];


        case {'error', 'warning'}
          out.is_test = true;
          out.is_warning = isequal (out.type, 'warning');
          [pattern, id, code] = this.find_pattern (contents);
          if (id)
            pat_str = ['id=' id];
          else
            if ~strcmp (pattern, '.')
              pat_str = ['<' pattern '>'];
            else
              pat_str = tifh (out.is_warning, 'a warning', 'an error');
            end
          end
          out.pattern = pattern;
          out.pat_str = pat_str;
          out.error_id = id;
          out.code = code;

        case 'testif'
          out.is_test = true;
          e = regexp (contents, '.$', 'lineanchors', 'once');
          %% Strip any comment and bug-id from testif line before
          %% looking for features
          feat_line = strtok (contents(1:e), '%%');
          out.feature_line = feat_line;
          contents_rest = contents(e+1:end);
          ix1 = index (feat_line, '<');
          if ix1
            tmp = feat_line(ix1+1:end);
            ix2 = index (tmp, '>');
            if (ix2)
              bug_id = tmp(1:ix2-1);
              fixed_bug = false;
              if (strncmp (bug_id, '*', 1))
                bug_id = bug_id(2:end);
                fixed_bug = true;
              end
              feat_line = feat_line(1:ix1-1);
              out.bug_id = bug_id;
              out.fixed_bug = fixed_bug;
            end
          end
          ix = index (feat_line, ';');
          if (ix)
            out.runtime_feature_test = feat_line(ix+1:end);
            feat_line = feat_line(1:ix-1);
          else
            out.runtime_feature_test = '';
          end
          feat = regexp (feat_line, '\w+', 'match');
          feat = strrep (feat, 'HAVE_', '');
          out.feature = feat;
          out.code = contents_rest;

        case 'test'
          [bug_id, code, fixed_bug] = this.find_bugid_in_assert (contents);
          out.bug_id = bug_id;
          out.fixed_bug = fixed_bug;
          out.is_test = true;
          out.is_xtest = ~isempty (bug_id);
          out.code = code;

        case 'xtest'
          [bug_id, code, fixed_bug] = this.find_bugid_in_assert (contents);
          out.bug_id = bug_id;
          out.fixed_bug = fixed_bug;
          out.is_test = true;
          out.is_xtest = true;
          out.code = code;

        case '%'
          % Comment block
          out.type = 'comment';

        otherwise
          % Unrecognized block type: no further parsing
          % But treat it as a test~?~?
          out.is_test = true;
          out.code = '';
      end
      out.code = regexprep(out.code, '^\r?\n', '');
    end

    function out = find_function_name (this, def)
      pos = [];

      %% Find the end of the name.
      right = find (def == '(', 1);
      if (isempty (right))
        return;
      end
      right = find (def(1:right-1) ~= ' ', 1, 'last');

      %% Find the beginning of the name.
      left = max ([find(def(1:right)==' ', 1, 'last'), ...
                   find(def(1:right)=='=', 1, 'last')]);
      if (isempty (left))
        return;
      end
      left = left + 1;

      %% Return the end points of the name.
      pos = [left, right];
      out = pos;
    end

    function [bug_id, rest, fixed] = find_bugid_in_assert (this, str)
      bug_id = '';
      rest = str;
      fixed = false;

      str = trimleft (str);
      if (~isempty (str) && str(1) == '<')
        close = index (str, '>');
        if (close)
          bug_id = str(2:close-1);
          if (strncmp (bug_id, '*', 1))
            bug_id = bug_id(2:end);
            fixed = true;
          end
          rest = str(close+1:end);
        end
      end
    end

    function [pattern, id, rest] = find_pattern (this, str)
      pattern = '.';
      id = [];
      rest = str;
      str = trimleft (str);
      if (~isempty (str) && str(1) == '<')
        close = index (str, '>');
        if (close)
          pattern = str(2:close-1);
          rest = str(close+1:end);
        end
      elseif (strncmp (str, 'id=', 3))
        [id, rest] = strtok (str(4:end));
      end
    end

    function out = print_results_format_key (this, fid)
        fid = 1;
      %% Output from test is prefixed by a 'key' to quickly understand the issue.
      persistent signal_fail;
      if(isempty(signal_fail)); signal_fail  = '~~~~~'; end;
      persistent signal_empty;
      if(isempty(signal_empty)); signal_empty = '????? '; end;
      persistent signal_block;
      if(isempty(signal_block)); signal_block = '***** '; end;
      persistent signal_file;
      if(isempty(signal_file)); signal_file  = '>>>>> '; end;
      persistent signal_skip;
      if(isempty(signal_skip)); signal_skip  = '----- '; end;

      fprintf (fid, '% %s new test file\n', signal_file);
      fprintf (fid, '% %s no tests in file\n', signal_empty);
      fprintf (fid, '% %s test had an unexpected result\n', signal_fail);
      fprintf (fid, '% %s test was skipped\n', signal_skip);
      fprintf (fid, '% %s code for the test\n\n', signal_block);
      fprintf (fid, '% Search for the unexpected results in the file\n');
      fprintf (fid, '% then page back to find the filename which caused it.\n');
      fprintf (fid, '% The result may be an unexpected failure (in which\n');
      fprintf (fid, '% case an error will be reported) or an unexpected\n');
      fprintf (fid, '% success (in which case no error will be reported).\n');  
    end

    function out = print_test_results (this, rslt, file, fid)
      if nargin < 4 || isempty (fid); fid = stdout; end

      persistent signal_empty;
      if( isempty(signal_empty)) ; signal_empty = '????? '; end;
      if(isempty(signal_empty)); signal_empty = '????? '; end;
      r = rslt;
      if (r.n_test || r.n_xfail || r.n_xfail_bug || r.n_skip)
        if (r.n_xfail || r.n_xfail_bug)
          if (r.n_xfail && r.n_xfail_bug)
            fprintf (fid, 'PASSES %d out of %d test%s (%d known failure%s; %d known bug%s)\n', ...
                    r.n_pass, r.n_test, tifh (r.n_test > 1, 's', ''), ...
                    r.n_xfail, tifh (r.n_xfail > 1, 's', ''), ...
                    r.n_xfail_bug, tifh (r.n_xfail_bug > 1, 's', ''));
          elseif (r.n_xfail)
            fprintf (fid, 'PASSES %d out of %d test%s (%d known failure%s)\n', ...
                    r.n_pass, r.n_test, tifh (r.n_test > 1, 's', ''), ...
                    r.n_xfail, tifh (r.n_xfail > 1, 's', ''));
          elseif (r.n_xfail_bug)
            fprintf (fid, 'PASSES %d out of %d test%s (%d known bug%s)\n', ...
                    r.n_pass, r.n_test, tifh (r.n_test > 1, 's', ''), ...
                    r.n_xfail_bug, tifh (r.n_xfail_bug > 1, 's', ''));
          end
        else
          fprintf (fid, 'PASSES %d out of %d test%s\n', r.n_pass, r.n_test, ...
                 tifh (r.n_test > 1, 's', ''));
        end
        if (r.n_skip_feature)
          fprintf (fid, 'Skipped %d test%s due to missing features\n', r.n_skip_feature, ...
                  tifh (r.n_skip_feature > 1, 's', ''));
        end
        if (r.n_skip_runtime)
          fprintf (fid, 'Skipped %d test%s due to run-time conditions\n', r.n_skip_runtime, ...
                  tifh (r.n_skip_runtime > 1, 's', ''));
        end
      else
        fprintf (fid, '%s%s has no tests available\n', signal_empty, file);
      end      
    end

  end

end

function out = trimleft (str)
  % Strip leading blanks from string(s)
  out = regexprep (str, '^ +', '');
end