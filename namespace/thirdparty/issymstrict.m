function flag = issymstrict( x )
% [ flag ] = issymstrict( x )
% Tests if an object is symbolic, excludung vpa.
% flag = logical(strcmp(class(x),'sym'));
%
% E.g.: issym( [sym('23.2') 0] )
%
% See also: isvpa
%
% Written by: tommsch, 2018

%               tommsch, 2022-01-28,    Behaviour change. Function now returns an array instead of a scalar
%                                       This change is necessary, since Matlab allows vpa`s and sym`s in the same array
% Changelog:    

    if( ~issym(x) );
        flag = false( size(x) );
    else;
        flag = ~isvpa( x ); end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
