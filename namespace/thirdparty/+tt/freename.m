function [ filename ] = freename( dirname, base, n, ext )
% Finds first unused filename in a directory.
% [ filename ] = freename()
% [ filename ] = freename( dirname )
% [ filename ] = freename( dirname, base )
% [ filename ] = freename( dirname, base, n )
%
% Input:
%       dirname     str, default = pwd, name of the directory, where to find a new name
%       base        str, default = filename of calling function, base of the new name (without extension)
%       filename    integer, default = 3, free name equals a base appended by 'n' figure characters
%
% Output:
%       filename    a filename which is not present yet in the directory dirname
%
%
% Example:
%   freename( 'Results', 'meas', 2 )
%   [freename( './', 'data' ) '.dat']; % yields 'data001.dat' if no file file with the base name 'data' exists in the current directory.
%
% Written by: M. Balda, 2002-12-07
% Modified by: Clara Hollomey, 2021
%              tommsch, 2023-05-05

    if( nargin <= 0 || isempty(dirname) );
        dirname = pwd; end;
    
    if( nargin <= 1 || isempty(base) );
        st = dbstack;
        if( numel(st) >= 2 );
            base = st(2).name; 
        else;
            base = 'tt'; end; end;
    
    if( nargin <= 2 || isempty(n) );
        n = 3; end;
    
    if( nargin<=3 || isempty(ext) );
        ext = ''; end;
    
    if( ~exist(dirname, 'dir') );  % Test directory existence
        warning( 'freename:directory', ['Directory ' dirname ' does not exist'] )
        filename = '';
        return; end;
    
    base(base == ' ') = '';  % delete all spaces in name base
    Dir = dir( dirname );
    D   = strvcat( Dir(3:end).name );
    lb  = length( base );
    m   = size( D, 1 );
    w   = 0;

    for k = 1:m;  % Cycle directory items
        j = strfind( D(k,:), base );
        if( ~isempty(j) );
            j = j + lb;
            w = max( str2double(D(k,j:j+n-1)), w ); end; end;

    w = w + 1;
    ord = sprintf( ['%0' num2str(n) 'd'], w );
    filename = [base ord ext];
    

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
