function [ file ] = scl();
% Copyright (c) 2016, Hiran Wijesinghe
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% Hiran (2023). Change Directory to Current File in Editor (https://www.mathworks.com/matlabcentral/fileexchange/56624-change-directory-to-current-file-in-editor), MATLAB Central File Exchange. Retrieved July 12, 2023. 
    % Set [path to] Current Location
    %
    % DESCRIPTION:
    %   Changes the directory to the path of the current file in editor.
    %
    % SETUP:
    %   Place scl.m in a directory and run the following line once.
    %
    %       addpath('<path to the directory of scl.m>'); savepath;
    %
    % Eg:
    %   addpath('/Users/totallySomeoneElse/Documents/MATLAB/'); savepath;
    try;
        if( ismatlab );
            editor_service = com.mathworks.mlservices.MLEditorServices;
            editor_app = editor_service.getEditorApplication;
            if( verLessThan('matlab', '9.9') );
                active_editor = editor_app.getActiveEditor;
                storage_location = active_editor.getStorageLocation;
                file = char( storage_location.getFile );
            else;
                active_editor = matlab.desktop.editor.getActive;
                file = active_editor.Filename; end;
        else;
            fprintf( 2, 'Calling scl from the command line is not supported on Octave currently.\n' ); 
            file = '';
            return;
            end
    catch me;
        %disp( me );
        %disp( me.stack(1) );
        %disp( me.stack(2) );
        %whos;
        error( 'scl:scl', 'scl failed to change path to currently opened file.' ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
