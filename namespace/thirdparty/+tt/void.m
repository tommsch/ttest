function [ out ] = void( f, out, capture_error, verbose, condition )
    if( nargin <= 0 || isempty(f) );
        f = @nop; end;

    if( nargin <= 1 );
        out = 1; end;

    if( nargin <= 2 );
        capture_error = true; end;

    if( nargin <= 3 || isempty(verbose) );
        verbose = 1; end;

    if( nargin <= 4 || isempty(condition) );
        condition = true; end;
    
    if( isa(condition, 'function_handle') && ~condition() || ...
        ~condition ...
      );
        return; end;

    if( capture_error );
        try;
            f();
        catch me;
            if( verbose >= 1 );
                if( isempty(me.identifier) );
                    id = 'TTEST:void'; 
                else;
                    id = me.identifier; end;
                msg = ['`void` captured an error and rethrows it as warning.' newline 'Id:' newline id newline 'Message:' newline me.message];
                warning( id, '\n%s', msg ); end; end;
    else;
        f(); end;
    
end

function nop
end

%#ok<*NOSEL,*NOSEMI,*ALIGN>
