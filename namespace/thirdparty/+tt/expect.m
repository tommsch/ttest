function expect( varargin );
% expect( cond, [errID], msg, A1, ..., An )
% Throw warning if condition is false
%
% Depends on : //
% 
% See also: assert
%
% Written by: tommsch, 2020

if( ~varargin{1} );
    if( nargin==1 );
        warning( 'Expect failed.' );
    else;
        warning( varargin{2:end} ); end; end;
end     
        
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.