function [ A ] = randxcp( varargin );

A = tt.randx( varargin{:} ) + 1i*tt.randx( varargin{:} );

end