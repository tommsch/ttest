function [ value, args, retname, allvalue, contained ] = parsem( varargin )
% [ value, args, retname, allvalue, contained ] =  parsem( name, args , [input3], [options] )
% [ check ] =  parsem( args , 'test')
% Parses varargin and is easier to use than Matlabs parse.
%
% Input:
% ======
%   name                string or cell-array of strings. 
%                       The questionable argument. The function searches for string <name> in <args>. The return value depends on [input3] and on args.
%
%   args                string or cell array of strings
%   'help', 'helptext'   If contained in args, then an help is printed
%
%
%   [input3]            default value
%                           If input3 is not given: If the string can be found, and the entry in args behind the string is
%                                                       - not a string, or
%                                                       - a string with the value 'no',
%                                                   then this entry is returned.
%                                                   Otherwise, If the string can be found the function returns 1, otherwise 0.
%                           If input3 is given:     If the string can be found, the function returns the value in <args> which is one position behind the last occurence of <name>.
%                                                   If that string cannot be found, the function returns the value which is defined input3.
%                       The last occurence of 'name' defines what happens
% Options:
% ========
%   'help', 'helptext'   Adds some help-text to the help-output
%   'test'              Tests if args is empty. If not, a warning is printed.
%                       If 'test' is given, 'name' must be ommited and no other option must be given
%   'expect',val        expected values for the <value> one position behind the last occurence of <name>
%                       If <value> is not an expected value, a warning is given
%                       only allowed if input3 is given, 
%                       format of val:
%                           {'clop',[lb ub]}             lb <= value <  ub
%                           {'opop',[lb ub]}             lb <  value <  ub
%                           {'opcl',[lb ub]}             lb <  value <= ub
%                           {'clcl',[lb ub]}             lb <= value <= ub
%                           {e1, ..., en}                value must be equal to an element in {e1,...,en} (in particular e1 must be unequal to 'clop', 'opop', 'opcl' and 'clcl')
%                           f                            f(value) must return true, where f is a function handle
%                           'name'                       value must be equal to an element in <name>. If the option <name> is not given, thus a warning is printed. 
%                                                        Thus, 'expect', 'name' shall only be given for mandatory options
%   'expecte',val       The same as 'expect', but an empty matrix as argument (i.e. []) is always implicitely allowed
%   'postprocess',@h    (experimental), handle, the returned `value` is passed to h
%   'multiple',1        If given, only one occurence of a given name / name-value pair is removed.
%                       
%
% Remark:
%   If an option shall be removed, call this function with the option to be removed and use the second return argument.
%
% Output:
% =======
%   value               The parsed value or the set value
%   args                input arguments without the parsed values (except 'set' or 'condset' is given).
%                       If the input argument is contained more than once, then all of those are removed in the return value.
%   retname             The parsed name. First entry  in 'name' if any of 'name' is not present in 'args'
%   allvalue            All parsed values, mostly unprocessed
%   contained           bool, true if value was found
%
% Eg.:  [v, arg] = parsem( 'tommsch', {'help', 'Olga'} );
%
% Written by: tommsch, 2016

%               tommsch, 2019-05-08,    Added warning if name with parameter shall be parsed, but parameter is missing (e.g. parsem('tommsch',{23, 'tommsch'},[]) )
%               tommsch, 2019-09-19,    Added experimental feature: Allowing cell array of string in argument 'name' (feature is not experimental anymore)
%               tommsch, 2020-01-08,    Added return value retname
%               tommsch, 2020-04-20,    Behaviour change of retname
%               tommsch, 2020-04-25,    Added experimental option 'expect'
%               tommsch, 2020-09-25,    Behaviour change: when given, 'parse_one' and 'parse_random' must be the only given args (gives more speed)
%                                       Added experimental option 'help'
%               tommsch, 2021-01-19,    Bugfixes for expected values
%               tommsch, 2021-01-21,    Added options 'assert', 'asserte'
%               tommsch, 2021-11-25,    Removed options 'condset', 'set', 'parse_random'
%               tommsch, 2022-01-30,    Added output variable `allvalue`
%               tommsch, 2023-01-23,    Added output variable `contained`, added experimental option 'postprocess'
%               tommsch, 2023-03-01,    Added experimental option 'multiple'
%               tommsch, 2023-07-04,    Added warning if name with parameter shall be parsed, but parameter is missing (e.g. parsem('tommsch',{23, 'tommsch'},[]) )
%               tommsch, 2019-09-19,    Added experimental feature: Allowing cell array of string in argument 'name' (feature is not experimental anymore)
%               tommsch, 2020-01-08,    Added return value retname
%               tommsch, 2020-04-20,    Behaviour change of retname
%               tommsch, 2020-04-25,    Added experimental option 'expect'
%               tommsch, 2020-09-25,    Behaviour change: when given, 'parse_one' and 'parse_random' must be the only given args (gives more speed)
%                                       Added experimental option 'help'
%               tommsch, 2021-01-19,    Bugfixes for expected values
%               tommsch, 2021-01-21,    Added options 'assert', 'asserte'
%               tommsch, 2021-11-25,    Removed options 'condset', 'set', 'parse_random'
%               tommsch, 2022-01-30,    Added output variable `allvalue`
%               tommsch, 2023-01-23,    Added output variable `contained`, added experimental option 'postprocess'
%               tommsch, 2023-03-01,    Added experimental option 'multiple'
%               tommsch, 2023-07-04,    Behaviour change: Removed output variable `allvalue`
%               tommsch, 2024-02-18,    Deprecated option 'help'
%               tommsch, 2024-05-28,    Behaviour change: lower/upper case and underscores are ignored during name lookup, whenever the string has more than one character
%               tommsch, 2024-09-09,    Behaviour change: input3 is always returned if given. special casing string is removed.
%                                       Behaviour change: 'no' is a now valid value for a query without input3
%               tommsch, 2024-21-10     Bugfix: Parsing was wrong when multiple options using different aliases were given
% Changelog:    

% XX Add option for mandatory parameters - implement using 'expect' syntax ?
% XX Speed up function

    if( check_test( varargin{:} ) );
        return; end;

    [name, NAME, args,] = set_name_args( varargin{:} );

    [default, defaultgiven, aegiven, ae, postprocess, multiple, varargin] = parse_input( varargin{:} );  %#ok<ASGLU>

    [value, args, retname, allvalue, contained ] = parse_name_args( name, NAME, args, default, defaultgiven, postprocess, multiple );

    check_ae( value, retname, NAME, aegiven, ae );
   
end


%% %%%%%%%%%%%%%%%

function [ stop ] = check_test( varargin );
stop = false;
if( numel(varargin) == 2 && isequal(varargin{2}, 'test') );
    if( ~isempty(varargin{1}) );
        [ST, ~] = dbstack;
        warning( 'parsem:unkown', 'Error in %s()/parsem(): Unkown argument(s):', ST(end).name );
        try;
            if( isempty(varargin{1}) );
                fprintf( 'Empty argument.\n' ); 
            else;
                disp( varargin{1} ); end;
        catch me;  %#ok<NASGU>
            fprintf( 'Failed to print arguments.\n' ); end;
        fprintf( '\n' ); end;  % do not call vprintf, since vprintf calls parsem
    stop = true; end;
end

function [ name, NAME, args ] = set_name_args( varargin );
    NAME = varargin{1};
    name = normalize_name( NAME );
    args = varargin{2};
    if( ~iscell(args) ); 
        args = {args}; end;
    if( ~iscell(name) );
        NAME = {NAME};
        name = {name}; end;
end

function [ default, defaultgiven, aegiven, ae, postprocess, multiple, varargin ] = parse_input( varargin );
    postprocess = [];
    aegiven = false;
    multiple = false;
    ae = [];
    default_override = false;
    while( true );
        n = numel( varargin );
        if( n <= 3 );
            break; end;
        switch varargin{n-1};
            case {'default'};
                default_override = true;
                default_override_value = varargin{n};
                varargin(end-1:end) = [];

            case {'multiple'};
                multiple = varargin{n};
                varargin(end-1:end) = [];

            case {'postprocess'}
                postprocess = varargin{n};
                varargin(end-1:end) = [];
                assert( isa(postprocess, 'function_handle'), 'parsem:options', 'Argument to ''postprocess'' must be a function handle.' );

            case 'help';
                persistent help_warning_shown;  %#ok<TLEV>
                if( isempty(help_warning_shown) );
                    warning( 'parsem:help', 'The `help` option is deprecated and may be removed in a future release.\n  If you have objections, write to the author: tommsch@gmx.at.' );
                    help_warning_shown = true; end;
                varargin(end-1:end) = [];

            case {'assert', 'asserte', 'expect', 'expecte'};
                aegiven = true;
                ae.idxa  = strcmp( varargin, 'assert' );   % look, if 'assert'  is set
                ae.idxae = strcmp( varargin, 'asserte' );  % look, if 'asserte' is set
                ae.idxe  = strcmp( varargin, 'expect' );   % look, if 'expect'  is set
                ae.idxee = strcmp( varargin, 'expecte' );  % look, if 'expecte' is set
                if( any(ae.idxae | ae.idxee) );
                    ae.emptystring = '[] or ';
                    ae.emptyallowed = 1;
                else;
                    ae.emptystring = '';
                    ae.emptyallowed = 0; end;
                if( any(ae.idxa | ae.idxae) );
                    ae.handler = @error;
                else;
                    ae.handler = @warning; end;
                ae.value = varargin{n};
                varargin(end-1:end) = [];

            otherwise;
                error( 'parsem:option', 'Wrong option given.' ); end; end;
        
    % Set default value, determine if strings are allowed for value
    if( n == 3 );
        default =  varargin{3};
        defaultgiven = 1;
    elseif( default_override );
        default = default_override_value;
        defaultgiven = 0;
    else;
        default = 0;
        defaultgiven = 0; end;
end

function [ value, args, retname, allvalue, contained ] = parse_name_args( name, NAME, args, default, defaultgiven, postprocess, multiple );
    % read value for name, remove everything which is read 
    idx = [];
    if( ~isempty(name) );
        retname = NAME{1}; 
    else
        retname = []; end;
    for i = 1:numel( name );
        idx_ = find( strcmp(normalize_name(args), name{i}) );
        if( ~isempty(idx_) );
            retname = args{idx_}; end;
        idx = [idx idx_]; end;  %#ok<AGROW>
    if( ~isempty(idx) && multiple );
        idx = idx(1); end;
    idx = sort( idx );
    contained = ~isempty( idx );
    szeargs = size( args, 2 );  % number of elements in args
    allvalue = cell( 1, numel(idx) );
    if( ~isempty(idx) );
        for i = 1:size( idx, 2 );
            k = idx(i) + 1;
            if( szeargs >= k && ...
                (~ischar(args{k}) || isequal(args{k}, 'no') || defaultgiven) ...
              );
                idx(end+1) = k;  %#ok<AGROW>  % save the index of value, so that it can get deleted afterwards
                allvalue{i} = args{k};
            elseif( defaultgiven )
                warning( 'parsem:missing', 'In the given function arguments, the option ''%s'' has most likely no ''value'' (but it should have a value).', args{idx} );
                allvalue = {1};
            else
                allvalue = {1}; end; end;
        args(idx) = [];
    else;
        allvalue = {default}; end;
    value = allvalue{end};
    
    if( ~isempty(postprocess) );
        switch nargin( postprocess );
            case 0; value = postprocess();
            case 1; value = postprocess( value );
            case 2; value = postprocess( value, args );
            case 3; value = postprocess( value, args, retname );
            case 4; value = postprocess( value, args, retname, allvalue );
            case 5; value = postprocess( value, args, retname, allvalue, contained );
            otherwise; fatal_error; end; end;
end

function [] = check_ae( value, retname, NAME, aegiven, ae );
    value = normalize_name( value );
    if( ~aegiven );
        return; end;

    bad_str = sprintf( 'Unexpected value for option ''%s''.\n', retname );
    bad_str2 = 'Allowed values are: '; 
    try;
        if( ae.emptyallowed && isequal(value, []) );
            % everything ok
    
        elseif( iscell(ae.value) && strcmp(ae.value{1}, 'clop') )
            if( ~isscalar(value) || ...
                value < ae.value{2}(1) || ...
                value >= ae.value{2}(2) ...
              );
                dvalue = dispc( value );
                ae.handler( 'parsem:expect', [bad_str bad_str2 '%s%f <= val < %f\n Given value is: %s\n'], ae.emptystring, ae.value{2}(1), ae.value{2}(2), dvalue ); end;
    
        elseif( iscell(ae.value) && strcmp(ae.value{1}, 'opop') )
            if( ~isscalar(value) || ...
                value <= ae.value{2}(1) || ...
                value >= ae.value{2}(2) ...
              );
                dvalue = dispc( value );
                ae.handler( 'parsem:expect', [bad_str bad_str2 '%s%f < val < %f\n Given value is: %s\n'], ae.emptystring, ae.value{2}(1), ae.value{2}(2), dvalue ); end;
    
        elseif( iscell(ae.value) && strcmp(ae.value{1}, 'opcl') )
            if( ~isscalar(value) || ...
                value <= ae.value{2}(1) || ...
                value > ae.value{2}(2) ...
              );
                dvalue = dispc( value );
                ae.handler( 'parsem:expect', [bad_str bad_str2 '%s%f < val <= %f\n Given value is: %s\n'], ae.emptystring, ae.value{2}(1), ae.value{2}(2), dvalue ); end;         
    
        elseif( iscell(ae.value) && strcmp(ae.value{1}, 'clcl') )
            if( ~isscalar(value) || ...
                value < ae.value{2}(1) || ...
                value > ae.value{2}(2) ...
              );
                dvalue = dispc( value );
                ae.handler( 'parsem:expect', [bad_str bad_str2 '%s%f <= val <= %f\n Given value is: %s\n'], ae.emptystring, ae.value{2}(1), ae.value{2}(2), dvalue ); end;
    
        elseif( iscell(ae.value) );
            if( ~any(cellfun(@(x)isequal(x, value), ae.value)) );
                s = dispc( ae.value );
                dvalue = dispc( value );
                ae.handler( 'parsem:expect', [bad_str bad_str2 '%s%s\n  Given value is: %s\n'], ae.emptystring, s, dvalue ); end;
    
        elseif( isa(ae.value, 'function_handle') );
            if( ~ae.value(value) );  % value is checked by function handle wheter it is allowed
                s = dispc( ae.value );
                dvalue = dispc( value );
                ae.handler( 'parsem:expect', [bad_str 'Allowed values are %sdetermined by: %s\n  Given value is: %s \n'], ae.emptystring, s, dvalue ); end;
    
        elseif( any(strcmp(ae.value, 'name')) );
            if( isequal(value, 0) );
                s = dispc( NAME );
                ae.handler( 'parsem:expect', 'Missing argument. One argument must have one of the values: %s%s\n', ae.emptystring, s ); end;
    
        else;
            error( 'parsem:expect', 'Wrong value for option ''expect''/''assert''.' ); end;
    catch me;  %#ok<NASGU>
        ae.handler( 'parsem:expect', bad_str, retname );
    end;
end

%% %%%%%%%%%%%%%%%%%%%%%

function [ str ] = fdisp( var );  %#ok<INUSD>
    str = strtrim( evalc( 'disp( var );' ) );
end

function [ name ] = normalize_name( name )
    if( iscell( name ) );
        for i = 1:numel( name );
            if( ischar(name{i}) || isstring(name{i}) );
                if( numel(name{i}) > 1 );
                    name{i} = lower( name{i} ); end;
                name{i}(name{i} == '_') = [];
                name{i}(name{i} == '-') = []; end; end;
    else;
        if( ischar(name) || isstring(name) );
            if( numel(name) > 1 );
                name = lower( name ); end;
            name(name == '_') = [];
            name(name == '-') = []; end; end;
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
