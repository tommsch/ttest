function [ varargout ] = wwhich( varargin );
% WWHICH Locate m-files using wildcards
%    Works exactly as WHICH, but you can
%    use wild cards.
%    
%    out is a cell whenever 2 or more
%    matches are found or if the flag
%    -all was used
%
%    Note: a function may be shawdowed,
%          use the flag -all to locate them
%
%    See WHICH for more info 
%
%   by Lucio Andrade

    assert( nargin >= 1, 'ttest:which', 'Not enough input arguments.' );
    s1 = varargin{1};
    s2 = varargin{end};
    
    p = path;
    out = cell(0);
    h = [0 find( p == pathsep ) length( p )+1]; 
    for i = length( h ):-1:2
        direc = p(h(i-1)+1:h(i)-1);
        a = dir( [direc '/' s1 '.m'] );
        for j = 1:length( a );
            if( nargin > 1 );
                outtmp = which( a(j).name, s2 );
                if( iscell(outtmp) );
                    out = [out;outtmp];  %#ok<AGROW>
                else;
                    out = [out;{outtmp}]; end;  %#ok<AGROW>
                
            else;
                outtmp = which( a(j).name );
                out = [out;{outtmp}]; end; end; end;  %#ok<AGROW>

    % take out repetitions
    todel = [];
    for i = 1:length( out );
        for j = i+1:length( out );
            if( strcmp(out{i}, out{j}) );
                todel = [todel j]; end; end; end;  %#ok<AGROW>
    out(unique( todel )) = [];

    % change to string when needed
    if( nargout == 1 );
        varargout = {out};
    elseif( nargout == 2 );
        type = repmat( {''}, [numel(out) 1] );
        varargout = {out, type};
    else;
        for i = 1:length( out );
            disp( out{i} ); end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% Copyright (c) 2009, Lucio Cetto
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

