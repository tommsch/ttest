function [ ret ] = startsWith( str, pat, varargin );  % needed for Octave compatibilty
% Octave 8.1 `startsWith` function behaves different to Matlabs
    if( ismatlab() );
        warning( 'TTEST:shim', 'A TTEST shim for Octave happens to be on the Matlab path. This should not happen.' );
        ret = builtin( 'startsWith', str, pat, varargin{:} );
        return; end;
        
    if( isempty(pat) );
        if( iscell(str) );
            ret = ones( size(str) );
        else;
            ret = 1; end;
        return; end;

    if( numel(varargin) >= 2 && isequaln(varargin{1}, 'IgnoreCase') && isequal(varargin{2}, true) );
        if( iscell(str) );
            for i = 1:numel( str );
                str{i} = tolower( str ); end;
        else;
            str = tolower( str ); end;
        if( iscell(pat) );
            for i = 1:numel( pat );
                pat{i} = tolower( pat ); end;
        else;
            pat = tolower( pat ); end; end;
        
    idx = strfind( str, pat );
    if( iscell(idx) );
        ret = zeros( size(str) );
        for i = 1:numel(idx);
            if( ~isempty( idx{i} ) && idx{i}(1) == 1 ...
              );
                ret(i) = 1; end; end;
    else;
          ret = ~isempty(idx) && idx(1) == 1; end;
end

function ret = ismatlab ()
    % returns true when run on m
    persistent x;
    if( isempty (x) );
        x = ~exist( 'OCTAVE_VERSION', 'builtin' ); end;
    ret = x;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
