function ret = endsWith( str, pat, varargin );  % needed for Octave compatibilty
    
    if( isempty(pat) );
        if( iscell(str) );
            ret = ones( size(str) );
        else;
            ret = 1; end;
        return; end;

    if( numel(varargin) >= 2 && isequaln(varargin{1}, 'IgnoreCase') && isequal(varargin{2}, true) );
        if( iscell(str) );
            for i = 1:numel( str );
                str{i} = tolower( str ); end;
        else;
            str = tolower( str ); end;
        if( iscell(pat) );
            for i = 1:numel( pat );
                pat{i} = tolower( pat ); end;
        else;
            pat = tolower( pat ); end; end;    
    

    idx = strfind( str, pat );
    if( iscell(idx) );
        ret = zeros( size(str) );
        for i = 1:numel(idx);
            if( ~isempty( idx{i} ) && ...
                idx{i}(end) == numel( str{i} ) - numel( pat ) + 1 ...
              );
                ret(i) = 1; end; end;
    else;
          ret = ~isempty(idx) && idx(end) == numel( str ) - numel( pat ) + 1; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
