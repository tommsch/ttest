function throw( in );
    if( ismatlab() );
        warning( 'TTEST:shim', 'A shim for Octave happens to be on the Matlab path. This should not happen.' );
        builtin( 'throw', in );
        return; end;
	error( in.identifier, in.message );
	
end

function ret = ismatlab ()
    % returns true when run on m
    persistent x;
    if( isempty (x) );
        x = ~exist( 'OCTAVE_VERSION', 'builtin' ); end;
    ret = x;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>
