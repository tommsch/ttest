function [ successflag ] = ttest_patch_startup_file( install_flag, verbose );  %#ok<INUSD>
% Disables octave warnings
% [ successflag ] = writeOctaverc( install_flag );
% Writes all the commands necessary to disable the known and unnecessary warnings
% that may occur while running ttest to ~/.octaverc
% Returns true if function succeeds
%
% Input:
%   install_flag     1 ... writes new ~/.octaverc
%                    0 ... only deletes lines generated by TTEST in ~/.octaverc
%
% Written by: Clara Hollomey, 2019

%           2023-04-21, tommsch, Bugfix: Old autogenerated TTEST lines are not duplicated anymore
% Changelog: 

    if( nargin <= 0 );
        install_flag = true; end;
    if( nargin <= 1 );
        verbose = 1; end;  %#ok<NASGU>

    successflag = true;
    fname = fullfile( '~', '.octaverc' );
    fname_tmp = [fname '.tmp'];
    [status, msg, ~] = copyfile ( fname, fname_tmp, 'f');
    if( ~isequal(status, 1 ) );
        fprintf( 2, 'TTEST: Patching startup files failed.\n  Message:' );
        disp( msg );
        successflag = false;
        return; end;
    
    try;
        TTEST_DELETE_LINE( fname_tmp );
    catch me;
        successflag = false;
        fprintf( 2, 'Could not remove old lines autogenerated by TTEST from .octaverc\n  Error:' );
        disp( me );
        return;
        end;

    str = [ 'try; assert( ~isempty(which(''TTEST_disableWarnings'')) ); TTEST_disableWarnings(); catch me; fprintf( ''TTEST: TTEST seems not be installed properly.\n  Thrown error:\n'' ); disp( me ); for i = 1:numel(me.stack); disp( me.stack(i) ); end; end;  %TTEST_AUTOGENERATE' newline];
    if( install_flag );
        fprintf( 'Determined name of file to be patched: ''%s''\n', fname_tmp );
        fid = fopen( fname_tmp, 'a' );
        if( fid ~= -1 );
            fileCleanup = onCleanup( @() fclose(fid) );
            fprintf( fid, '%s', str );
            clear fileCleanup
        else;
            successflag = false;
            fprintf( 2, ['Could not patch Octave warnings.\n' ...
                         '  Most likely cause: No write acces to the file ~/.octaverc .\n'
                         '  TTESTs may work, but a lot of warnings will be thrown all the time.\n'] );
            return; end; end;
    [status, msg, ~] = copyfile ( fname_tmp, fname, 'f');
    if( ~isequal(status, 1 ) );
        fprintf( 2, 'TTEST: Patching startup files failed.\n  Message:' );
        disp( msg );
        successflag = false;
        return;
    else;
        fprintf( 'Octave warnings patched.\n' ); end;
end

%% helper functions

function TTEST_DELETE_LINE( name );
    [fid_read, closefid_read] = TTEST_OPEN_FILE( name, 'r' );  %#ok<ASGLU>

    output = '';
    while( ~feof(fid_read) );
        line = fgetl( fid_read );
        if( ~isequal( line, -1 ) && ...
            contains( line, '%TTEST_AUTOGENERATE' ) ...
          );
            continue; end
        output = [output newline line]; end;  %#ok<AGROW>
    clear closefid_read;
    if( numel(output) >= 1 && isequal(output(1), newline) );
        output(1) = []; end;
    if( ~isequal(output(end), newline) );
        output = [output newline]; end;

    [fid_write, closefid_write] = TTEST_OPEN_FILE( name, 'w+' );  %#ok<ASGLU>
    
    numbytes = fprintf( fid_write, '%s', output );
    if( numbytes < numel(output) );
        warning( 'TTEST_DELETE_LINE:writefailure', 'It seems that TTEST_DELETE_LINE could not write to the file.'  ); end;
    clear closefid_write;
end

function [ fid, cleanup ] = TTEST_OPEN_FILE( name, permission  );

    if( nargin <= 1 || isempty(permission) );
        permission = 'r'; end;

    name_orig = name;
    fid = fopen( name, permission  );  % File to be written
    if( fid == -1 );
        name = which( name );
        idx = strfind( name, '%' );
        if( ~isempty(idx) );
            name(idx(1):end) = []; end;
        fid = fopen( which(name, 'all'), permission ); end;

    assert( fid >= 3, 'TTEST_OPEN_FILE:cannotopenfile', 'Cannot open file %s', name_orig );
    cleanup = onCleanup( @() fclose( fid ) );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
