function [ ret ] = contains( str, pat );  % needed for Octave compatibilty
    if( ismatlab() );
        warning( 'TTEST:shim', 'A shim for Octave happens to be on the Matlab path. This should not happen.' );
        ret = builtin( 'contains', str, pat );
        return; end;
    if( isempty(pat) );
        ret = true;
    else;
        idx = strfind( str, pat );
        if( iscell(idx) );
            ret = any( [idx{:}] );
        else
            ret = any( idx ); end; end;
end

function [ ret ] = ismatlab ()
    % returns true when run on m
    persistent x;
    if( isempty (x) );
        x = ~exist( 'OCTAVE_VERSION', 'builtin' ); end;
    ret = x;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>
