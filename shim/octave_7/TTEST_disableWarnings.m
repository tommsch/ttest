function TTEST_disableWarnings();

    try;
        persistent warning_printed;  %#ok<TLEV>
        if( isempty(warning_printed) );
            warning_printed = true;
            fprintf( 2, '\nTTEST occasionally disables some warnings. This message is only printed once.\n' ); end;
    
        try;
            builtin( 'warning', 'off', 'Octave:shadowed-function' );
            builtin( 'warning', 'off', 'Octave:addpath-pkg' );
            builtin( 'warning', 'off', 'Octave:empty-index' );
            builtin( 'warning', 'off', 'Octave:get_input:invalid_utf8' );
            builtin( 'warning', 'off', 'Octave:language-extension' );
            builtin( 'warning', 'off', 'Octave:logical-conversion' );
            builtin( 'warning', 'off', 'Octave:missing-semicolon' );
            builtin( 'warning', 'off', 'Octave:mixed-string-concat' );
            builtin( 'warning', 'off', 'Octave:num-to-str' );
            builtin( 'warning', 'off', 'Octave:onCleanup' );
            builtin( 'warning', 'off', 'Octave:remove-init-dir' );
            builtin( 'warning', 'off', 'Octave:savepath-local' );

        catch me;
            warning( 'TTEST:warnings', 'TTEST could not disable some warnings.\n Thrown error:\n' );
            disp( me ); end;
    catch me;
        warning( 'TTEST:warnings', 'Some error happened during a call to TTEST_disableWarnings.\n Thrown error:\n' );
        disp( me ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

