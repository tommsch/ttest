function [ varargout ] = lasterror( varargin );
    if( ismatlab() );
        warning( 'TTEST:shim', 'A shim for Octave happens to be on the Matlab path. This should not happen.' );
        s = builtin( 'lasterror', varargin{:} );
    else;
        if( numel(varargin) == 1 && isstruct(varargin{1}) && isfield(varargin{1}, 'message') && isfield(varargin{1}, 'identifier') );
            s = builtin( 'lasterr', varargin{1}.message, varargin{1}.identifier );
        else;
            s = builtin( 'lasterror', varargin{:} ); end; end;
    if( nargout == 1 );
        varargout = {s}; end;
end

function ret = ismatlab ()
    % returns true when run on m
    persistent x;
    if( isempty (x) );
        x = ~exist( 'OCTAVE_VERSION', 'builtin' ); end;
    ret = x;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>
