% TTEST:startup
% 3.2025.02.28
% These comment lines at the beginning must be present in this file.
% They are overwritten automatically by a call to `TTEST install`
function startup();
    %%
    % keyboard;  % For debugging during startup
    % Check overloads
    err = lasterror();  %#ok<LERR>
    try;
	    str = evalc( 'TTEST overload' );
        fprintf( 'TTEST is installed.\n  Active overloads:\n%s\n', str );
    catch me;  %#ok<NASGU>
        str = evalc( 'disp( me );' );
        fprintf( 2, 'TTEST seems to be installed, but not working.\n  Thrown error:\n  %s', str ); end;
    lasterror( err );  %#ok<LERR>
    % Store startup folder
    TTEST( 0, 'var', 'startup_folder', pwd );  % store start up folder in TTEST database. We can use this information for selecting the correct folder for the `savepath` command
    % Check installed version number
    filename = [mfilename( 'fullpath' ) '.m'];
    try;
        err = lasterror();  %#ok<LERR>
        str = TTEST( 'ver' );
        lines = readlinest( filename );
        if( numel(lines) < 2 );
            goto_catch_1289hajskdfiz89q32t;
            return; end;
        if( ~contains( lines{1}, 'TTEST:startup' ) ||  ~contains( lines{2}, str ) );
            fprintf( 2, 'TTEST are not installed in the correct version. You should reinstall ttoolboxes by calling `ttoolboxes install`.' ); end;
    catch me;  %#ok<NASGU>
        lasterror( err );  %#ok<LERR>
        fprintf( 2, 'Could not test whether TTEST is installed in the correct version.\n  You either have not sufficient write permissions to the file `./TTEST/shim/???/startup.m` or,\n  you may have to reinstall TTEST.\n' );
        end;
end
function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

