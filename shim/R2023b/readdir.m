function [ lst_cell, err, msg ] = readdir( folder );
% Implementation of Octavs readdir function
% [ lst_cell, err, msg ] = readdir( [folder] );

if( nargin == 0 );
    folder = '.'; end;

try;
    lst_struct = dir ( folder );
    lst_cell = cell( numel(lst_struct), 1 );
    [lst_cell{:}] = deal( lst_struct.name );
    err = 0;
    msg = '';
catch me;
    err = 1;
    msg = me.msg;
    lst = {}; end;

end



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
