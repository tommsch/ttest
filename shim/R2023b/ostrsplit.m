function [ out ] = ostrsplit( in, delim );
% Implementation of Octavs ostrsplit function
% Example: 
%   ostrsplit ('a,b,c', ',');  % yields {'a','b','c'}
%   ostrsplit (['a,b' ; 'cde'], ',')  % yields {'a', 'b', 'cde'}

    out = {};
    if( ~iscell(in) );
        in = {in}; end;
    
    
    for i = 1:numel( in );
        for r = 1:size( in{i}, 1 );
            if( nargin==1 );
                val = strsplit( in{i}(r,:) );
            else;
                val = strsplit( in{i}(r,:), delim ); end; 
            out(end+1:end+numel(val)) = deal( val ); end; end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
