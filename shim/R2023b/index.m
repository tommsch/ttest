function [ idx ] = index( s, t, direction );
% Implementation of Octaves index function
% Example: 
%   index( 'Teststring', 't' )  % yields 4

    idx = strfind( s, t );
    if( isempty(idx) );
        idx = 0;
        return;
    elseif( nargin <= 2 || strcmp(direction, 'first') );
        idx = idx(1);
    elseif( nargin >= 3 && strcmp(direction, 'last') );
        idx = idx(end); end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
