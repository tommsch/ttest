% patched version of `savepath`

########################################################################
##
## Copyright (C) 2005-2024 The Octave Project Developers
##
## See the file COPYRIGHT.md in the top-level directory of this
## distribution or <https://octave.org/copyright/>.
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.
##
########################################################################

## -*- texinfo -*-
## @deftypefn  {} {} savepath
## @deftypefnx {} {} savepath @var{file}
## @deftypefnx {} {@var{status} =} savepath (@dots{})
## Save the unique portion of the current function search path to @var{file}.
##
## The list of folders that are saved in @var{file} does @emph{not} include
## the folders that are added for Octave's own functions, those that belong to
## Octave packages (see @ref{XREFpkg,,pkg load}), and those added via command
## line switches.
##
## If @var{file} is omitted, Octave looks in the current directory for a
## project-specific @file{.octaverc} file in which to save the path
## information.  If no such file is present then the user's configuration file
## @file{~/.octaverc} is used.
##
## If successful, @code{savepath} returns 0.
##
## The @code{savepath} function makes it simple to customize a user's
## configuration file to restore the working paths necessary for a particular
## instance of Octave.  Assuming no filename is specified, Octave will
## automatically restore the saved directory paths from the appropriate
## @file{.octaverc} file when starting up.  If a filename has been specified
## then the paths may be restored manually by calling @code{source @var{file}}.
## @seealso{path, addpath, rmpath, genpath, pathdef}
## @end deftypefn

function status = savepath (file)
  disp( "TTEST INFO: Patched 'savepath' file from Octave 9.1, Windows, is used." );  % ADDED PATCH, tommsch, 2024-03-31
  beginstring = "## Begin savepath auto-created section, do not edit";
  endstring   = "## End savepath auto-created section";

  ## Use project-specific or user's .octaverc when no file specified
  if (nargin == 0)
    file = fullfile (pwd, ".octaverc");
    if (! exist (file, "file"))
      file = fullfile ("~", ".octaverc");
    endif
  endif

  ## Read in the file
  [filelines, startline, endline] = getsavepath (file);

  ## Determine where the savepath lines are placed in the file.
  if (isempty (filelines)
      || (startline == 1 && endline == length (filelines)))
    ## savepath is the entire file.
    pre = post = {};
  elseif (endline == 0)
    ## Drop the savepath statements at the end of the file.
    pre = filelines;
    post = {};
  elseif (startline == 1)
    pre = {};
    post = filelines(endline+1:end);
  elseif (endline == length (filelines))
    pre = filelines(1:startline-1);
    post = {};
  else
    ## Insert in the middle.
    pre = filelines(1:startline-1);
    post = filelines(endline+1:end);
  endif

  ## Write the results.
  [fid, msg] = fopen (file, "wt");
  if (fid < 0)
    error ("savepath: unable to open FILE for writing, %s, %s", file, msg);
  endif
  unwind_protect
    fprintf (fid, "%s\n", pre{:});

    ## Remove the portion of the path defined via the command line
    ## and/or the environment.
    workingpath = parsepath (path);
    cmd_line_path = parsepath (command_line_path ());
    octave_path = parsepath (getenv ("OCTAVE_PATH"));
    default_path = pathdef ();
    if (isempty (default_path))
      ## This occurs when running octave via run-octave.  In this instance
      ## the entire path is specified via the command line and pathdef()
      ## is empty.
      [~, n] = setdiff (workingpath, octave_path);
      default_path = cmd_line_path;
    else
      [~, n] = setdiff (workingpath, union (cmd_line_path, octave_path));
      default_path = parsepath (default_path);
    endif
    ## This is the path we'd like to preserve when octave is run.
    path_to_preserve = workingpath(sort (n));

    ## Determine the path to Octave's user and system wide packages.
    [pkg_user, pkg_system] = pkg ("list");

    ## Conversion from cell array of structs to cellarray of strings with the
    ## unique contents of the fields "dir" and "archprefix".
    pkg_path = unique ([cellfun(@(elt) elt.dir,
                                [pkg_user, pkg_system],
                                "uniformoutput", false);
                        cellfun(@(elt) elt.archprefix,
                                [pkg_user, pkg_system],
                                "uniformoutput", false)]);

    ## Rely on Octave's initialization to include the pkg path elements.
    for i_pkg = 1:numel (pkg_path)
      ## Remove all paths that are (sub-)folders of a package folder.
      if( isempty(pkg_path{i_pkg}) );  % ADDED PATCH, tommsch, 2024-03-31
          continue; end;               % ADDED PATCH, tommsch, 2024-03-31
      pkg_path_pattern = [regexptranslate("escape", pkg_path{i_pkg}), ".*"];
      not_pkg_path ...
        = cellfun (@isempty, regexp (path_to_preserve, pkg_path_pattern));
      path_to_preserve = path_to_preserve(not_pkg_path);
    endfor

    ## Split the path to be saved into two groups.  Those path elements that
    ## belong at the beginning and those at the end.
    if (! isempty (default_path))
      n1 = find (strcmp (default_path{1}, path_to_preserve));
      n2 = find (strcmp (default_path{end}, path_to_preserve));
      n_middle = round ((n1+n2)/2);
      [~, n] = setdiff (path_to_preserve, default_path);
      path_to_save = path_to_preserve(sort (n));
      ## Remove pwd
      path_to_save(strcmp (path_to_save, ["." pathsep()])) = [];
      if (! isempty (path_to_save))
        n = ones (numel (path_to_save), 1);
        for m = 1:numel (path_to_save)
          n(m) = find (strcmp (path_to_save{m}, path_to_preserve));
        endfor
        path_to_save_begin = path_to_save(n <= n_middle);
        path_to_save_end   = path_to_save(n > n_middle);
      else
        path_to_save_begin = {};
        path_to_save_end   = {};
      endif
    else
      path_to_save_begin = path_to_preserve;
      path_to_save_end   = {};
    endif
    path_to_save_begin = cell2mat (path_to_save_begin);
    path_to_save_end   = cell2mat (path_to_save_end);

    ## Use single quotes for PATH argument to avoid string escape
    ## processing.  Since we are using single quotes around the arg,
    ## double any single quote characters found in the string.
    fprintf (fid, "%s\n", beginstring);
    if (! isempty (path_to_save_begin))
      n = find (path_to_save_begin != pathsep, 1, "last");
      fprintf (fid, "  addpath ('%s', '-begin');\n",
               strrep (path_to_save_begin(1:n), "'", "''"));
    endif
    if (! isempty (path_to_save_end))
      n = find (path_to_save_end != pathsep, 1, "last");
      fprintf (fid, "  addpath ('%s', '-end');\n",
               strrep (path_to_save_end(1:n), "'", "''"));
    endif
    fprintf (fid, "%s\n", endstring);

    fprintf (fid, "%s\n", post{:});
  unwind_protect_cleanup
    sts = fclose (fid);
    if (sts < 0)
      error ("savepath: could not close savefile after writing, %s", file);
    elseif (nargin == 0)
      warning ("off", "backtrace", "local");
      warning ("Octave:savepath-local",
               "savepath: current path saved to %s", file);
    endif
  end_unwind_protect

  if (nargout > 0)
    status = 0;
  endif

endfunction

## Convert single string of paths to cell array of paths
function path_elements = parsepath (p)
  path_elements = strcat (ostrsplit (p, pathsep), pathsep);
endfunction


########################################################################
##
## Copyright (C) 2014-2024 The Octave Project Developers
##
## See the file COPYRIGHT.md in the top-level directory of this
## distribution or <https://octave.org/copyright/>.
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.
##
########################################################################

## -*- texinfo -*-
## @deftypefn {} {[@var{filelines}, @var{startline}, @var{endline}] =} getsavepath (@var{file})
## Undocumented internal function.
## @end deftypefn

function [filelines, startline, endline] = getsavepath (file)

  beginstring = "## Begin savepath auto-created section, do not edit";
  endstring   = "## End savepath auto-created section";

  ## Read in the file while checking for errors along the way.
  startline = endline = 0;
  filelines = {};
  if (exist (file) == 2)
    [fid, msg] = fopen (file, "rt");
    if (fid < 0)
      error ("getsavepath: could not open file, %s: %s", file, msg);
    endif
    linenum = 0;
    while (ischar (line = fgetl (fid)))
      filelines{++linenum} = line;
      ## Find the first and last lines if they exist in the file.
      if (strcmp (line, beginstring))
        startline = linenum;
      elseif (strcmp (line, endstring))
        endline = linenum;
      endif
    endwhile
    if (fclose (fid) < 0)
      error ("getsavepath: could not close file after reading, %s", file);
    endif
  endif

  ## Verify the file was correctly formatted.
  if (startline > endline || (startline > 0 && endline == 0))
    error ("getsavepath: unable to parse file, %s", file);
  endif

endfunction



function TTEST_dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
