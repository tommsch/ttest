function [ varargout ] = cwd( varargin );
% Searches files and changes working directory
% [ old, new, paths ] = cwd( [func], [options] );
%
% Input:
%   func            string, file or directory to be located, wildcards '*' and '?' are supported
%                   When file/folder is found multiple times, iterative calling of this function changes to the next found occurence
%
%   [no input inside of function]
%                   When called without arguments, then the filename of current executing function is used
%                   This call is only possible from within a function
%
%   [no input from commandline]
%                   uses filename of currently opened file in editor window (Only Matlab)
%                   
%
% Options:
%   '-eager'        Searches on more places
%   '-edit'/e       Opens the file in editor, when its found
%   '-explorer'     Opens the file in the systems file explorer, when its found (Windows Explorer, Konquererer, ... )
%   '-back'         Goes back to the last directors
%   '-recursive'    If given, then in all subdirectories is also searched. This may take some time, 
%                   and the results depends on where the function was called
%   num             integer, can be used to select specifically a path.
%   :number         opens file and jumps to the specified line number
%
% Screen Output:        Paths where file/directory was found
% Working Directory:    Working directory is changed
% Data Output:
%   old             string, old working directory (the directory prior executing this command)
%   new             string, new working directory (the directory after executing this command)
%   paths           all found folders where file/directory was found. Empty if nothing was found
%
% Notes:
%  - If func is found multiple times, a warning is printed, but the directory is changed nevertheless.
%  - If func is not found, the directory is not changed.
%  - The places where this function searches for files may change in subsequent releases. This function is not ment to be used programatically
%
% E.g: cwd spy:10
%      cwd plot explore;
%
% Written by: tommsch, 2021-10-22

%               tommsch,    2021-10-22,     Added search in working directory history (only for matlab)
%               tommsch,    2022-02-09,     Improved search results, 
%                                           directories are found too
%                                           Added option -explore, -back, num
%               tommsch,    2022-09-03,     Added rudimentary possibility to add user defined search paths
%                           2023-03-30,     Added option aliases with `--` at the front
%                                           Bugfix, Furthermore, this function should find more files now
%               tommsch,    2023-06-08,     Added possibility to specify line number by appending `:number`
%                                           Behaviour change: Methods finding files which take long are only used if nothing can be found, or the option '-eager' is supplied
%               tommsch,    2023-07-12,     Added feature: On matlab, when called from the command line, path is changed to the folder of the currently opened file in the editor
%               tommsch,    2024-03-13,     Added feature: Support of wildcards
%               tommsch,    2024-04-03,     Improved search results, 
%               tommsch,    2024-04-16,     Added experimental feature: cwd searches for files named `cwd_paths`, and uses paths stored in that file to search for new places.
%               tommsch,    2024-09-26,     Option eager is deprecated
%               tommsch,    2024-11-06,     Bugfix for paths with/without trailing filesep character
% Changelog:    

    persistent lastidx;
    persistent lastfunc;  % filename used in last search
    persistent lastfullpath;
    persistent backpath;
    persistent lastsub;  % whether last time was used option -recursive
    which_h();  % clear which_h
    
    if( isempty(lastidx) );
        lastidx = 1; end;
    if( isempty(backpath) );
        backpath = {}; end;
    if( isempty(lastsub) );
        lastsub = false; end;
    
    [func, opt] = parse_input( varargin{:} );
    
    if( opt.back );
        [fullpath, backpath] = do_back( backpath );
    elseif( isequal(lastfunc, func) && ~opt.subdir && ~lastsub );
        lastidx = lastidx + 1;
        fullpath = lastfullpath;
    elseif( opt.subdir || lastsub );
        lastidx = lastidx + 1;
        fullpath = [];
    else;
        lastidx = 1;
        fullpath = [];end;
    
    
    if( opt.tocurrent );
        fullpath = scl(); 
    elseif( isempty(fullpath) );
        fullpath = get_directories( func, opt );
        fullpath = sort_directories( fullpath );
        lastfullpath = fullpath; end;

    [backpath, fullpath_selected, old_pwd, new_pwd] = change_path( lastidx, func, lastfunc, fullpath, backpath, opt );    
    lastfunc = func; 
    lastsub = opt.subdir;

    if( opt.edit );
        do_edit( func, fullpath_selected, opt ); end;
    if( opt.explore );
        do_explore( func, fullpath_selected, new_pwd, opt ); end;

    
    if( nargout >= 1 );
        varargout{1} = old_pwd; end;
    if( nargout >= 2 );
        varargout{2} = new_pwd; end;
    if( nargout >= 3 );
        varargout{3} = fullpath; end;        


    do_advertisment();
    
end

%% get directories functions

function [ fullpath ] = get_directories( func, opt );
    fullpath_which = get_from_which( func );
    fullpath_history = get_from_folders( func, 'history', opt );
    fullpath_what = get_from_what( func );
    fullpath_path = get_from_path( func );
    fullpath_editor = get_from_folders( func, 'editor', opt );
    fullpath_cwd = get_from_folders( func, 'cwd_paths', opt );
    
    fullpath = [fullpath_which; fullpath_history; fullpath_what; fullpath_path; fullpath_editor; fullpath_cwd];
    if( opt.subdir );
        path_subdir = get_from_subdir( func, fullpath, opt.subdir );
        fullpath = [fullpath; path_subdir]; end;
    
    if( ~isempty(fullpath) );
        fullpath(cellfun( 'prodofsize', fullpath ) == 0) = []; end;  % remove empty entries        
    fullpath = unique_filenames( fullpath );
    if( iswindows() );
        fullpath_lower = fullpath;
        for i = 1:numel( fullpath );
            fullpath_lower{i} = lower( fullpath{i} ); end; 
        [~, idx] = unique( fullpath_lower, 'stable' );
        fullpath = fullpath( idx );
    else;
        fullpath = unique( fullpath, 'stable' ); end;
end

function [ fullpath ] = sort_directories( fullpath );
    keywords = {'builtin',   10;
                matlabroot,   5;
                'ttoolboxes',-2;
               };
    w = 0;
    for i = 1:size( keywords, 1 );
        w_i = cellfun( @isempty, strfind( fullpath, keywords{i,1}, 'ForceCellOutput',true ) ) == 0;
        w = w + keywords{i,2}*w_i; end;
    [~, idx] = sort( w );  % sorting is stable according to Matlab and Octave documentation
    fullpath = fullpath(idx);
end


function [ fullpath ] = get_from_which( func );
    
    if( ismatlab() );
        [fullpath, ~] = which_h( func, '-all' );
    else;
        fullpath = which_h( func ); end;
    if( isempty(fullpath) );  % Matlab/Octave bugfixes
        fullpath = {};
    elseif( ~iscell(fullpath) );
        fullpath = {fullpath}; end;

    % remove stuff not part of the filename
    for i = 1:numel( fullpath );        
        if( numel(fullpath{i}) >= 10 && ...
            isequal( fullpath{i}(1:10), 'built-in (' ) && ...
            isequal( fullpath{i}(end), ')' ) ...
          );
            fullpath{i} = fullpath{i}(11:end-1); end; end;
    
    fullpath = unique( fullpath, 'stable' );

    % look for cwd_paths.m files
    cwd_paths_fullpath = which_h( 'cwd_paths' );
    if( isempty(cwd_paths_fullpath) );  % Matlab/Octave bugfixes
        fullpath = {};
    elseif( ~iscell(cwd_paths_fullpath) );
        cwd_paths_fullpath = {cwd_paths_fullpath}; end;
    for i = 1:numel( cwd_paths_fullpath );        
        if( numel(cwd_paths_fullpath{i}) >= 10 && ...
            isequal( cwd_paths_fullpath{i}(1:10), 'built-in (' ) && ...
            isequal( cwd_paths_fullpath{i}(end), ')' ) ...
          );
            cwd_paths_fullpath{i} = cwd_paths_fullpath{i}(11:end-1); end; end;
    
    cwd_paths_fullpath = unique( cwd_paths_fullpath, 'stable' );
    cwd_path_container( cwd_paths_fullpath );

end

function [ fullpath ] = get_from_folders( func, src, opt );
    
    old = pwd;
    cleanold = onCleanup( @() cd(old) );
    if( ~ismatlab );
        fullpath = {}; 
        return; end;

    switch src;
        case {'cwd_path','cwd_paths'};
            folders = cwd_path_container();
            src = {};
            oldpwd = pwd;
            clean_folder = onCleanup( @() cd(oldpwd) );
            for i = 1:numel( folders );
                try;
                    cd( fileparts(folders{i}) );
                catch me; %#ok<NASGU>
                    fatal_error; end;
                try;
                    [~, src_i] = evalc( 'cwd_paths()' );  % we use evalc to hide from Matlab that a third party function may be called. Otherwise the unit tests fail
                    if( iscell(src_i) && ...
                        (all(cellfun('isclass', src_i, 'char') | cellfun('isclass', src_i, 'string'))) ...
                      );
                        % everything ok
                    else;
                        fprintf( 'File cwd_paths in folder %s returns something wrong. The function must return a cell array of strings.\n', folders{i} );
                        continue; end;
                catch me;
                    fprintf( 'File cwd_paths in folder %s generated an error while executing.\n  Thrown error:', folders{i} );
                    disp( me );
                    continue; end;

                % paths in cwd_paths can be absolute or relative, we have to convert the paths to absolute paths
                for j = 1:numel( src_i );
                    try;
                        cd( fileparts(folders{i}) );
                        cd( src_i{j} );
                        src_i{j} = pwd;
                    catch me;  %#ok<NASGU>
                        fprintf( 'File cwd_paths in folder %s holds an inaccesible directory: %f.', folders{i}, src_i{j} ); end; end;
                src_i = src_i(:);
                src = [src; src_i ]; end;  %#ok<AGROW>
            
        case 'history';
            settings_ = settings();
            src = settings_.matlab.desktop.currentfolder.History.PersonalValue;

        case 'editor';
            document = matlab.desktop.editor.getAll;
            src = cell( 1, numel(document) );
            [src{:}] = document.Filename;
            for i = 1:numel( src );
                src{i} = fileparts( src{i} ); end; end;
    
    fullpath = {};
    src = unique( src, 'stable' );
    for i = 1:numel( src );
        try;
            cd( src{i} );
            fullpath_i = get_from_which( func );
            fullpath = [fullpath; fullpath_i];  %#ok<AGROW>
        catch me;
            if( strcmp(me.identifier, 'MATLAB:cd:NonExistentFolder') );
                % do nothing
            else;
                error( me.identifier, me.message ); end; end; end;
    
    fullpath = unique( fullpath, 'stable' );
end            

function [ fullpath ] = get_from_path( func );
    % retrieves folders from `path()`
    dirs = regexp( path, pathsep, 'split' );
    fullpath = cell( 0, 1 );
    for i = 1:numel( dirs )
        dirs_i = fullfile( dirs{i}, func );
        files_i = fullfile( dirs{i}, [func '.m'] );
        if( isfolder( dirs_i ))
            fullpath(end+1,1) = {dirs_i};  %#ok<AGROW>
        elseif( isfile( files_i ) );
            fullpath(end+1,1) = {files_i}; end; end;  %#ok<AGROW>
end

function [ fullpath ] = get_from_what( func );
    % retrieves folders from what( func );
    try;
        if( isoctave );
            [e_msg, e_id] = lasterr(); end;  %#ok<LERR>
        s = what( func );
    catch me;  %#ok<NASGU>  % octave errors if it can not find func
        if( isoctave );
            lasterr( e_msg, e_id ); end;  %#ok<LERR>
        s = []; end;
    if( ~isempty(s) );
        fullpath = cell( 1, numel(s) );
        [fullpath{:}] = s(:).path;
        fullpath = fullpath';
    else;
        fullpath = {}; end;
end


function [ path ] = get_from_subdir( func, directories, subdir );
    % add here some paths which shall be searched when a recursive search is used
    path = {};
    start = tic;
    if( ~iscell(directories) );
        directories = {directories}; end;
    maxtime = 5 + 5*numel( directories );
    if( subdir );
        searchstring =  {['**' filesep func ];
                         ['**' filesep func '*.m'];
                         ['.' filesep func]};
    else;
        searchstring =  {[func];
                         [func '*.m'];
                         ['*' filesep func ];
                         ['*' filesep func '*.m'];
                         ['.' filesep func]};end;  %#ok<NBRAK2>
    
    olddir = pwd;
    cleandir = onCleanup( @() cd(olddir) );
    directories = [directories(:); olddir];
    for k = 1:numel( directories );
        directories{k} = fileparts( directories{k} ); end;
    directories = unique_filenames( directories );
    if( iswindows );
        func = lower( func ); end;
    for k = 1:numel( directories );
        try;
            cd( directories{k} );
        catch me;  %#ok<NASGU>
            fprintf( 2, 'Could not search in path: %s\n', directories{k} ); 
            continue; end;
        for l = 1:numel( searchstring );
            try;
                err = lasterror();  %#ok<LERR>
                x = dir( searchstring{l} );
            catch me; %#ok<NASGU>
                lasterror( err ); end;  %#ok<LERR>
            for i = 1:numel( x );
                if( iswindows );
                    x(i).name = lower( x(i).name );
                    x(i).folder = lower( x(i).folder ); end;
                if( strcmp( x(i).name, func ) );
                    path{end+1} = x(i).name; end;  %#ok<AGROW>
                if( strcmp( x(i).name, [func '.m'] ) );
                    path{end+1} = x(i).folder; end;  %#ok<AGROW>
                if( ismatlab && strcmp( lastfolder(x(i).folder), func  ) );
                    path{end+1} = x(i).folder; end; end; end;  %#ok<AGROW>        
        %searchstring = ['..' filesep searchstring];  %#ok<AGROW>
        if( toc(start) > maxtime );
            fprintf( 2, 'Did not finish searching in subdirectory, because it took too long.\n  Subdirectory: %s\n', directories{k} );
            break; end; end;
    
    path = path(:);
end


function [ varargout ] = wwhich( varargin );
% Copyright (c) 2009, Lucio Cetto
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


% WWHICH Locate m-files using wildcards
%    Works exactly as WHICH, but you can
%    use wild cards.
%    
%    out is a cell whenever 2 or more
%    matches are found or if the flag
%    -all was used
%
%    Note: a function may be shawdowed,
%          use the flag -all to locate them
%
%    See WHICH for more info 
%
%   by Lucio Andrade

    assert( nargin >= 1, 'ttest:which', 'Not enough input arguments.' );
    s1 = varargin{1};
    s2 = varargin{end};
    
    p = path;
    out = cell(0);
    h = [0 find( p == pathsep ) length( p )+1]; 
    for i = length( h ):-1:2
        direc = p(h(i-1)+1:h(i)-1);
        a = dir( [direc '/' s1 '.m'] );
        for j = 1:length( a );
            if( nargin > 1 );
                outtmp = which( a(j).name, s2 );
                if( iscell(outtmp) );
                    out = [out;outtmp];  %#ok<AGROW>
                else;
                    out = [out;{outtmp}]; end;  %#ok<AGROW>
                
            else;
                outtmp = which( a(j).name );
                out = [out;{outtmp}]; end; end; end;  %#ok<AGROW>

    % take out repetitions
    todel = [];
    for i = 1:length( out );
        for j = i+1:length( out );
            if( strcmp(out{i}, out{j}) );
                todel = [todel j]; end; end; end;  %#ok<AGROW>
    out(unique( todel )) = [];

    % change to string when needed
    if( nargout == 1 );
        varargout = {out};
    elseif( nargout == 2 );
        type = repmat( {''}, [numel(out) 1] );
        varargout = {out, type};
    else;
        for i = 1:length( out );
            disp( out{i} ); end; end;
end

%%

function [ func, opt ] = parse_input( varargin );
    [ opt.edit, varargin ] =      parse_input_worker( {'-edit','-e','-ed','--edit','--e','--ed'},                                         false, varargin{:} );
    [ opt.back, varargin ] =      parse_input_worker( {'-back','-b','--back','--b'},                                                      false, varargin{:} );
    [ opt.explore, varargin ] =   parse_input_worker( {'-explore', '-ex','--explore', '--ex'},                                            false, varargin{:} );
    [ opt.subdir, varargin ] =    parse_input_worker( {'-sub', '-subdir','-recursive','-rec','--sub', '--subdir','--recursive','--rec','--eager','-eager'},  false, varargin{:} );
    
    opt.tocurrent = false;
    if( opt.back );
        func = [];
        % do nothing
    elseif( numel(varargin) == 0 );
        db = dbstack;
        if( numel(db) < 3 && ismatlab );
            func = [];
            opt.tocurrent = true;
        elseif( numel(db) < 3 );
            error( 'cwd:input', 'On Octave, `cwd` cannot be called from the command line.' );
        else;
            func = db(3).name; end;
        
    else;
        func = varargin{1};
        varargin(1) = []; end;
    
    [ opt.edit, varargin ] =    parse_input_worker( {'edit','e','ed'},                              opt.edit,       varargin{:} );
    [ opt.back, varargin ] =    parse_input_worker( {'back','b'},                                   opt.back,       varargin{:} );
    [ opt.explore, varargin ] = parse_input_worker( {'explore', 'ex'},                              opt.explore,    varargin{:} );
    [ opt.subdir, varargin ] =  parse_input_worker( {'sub', 'subdir','recursive','rec','eager'},    opt.subdir,     varargin{:} );
    
    opt.idx = [];
    if( numel(varargin) >= 1 )
        if( isnumeric(varargin{end}) );
            num = varargin{end};
        else;
            num = str2double( varargin{end} ); end;
        if( ~isnan(num) );
            assert( isnumeric(num) && num >= 1 && num < inf && round(num) == num, 'cwd:input', 'Given index is bad or out of range. Given idx: %g', num  );
            varargin(end) = [];
            opt.idx = num; end; end;
    
    if( numel(func) >= 2 && strcmp(func(end-1:end), '.m') );
        func = func(1:end-2); end;
    
    % parse filename
    opt.linenum = [];
    if( ~isempty(func) );
        idx = strfind( func, ':' );  % octave bugfix
    else;
        idx = []; end;
    if( numel(idx) == 1 && numel(func) > idx+1 );
        n = str2double( func(idx+1:end) );
        if( ~isnan(n) );
            opt.linenum = n;
            func(idx:end) = []; 
            opt.edit = 1; end; end;
    assert( isempty(varargin), 'cwd:input', 'Bad input/options given.' );
end

function [ flag, varargin ] = parse_input_worker( pattern, flag, varargin )
    idx = false( 1, numel(varargin) );
    for i = 1:numel( pattern );
        idx = idx | strcmp( pattern{i}, varargin ); end;
    idx = find( idx );
    if( ~isempty(idx) );
        flag = true; 
        varargin(idx) = []; end;
end

function [ fullpath,  backpath ] = do_back( backpath );
    persistent lastidx_matlab
    if( isempty(lastidx_matlab) );
        lastidx_matlab = 1; end;
    
    if( isempty(backpath) );
        if( ismatlab );
            fprintf( 'No stored path in cwd. I go further back in time using the matlab folder history.\n' );
            s = settings();
            back_history = s.matlab.desktop.currentfolder.History.PersonalValue;
            fullpath = back_history(lastidx_matlab);
            back_history = back_history(end:-1:1);
            back_history_name = 'Matlab-history';
            back_history_idx = numel( back_history ) - lastidx_matlab + 1;
            %back_history_idx = lastidx_matlab;
            lastidx_matlab = lastidx_matlab + 1;
        else;
            error( 'cwd:back', 'No stored path in cwd. I cannot go further back.' ); end;
        
    else;
        lastidx_matlab = 1;
        back_history = backpath;
        back_history_name = 'cwd-history';
        back_history_idx = numel( back_history );;
        fullpath = backpath(end);
        backpath(end) = []; end;

    fprintf( '%s:\n', back_history_name );
    for i = 1:numel( back_history );
        fprintf( 1+(i == back_history_idx), '(%3i): %s\n', numel( back_history )-i+1, back_history{i} ); end;
    if( back_history_idx > numel(back_history) );
        error( 'cwd:back', 'Cannot go back further in history.' );
        end;

    % print back_history
end

function [ backpath, fullpath_selected, old_pwd, new_pwd ] = change_path( lastidx, func, ~, fullpath, backpath, opt );
    old_pwd = pwd;
    if( numel(fullpath) >= 1 );
        if( ~isempty(opt.idx) );
            idx = opt.idx;
            assert( idx <= numel(fullpath), 'cwd:input', 'Given index too large.' );
        else;
            idx = mod( lastidx - 1, numel(fullpath) ) + 1; end;
        fullpath_selected = fullpath{idx};
    else;
        fullpath_selected = ''; end;
    
    if( numel(fullpath) > 1 );  % && ~isequal(func, lastfunc)
        len = max( 4, floor( log10(numel(fullpath)) ) + 1 );
        for i = numel( fullpath ):-1:1;
            if( i == idx );
                pre = '  -->';
            else;
                pre = '     '; end;
            fprintf( '%s %*i : %s\n', pre, len, i, fullpath{i} ); end;
        fprintf( 'More than one path found where queried name lies.\nRerun this function to choose another path; Or add the number of the desired file to the input. E.g.: ''cwd %s 2''.\n', func );
    else;
        if( ~opt.back );
            fprintf( '\b  |  ' ); end; end;
    
    if( opt.back );
        fprintf( '\nGoing back to: %s\n', fullpath{idx} );
        cd( fileparts(fullpath{idx}) );
        
    elseif( numel(fullpath) == 0 );
        fprintf( 'Name not found. Path will not be changed.\n' );
        
    elseif( exist( fullpath{idx}, 'dir' ) ~= 7 && isequal( old_pwd, fileparts(fullpath{idx}) ) || ...
            isequal( old_pwd, fullpath{idx} ) ...
          );
        fprintf( 'Path not changed.\n' );
        
    else;
        if( ispc ); 
            h_strcmp = @strcmpi;
        else; 
            h_strcmp = @strcmp; end;
        if( isempty(backpath) || ~h_strcmp(backpath{end}, old_pwd) );
            backpath{end+1} = old_pwd; end;
        backpath{end+1} = fullpath{idx};
        try;
            if( isfolder(fullpath{idx}) );
                cd( fullpath{idx} );
            else;
                cd( fileparts(fullpath{idx}) ); end;
            fprintf( 'Path changed to (%i): %s\n', idx, fullpath{idx} );
        catch me;  %#ok<NASGU>
            fprintf( 2, 'Could not change directory to: %s\n  The directory may not exist or you may not have rights to do it.\n', fullpath{idx} );
            end; end;
    new_pwd = pwd;
end

function do_edit( func, fullpath_selected, opt );
    try;
        if( ismatlab && ~isempty(opt.linenum) );
            matlab.desktop.editor.openAndGoToLine( which(fullpath_selected), opt.linenum );
        else;
            if( ~isempty(which(fullpath_selected)) );
                edit( fullpath_selected );
            elseif( ~isempty(fullfile(fullpath_selected, func)) ) ;
                edit( fullfile(fullpath_selected, func) );
            else;
                edit( func );
            end; end;
    catch me;  %#ok<NASGU>
        fprintf( 'Opening failed of file %s\n', fullpath_selected );
        end;
end

function [] = do_explore( fullpath_selected, path );
    explore( path, fullpath_selected );
       
end

function [] = explore( path, func );
    
    fullfunc = [fullfile( path, func ) '.m'];
    dirflag = exist( func, 'dir' );
    if( ispc );
        if( dirflag );
            cmd = {['explorer /e,' path]};
        else;
            cmd = {['explorer /select,"' fullfunc '"']}; end;
    elseif( ismac );
        if( dirflag );
            cmd = {['open -R ' path]};
        else;
            cmd = {['open -R ' fullfunc]}; end;
    elseif( isunix );
        if( dirflag );
            cmd = {['thunar ' path]
                   ['konqueror ' path ' &']};
        else;
            cmd = {['konqueror --select ' fullfunc ' &']}; end;
    else;
        error( 'cwd:fatal', 'Programming error.' ); end;
    
    success = false;
    for i = 1:numel( cmd );
        try; 
            system( cmd{i} );
            success = true;
            break;
        catch me;  %#ok<NASGU>
            end; end;
        
    if( ~success );
        warning( 'cwd:explore', 'Could not open file/folder: %s', func ); end;
end

function do_advertisment();
    try;  %#ok<TRYNC>
        [~, name] = system('hostname');
        if( numel(name) >= 7 && isequal(name(1:7), 'BEQUIET') );
            return; end; end;
    
    if( randi(20) == 1 );
        fprintf( 2, 'If you like the function `cwd`, please rate it and leave a comment at the Matlab File Exchange.' ); 
        if( ismatlab );
            fprintf( ' <a href="https://mathworks.com/matlabcentral/fileexchange/106565">Link to FEX</a>' ); end;
        fprintf( '\n' ); end;
    
end

function [ varargout ] = which_h( varargin );
    persistent handle;
    if( nargin == 0 );
        handle = [];
        return; end;
    if( isempty(handle) );
        if( contains( varargin{1}, {'*','?'} ) );
            handle = @wwhich;
        else;
            handle = @which; end; end;
    varargout = cell( 1, nargout );
    [varargout{:}] = handle( varargin{:} );
end

%% cwd pahts

function [ p ] = cwd_path_container( p )
    persistent paths;
    if( isequal(paths, []) );
        paths = {}; end;
    if( nargin == 1 );
        paths = [paths p];
        paths = unique( paths );
    else;
        p = paths; end;
end

%% scl

function [ p ] = scl();
% gets name of currently opened file in Editor
    try;
        if( ismatlab );
            if( verLessThan('matlab', '9.9') );  %#ok<VERLESSMATLAB>
                editor_service = com.mathworks.mlservices.MLEditorServices;  %#ok<JAPIMATHWORKS>
                editor_app = editor_service.getEditorApplication;
                active_editor = editor_app.getActiveEditor;
                storage_location = active_editor.getStorageLocation;
                file = char( storage_location.getFile );
            else;
                active_editor = matlab.desktop.editor.getActive;
                file = active_editor.Filename; end;
            p = {fileparts( file )};
        else;
            fprintf( 2, 'Calling cwd from the command line is not supported on Octave currently.\n' ); 
            p = {};
            return;
            end
    catch me;
        fprintf( 2, 'cwd failed to get path of currently opened file.\n Thrown error:\n' );
        disp( me ); end;
end

%% helper

function [ ret ] = ismatlab();
    persistent ismatlab_
    if( isempty(ismatlab_) );
        ismatlab_ = ~exist( 'OCTAVE_VERSION', 'builtin' ); end;
    ret = ismatlab_;
end

function ret = isoctave();
    % returns true when run on octave
    persistent isoctave_;
    if( isempty (isoctave_) );
        isoctave_ = logical( exist ('OCTAVE_VERSION', 'builtin') ); end;
    ret = isoctave_;
end

function ret = iswindows ();
    % returns true when run on windows
    ret = ispc();
end

function [ fullpath ] = unique_filenames( fullpath );
    if( iswindows() );
        fullpath_lower = fullpath;
        for i = 1:numel( fullpath );
            fullpath_lower{i} = lower( fullpath{i} ); end; 
        [~, idx] = unique( fullpath_lower, 'stable' );
        fullpath = fullpath( idx );
    else;
        fullpath = unique( fullpath, 'stable' ); end;
end

function [ folder ] = lastfolder( p );
    % inspects a path string, and returns the last folder in the  string
    % folder = split( p, filesep );
    % folder = folder{end};

    while( numel(p) && p(end) == filesep );
        p(end) = []; end;
    idx = find( p == filesep, 1, 'last' );
    folder = p(idx+1:end);
end

%%




function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>  % cd, 
%#ok<*MCMLR>  % matlabroot

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

