function [ ret ] = vercmp( lhs, rhs, R )
% Compares lhs < rhs lexicographically, with dots 
% [ ret ] = vercmp( lhs, rhs, [R] )
%
% Input:
%   lhs, rhs    vectors, or char arrays where the entries are separated by '.',  ' ', ',', ';', '\s' (whitespace)/
%   R           one of: '<','<=','=','~=','>=','>', default:'<', OR
%               a binary function handle
%
% Ouput:
%   ret         true if (lhs R rhs) lexicographically, where the first entry is the most significant
%
% Notes:
%   If lhs/rhs have unequal lenght, missing places are filled up with zeros at the end
%
% E.g.:
%   lexcmp( [1 2], [1 0] )
%   lexcmp( '>=', [3 2 1], [3 2 3] )
%
% Written by: tommsch, 2021-10-14

    if( nargin == 2 || isempty(R) );
        R = @lt;
        if( numel(rhs) >= 2 );
            switch rhs(1:2);
                case '<='; R = @le; rhs(1:2) = [];
                case '>='; R = @gt; rhs(1:2) = [];
                case '=='; R = @eq; rhs(1:2) = [];
                case {'!=','~='}; R = @ne; rhs(1:2) = [];
                otherwise; end; end; % do nothing
        if( numel(rhs) >= 1 );
            switch rhs(1);
                case '<'; R = @lt; rhs(1) = [];
                case '>'; R = @gt; rhs(1) = [];
                case '='; R = @eq; rhs(1) = [];
                otherwise; end; end; % do nothing
    elseif( ischar(R) || isstring(R) );
        switch R;
            case {'le','leq','leqq','<='};  R = @le;
            case {'lt','<'};                R = @lt;
            case {'eq','=','=='};           R = @eq;
            case {'ne','neq','!=','~='};    R = @ne;
            case {'gt','>'};                R = @gt;
            case {'ge','geq','geqq','>='};  R = @ge;
            otherwise; 
                error( 'lexcmp:R', 'Wrong comparison operator given' ); end;
    elseif( isa(R, 'function_handle') );
        % do nothing
    else;
        error( 'lexcmp:R', 'Wrong comparison operator given' ); end;
        
    if( ischar(lhs) || isstring(lhs) );    
        lhs = char( lhs );
        lhs = cellfun( @str2double, strsplit( lhs, {'.', ' ', ',', ';' }, 'CollapseDelimiters', false ) ); end;
    if( ischar(rhs) || isstring(rhs) );    
        rhs = char( rhs );
        rhs = cellfun( @str2double, strsplit( rhs, {'.', ' ', ',', ';' }, 'CollapseDelimiters', false ) ); end;
    
    if( numel(lhs) > numel(rhs) );
        rhs(numel( lhs )) = 0; end;
    if( numel(rhs) > numel(lhs) );
        lhs(numel( rhs )) = 0; end;
    
    diff = lhs - rhs;
    first = diff(find( diff, 1, 'first' ));
    if( isempty(first) );
        first = 0; end;
    ret = R( first, 0 );
  
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
