function [ ret ] = isconvertible_to_bool( x )
    ret = numel(x) == 1 && ...
          (islogical(x) || isnumeric(x) || ischar(x));  % do the fast checks first
    if( ~ret );
        [w_msg, w_id] = lastwarn();
        err = lasterror();  %#ok<LERR>
        try;
            ret = logical( x );
            ret = islogical( ret );
        catch me;  %#ok<NASGU>
            ret = false; end;
        lastwarn( w_msg, w_id );
        lasterror( err ); end;  %#ok<LERR>
end

function dummy; end  %#ok<DEFNU> % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.