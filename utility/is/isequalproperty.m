function [ ret, st ] = isequalproperty( varargin )
% Returns false whenever the underlying properties of all inputs are not the same
% [ ret, st ] = isequalproperty( C1, C2, ... )
%
% Input:
%   Ci                  anything, at least two inputs must be given
%
% Output:
%   ret                 bool, true when the underyling types of call Ci are the same
%                       tested is: class, sparsity, size, 
%   st                  struct, having the fields .class, .complex, .size, .sparse
%
% E.g.: [ret, st] = isequalproperty( [2 3],single([10 30]) );
% 
% Written by: tommsch, 2021-05-06

%            tommsch, 2021-09-14, types of doubles and logicals are considered the same whenever the double array only consists of 0s and 1s.
%            tommsch, 2023-04-11, Behaviour change: Added test whether values are complex valued
%                                 Behaviour change: At least two inputs must be given
%                                 Behaviour change: Function now iterates over the elements of structs and cell arrays
%            tommsch, 2023-04-11, Added optional first argument 'strict'
% Changelog: 

    strict = isa( varargin{1}, 'OPTION' ) && isequal( varargin{1}.id, 'strict' );
    if( strict );
        varargin(1) = []; end;

    assert( numel(varargin) >= 2, 'isequalproperty:nargin', 'This function needs at least two inputs' );
    
    st = make_st();
    
    for i = 1:numel( varargin ) - 1;
        
        while( true );
            if( isstruct(varargin{i}) && isstruct(varargin{i+1}) );
                fn1 = fieldnames( varargin{i} );
                fn2 = fieldnames( varargin{i+1} );
                if( isequal(fn1, fn2) );
                    for j = 1:numel( fn1 );
                        st = st_combine( st, isequalproperty_worker( strict, varargin{i}.(fn1{j}), varargin{i+1}.(fn1{j}) ) ); end;
                else;
                    break; end;
            elseif( iscell(varargin{i}) && iscell(varargin{i+1}) );
                sze1 = size( varargin{i} );
                sze2 = size( varargin{i} );
                if( isequal(sze1, sze2) );
                    for j = 1:numel( varargin{i} );
                        st = st_combine( st, isequalproperty_worker( strict, varargin{i}{j}, varargin{i+1}{j} ) ); end;
                else;
                    break; end; end;
            break; end;
        
        st = st_combine( st, isequalproperty_worker( strict, varargin{i}, varargin{i+1} ) );
            
            
        end;
        
    
    ret = st.class && st.size && st.sparse && st.complex;

end

function [ st ] = make_st();
    st = struct;
    st.class = true;
    st.complex = true;
    st.size = true; 
    st.sparse = true;
    
end

function [ st ] = st_combine( st, st2 );
    st.class = st.class && st2.class;
    st.complex = st.complex && st2.complex;
    st.size = st.size && st2.size;
    st.sparse = st.sparse && st2.sparse;
end

function [ st ] = isequalproperty_worker( strict, arg1, arg2 );

    st = make_st();
        
    % check class
    if( false );
    elseif( ~strict && ...
            isa( arg1, 'double' ) && isa( arg2, 'logical' ) && ...
            isequal( double(logical(arg1) ), arg1 ) ...
      );
        % do nothing
    elseif( ~strict && ...
            isa( arg1, 'logical' ) && isa( arg2, 'double' ) && ...
            isequal( double(logical(arg2)), arg2 ) ...
          );
        % do nothing
    elseif( ~isequal( class(arg1), class(arg2) ) );
        st.class = false; end;

    % check complex valued thingy
    try;
        if( xor( isreal(arg1), isreal(arg2) ) );
            st.complex = false; end;
    catch me; %#ok<NASGU>
        end;

    % check size
    try;
        if( ~isequal( size(arg1), size(arg2) ) );
            st.size = false; end;
    catch me; %#ok<NASGU>
        end;

    try;
        % check sparsity
        if( ~isequal( issparse(arg1), issparse(arg2) ) );
            st.sparse = false; end; 
    catch me; %#ok<NASGU>
        end; 
end

function dummy; end %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
