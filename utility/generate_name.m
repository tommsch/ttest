function name = generate_name( varargin );
% generates a name from varargin and the dbstack
    if( ismatlab );
        vv = ver( 'matlab' );  %#ok<VERMATLAB>
        verstr = ['matlab' vv.Release];
    else;
        vv = ver( 'octave' );
        verstr = ['octave' vv.Version]; end;
    idx = isstrprop( verstr, 'alphanum' );
    verstr = verstr(idx);

    if( ismatlab );
        hash_varargin = thash( varargin );
    else;
        hash_varargin = hash( 'MD5', TTEST_DISP( inf, varargin{:} ) ); end;
    hash_varargin = hash_varargin(1:8);

    name_varargin = TTEST_DISP( inf, varargin{:} );
    idx = isstrprop( name_varargin, 'alphanum' );
    name_varargin = name_varargin(idx);
    
    ds = dbstack;
    hash_command = '00000000';
    name_command = '';
    if( numel(ds) >= 3 );
        ds = dbstack( '-completenames' );
        fid = fopen( ds(3).file );
        if( fid >= 0 );
            closefile = onCleanup( @() fclose(fid) ); 
            for i = 1:ds(3).line - 1
                tmp_line = fgetl( fid ); end; 
            name_command = fgetl( fid );
            if( ischar(name_command) );
                hash_command = strtrim( name_command );
                hash_command(hash_command == ' ') = [];
                hash_command = thash( hash_command );
                hash_command = hash_command(1:8); end; end;
        idx = isstrprop( name_command, 'alphanum' );
        name_command = name_command(idx); end;

    while( true );
        name = ['ttest_' verstr '_' name_varargin '_' name_command '_' hash_varargin '_' hash_command];
        if( numel(name_varargin) >= 1 );
            name_varargin(end:end) = []; end;
        if( numel(name_command) >= 1 );
            name_command(end) = []; end;
        if( numel(name) < 100 || ...
            isempty(name_varargin) && isempty(name_command) ...
          );
            break; end; end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
