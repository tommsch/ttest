function [ ret ] = evalfirstreturnlast( varargin );
    assert( numel(varargin) >= 1, 'evalfirstreturnlast:nargin', 'Function needs at least one input argument' );
    ret = varargin{end};
    try;
        if( isstring(varargin{1}) || ischar(varargin{1}) );
            evalin( 'caller', [varargin{1} ';'] );
        else;
            varargin{1}(); end;
    catch me;
        if( isempty(me.identifier) );
            id = 'TTEST:evalat:throw'; 
        else;
            id = me.identifier; end;
        msg = ['A TTEST-evalat command threw an error.' newline 'Message:' newline me.message];
        warning( id, '\n%s', msg ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
