function ret = assign( name, value );
% Assigns to the variable name the given value
% assign( name, val );
% Input:
%   name     string, variable name
%   value    anything, value of variable
%
% Written by: tommsch, 2020

    if( nargout == 1 );
        err = lasterror();  %#ok<LERR>
        try;
            ret = evalin( 'caller', [name ';'] ); 
        catch me;  %#ok<NASGU>
            ret = []; end;
        lasterror( err );  %#ok<LERR>
        end;
    assignin( 'caller', name, value );
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
