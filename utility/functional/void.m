function void( varargin );
% takes everything, returns nothing
% can be used - in connection with the comma operator - to hide comments in commands, e.g.:
% void( "this is a comment" ), a = 2;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>
