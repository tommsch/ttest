function [ ret ] = returnfalse( varargin );
% takes everything, returns false
% [ ret ] = returnfalse( varargin );  %#ok<INUSL>
	ret = false; 
end

% function dummy; end  %#ok<DEFNU>  % no dummy function, because we use call all localfunctions here
%#ok<*NOSEL,*NOSEMI,*ALIGN>
