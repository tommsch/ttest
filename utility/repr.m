function [ varargout ] = repr( X, varargin );
% [ str ] = repr( X, [varname], [options] );
% Returns a printable repr-esentational string of the given object.
% In the optimal case, this string can be used to eval-uate it to the original object
%
% Input:
%   X           anything, the stuff to be repred
%               If X is a string, then the variable with name X in the caller workspace is repred, NOT the string.
%               If a variable name is provided as second argument, then the string will be repred
%               If X is not given, the `ans` variable is repred
%
%   varname     string, optional, the variable name. If not given, the name is determined from the input X.
%               If no name is given, nor one can be determined, a random name is generated
%               If '_' is used as variablename, no name is used
%
% Options:
%   '-check'        If given, evalutes the repred string and compares it with the input
%   '-silent'       NO warnings are printed
%   '-clipboard'    Copies repr string to clipboard
%
% Notes:
%   Symbolic input is experimental
%
% Output:
%   str         optional, string, repred input
%
% Eexample:
%      repr( [1 2 3] )
%      A = [1 2 3]; repr A 
%
% Original code by: David Holl, 2007-01, mathworks.com/matlabcentral/fileexchange/19953
% Modified by: tommsch, 2023-04


%               2023            tommsch,    Added check whether repred stuff evalues to original stuff
%               2024-03,        tommsch,    Added repring of symbolic stuff
%               2024-04-29,     tommsch,    Experimental behaviour change, string input means variable name now
%               2024-07,20,     tommsch,    repr without argumts repres the `ans` variable
%               2024-10-30,     tommsch,    new option '-clipboard'
% Changelog:    

    opt.checkflag = false;
    opt.silent = false;
    opt.clipboard = false;
    opt.print_no_input_warning = false;
    opt.print_string_input_warning = false;
    opt.varname_override = false;  % this option cannot be set by the user

    for i = numel( varargin ):-1:1;
        if( isempty(varargin{i}) );
            continue; end;
        switch( varargin{i} );
            case {'-silent'};
                varargin(i) = []; 
                opt.silent = true;
            case {'-check'};
                varargin(i) = [];
                opt.checkflag = true;
            case {'-clipboard'};
                varargin(i) = [];
                opt.clipboard = true;
            otherwise; 
                end; end; % do nothing

    if( nargin == 0 );
        X = evalin( 'caller', 'ans' );
        varname = [];
        opt.print_no_input_warning = true;

    elseif( numel(varargin) == 0 && ...
        (isstring(X) || ischar(X) ) ...
      );
        opt.print_string_input_warning = true;
        if( isvarname(X) );
            varname = X;
        else;
            varname = []; end;
        X = evalin( 'caller', X );
    else;
        varname = inputname( 1 );
        end;

    for i = numel( varargin ):-1:1;
        if( isempty(varargin{i}) );
            continue; end;
        if( ~opt.varname_override && ...
            (ischar( varargin{i} ) || isstring( varargin{i} )) ...
          );
            opt.varname_override = true;
            varname = varargin{i};
            varargin(i) = [];
        else;
            error( 'repr:input', 'The input is ambigious. There is a string input which can be either mean the variable name, or some option - but the option name was not understood.' ); end; end;

    if( ~opt.silent );
        if( opt.print_no_input_warning );
            warning( 'repr:input', 'No input is given. I take the ''ans'' variable from the caller workspace.' ); end;
        if( opt.print_string_input_warning );
            warning( 'repr:string', 'String input is given. I assume that the eval-ed input is meant to be repred, and not the string itself.\nAdd as second argument the variable name, to disable this behaviour.' ); end; end;

    if( numel( varargin ) == 1 && ...
        (isstring( varargin{1} ) || ischar( varargin{1}) ) ...
      );
        varname = varargin{1}; 
        varargin(1) = []; end;
    if( isempty(varname) );
        if( ~opt.silent );
            warning( 'repr:varname', 'Could not deduce variable name. I take ''X''.' ); end;
        varname = 'X'; end;
    if( isequal(varname, '_') );
        varname = ''; end;
    
    assert( isempty(varargin), 'repr:input', 'Wrong input given.' );
    if( opt.checkflag && isempty(varname) );
        if( ~opt.silent );
            fprintf( 2, 'No variable name can be determined from the input. I use ''x'' as default variable name.\n' ); end;
        varname = 'x'; end;

    if( isempty(varname) );
        [str, msg] = uneval( X );
    else;
        [str, msg] = uneval( varname, X ); end;
    if( ~opt.silent && ~isempty(msg) );
        fprintf( '%s', msg ); end;
    if( numel(str) >= numel(newline) && isequal(str(end-numel(newline)+1:end), newline) );
        str(end-numel(newline)+1:end) = []; end;

    if( opt.checkflag );
        if( do_check( varname, X, str ) )
            if( ~opt.silent );
                fprintf( 'The repred string does evaluate to the input.\n' );  end;
        else;
            warning( 'repr:check', 'The repred string does not evaluate to the input.' ); end; end;

    if( opt.clipboard );
        fprintf( 'Output copied to clipboard.\n' ); 
        clipboard( 'copy', str ); end;
    
    if( nargout == 0 );
        fprintf( '%s\n', str ); 
    else;
        varargout = cell( 1, 1 );
        varargout{1} = str; end;
end


function [ ret_32rt327we9fzg78342hgzwruigh798432g ] = do_check( varname_82190jq3ifjh8739th7w8gf9h743298gh7wq389, X_712859hnajsfk182395hqajklfza8901235, str_3217u5898fhewzua8f9hzq984bgz82q439g );
    eval( str_3217u5898fhewzua8f9hzq984bgz82q439g );
    ret_32rt327we9fzg78342hgzwruigh798432g = isequal( eval(varname_82190jq3ifjh8739th7w8gf9h743298gh7wq389), X_712859hnajsfk182395hqajklfza8901235 );
end


%% uneval

%uneval - Create eval'able string from a variable.
%
%string = uneval(value);
%string = uneval('varname', value);
%string = uneval('varname', value, false); % all lines concatenated
%strings = uneval('varname', value, true); % 1 line per string in cell entry
%
%	Convert a MATLAB variable into a text string, which when evaluated,
%	produces the original variable, and maintains machine precision for
%	floating point values.
%
%	This function handles many data types including structures of arrays of
%	structurs but errors on unknown data types such as classes.
%
% Contrived usage:
%	settings = load('some_data.mat');
%	string = uneval('settings2', settings)
%	eval(string);
%	disp(isequalwithequalnans(settings, settings2));
%
% WARNING --- This probably won't work with handles to nested functions.
% Jan 2007 - D. Holl - Initial coding.
%	I needed this utility for various odds & ends.

function [ the_string, msg ] = uneval( var_name, value, return_cell_array );
    switch nargin;
        case 1;
            value = var_name;
            var_name = '';
            return_cell_array = false;
        case 2
            return_cell_array = false;
        otherwise;
            fatal_error; end;
    [the_string, msg] = value2strings( var_name, value );
    if( ~return_cell_array );
        the_string = sprintf( '%s\n', the_string{:} ); % Concatenate the strings with linefeeds after each:

    else;
        % If the caller wanted cell array output, make sure it is a
        % column vector instead of a row.  (This is the standard for
        % MATLAB's multiline edit-text uicontrols.)
        the_string = the_string(:); end;
    return;
end

function [ c ] = badchars();
    persistent c_;
    if( isempty(c_) );
        c_ = escape_sprintf(); end;
    c = c_;
end

function [ c ] = reallybadchars();
    persistent c_;
    if( isempty(c_) );
        c_ = setdiff( char([0:31 127:255]), badchars ); end;
    c = c_;
end

function [ l ] = limit_uint64();
    persistent l_
    if( isempty(l_) );
        l_ = get_limit_uint64(); end;
    l = l_;
end

function [strings, msg] = value2strings( var_name, value );
    msg = '';
    strings = {};
    switch class( value );
        case {'int8', 'uint8', 'int16', 'uint16', 'int32', 'uint32', 'int64', 'uint64','single','double'}
            if( isa(value, 'double') );
                type_str1 = '';
                mat2str_args = {17}; % I ask for 17 digits to preserve doubles, and we don't need 'class' because MATLAB defaults to double.
            elseif( isa(value, 'single') );
                type_str1 = ', ''single''';
                mat2str_args = {8, 'class'}; % I ask for 8 digits to preserve singles
            elseif( isa(value, 'int64') || isa(value, 'uint64') );
                type_str1 = [', ''' class(value) ''''];
                mat2str_args = {16, 'class'};
                % I ask for 16 digits because the default 15 doesn't
                % preserve 64-bit ints that are <= limit_uint64.
            else;
                type_str1 = [', ''' class(value) ''''];
                mat2str_args = {'class'}; end;
            % mat2str also handles complex!
            if( numel(value)>4 && all(arrayfun(@(x)isequaln(value(1), x), full(value(:)))) );  % make sure all elements are the same
                switch value(1);
                    case 1;
                        strings{end+1} = ['ones(' mat2str(size(value)) type_str1 ')'];
                    case -1;
                        strings{end+1} = ['-ones(' mat2str(size(value)) type_str1 ')'];
                    case 0;
                        strings{end+1} = ['zeros(' mat2str(size(value)) type_str1 ')'];
                    case inf;
                        strings{end+1} = ['inf(' mat2str(size(value)) type_str1 ')'];
                    case -inf;
                        strings{end+1} = ['-inf(' mat2str(size(value)) type_str1 ')'];
                    otherwise;
                        if isnan(value(1)) % Because "case nan" doesn't work.  (nan==nan is false)
                            strings{end+1} = ['nan(' mat2str(size(value)) type_str1 ')'];
                        else
                            if isa(value, 'int64') || (isa(value, 'uint64') && any(value(:)>limit_uint64))
                                % This output will be ugly.
                                % TODO: How can we make this less ugly?
                                %
                                % mat2str appears to internally convert to
                                % double precision, which drops the lower
                                % bits in 64-bit integers.  Convert just
                                % the first value, but as uint32:
                                mat2str_output = mat2str(typecast(value(1), 'uint32'), 'class');
                                % Then wrap mat2str_output with a typecast to get back to int64 or uint64:
                                mat2str_output = sprintf('typecast(%s, ''%s'')', mat2str_output, class(value));
                            else
                                % Convert just the first value:
                                mat2str_output = mat2str(value(1),mat2str_args{:});
                            end
                            % Now wrap mat2str_output with repmat to get back to the original size:
                            strings{end+1} = ['repmat(' mat2str_output ', ' mat2str(size(value)) ')']; %don't need 'class' because MATLAB defaults to double
                        end
                end
            else
                if( isa(value, 'int64') || ...
                    (isa(value, 'uint64') && any(value(:)>limit_uint64)) ...
                  );
                    % This output will be ugly.
                    % TODO: How can we make this less ugly?
                    %
                    % mat2str appears to internally convert to double
                    % precision, which drops the lower bits in 64-bit
                    % integers.  So, we'll typecast to uint32.  However,
                    % typecast appears to only preserve dimenstions for row
                    % or column vectors.
                    %size_value_str = mat2str(size(value));
                    if( ndims( value ) <= 2 && ( size(value, 1 ) == 1 || ...
                        size( value, 2 ) == 1 ) ...
                      );
                        % This is a row or column vector, so we can trust typecast:
                        mat2str_output = mat2str( typecast(value, 'uint32'), 'class' );
                        mat2str_output = sprintf( 'typecast(%s, ''%s'')', mat2str_output, class(value));
                        strings{end+1} = mat2str_output;
                    else;
                        % Handle general case here:
                        mat2str_output = mat2str(typecast(value(:).', 'uint32'), 'class');
                        mat2str_output = sprintf('typecast(%s, ''%s'')', mat2str_output, class(value));
                        strings{end+1} = ['reshape(' mat2str_output ', ' mat2str(size(value)) ')'];
                    end
                elseif( ndims(value) > 2 );  %#ok<ISMAT>
                    % mat2str cannot handle ndims>2, so we flatten
                    % value(:,:), and then output with reshape:
                    strings{end+1} = ['reshape(' mat2str(value(:,:),mat2str_args{:}) ', ' mat2str(size(value)) ')'];
                else
                    strings{end+1} = mat2str( value, mat2str_args{:} ); end;
            end
            if ~isempty(var_name), strings{end} = sprintf('%s=%s;', var_name, strings{end}); end;

        case 'char';
            value_size = size(value);
            % Hack for char matrices: make 'em look like rows:
            value = value(:).';
            if( any(ismember(reallybadchars, value)) );
                % If they're really bad, convert them all to numbers:
                strings{end+1} = ['char(' mat2str(double(value)) ')'];
            elseif( any(ismember(badchars, value)) );
                % If they're only a little bad, wrap them with sprintf:
                strings{end+1} = ['sprintf(''' escape_sprintf(value) ''')'];
            else;
                % The string must be safe:
                strings{end+1} = ['''' value '''']; end;
            % Now we preserve the original dimensions somehow:
            if numel(value_size)==2 && value_size(1)~=1 && value_size(2)==1
                % This is a column vector, so add a .' to the end:
                strings{end} = [strings{end} '.'''];
            elseif numel(value_size)>2 || value_size(1)~=1
                % This is either 2-D or higher dimensional:
                strings{end} = ['reshape(' strings{end} ', ' mat2str(value_size) ')'];
            end
            if ~isempty(var_name);
                strings{end} = sprintf('%s=%s;', var_name, strings{end}); end;

        case 'logical';
            % I could group logicals with the numeric types, but that would
            % produce: [true false true true ...] instead of [1 0 1 1 ...].
            if( numel(value) > 4 && all(value(1) == value(:)) );  % make sure all elements are the same
                if value(1)
                    strings{end+1} = ['true(' mat2str(size(value)) ')'];
                else
                    strings{end+1} = ['false(' mat2str(size(value)) ')']; end;
            else;
                if( ndims(value) > 2 );  %#ok<ISMAT>
                    % mat2str cannot handle ndims>2, so we flatten
                    % value(:,:), and then output with reshape:
                    strings{end+1} = ['reshape(logical(' mat2str( double(value(:,:)) ) '), ' mat2str( size(value) ) ')'];
                else;
                    strings{end+1} = ['logical(' mat2str( double(value) ) ')'];
                end; end;
            if( ~isempty(var_name) );
                strings{end} = sprintf('%s=%s;', var_name, strings{end}); end;

        case 'function_handle';
            msg = sprintf( 'Repring of function handles is experimental,\n  and most likely will not succeed.\n  The repred string may be even not evalable.\n' );
            strings{end+1} = func2str( value );
            if( ~isequal(strings{end}(1), '@') );
                strings{end} = ['@' strings{end}]; end;
            if( ~isempty(var_name) );
                strings{end} = sprintf( '%s=%s;', var_name, strings{end} ); end;

        case 'struct';
            value_size = size(value);
            if prod(value_size)==1
                strings{end+1} = 'struct';
            else
                strings{end+1} = ['repmat(struct, ' mat2str(value_size) ')'];
            end
            if( ~isempty(var_name) );
                strings{end} = sprintf( '%s=%s;', var_name, strings{end} ); end;
            fields = fieldnames(value);
            for ndx = 1:numel( value )
                if( numel(value) == 1 );
                    subscripts = '';
                elseif numel(value_size)==2 && any(value_size==1)
                    subscripts = sprintf('(%i)', ndx);
                else
                    subscripts = cell_ind2sub(value_size, ndx);
                    subscripts = sprintf('(%i%s)', subscripts(1), sprintf(',%i',subscripts(2:end)));
                end
                for fndx=1:numel(fields);
                    [sub_strings, msg_sub] = value2strings(sprintf('%s%s.%s', var_name, subscripts, fields{fndx}), value(ndx).(fields{fndx}));
                    msg = [msg msg_sub];  %#ok<AGROW>
                    strings = [strings(:)', sub_strings(:)']; end; end;  % original code: {strings{:}, sub_strings{:}}

        case 'cell'
            value_size = size(value);
            strings{end+1} = ['cell(' mat2str(value_size) ')'];
            if( ~isempty(var_name) );
                strings{end} = sprintf('%s=%s;', var_name, strings{end}); end;
            for ndx = 1:numel(value)
                if( numel(value_size)==2 && any(value_size == 1) );
                    subscripts = sprintf(['{%' num2str(ceil(log10(numel(value)))) 'i}'], ndx);
                else;
                    subscripts = cell_ind2sub(value_size, ndx);
                    subscripts = sprintf('{%i%s}', subscripts(1), sprintf(',%i',subscripts(2:end))); end;
                [sub_strings, msg_sub] = value2strings(sprintf('%s%s', var_name, subscripts), value{ndx});
                msg = [msg msg_sub];  %#ok<AGROW>
                strings = [strings(:)', sub_strings(:)']; end;  % {strings{:}, sub_strings{:}}
            
        case 'sym';
            %persistent sym_repr_warning_printed;  %#ok<TLEV>
            %if( isempty(sym_repr_warning_printed) || ...
            %    toc(sym_repr_warning_printed) > 60*60 ...
            %  );
            %    sym_repr_warning_printed = tic;
            %    warning( 'repr:sym', 'Repring of symbolic expression is experimental. This warning is printed only once per hour.' ); end;
            if( isoctave );
                strings{end+1} = ['sym(''' char(value) ''')'];
            else;
                strings{end+1} = ['str2sym(''' char(value) ''')']; end;
            if( ~isempty(var_name) );
                strings{end} = sprintf( '%s=%s;', var_name, strings{end} ); end

        otherwise;
            error( 'uneval:unknown_class', 'This type is not implemented to be repred. Type: %s ', class(value) ); end;
end

%escape_sprintf - Escape a string to be used for sprintf formatting.
%
%string = escape_sprintf(string);
%badchars = escape_sprintf;
%	Replace any "bad" characters in a string with sprintf escapes.
%	If called with no input, return the list of characters that we'll replace.
%
% 1/2007 - D. Holl
function [ string ] = escape_sprintf( string );
    % \\ must come BEFORE the other \ codes.
    char_list = {'''', '%%', '\\', '\b', '\f', '\n', '\r', '\t'};
    if( nargin == 0 );
        string = sprintf( [char_list{:}] );
    else;
        for ndx = 1:numel( char_list );
            char = sprintf( char_list{ndx} );
            string = strrep( string, char, char_list{ndx} ); end; end;
end

%CELL_IND2SUB Multiple subscripts from linear index.
%   CELL_IND2SUB is used to determine the equivalent subscript values
%   corresponding to a given single index into an array.  This function
%   is identical to MATLAB's IND2SUB except that it returns all subscripts
%   as a single array.
%
% 3/2007 - D. Holl
function [ subscripts ] = cell_ind2sub( siz, ndx );
    siz = double( siz );
    subscripts = nan( size(siz) );
    k = [1 cumprod(siz(1:end-1))];
    for i = numel(siz):-1:1,
        vi = rem(ndx-1, k(i)) + 1;
        vj = (ndx - vi)/k(i) + 1;
        subscripts(i) = vj;
        ndx = vi;
    end
end

function [ x ] = get_limit_uint64()
    % This returns the largest uint64 that can still be safely cast to a
    % double precision float.  This will return 0x001F FFFF FFFF FFFF, which
    % is 2^53-1.
    x = uint64(1);
    while( uint64(double(x)*2+1) == x*2+1 );
        x = x * 2 + 1; end;
    % Or should we consider using x = flintmax('double'); ?
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% License for parts of source code written by David Holl
% Copyright (c) 2015, David Holl
%
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of Subspace Dynamics, LLC nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NOCOL>
 