function [ h ] = getmatlabfunction( name, pattern )
% Returns a handle to the original function in case there exists an overload
% [ h ] = getmatlabfunction( name, [pattern] )
%
% On Matlab:
% returns a handle to the original matlab function, even when it is overloaded
% if there are more candidates, a function is chosen in whose path the pattern is contained
% pattern defaults to ''
%
% On Octave:
% This function may only able to return handles to some functions, in particular:
%       addpath, dbclear, dbstop, path, restoredefaultpath, rmpath
%
% Written by: 2021, tommsch

    if( nargin <= 1 );
        pattern = ''; end;
    
    if( exist( 'OCTAVE_VERSION', 'builtin' ) )
        h = getbuiltinfunction_octave( name, pattern );
    else;
        h = getbuiltinfunction_matlab( name, pattern ); end;

end

function [ h ] = getbuiltinfunction_matlab( name, pattern )

    n = which( name, '-all' );
    m = matlabroot;
    for i = numel( n ):-1:1;
        if( ~isempty(pattern) && ~any(strfind(n{i}, pattern)) );
            n(i) = [];
        elseif( any(strfind(n{i}, 'built-in')) );
            h = @(varargin) builtin( name, varargin{:} );
            return;
        elseif( any(n{i} == '@') );
            n(i) = [];
        elseif( any(strfind(n{i}, m)) );
            continue;
        else;
            n(i) = []; end; end;

    length = cellfun( 'length', n );
    [~, idx] = min( length );
    folder = fileparts( n{idx} );
    olddir = cd( folder );
    cleandir = onCleanup( @() cd(olddir) );
    h = eval( ['@' name] );

end


function [ h ] = getbuiltinfunction_octave( name, pattern )
    assert( isempty(pattern), 'TTEST:getmatlabfunction', 'On Octave no ''pattern'' can be passed.' );
    switch name;
        case {'addpath', 'dbclear', 'dbstop', 'path', 'restoredefaultpath', 'rmpath'};
            h = @(varargin) builtin( name, varargin{:} );
        otherwise;
            error( 'TTEST:getmatlabfunction', 'Unsupported function name passed. This restriction applies to Octave only.' ); end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCMLR,*MCCD>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
