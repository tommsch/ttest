function [ str ] = me2str( me, verbose, filterwords );
    if( nargin <= 0 || isempty(me) );
        me = lasterror; end;  %#ok<LERR>

    if( nargin <= 1 || isempty(verbose) );
        verbose = 1; end;
    
    if( nargin <= 2 || isempty(filterwords) );
        filterwords = {}; end;
    
    if( ~iscell(filterwords) );
        filterwords = {filterwords}; end;
    
    assert( isa(me, 'struct') || isa(me, 'MException'), 'TTEST:input', 'Input argument must be an exception.' );
    me = tostruct( me );
   
    idx = cellfun( 'isclass', filterwords, 'double' );
    filteridx = cell2mat( filterwords(idx) );
    filteridx(filteridx < 0) = mod( filteridx(filteridx < 0) + 1, numel(me.stack) );
    filteridx(filteridx == 0) = numel( me.stack );
    filterwords(idx) = [];
    
    if( verbose <= 1 && ...
        contains( me.identifier, 'TTEST' ) && contains( me.identifier, 'assume' ) ...
      );
        str = '';
    else;
        msg = strrep( me.message, newline, [newline '              '] );
        str = ['  Error id:   ' me.identifier newline '  Error msg:  ' msg newline];
        end;
    

    if( isfield(me,'Correction') && ~isempty(me.Correction) );
        Corr = strrep( me.Correction, newline, [newline '              '] );
        str = [str '  Correction: ' Corr newline]; end;

    if( isfield(me, 'cause') && numel(me.cause) >= 0 );
        me.cause = tocell( me.cause );
        for i = 1:numel(me.cause);
            str = [str 'Cause:' newline me2str( me.cause{i} ) ]; end; end; %#ok<AGROW>
        
    % if( isfield(me, 'remotecause') && numel(me.remotecause) >= 0 );
    %     me.remotecause = tocell( me.remotecause );
    %     for i = 1:numel(me.remotecause);
    %         str = [str 'Remotecause:' newline me2str( me.remotecause{i} ) ]; end; end; %#ok<AGROW>
        
    
    % print stack
    if( verbose >= 1 );
        max_num_entry = inf;
    else;
        max_num_entry = 1 + 2*verbose;
        end;
    
    can_add_replacement = true;
    
    i = 0;
    print_depth = 0;
    if( verbose >= 1 );
        replacementstring = [newline '  ...' newline];
    else;
        replacementstring = ''; end;
    while( true );
        do_replace = false;  %#ok<NASGU>
        i = i + 1;    
        if( print_depth > max_num_entry || i > numel( me.stack ) );
            break; end;
        if( isoctave );
            [~, file, ~] = fileparts( me.stack(i).file );
            file = [file '.m'];  %#ok<AGROW>
        else;
            file = me.stack(i).file; end;
        if( verbose <= 1 && ( ...
                 contains( lower(file), {'expect_','assert_','todo_','assume_','TTEST_'} ) || ...
                 contains( file, {'runttests.m','GIVEN.m','run.m','CACHE.m'} ) || ...
                 contains( file, filterwords ) || ...
                 contains( file, matlabroot ) || ...
                 contains( me.stack(i).name,  {'TTEST_'} ) ...
                            ) ...
            );
            do_replace = true;
            replacement = replacementstring;
        elseif( contains( file, filterwords ) || ...
                any( i == filteridx ) ...
              );
            do_replace = true;
            replacement = replacementstring;
        else
            do_replace = false;
            replacement = '';
            end;
        
        if( do_replace && can_add_replacement && i > 1 );
            can_add_replacement = false;
            str = [str replacement];  %#ok<AGROW>
        elseif( do_replace && ~can_add_replacement || ...
                do_replace && i == 1 ...
              );
            % do nothing
        else;
            print_depth = print_depth + 1;
            can_add_replacement = true;
            fullname = which( file );
            funcname = me.stack(i).name;
            intxt = sprintf( '%s:%i', file, me.stack(i).line );
            if( ismatlab() && numel(file) >= 2 && ~strcmp(file(end-1:end), '.p') );
                str = [str sprintf( '\n  (%i) In: %s\n      <a href="matlab: opentoline(''%s'',%i,0)"  style="font-weight:bold">%s</a>\n', i, funcname, fullname, me.stack(i).line, intxt )];  %#ok<AGROW>
            else; 
                str = [str sprintf( '\n  (%i) In: %s\n      %s\n', i, funcname, intxt )];  %#ok<AGROW>
                end; end; end;
      
end

function [ s ] = tostruct( s );
    [w_msg, w_id] = lastwarn();
    evalc( 's = struct( s );' );
    lastwarn( w_msg, w_id );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.  
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCMLR>
