function [ out, classname ] = existid2existstr( in );
    classname = '';
    if( isnumeric(in) );
        switch( in );
            case -1;            out = 'any';
            case  0;            out = 'does not exist';
            case  1;            out = 'var';
            case  {2,3,4,6};    out = 'file';
            case  5;            out = 'builtin';
            case  7;            out = 'dir';
            case  8;            out = 'class';
            otherwise; error( 'TTEST:EXIST', 'Wrong number for type given.' ); end;
    elseif( isstring(in) || ischar(in) );
        switch( in );
            case {'any', 'anything'};                                   out =  -1;
            case {'void','no','not','does not exist'};                  out =   0;
            case {'var','variable'};                                    out =   1;
            case {'file','mfile','mlx','m','mlapp','mat','fig','txt'};  out =   2;
            case {'mex','mexfile','oct','octfile'};                     out =   3;
            case {'simulink','model'};                                  out =   4;
            case {'builtin'};                                           out =   5;
            case {'pfile','pcode'};                                     out =   6;
            case {'dir','folder','directory'};                          out =   7;
            case {'class','java','javaclass'};                          out =   8;
            case {'filelessfunction'};                                  out = 103;  % octave specific
            otherwise; 
                out = 8;
                classname = in;
                end; end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
