function [ varargout ] = sysinfo( verbose );
% prints out informations about the Matlab/Octave installation and the hardware
% [ str ] = sysinfo( [verbose] )
%
% Input
%   verbose     default = 1, verbosity level,
%
% Output
%   str         if an output argument is provided, then no screen output is printed, but returned in `str`


%            tommsch, 2023-05-02, Bugfix
% Changelog: 

    if( nargin <= 0 );
        verbose = 1; end;
    try;
        mo = matlab_octave_info( verbose );
    catch me;
        if( ismatlab );
            str = 'Matlab';
        else;
            str = 'Octave'; end;
        mo = sprintf( 'Could not retrieve %s info\n  Error: %s\n', str, me2str(me) ); end;

    try;
        os = osinfo();
    catch me;
        os = sprintf( 'Could not retrieve OS info\n  Error: %s\n', me2str(me) ); end;
    
    try;
        cpu = cpuinfo();
    catch me;
        cpu = sprintf( 'Could not retrieve CPU info\n  Error: %s\n', me2str(me) ); end;
        
    nfo = [mo newline ...
           os newline ...
           cpu newline ...
           
           ];
       
    if( nargout==0 );
        disp( nfo );
    else;
        varargout{1} = nfo; end;
    
end

function [ cpu ] = cpuinfo();

    if( ismatlab );
        cpu = tt.cpuinfo_mw();  %#ok<NASGU>
        cpu = evalc( 'disp( cpu );' );
        cpu = ['System Info:' newline cpu];
    else;
        try;
            if( ispc )      % Windows
                command = 'for /f "tokens=2 delims==" %A in (''wmic cpu get name /value'') do @(echo %A)';
            elseif( ismac ) % Mac
                command = 'sysctl machdep.cpu | grep brand_string | cut -d: -f2';
            else            % Linux
                command = 'grep -m 1 "model name" /proc/cpuinfo | cut -d: -f2'; end;
            [status, cpu] = system( command );
            cpu = strtrim( cpu );
        catch;
            status = true; end;

        if( status )
            cpu = 'could not retrieve information'; end; end;
end

function [ os ] = osinfo();
    os = sprintf( 'System architecture: %s\n', computer('arch') );
end

function [ m ] = matlab_octave_info( verbose );
    if( ismatlab );
        str = 'Matlab';
    else;
        str = 'Octave'; end;
    
    if( verbose<=1 );
        if( ismatlab );
            m1 = evalc( 'ver( ''matlab'' )' );
        else;
            m1 = evalc( 'ver( ''octave'' )' ); end;
    else;
        m1 = evalc( 'ver' ); end;

    m2 = sprintf( '%s version: %s\n', str, version );
    m = [m1 newline m2];
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
