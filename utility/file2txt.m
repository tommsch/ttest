function [ varargout ] = file2txt( name, var, newlineflag );
% file2txt reads a text file and produces a text which matlab parses as the text file.
%
% [ ret ] = file2txt( in, [out], [newlineflag] )
%
% Input:
%   in              string, filename or functionname whose content shall be converted to a matlab evalable string.
%                       If the file cannot be found, `which(name)` is used to locate it
%   var             string, optional, default = [], if given, then 'var = ' is prepended to the output
%   newlineflag     bool, optional, default = false, if set, then newline characters are pre and appeneded to the first and last line of the output
%
% Output:
%   ret             character array
%
%
% Example:
%   file2txt( 'pinv' )
%   file2txt( 'file2txt.m', 'varname', true )
%
% Written by: 2020-10-07, tommsch

%            2024-03-20, tommsch, Behaviour change. Prints to screen if nargout == 0. Printout handles html tags correctly (i.e. they will most likely not get rendered)
% Changelog: 

 %#ok<*AGROW>

    if( nargin == 0 && ismatlab );
        name = tt.scl(); end;

    if( nargin <= 2 || isempty(newlineflag) );
        newlineflag = false; end;

    if( newlineflag );
        ret = newline;
    else;
        ret = ''; end;

    if( nargin<=1 || isempty(var) );
        ret = [ret '[' newline];
    else;
        ret = [ret var ' = [' newline]; end;

    [fid, cleanfid] = TTEST_OPEN_FILE_FILE2TXT( name, 'r' );  %#ok<ASGLU>

    while( ~feof(fid) );
        line = fgetl( fid );
        %assert( ~isequal(line, -1), 'file2txt:fgetl', 'Something went wrong in reading the file.' );
        line = regexprep( line, '''', '''''' );  % Replace the ' with ''
        line = regexprep( line, '\t', '\\t');
        line = regexprep( line, '\r', '\\r');
        if( isempty(line) );
            ret = [ret 'newline ...' newline];
        else;
            ret = [ret '''' line ''' newline ...' newline]; end; end;
    ret = [ret  '];'];

    if( newlineflag );
        ret = [ret newline]; end;


    if( nargout == 0 );
        if( ismatlab );
            % This complicated stuff is needed to correctly print out html stuff, which would otherwise be rendered as html stuff
            fprintf( ' ' );
            for i = 1:numel( ret );
                fprintf( '\b%s ', ret(i) ); end;
            fprintf( '\b' );
        else;
            disp( ret ); end;
    elseif( nargout == 1 );
        varargout{1} = ret;
    else;
        error( 'file2txt:nargout', 'Wrong number of output arguments.' ); end;
end

function [ fid, cleanup ] = TTEST_OPEN_FILE_FILE2TXT( name, permission  );

    if( nargin <= 1 || isempty(permission) );
        permission = 'r'; end;
    
    name_orig = name;
    fid = fopen( name, permission  );  % File to be written
    if( fid==-1 );
        name = which( name );
        idx = strfind( name, '%' );
        if( ~isempty(idx) );
            name(idx(1):end) = []; end;
        fid = fopen( which(name, 'all'), permission ); end;
    
    assert( fid >= 3, 'open_file:cannotopenfile', 'Cannot open file %s', name_orig );
    cleanup = onCleanup( @() fclose( fid ) );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.


