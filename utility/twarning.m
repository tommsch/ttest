function [ ret ] = twarning( varargin );
% Throws a warning, and returns ones.
    if( numel(varargin) == 0 );
        varargin{1} = 'twarning:noid';
        varargin{2} = 'twarning:noid';
    elseif( numel(varargin) == 1 && contains(varargin{1}, ':') );
        varargin{2} = varargin{1}; end;
    warning( varargin{:} );
    ret = 1;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
