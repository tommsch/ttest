function [ t, numargin, numargout ] = filetype( filename, in, out )
% written by: Loren Shure
% https://blogs.mathworks.com/loren/2013/08/26/what-kind-of-matlab-file-is-this/
%
% See also: [Function definition meta-info](https://undocumentedmatlab.com/articles/function-definition-meta-info)
% Takes a filename and returns 'script', 'function' or 'class'

    persistent isoctave;
    if( isempty(isoctave) );
        isoctave = exist( 'OCTAVE_VERSION', 'builtin' ) ~= 0; end;  % here to remove dependency
    
    numargin = nan;
    if( nargin <= 1 );  % one of nargin/nargout must be called in order to disambuigate scripts from functions
        in = 1; end;
    if( nargout <= 1 );
        in = 0; end;

    try;
        [dir, fn, ~] = fileparts( filename );
        if( ~isempty(dir) && ~strcmp(pwd,dir) );
            olddir = cd( dir );
            dirclean = onCleanup( @() cd(olddir) ); end;
        if( isoctave );
          try;
              err = lasterror();  %#ok<LERR>
              if( in );
                  numargin = nargin( fn ); end;
              numargout = nargout( fn );
          catch me;
              if( any(strfind( me.message, 'script' )) );
                  lasterror( err );  %#ok<LERR>
                  numargin = 0;
                  numargout = 0;
                  t = 'script';
                  return;
              else;
                  error( 'TTEST:filetype', 'This type of file is not yet identifiable by this function. It must be added!' ); end; end;
        else;
            err = lasterror();  %#ok<LERR>
            if( in );
                numargin = nargin( filename ); end;
            numargout = nargout( filename ); end;
        if( isoctave );
            t = 'function';
        else;
          val = exist( filename, 'class' );
          if( isequal(val,8) );
              t = 'class';
          else;
              t = 'function'; end; end;
    catch me;
        assert( exist('err', 'var') == 1, 'filetype:input', 'Bad input given or some fatal error happend.' );
        if( strcmp(me.identifier, 'MATLAB:nargin:isScript') );
            numargin = 0;
            numargout = 0;
            t = 'script';
            lasterror( err );  %#ok<LERR>
        else;
            rethrow( me ); end; end;
end

function TTEST_dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>
