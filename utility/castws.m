function [ nerr, firstws_, lastws_ ] = castws( varargin );
% (experimental) casts all variables in the workspace to the given type
% [ nerr, firstws, lastws ] = castws( 'like',p, [options] )
% [ nerr, firswst, lastws ] = castws( str, [options] )
%
% Input:
%   p       variable, target type
%   str     string, name of target type
%
% Options:
%   'first'             loads the first saved workspace (i.e. the workspace before at the moment of the first call of castws)
%   'last'              loads the last saved workspace (i.e. the workspace before the last call of castws)
%   'clear'             clears the first workspace
%   'dry'               just tries to cast the variables, but does not store the result
%   'verbose',val       integer, default=1, verbose level
%   'exclude',val       string or cell array of strings, default = 'ttest_', defines pattern to exclude variables
%   'include',val       string or cell array of strings, default = '', defines pattern to include variables
%   'overwrite',val     default = true, if false, casted variables get the name `originalname_cast`.
%
% Output:
%   nerr                integer, number of variables which could not totally casted. Is zero if everything worked out.
%   firstws             struct, the saved 'first' workspace
%   lastws              struct, the saved 'last' workspace
%   
% Notes:
%   - By default, variables whose name start with 'ttest_' are not casted
%     The variable ans is never casted on Octave
%   - Function works also in static workspaces
%   - If some variables could not be casted, a message is printed after the cast
%   - Variables must be castable using cast( variable, classname ) OR classname(variable)
%   - Variables in caller workspace are overwritten using assignin.
%
% E.g.: st.c = 4; castws( 'single' );
%
% Written by: tommsch, 2021-03-02

%            tommsch, 2021-03-17, Added options 'first','last','clear'
%            tommsch, 2021-08-23, Added option 'dryrun'
%            tommsch, 2021-10-21, Added option 'include', added alias 'exclude'
% Changelog: 

    persistent firstws;
    persistent lastws;
    
    [opt.verbose, varargin]    = tt.parsem( {'verbose','v'}, varargin, 1 );
    [opt.includevar, varargin] = tt.parsem( {'include'}, varargin, [] );
    [opt.excludevar, varargin] = tt.parsem( {'skip','exclude'}, varargin, {'ttest_'} );
    [opt.clearflag, varargin]  = tt.parsem( 'clear', varargin );
    [opt.firstflag, varargin]  = tt.parsem( 'first', varargin );
    [opt.lastflag, varargin]   = tt.parsem( 'last', varargin );
    [opt.dryrun, varargin]     = tt.parsem( {'dry','dryrun'}, varargin );
    [opt.overwrite, varargin]  = tt.parsem( 'overwrite', varargin, true );
    if( ~iscell(opt.includevar) );
        opt.includevar = {opt.includevar}; end;    
    if( ~iscell(opt.excludevar) );
        opt.excludevar = {opt.excludevar}; end;

    if( opt.clearflag );
        firstws = [];
        
    elseif( opt.firstflag );
        fn = fieldnames( firstws );
        if( isempty(fn) );
            if( opt.verbose >= 0 );
                warning( 'castws:empty', 'First workspace is not set yet. Cannot be loaded.' ); end;
            return; end;
        for i = 1:numel( fn );
            if( ~opt.dryrun );
                assignin( 'caller', fn{i}, firstws.(fn{i}) ); end; end;
        
    elseif( opt.lastflag );
        fn = fieldnames( lastws );
        if( isempty(fn) );
            if( opt.verbose >= 0 );
                warning( 'castws:empty', 'Last workspace is not set yet. Cannot be loaded.' );  end;
            return; end;
        for i = 1:numel( fn );
            if( ~opt.dryrun );
                assignin( 'caller', fn{i}, lastws.(fn{i}) ); end; end;
        
    else;
        if( isempty(lastws) );
            lastws = struct; end;
        ws = evalin( 'caller', 'whos();' );

        if( numel(varargin)==2 && strcmp(varargin{1},'like') );
            type = class( varargin{2} );
        elseif( numel(varargin)==1 && (isstring(varargin{1})||ischar(varargin{1})) )
            type = varargin{1}; 
        else;
            error( 'castws:input', 'Wrong input given' ); end;

        failedvar = {};
        for i = 1:numel( ws );
            if( ~isempty(opt.includevar) ); 
                if( ~contains_t(ws(i).name, opt.includevar) );
                    continue; end; end;
            if( contains_t(ws(i).name, opt.excludevar) || ...
                    isoctave && strcmp(ws(i).name,'ans') );
                continue; end;
            
            var = evalin( 'caller', ws(i).name );
            lastws.(ws(i).name) = var;
            [var,successflag] = cast_preworker( var, type, opt );
            if( successflag );
                if( ~opt.dryrun );
                    newname = ws(i).name;
                    if( numel(newname)>=58 );
                        newname = ws(1:58); end;
                    if( ~opt.overwrite );
                        newname = [newname '_cast']; end;  %#ok<AGROW>
                    assignin( 'caller', newname, var ); end;
            else;
                failedvar{end+1} = ws(i).name; end; end;  %#ok<AGROW>

        nerr = numel( failedvar );
        if( opt.verbose >= 1 && nerr > 0 );
            fprintf( 'Failed to cast the following variables:' );
            disp( failedvar ); end; 
        
        if( isempty(firstws) );
            firstws = lastws; end; end;
    
    firstws_ = firstws;
    lastws_ = lastws;    

end

function [ var, successflag ] = cast_preworker( var, type, opt );
    if( isstruct(var) );
        [var,successflag] = cast_struct( var, type, opt );
    elseif( iscell(var) );
        [var,successflag] = cast_cell( var, type, opt );
    elseif( isnumeric(var) || islogical(var) || ischar(var) || isa(var, 'sym') );
        [var,successflag] = cast_numeric( var, type, opt );
    elseif( ismatlab && istable(var) );
        [var,successflag] = cast_table( var, type, opt );
    else
        successflag = false; end;
end

function [ var, successflag ] = cast_struct( var, type, opt );
    successflag = true;
    fn = fieldnames( var );
    for k = 1:numel( fn )
        [var.(fn{k}),fl] = cast_preworker( var.(fn{k}), type, opt );
        successflag = successflag && fl; end;
end

function [ var, successflag ] = cast_cell( var, type, opt );
    successflag = true;
    % fprintf( '%i ', numel(var) );
    for k = 1:numel( var );
        [var{k},fl] = cast_preworker( var{k}, type, opt );
        successflag = successflag && fl; end;
end

function [ var, successflag ] = cast_table( var, type, opt );  %#ok<INUSD>
    successflag = true;
    tn = var.Properties.VariableNames;
    for k = 1:numel( var )
        try;
            var = convertvars( var, tn{k}, type );
        catch;
            successflag = false; end; end;
end


function [ var, successflag ] = cast_numeric( var, type, opt );
    if( opt.verbose < 0 );
        err = lasterror(); end;  %#ok<LERR>
    successflag = true;
    try;
        var = cast( var, type );        
    catch me1;
        % XX check me
        try;
            var = eval( [type '(var);'] );
        catch me2;
            try;
                var1 = double( var );
                cast( var1, type );
                if( opt.verbose >= 0 );
                    warning( 'castws:doublecast', 'Direct cast failed. Cast was done via intermediate cast over ''double''.' ); end;
            catch me;  %#ok<NASGU>
                try;
                    var1 = eval( 'double( var );' );  %#ok<EVLCS,NASGU>
                    var = eval( [type '(var1);'] ); 
                    if( opt.verbose >= 0 );
                        warning( 'castws:doublecast', 'Direct cast failed. Cast was done via intermediate cast over ''double''.' ); end;
                catch me; %#ok<NASGU>
                    fprintf( 2, 'Cast failed:\n  %s\n  %s\n', me1.message, me2.message );
                    successflag = false;
                    end; end; end; end;
    if( opt.verbose < 0 );
        lasterror( err ); end;  %#ok<LERR>
end

function ret = contains_t( str, pattern );
    str = char( str );
    ret = true;
    for j = 1:numel( pattern );
        if( contains(str,char(pattern{j})) );
            return; end; end;
    ret = false;
end
    
    
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
