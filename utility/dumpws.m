function [ st ] = dumpws( varargin )
% dumps (saves) the current workspace
% [ [st] ] = dumpws( [names of variables], [options] ) 
%
% Input:
%   [names of variables]    strings, name of the variables to be dumped
%   [overwriteflag]         if set, variables are silently overwritten

% Options:
%   '-get'                  if added, variables are not dumped, but loaded from base workspace
%   '-disp'                 Variables are printed to screen, not dumped. If given, no output variable must be present
%   '-repr'                 (experimental) Variables are printed to screen in repr format
%   '-var'                  if given, variables are dumped to a variable in the base workspace
%
% Output:
%   st                      If given, variables are dumped to the output struct per default
%
% Note: When a variable appears on both sides of an assignment statement, the variable may become temporarily unavailable during processing.
%       These variables then cannot by dumped. This function also does not emit a warning in this case.
%
% E.g.: a = 1; st = dumpws;  % stores value of `a` in struct `st`
%       dumpws a  % copies variable `a` to base workspace
%
% Written by: tommsch, 2021-03-04

%               tommsch,    2023-05-17, Behaviour change: overwriteflag must be given as string 'overwrite' now
%                                       New option 'disp'
%                                       New option 'get'
%               tommsch,    2023-12-15, Behaviour change: Variable `ans` is not dumped
%                                       better, more reable output
%               tommsch,    2024-04-15, Added experimental option '-repr' (needs external repr utility)
%               tommsch,    2025-01-28, Added new output option '-var'
% Changelog:    

% XX Add option: stash, unstash: to save ws in persistent variable
    
    % check whether to dump or load
    [opt, varargin] = parse_input( nargout, varargin{:} );
    
    ws = evalin( opt.source, 'whos()' );
    allname = varargin;
    for i = 1:numel( ws );
        continueflag = false;
        if( isequal(ws(i).name, 'ans') );
            continue; end;
        if( ~isempty(allname) && ~any(strcmp(allname, ws(i).name)) );
            continue; end;
        
        value = evalin( opt.source, ws(i).name );
        
        switch opt.target;
            case {'caller','base'};
                variable_exists = evalin( opt.target,   ['exist( ''' ws(i).name ''', ''var'' );'] );
            case {'disp','repr','struct','var'};
                variable_exists = false;
            otherwise;
                fatal_error; end;

        overwritten = variable_exists ~= 0;
        if( variable_exists ~= 0 && ~opt.overwrite );
            if( ismatlab );
                len = fprintf( ['Do you want to overwrite the variable: [' 8 ' %s ]' 8 ],  ws(i).name );
                len = len - 4;
            else;
                len = fprintf( 'Do you want to overwrite the variable: %s     ',  ws(i).name ); end;
            while( true );
                str_input = '(y)es/(n)o/a(p)pend number/(a)ll overwrite/(d)isplay content/(s)top dumping: ';
                in = input( str_input, 's' ); 
                switch in;
                    case 'n';
                        continueflag = true; 
                        break;
                    case {'y','o'}; 
                        break;
                    case 'd'; 
                        disp( value );
                    case 'a'; 
                        opt.overwrite = true; 
                        break;
                    case 'p'; 
                        n = 0;
                        while( true );
                            n = n + 1;
                            newname = [ws(i).name '_' num2str(n)];
                            switch opt.target;
                                case {'base','caller'};
                                    if( ~evalin( opt.target, ['exist( ''' newname ''' );'] ) );
                                            break; end; end; end;
                        fprintf( ['Old variable name: [' 8 ' %s ]' 8 ', new variable name: [' 8 ' %s ]' 8 '\n'], ws(i).name, newname );
                        ws(i).name = newname;
                        break;
                    case 's';
                        return;
                    otherwise;
                        fprintf( 'Wrong input\n'); end; end; 
                fprintf( repmat('\b', [1 len + numel(str_input) + 2] ) );
            if( continueflag );
                continue; end; end;

        switch opt.target;
            case {'base','caller'};
                assignin( opt.target, ws(i).name, value ); 
                if( overwritten );
                    fprintf( 2, 'Overwrote variable in %s workspace: ', opt.target );
                    if( ismatlab );
                        fprintf( 1, ['[' 8 ' %s ]' 8 '\n'],  ws(i).name );
                    else;
                        fprintf( 1, '%s',  ws(i).name ); end;
                else;
                    if( ismatlab );
                        fprintf( 1, ['Dumped variable to %s workspace:    [' 8 ' %s ]' 8 '\n'], opt.target, ws(i).name );
                    else;
                        fprintf( 1, 'Dumped variable to %s workspace:   %s\n', opt.target, ws(i).name ); end; end;

            case {'repr'};
                try;
                    repr_str = evalc( 'repr( value, ws(i).name );' );  % we use evalc to hide from Matlab that we use a function, not included in this one ere.
                    fprintf( '%s', repr_str );
                catch me;  %#ok<NASGU>
                    try;
                        [w_msg, w_id] = lastwarn();
                        wl_clean = onCleanup( @() lastwarn(w_msg, w_id) );
                        w_old = warning();
                        warning( 'off', 'MATLAB:structOnObject' );
                        w_clean = onCleanup( @() warning(w_old) );
                        value_str = evalc( 'disp( struct(value) );' );
                    catch me;  %#ok<NASGU>
                        value_str = evalc( 'disp( value );' ); end;
                    value_str = ['%' strrep( value_str, newline, [newline '%'] )];
                    if( ismatlab );
                        fprintf( ['\n%% Could not repr variable. I display it instead.\n%%    Variable: [' 8 ' %s ]' 8 '\n%%    Type: %s\n%%    Value:\n%s\n%% ========================\n'], ws(i).name, class(value), value_str );
                    else;
                        fprintf( '\n%% Could not repr variable. I display it instead.\n%%    Variable: %s\n%%    Type: %s\n%%    Value:\n%s\n%% ========================\n', ws(i).name, class(value), value_str ); end; end;

            case {'var'};
                varname = evalin( 'base', 'genvarname(''dumpws_'', [who; ''dumpws_'']);' );
                var_st.(ws(i).name) = value;

            case {'disp'};
                if( ismatlab );
                    fprintf( ['Variable: [' 8 ' %s ]' 8 '\n value:\n'], ws(i).name );
                else;
                    fprintf( 'Variable: %s\n value:\n', ws(i).name ); end;
                disp( value );
                fprintf( '=========================\n' );

            case {'struct'};
                st.(ws(i).name) = value;

            otherwise;
                fatal_error; end; end;

    switch opt.target;
            case {'base','caller'}; % do nothing
            case {'repr'}; % do nothing
            case {'var'};
                assignin( 'base', varname, var_st );
                fprintf( 'Dumped to variable with name: %s\n', varname );
            case {'disp'}; % do nothing
            case {'struct'}; % do nothing
        otherwise; fatal_error; end;

end

function [ opt, varargin ] = parse_input( nout, varargin );
    from_base = false;
    repr_flag = false;
    disp_flag = false;
    var_flag = false;
    opt.overwrite = false;
    for i = numel( varargin ):-1:1;
        if( any(strcmp( varargin{i}, {'-repr','--repr'} )) );
            repr_flag = true;
            varargin(i) = []; 
            continue; end;
        if( any(strcmp( varargin{i}, {'-get','--get','-load','--load','-frombase','--frombase'} )) );
            from_base = true;
            varargin(i) = []; 
            continue; end;
        if( any(strcmp( varargin{i}, {'-var','--var','-variable','--variable','-struct','--struct'} )) );
            var_flag = true;
            varargin(i) = []; 
            continue; end;
        if( any(strcmp( varargin{i}, {'-disp','--disp','-display','--display','-print','--print','-show','--show'} )) );
            disp_flag = true; 
            varargin(i) = []; 
            continue; end;
        if( any(strcmp( varargin{i}, {'-overwrite','--overwrite','-overwriteall','--overwriteall','-a','-all','--all'} )) );
            opt.overwrite = true; 
            varargin(i) = []; 
            continue; end; end;
        
    switch from_base;
        case true; opt.source = 'base';
        case false; opt.source = 'caller';
        otherwise; fatal_error; end;
    if( repr_flag );
        assert( nout == 0, 'dumpws:args', 'If ''-repr'' is used, no output variable is allowed.' );
        opt.target = 'repr';
	elseif( disp_flag );
        assert( nout == 0, 'dumpws:args', 'If ''-disp'' is used, no output variable is allowed.' );
        opt.target = 'disp';
    elseif( var_flag );
        assert( nout == 0, 'dumpws:args', 'If ''-var'' is used, no output variable is allowed.' );
        opt.target = 'var';
    elseif( nout == 1 );
        opt.target = 'struct';
    elseif( isequal(opt.source, 'base') );
        opt.target = 'caller';
    elseif( isequal(opt.source, 'caller') );
        opt.target = 'base';
    else;
        fatal_error; end;
end

function ret = ismatlab ()
    % returns true when run on m
    persistent x;
    if( isempty (x) );
        x = ~exist( 'OCTAVE_VERSION', 'builtin' ); end;
    ret = x;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
