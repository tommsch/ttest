function [ varargout ] = terror( varargin );
% Wrapper for error() accepting any number of input and any number of output
% [ varargout ] = terror( varargin );
    if( numel(varargin) == 0 );
        varargin{1} = 'terror:noid';
        varargin{2} = 'terror:noid';
    elseif( numel(varargin) == 1 && contains(varargin{1}, ':') );
        varargin{2} = varargin{1}; end;
    error( varargin{:} );  %#ok<ERTAG>
    varargout = cell( 1, nargout );
    % we do not need to set values for varargout, since an error is thrown
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
