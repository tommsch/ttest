function [ varargout ] = profileit( f, num_outputs )
% (experimental) profiles the given function handle
    try;
        timeit( @() 1 );
        hastimeit = true;
    catch;
        hastimeit = false; end;

    stop_profiler = onCleanup( @() profile('off') );

    profile clear
    profile on

    try;
        if( hastimeit );
            if( nargin==1 );
                timeit( f );
            else;
                timeit( f, num_outputs ); end;
        else;
            if( nargin==1 );
                f();
            else
                outputs = cell( 1, num_outputs );
                [outputs{:}] = f(); end; end;  %#ok<NASGU>
    catch me;
        disp( 'Function handle threw an error:' );
        disp( me ); end;

    profile viewer;

    if( nargout==1 );
        varargout{1} = profile( 'info' ); end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
