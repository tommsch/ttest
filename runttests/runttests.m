function [ varargout ] = runttests( varargin );  
% [ successflag ] = runttests( ['test' | 'doctest'], [filename1, ..., filenamen], [options] )
% This function collects and executes tests.
%
% Input:
%   filenamei       strings, default = test*, *test,
%                   one or more strings, the files to be executed,
%                   can be passed with or without trailing '.m'
%                   accepts wildcards
%   'test'          optional, (default behaviour) if given, then all files starting or ending with filenamei are executed, option can also be given with dash at the beginning, i.e. '-test'
%   'doctest'       optional, if given, then only code in comment-lines starting with %TT are executed, option can also be given with dash at the beginning, i.e. '-doctest'
%
% Options:
%   '-search_parent_dirs'       optional, default: true if no filenames are given, false otherwise. If true, unit test files are searched in parent directories if no unit tests in the current directory can be found
%   '-shuffle'                  optional, if given, the test files are executed in random order. Default=true if only one filename is passed
%   '-subfolder'                optional, if given, then also all subdirectories are scanned for tests
%   '-verbose',val              Verbosity level
%   '-throw'                    default=true, When given, an error is thrown when the test run fails. This is useful for continuos integration.
%   '-repeat'/'-continue'       Repeats the failed tests from the last test run. Also works, when the last test run was aborted
%   '-dry'                      If given, test files are not executed, and it is assumed the tests in it succeeded
%   '-strict'                   If given, unexpected warnings and errors lead to a test run failure
%   '-recursiveoverride'        developer option, default=false, if given, then it is allowed that runttests is called recursively. Should only be used for tests, since test failures may not get reported correctly in that case
%   '-stopearly'                test run is stopped after first fail
%   '-unattended'               Disables Matlab features which would potentially stop the testrun, e.g. debug breakpoints
%
% Output:
%   successflag     (experimental) true when all tests succeeded, false when one test did not succeed or when no tests were executed at all
%
% Notes:
%   multiline comments are not yet supported with doctests
%
% Example:
%   runttests( );              % execute all files in current directory that start or end with 'test'
%   runttests( 'subfolder' );  % execute all files in current and subdirectories that start with 'test'
%   runttests( 'testxy' );     % execute script 'testxy.m'
%   runttests( 'testabc', 'testdef', 'testxyz' );            % execute scipts 'testabc.m', 'testdef.m', and 'testxyz.m'
%   runttests( 'subfolder', '*xyz*',  );  % Search for files containing the pattern 'xyz' in current and subdirectories
%   runttests( 'doctest', '*xyz*',  );    % Search for files containing the pattern 'xyz', and execute only those starting with %TT
%
% Written by: Clara Hollomey, 2021-03-01

%               tommsch,    2021-06,        Added function style tests
%               tommsch,    2021-08-24,     Created doctest functions are deleted after the test run
%               tommsch,    2021-09-10,     Added json function signature file
%               tommsch,    2021-10-12,     Behaviour change: Doctests reset value 
%               tommsch,    2022-02-24,     Added experimental options 'dry','repeatfailed','throw', Improved parsing of options
%               tommsch,    2024-03-06,     Better handling of exceptions which are thrown during the test
%                                           Behaviour change: Only ttest breakpoints are now restored when entering a new section
%                                           Unexpected warnings and errors will lead to a failed test run in future releases.
%                                           Added option '-strict'
%               tommsch,    2024-06-26,     Improved option '-repeat'. Now it is possible to also continue aborted (e.g. via Ctrl-C) test runs
%               tommsch,    2024-07-02,     Behaviour change: If more than one filename is passed, then 'shuffle' defaults to false.
%               tommsch,    2024-10-18,     Improved finding of test suites. Now runttests also searches in super-folders, if it cant find tests in the current folder
%                                           Added option: 'no_search_parent_dirs'
%                                           Better parsing of command line options when given in "shell-format"
%               tommsch,    2024-10-23,     More robust behaviour when runttests executes an m-file which is by chance not a TTEST test suite
%                                           Behaviour change: function based tests must not take nor return arguments
%               tommsch,    2025-02-28,     Better support for recursive calls of runttests,
%                                           Better support for test suites with multiple `TTEST init` calls
% Changelog:    tommsch,    2025-03-03,     Reporting of duration of test suites

    [opt, varargin] = parseinputs( varargin{:} );
    if( ~opt.recursiveoverride );
        clean_RUNTTESTS_CHECKED(); end;
    cleanup_unattended = unattended_run( opt );  %#ok<NASGU>
    trigger_warnings();
    [cleanupvar, retflag] = check_recursive_call( opt, varargin{:} );  %#ok<ASGLU>
    if( retflag );
        return; end;
    check_installation();
    dirCleanup = onCleanup_lazy( @(x) cd(x), pwd );  %#ok<NASGU>

    [filelist_userinput, folderlist_execute] = makefilelist( opt.searchterm, opt.subfolder, opt.search_parent_dir );
    if( isempty(filelist_userinput) );
        warning( 'runttests:notests', 'No tests found.');
        return; end;

    printinfo( opt );
    [filelist_execute, filelist_userinput] = handle_doctest_comments( filelist_userinput, opt );
    [filelist_execute, folderlist_execute, filelist_original] = shuffle_tests( filelist_execute, folderlist_execute, filelist_userinput, opt );
    [successidx, duration] = runttests_worker( filelist_execute, folderlist_execute, filelist_original, opt );
    successflag = runttests_postprocess( filelist_execute, filelist_original, successidx, duration, opt );

    if( nargout == 1 );
        varargout = {successflag}; end;

end

function clean_RUNTTESTS_CHECKED();
    c = TTEST( 'cache', 'all' );
    names = fieldnames( c );
    for i = 1:numel( names );
        if( isequal(names{i}, 'last_ttest_id') );
            continue; end;
        TTEST( names{i}, 'runttests_checked', true ); end;
end

function [ cleanupvar, retflag ] = check_recursive_call( opt, varargin );
    persistent in_testrun;
    cleanupvar = [];
    retflag = false;
    
    if( any( strcmp( varargin, 'clear_in_testrun' ) ) ...
      );
        retflag = true;
        in_testrun = [];
    elseif( opt.recursiveoverride );
        clear runttests_worker;
        in_testrun = true;
        cleanupvar = onCleanup( @() runttests( 'clear_in_testrun' ) );
    elseif( isempty(in_testrun) );
        in_testrun = true;
        cleanupvar = onCleanup( @() runttests( 'clear_in_testrun' ) );
        clearat -all;
    else;
        db = dbstack;  % octave has problems with onCleanup
        runttests_maybe_running = false;
        for i = 1:numel( db );
            if( contains( db(i).name, 'runttests' ) );
                runttests_maybe_running = true;
                break; end; end;
        if( runttests_maybe_running );
            error( 'runttests:recursive', 'This function must not be called recursively.\n  If this is a false-alarm, you can disable this check by providing the option ''-recursiveoverride''\n  or calling runttests( -clear_in_testrun )` before starting the test suite.' .' ); 
        else;
            in_testrun = true;
            cleanupvar = onCleanup( @() runttests( 'clear_in_testrun' ) );
            clearat -all;
            end; end;
end

function check_installation();
    persistent TTEST_INSTALLED
    flag = true;
    if( isempty(TTEST_INSTALLED) );
        flag = flag && ~isempty( which( 'TTEST' ) );
        flag = flag && ~isempty( which( 'TTEST_INSTALL' ) );
        flag = flag && ~isempty( which( 'cwd' ) );
        flag = flag && ~isempty( which( 'EXPECT_EQ' ) );
        TTEST_INSTALLED = flag; end;
    
    assert( TTEST_INSTALLED, 'TTEST seems not to be installed correctly.\n  Read how to install TTEST in the documentation.\n  Most likely it suffices to execute `TTEST install` once.' );
end

function [ successidx, duration ] = runttests_worker( filelist, folderlist, filelist_original, opt );
    persistent repeat_data;
    if( opt.recursiveoverride );
        if( opt.verbose >= 0 );
            fprintf( 'runttests is called recursively. Therefore, failed tests can not be repeated with the ''--repeat'' option.\n' ); end;
        repeat_data = [];
    elseif( opt.repeat && ~isempty(repeat_data) );
        filelist = repeat_data.filelist;
        filelist_original = repeat_data.filelist_original;
        folderlist = repeat_data.folderlist;
    else;
        if( opt.verbose >= 0 );
            tt.expect( ~opt.repeat, 'runttests:repeat', 'Option ''repeat'' only works after a failed test run.' );  end;
        repeat_data.filelist = filelist;
        repeat_data.filelist_original = filelist_original;
        repeat_data.folderlist = folderlist;
        repeat_data.state = REPEAT_DATA_STATE_EMPTY*ones( size(filelist) );
        end;
    
    successidx = nan( size(filelist) );
    duration = nan( size(filelist) );
    if( ~isempty(repeat_data) );
        successidx( repeat_data.state == REPEAT_DATA_STATE_SUCCEEDED ) = true; end;
    lastfolder = '';
    teststr = '';

    fprintf( 'The following tests suites are executed:\n' );
    if( opt.verbose >= 1 );
        len = cellfun( 'prodofsize', filelist_original );
        for jj = 1:numel( filelist_original );
            fprintf( '  %s', filelist_original{jj} );
            if( opt.doctest );
                fprintf( '%s ( %s )', repmat( ' ', max(len) - len(jj), 1 ), filelist{jj} ); end;
            fprintf( '\n' ); end;
        fprintf( '\n' ); end;
    
    for jj = 1:numel( filelist );
        if( ~isempty(repeat_data) && numel(repeat_data.state) == numel( filelist ) && repeat_data.state(jj) == REPEAT_DATA_STATE_SUCCEEDED );  % the test `numel(repeat_data) == numel( filelist )` is a bloody hack - since it is possible to call runttests recursively (if one insists on it), the variable repeat_data may contain wrong information
            continue; end;
        lastwarn( 'ttest:runttests:before', 'ttest:runttests:before' );  % This is to test whether another warning is set during the test run
        lasterr( 'ttest:runttests:before', 'ttest:runttests:before' );  %#ok<LERR>  % This is to test whether another warning is set during the test run
        prestr = ''; poststr = '';
        starttime_subtest = tic;
        try;
            %if( opt.verbose >= 0 && opt.verbose <= 1 );
            %    [~, filename_original] = fileparts( filelist_original{jj} );
            %    temp_str = sprintf( 'Running %s\n', filename_original );
            %    fprintf( '%s', temp_str )
            %    end;
            
            if( opt.verbose >= 1 );
                prestr = [prestr sprintf( 'Running %s', filelist_original{jj} )]; end; %#ok<AGROW>
            if( opt.verbose >= 2 && ~strcmp(filelist_original{jj}, filelist{jj}))
                prestr = [prestr printf( '  ( using temporary file: %s )\n', filelist{jj} )]; end;  %#ok<AGROW>
            if( opt.verbose >= 1 );
                prestr = [prestr newline]; end;  %#ok<AGROW>

            ty = filetype( filelist{jj} );  % nargout test is too slow, so we disable this test

            clear cleanup_folder;
            [lastfolder, cleanup_folder] = handle_next_folder( lastfolder, folderlist{jj} );  %#ok<ASGLU>
            
            switch ty;
                case {'s','script'};
                    if( opt.dry );
                        continue; end;
                    if( opt.verbose >= 1 );
                        teststr = '';
                        %diary_content = monitor_console( filelist{jj} );
                        ttest_run_script_in_workspace_jaskldlhajskld7897925hjlhjsfk23_( filelist{jj} );
                        %diary_content = diary_content();
                    else;
                        teststr = ttest_run_script_in_workspace_captured_jaskldlha7925hjlhjsfk23_( filelist{jj} ); end;

                case {'f','function'};
                    if( isoctave );
                        [~, file_] = fileparts( filelist{jj} ); 
                    else;
                        file_ = filelist{jj}; end;
                    if( nargout(file_) ~=0 || nargin(file_) ~= 0 );
                        fprintf( '%s: Skipped, since it does not seem to be a ttest unit test suite (nargout/nargin does not equal 0)\n', file_ )
                        continue; end;
                    dbstop( 'in', file_, 'if', 'returnfalse( cellfun( @(x) tt.void( @() x(), 1, [], [], nargout(x) == 0 && nargin(x) == 0 ), localfunctions ), ''ttest - IF THIS IS FILE IS NOT A TTEST-SUITE, THEN THIS BREAKPOINT WILL TRIGGER THE DEBUGGER. THIS BREAKPOINT CAN USUALLY BE SAFELY DELETED.'');' );  % XX This needs to be fixed
                    if( opt.dry );
                        continue; end;
                    if( opt.verbose >= 1 );
                        teststr = '';
                        ttest_run_function_in_workspace_jaskldlha_( filelist{jj} );
                    else;
                        teststr = ttest_run_function_in_workspace_captured_jaskldlhajskld7897_( filelist{jj} ); end;
                    clearat( 'in', file_ );

                case {'c','class'};
                    error( 'ttest:fatal', '\nClass based tests are not supported by TTESTs.' ); 

                otherwise;
                    error( 'ttest:fatal', 'Programming error' ); end;

        catch me;
            successidx(jj) = interpret_error( filelist{jj}, filelist_original{jj}, me, successidx(jj), opt ); end;

        duration(jj) = toc( starttime_subtest );
        [successidx(jj), teststr] = interpret_TTEST_ERRORCODE( successidx(jj), teststr );
        if( isnan(successidx(jj)) );
            successidx(jj) = ~TTEST( 'ttest_initlast', 'errorflag' ); end;

        [successidx(jj), teststr] = interpret_unexpected_exceptions( filelist_original{jj}, successidx(jj), teststr, opt );

        if( opt.verbose >= 1 );
            if( ~successidx(jj) );
                poststr = [poststr sprintf( '\nTest failed: %s', filelist_original{jj} )];  %#ok<AGROW>
            else;
                poststr = [poststr sprintf( '\nTest succeeded: %s', filelist_original{jj} )]; end;  %#ok<AGROW>
            poststr = [poststr sprintf( ' (Duration: %f sec)\n__________\n', duration(jj) )];  %#ok<AGROW>
            if( successidx(jj) == false || ...
                opt.verbose >= 2 || ...
                any( teststr ~= '.' ) ...
              );
                fprintf( '\n%s\n%s\n%s\n', prestr, teststr, poststr ); 
            elseif( successidx(jj) == true && opt.verbose == 1 );
                fprintf( '.' ); end; end;

        if( ~isempty(repeat_data) );
            if( successidx(jj) );
                repeat_data.state(jj) = REPEAT_DATA_STATE_SUCCEEDED;
            else;
                repeat_data.state(jj) = REPEAT_DATA_STATE_FAILED; end; end;
        
        if( opt.stopearly && ~successidx(jj) );
            break; end;
        end;

end

function [ lastfolder, cleanup_path ] = handle_next_folder( lastfolder, folderlist );
    cleanup_path = [];
    if( isequal(lastfolder, folderlist) );
        return; end;

    cleanup_path = onCleanup_lazy( @(x) path(x), path );
    [w_msg_before, w_id_before] = lastwarn( '', 'ttest:runttests:addpath_before' );
    evalc( 'addpath( folderlist );' );  % capture warning output from this command
    [w_msg_after, w_id_after] = lastwarn;
    switch w_id_after;
        case {'MATLAB:mpath:privateDirectoriesNotAllowedOnPath'};
            lastwarn( w_msg_before, w_id_before );
            sanitized_folderlist_jj = folderlist;
            npr = numel( 'private' );
            while( true );
                if( numel(sanitized_folderlist_jj) > 1 && isequal(sanitized_folderlist_jj(end), filesep) );
                    sanitized_folderlist_jj(end) = [];
                    continue; end;
                if( numel(sanitized_folderlist_jj) > npr && strcmpi(sanitized_folderlist_jj(end-npr+1:end), 'private') );
                    sanitized_folderlist_jj(end-npr+1:end) = [];
                    continue; end;
                break; end;
            addpath( sanitized_folderlist_jj );

        case {'ttest:runttests:addpath_before'};
            lastwarn( w_msg_before, w_id_before );
        otherwise;
            warning( w_id_after, w_msg_after ); end;

    lastfolder = folderlist;
end

function [ successidx ] = interpret_error( filelist, filelist_original, me, successidx, opt );
    if( numel(me) >= 1 && ...
        numel(me.stack) >= 1 && ...
        contains( lower([me.stack(1).name me.stack(1).file]), 'ttest' ) && ...
        contains( lower([me.stack(1).name me.stack(1).file]), 'assert' ) ...
      );
        successidx = false;
        if( opt.verbose >= 0 );
            warning( 'runttests:assert', '\nASSERT test failed.\n  %s', me2str(me, opt.verbose-1) ); end;
    elseif( numel(me) >= 1 && ...
        numel(me.stack) >= 1 && ...
        contains( lower([me.stack(1).name me.stack(1).file]), 'ttest' ) && ...
        contains( lower([me.stack(1).name me.stack(1).file]), 'assume' ) ...
      );
        if( opt.verbose >= 0 );
            fprintf( '\nASSUME test failed. The test-suite ist aborted, but counts as succeeded.\n  %s', me2str(me, 0) ); end;
    else;
        successidx = false;
        if( opt.verbose >= 0 );
            warning( 'runttests:runtime', '\nError while executing %s (%s):\n%s', ...
                     filelist_original, filelist, me2str(me, opt.verbose) ); end; end;
end

function [ successidx, teststr ] = interpret_TTEST_ERRORCODE( successidx, teststr );
    successidx_last = true;
    if( isnan(successidx) );
        try;
            successidx_last = ~TTEST( 'ttest_initlast', 'errorflag' );
        catch me;  %#ok<NASGU>
            successidx_last = true; end; end;

    cache = TTEST( 'cache', 'all' );
    names = fieldnames( cache );
    successidx_all = true( 1, numel(names) - 1 );
    for ii = 1:numel( names );
        if( strcmp(names{ii}, 'last_ttest_id') );
            continue; end;
        if( ~cache.(names{ii}).TTEST_RUNTTESTS_CHECKED );
            successidx_all(ii) = ~TTEST( names{ii}, 'errorflag' ); end; end;
    if( ~all(successidx_all) );
        teststr = [teststr newline sprintf( 'Some prior test suite(s) failed:\n' )];
        for ii = 1:numel( successidx_all ); 
            if( ~successidx_all(ii) );
                teststr = [teststr sprintf( '  %s\n', names{ii} )]; end; end;  %#ok<AGROW>
        teststr = [teststr newline]; end;
    successidx = all(successidx_all) && successidx_last;
end

function [ successidx, teststr ] = interpret_unexpected_exceptions( filelist_original, successidx, teststr, opt );

    [lw_msg, lw_id] = lastwarn();
    if( successidx && ...
        ~isequal( lw_msg, 'ttest:runttests:before' ) && ...
        ~contains( lw_id, 'TTEST:assume_failed' ) && ...
        ~contains( lower(lw_msg), '_assume' ) ...
      );
        soft_failure = false;
        if( isoctave );
            if( contains( lw_msg, '__legend_handle__' ) || ...
                contains( lw_id, 'Octave:gnuplot-graphics' ) || contains( lw_msg, 'using the gnuplot graphics toolkit is discouraged' ) ...
              );
                soft_failure = true; end; end;
        if( ~soft_failure );
            teststr = [teststr newline sprintf( 'Unexpected warning was thrown somewhere during the test:\n  File: %s\n  Warning id:  %s\n  Warning msg: %s\n',  filelist_original, lw_id, lw_msg )];
            if( opt.strict );
                successidx = false; 
            else;
                teststr = [teststr newline sprintf( '  This will lead to a failure in a future release\n' )]; end; end; end;

    [err_msg, err_id] = lasterr();  %#ok<LERR>
    if( successidx && ...
        ~isequal( err_msg, 'ttest:runttests:before') && ...
        ~contains( err_id, 'TTEST:assume_failed') && ...
        ~contains( lower(err_msg), '_assume' ) ...
      );
        err_after = lasterror();  %#ok<LERR>
        soft_failure = false;
        if( isoctave );
            if( contains( err_after.message, '__legend_handle__' ) || ...
                contains( err_after.message, 'matrix cannot be indexed with .' ) ...
              );
                soft_failure = true; end; end;
        if( ~soft_failure );
            teststr = [teststr newline sprintf( 'Unexpected error was thrown during the test of file: %s\n%s\n',  filelist_original, me2str(err_after) )];
            if( opt.strict );
                successidx = false; 
            else;
                teststr = [teststr newline sprintf( '  This will lead to a failure in a future release\n' )]; end; end; end;
end

function [ successflag ] = runttests_postprocess( filelist_execute, filelist_original, successidx, duration, opt );

    successflag = ~isempty( successidx ) && all( successidx(:) == true );
    if( opt.doctest );
        for i = 1:numel( filelist_execute );
            delete( filelist_execute{i} ); end; 
        %filelist_execute = {};
        end;
    
    maxlen_fname = 0;
    maxlen_flo = 0;
    for i = 1:numel( filelist_original );
        [~, fname, ~] = fileparts( filelist_original{i} );
        maxlen_flo = max( maxlen_flo, numel(filelist_original{i}) );
        maxlen_fname = max( maxlen_fname, numel(fname) ); end;
    

    if( ~isempty(successidx) && any(successidx(:) == true) );
        if( opt.verbose >= 1 );
            fprintf( 1, '\nSuccessfull tests:\n' ); 
            for i = 1:numel( successidx );
                if( successidx(i) == true );
                    [~, fname, ~] = fileparts( filelist_original{i} );
                    spaces_fname = repmat( ' ', [1, maxlen_fname-numel(fname)+4] );
                    spaces_flo = repmat( ' ', [1, maxlen_flo-numel(filelist_original{i})+4] );
                    if( ismatlab );
                        fprintf( 1, '  <a href="matlab:opentoline(''%s'',1,1)">%s%s(%s)%s(%3.3f sec)</a>\n', filelist_original{i}, fname, spaces_fname, filelist_original{i}, spaces_flo, duration(i) );
                    else;
                        fprintf( 1, '  %s%s(%s)\n', fname, spaces_fname, filelist_original{i} ); end; end; end; end; end;

    if( ~isempty(successidx) && any(successidx(:) ~= true) );
        if( opt.verbose >= 0 );
            fprintf( 2, '\nFailed tests:\n' );
            for i = 1:numel( successidx );
                if( successidx(i) ~= true );
                    [~, fname, ~] = fileparts( filelist_original{i} );
                    spaces_fname = repmat( ' ', [1, maxlen_fname-numel(fname)+4] );
                    if( ismatlab );
                        fprintf( 2, '  <a href="matlab:opentoline(''%s'',1,1)">%s%s(%s)</a>\n', filelist_original{i}, fname, spaces_fname, filelist_original{i} );
                    else;
                        fprintf( 2, '  %s%s(%s)\n', fname, spaces_fname, filelist_original{i} ); end; end; end; end; end;

    if( opt.verbose >= 2 || ...
        opt.verbose >= 1 && ~successflag ...
      );
        end;
    if( opt.verbose >= 1 );
        fprintf( '\nDuration: %3.2f sec\n____________\n', toc( opt.starttime_test ) ); end;
        
    if( opt.throw && ~successflag );
        error( 'runttests:fail', 'Test run failed.' ); end;        
end


function ttest_run_function_in_workspace_jaskldlha_( ttest_name_jaskldlhajskld7897925hjlhjsfk_ );
    [~, ttest_file_jaskldlhajskld7897925hjlhjsfk_,~] = fileparts( ttest_name_jaskldlhajskld7897925hjlhjsfk_ );
    eval( ttest_file_jaskldlhajskld7897925hjlhjsfk_ );
end

function [ str_jaskldlhajskld7897925hjlhjsfk_ ] = ttest_run_function_in_workspace_captured_jaskldlhajskld7897_( ttest_name_jaskldlhajskld7897925hjlhjsfk_ );
    [~, ttest_file_jaskldlhajskld7897925hjlhjsfk_,~] = fileparts( ttest_name_jaskldlhajskld7897925hjlhjsfk_ );
    str_jaskldlhajskld7897925hjlhjsfk_ = evalc( ttest_file_jaskldlhajskld7897925hjlhjsfk_ );
end

function ttest_run_script_in_workspace_jaskldlhajskld7897925hjlhjsfk23_( ttest_name_jaskldlhajskld7897925hjlhjsfk23 );
    run( ttest_name_jaskldlhajskld7897925hjlhjsfk23 );
end

function [ str_jaskldlhajskld7897925hjlhjsfk23_ ] = ttest_run_script_in_workspace_captured_jaskldlha7925hjlhjsfk23_( ttest_name_jaskldlhajskld7897925hjlhjsfk23 );  %#ok<INUSD>
    str_jaskldlhajskld7897925hjlhjsfk23_ = evalc( 'run( ttest_name_jaskldlhajskld7897925hjlhjsfk23 )' );
end

function printinfo( opt );
    if( opt.verbose >= 1 );
        fprintf( 'Tests are executed of folder: %s\n', pwd ); end;
    if( opt.verbose >= 2 );
        sysinfo( opt.verbose );
        fprintf( '\nTTEST version: %s\n', TTEST('ver') ); end;
end

function [ filelist, filelist_original ] = handle_doctest_comments( filelist, opt );
    filelist_original = filelist;
    if( opt.doctest );
        bp = which( 'TTEST.m' );
        bp = bp(1:end-numel( 'TTEST.m' ));
        doctestdir = fullfile( bp, 'tmp', 'tr' );
        TTEST 0 clear
        filename = parselines( filelist, '%TT' );
        filelist = fullfile( doctestdir, filename ); 
        rehash; end;
end

function [ filelist, folderlist, filelist_original ] = shuffle_tests( filelist, folderlist, filelist_original, opt );
    % shuffle tests
    if( opt.shuffle );
        p = randperm( numel(filelist) );
        filelist = filelist( p );
        folderlist = folderlist( p );
        filelist_original = filelist_original( p ); end;
end

%%
function [ val ] = REPEAT_DATA_STATE_SUCCEEDED; val = -1; end
function [ val ] = REPEAT_DATA_STATE_EMPTY;     val =  0; end
function [ val ] = REPEAT_DATA_STATE_FAILED;    val =  1; end
function [ val ] = REPEAT_DATA_STATE_SKIPPED;   val =  2; end

function dummy; end  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>  % cd for mcc

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
