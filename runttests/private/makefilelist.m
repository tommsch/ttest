function [ filelist, folderlist ] = makefilelist( searchterm, subfolder, search_parent_dir );
% [ filelist, folderlist ] = makefilelist( searchterm, subfolder, search_parent_dirs )
%
% Input:
%    searchterm     cell array of strings, default = {'test*','*test.m'}, containing char arrays to be searched for
%                   if the string starts with ':' or '-', files whose filename contains the given pattern are excluded (case insensitive)
%                   
%    subfolder      flag (boolean), default = false, determines if subfolder search should be performed
%
% Output:
%   filelist        struct containing char arrays with the path to all the files found based on the searchterms
%   folderlist      corresponding folders where the files reside
%
% Written by: Clara Hollomey, 2021-03-01
%

%               tommsch, 2021-08-27:    Added return values `folderlist` and `switchflag`
%               tommsch, 2021-09-15:    Added search on matlab path if no test was found, but a searchterm was given
%               tommsch, 2021-10-12:    Added option to deselect test
%               tommsch, 2023-04-18:    Bugfix with folders without tests
%               tommsch, 2025-02-28:    Behaviour change: If no skipterms are given (starting with '-'), then 'disable' is a skipterm
%                                       Behaviour change: Skip terms are case-insensitive
% Changelog:    
    
    opt = parse_input( searchterm, subfolder, search_parent_dir );
    files = find_files( opt );
    [filelist, folderlist] = parse_files( files, opt );
    [filelist, folderlist] = filter_out( filelist, folderlist, opt );

end

function [ opt ] = parse_input( searchterm, subfolder, search_parent_dirs );

    if( ~iscell(searchterm) );
        searchterm = {searchterm}; end;
    
    skipTerm = {};
    for i = numel( searchterm ):-1:1;
        if( ~isempty(searchterm{i}) && ...
            (searchterm{i}(1) == '-' || searchterm{i}(1) == ':') ...
          );
            skipTerm{end+1} = searchterm{i}(2:end);  %#ok<AGROW>
            searchterm(i) = []; end; end;

    if( isempty(searchterm) );
        searchterm = {'test*','*test.m'}; end;
    if( isempty(skipTerm) );
        skipTerm = {'disable'}; end;
    opt.search_parent_dir = search_parent_dirs;
    opt.searchterm = searchterm;
    opt.skipTerm = skipTerm;
    opt.subfolder = subfolder;
end

function [ files ] = find_files( opt );
    files = [];
    if( opt.subfolder );
        for kk = 1:numel( opt.searchterm );
            files = [files; recursivesearch( fullfile(pwd, opt.searchterm{kk}) )]; end;  %#ok<AGROW>
    else;
        while( true );
            for kk = 1:numel( opt.searchterm )
                files = [files; dir( opt.searchterm{kk} )]; end;  %#ok<AGROW>
            if( ~isempty(files) || ~opt.search_parent_dir );
                break; end;
            old_pwd = pwd;
            testdir = dir( '*test*' );
            if( ~isempty(testdir) && ...
                ~isequal(fileparts(old_pwd), testdir(1).name ) && ...
                isfolder( testdir(1).name ) ...
              );
                cd( testdir(1).name );
            else;
                cd( '..' ); end;
            new_pwd = pwd;
            if( isequal(old_pwd, new_pwd) );
                break; end;
            fprintf( 2, 'Could not find any test files in the current folder. I continue my search in: %s\n', pwd ); end; end;
    
    if( isempty(files) );
        for i = 1:numel( opt.searchterm );
            x = which( opt.searchterm{i} );
            if( ~isempty(x) );
                files = [files; dir(x)]; end; end; end;  %#ok<AGROW>
end

function  [filelist, folderlist] = parse_files( files, opt );
    filelist = [];
    folderlist = [];
    for ii = 1:numel( files )
        if( strcmp( files(ii).name, '.' ) || ...
            strcmp( files(ii).name, '..' ) ...
          );
            continue; end;
        if( opt.subfolder );
            [folder, name, ext] = fileparts( files(ii).name );
        else;
            folder = files(ii).folder;
            name = files(ii).name;
            ext = name(end-1:end);
            name = name(1:end-2); end;
        if( numel( name ) >= 1 && ...
            strcmp( ext, '.m' ) && ...
            ~strcmp( [name, ext], 'runttests.m' ) && ...
            isvarname( name ) ...
          );
            oldfolder = cd( files(ii).folder );
            cd( oldfolder );
            folderlist{end+1} = folder;  %#ok<AGROW>
            filelist{end+1} = fullfile( folder, [name ext] ); end; end;  %#ok<AGROW>
    
    [filelist, idx] = unique( filelist, 'stable' );
    folderlist = folderlist(idx);
end

function [ filelist, folderlist ] = filter_out( filelist, folderlist, opt );
    if( ~isempty(filelist) );
        for j = 1:numel( opt.skipTerm );
            % if( iswindows );
            %     regexp_h = @regexpi;
            % else;
            %     regexp_h = @regexp; end;
            if( ismatlab );
                warningopt = {'warnings'};
            else;
                warningopt = {}; end;
            idx = regexpi( filelist, regexptranslate('wildcard', opt.skipTerm{j}), 'match', 'once', warningopt{:} );
            idx = ~cellfun( 'isempty', idx );
            filelist(idx) = [];
            folderlist(idx) = []; end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
