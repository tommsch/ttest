function [ filename_out, testlist ] = parselines( filelist, magiccommentstring )
% parses magic comments in files
% [ filename_out, testlist ] = parselines( fileList, searchTerm )
% Each passed in filelist is parsed and lines starting with a magic string are uncommented,
% while all other lines are prefixed by '%'. Line breaks are maintained.
% The yielded text is stored in a new file.
%
% Input:    
%   fileList                cell array, containing strings with filenames including path
%   magiccommentstring      char array, default = '%TT', indicating the prefix of the lines to be parsed
%
% Output:   
%   filename_out            cell array, strings of the file names of the generated filesall the files in fileList with those lines preceded 
%                           by the searchTerm uncommented and all other lines commented
%
% Written by: Clara Hollomey, 2021-03-01

    testlist = [];
    funcnamelist = [];
    basepath = which( 'TTEST.m' );
    basepath = basepath(1:end-numel('TTEST.m'));
    doctestdir = fullfile( basepath, 'tmp', 'tr' );
    
    for ii = 1:numel( filelist );
        
        try;
            S = fileread( filelist{ii} );
        catch me;
            warning( 'parselines:ioerror', 'Could not read file %s.\n  Error id: %s\n  Error msg: %s', filelist{ii}, me.identifier, me.message ); 
            continue; end;
        [~,funcname,~] = fileparts( filelist{ii} );
        funcnamelist = [funcnamelist, {funcname}];  %#ok<AGROW>
        S = strrep( S, '\r\n', '\n' );
        S = strsplit( S, '\n' );
        S = switchcomment( S, magiccommentstring );
        testlist = [testlist, {S}]; end;  %#ok<AGROW>
    
    basepath = pwd;
    cleandir = onCleanup( @() cd(basepath) );
    cd( doctestdir );
    
    filename_out = {};
    for ii = 1:numel( testlist );
        % create an .m-file in that directory
        
        filename_candidate = ['doctest_', funcnamelist{ii}];
        fn = [tt.freename( doctestdir, filename_candidate, 8 ), '.m'];
        fid = fopen( fn, 'w' );  %#ok<MCMFL>
        if( fid~=-1 );  % write all the tests in there and close the file
            fileCleanup = onCleanup( @() fclose(fid) );
            fprintf( fid, '%s\n', testlist{ii} );
            filename_out{end+1} = fn;  %#ok<AGROW>
        else;
            warning( 'parselines:ioerror', 'Could not write to file %s\n, which is the doctest file for %s\n', fn, filelist{ii} ); end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
