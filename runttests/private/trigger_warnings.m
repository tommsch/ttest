function trigger_warnings();
% A function which triggers warnings, which are otherwise maybe triggered during the test run,
% but which should not lead to a failure of the test run

    persistent warning_triggerd;
    if( ~isempty(warning_triggerd) );
        return; end;

    if( ismatlab );
        
        warning(  'off', 'MATLAB:hg:AutoSoftwareOpenGL' );
        plot3( [1 2],[3 4], [4 2], 'r-.' );
        
        warning(  'off', 'MATLAB:dispatcher:nameConflict' );

        cd( '.' );

    else;  % octave warnings
        
    end;
    
    warning_triggerd = true;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>
