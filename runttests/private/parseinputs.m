function [ opt, varargin ] = parseinputs( varargin )
% runs tests
% [ opt ] = parseInputs( ['subfolder'], ['doctest' | 'test'], ['filename_1', ..., 'filename_n'], [options] )
%
% Input:
%   filename    one or more char-arrays, the files to be executed,
%               can be passed with or without trailing '.m'
%               in the event of more than one char-array being passed to 'testrunner',
%               the type of execution ('test' or 'doctest') will be determined by the
%               name of the first file - if tests should be executed, the first file must be named 'test'
%   flag        char-array, can be either 'subfolder' or 'doctest', or both, order-agnostic
%               the char-array 'test' is given implicitly as default when a file starting with
%               'test' is passed to the testrunner
%
% Output:
%    opt.              option struct with fields
%       .searchTerm    cell array of strings, default = {'*test','test*'}, the search terms determing which test file shall be executed
%       .subfolder     boolean, defines whether the subfolders should be searched for the files or not
%       .doctest       boolean, defines whether the files are executed or parsed and searched
%
% Written by: Clara Hollomey, 2021-03-01

    % parse options given with prefix `-`
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [opt.shuffle,           varargin] = tt.parsem( {'-shuffle','-random'},                          varargin, [],   'postprocess',@str2numt );
    [opt.throw,             varargin] = tt.parsem( {'-throw'},                                      varargin,       'postprocess',@str2numt );
    [opt.verbose,           varargin] = tt.parsem( {'-verbose','-v'},                               varargin, 1,    'postprocess',@str2numt );
    [opt.subfolder,         varargin] = tt.parsem( {'-recursive','-r','-rec','-sub','-subfolder'},  varargin,       'postprocess',@str2numt );
    [opt.dry,               varargin] = tt.parsem( {'-dry'},                                        varargin,       'postprocess',@str2numt );
    [opt.doctest,           varargin] = tt.parsem( {'-doctest','-d'},                               varargin,       'postprocess',@str2numt );
    [ver_flag,              varargin] = tt.parsem( {'-ver','-version'},                             varargin,       'postprocess',@str2numt );
    [opt.repeat,            varargin] = tt.parsem( {'-repeat','-repeatfailed','-continue'},         varargin,       'postprocess',@str2numt );
    [opt.maxtime,           varargin] = tt.parsem( {'-maxtime'},                                    varargin, inf,  'postprocess',@str2numt );
    [opt.strict,            varargin] = tt.parsem( {'-strict'},                                     varargin,       'postprocess',@str2numt );
    [opt.recursiveoverride, varargin] = tt.parsem( {'-recursiveoverride','-override'},              varargin,       'postprocess',@str2numt );
    [opt.search_parent_dir, varargin] = tt.parsem( {'-search_parent_dirs','-search_parent_dir'},    varargin,       'postprocess',@str2numt, 'default',[] );
    [opt.stopearly,         varargin] = tt.parsem( {'-stop_early','-stop-after-first-fail'},        varargin,       'postprocess',@str2numt );
    [opt.unattended,        varargin] = tt.parsem( {'-unattended'},                                 varargin,       'postprocess',@str2numt );

    % parse options given as name-value pairs, where the value is a number or a number-string
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    idx = false( 1, numel(varargin) );
    for i = 2:numel( varargin );
        if( islogical(varargin{i}) || isnumeric(varargin{i}) || ~isnan(str2double(varargin{i})) );
            idx(i) = true; 
            idx(i-1) = true; end; end;
    option_arg = varargin( idx );
    varargin = varargin( ~idx );

    [opt.shuffle,           option_arg] =                   tt.parsem( {'shuffle','random'},                        option_arg, opt.shuffle,    'postprocess',@str2numt );
    [opt.throw,             option_arg] =                   tt.parsem( {'throw'},                                   option_arg, opt.throw,      'postprocess',@str2numt );
    [opt.verbose,           option_arg] =                   tt.parsem( {'verbose','v'},                             option_arg, opt.verbose,    'postprocess',@str2numt );
    [opt.subfolder,         option_arg] =                   tt.parsem( {'recursive','r','rec','sub','subfolder'},   option_arg, opt.subfolder,  'postprocess',@str2numt );
    [opt.doctest,           option_arg] =                   tt.parsem( {'doctest','d'},                             option_arg, opt.doctest,    'postprocess',@str2numt );
    [opt.dry,               option_arg] =                   tt.parsem( {'dry'},                                     option_arg, opt.dry,        'postprocess',@str2numt );
    [ver_flag,              option_arg] =                   tt.parsem( {'ver','version'},                           option_arg, ver_flag,       'postprocess',@str2numt );
    if( ver_flag );
        opt.verbose = max( 2, opt.verbose ); end;
    [opt.repeat,            option_arg] =                   tt.parsem( {'repeat','repeatfailed','continue'},        option_arg, opt.repeat,     'postprocess',@str2numt );
    [opt.maxtime,           option_arg] =                   tt.parsem( {'maxtime'},                                 option_arg, opt.maxtime,    'postprocess',@str2numt );
    [opt.strict,            option_arg] =                   tt.parsem( {'strict'},                                  option_arg, opt.strict,     'postprocess',@str2numt );
    [opt.recursiveoverride, option_arg] =                   tt.parsem( {'recursiveoverride','override'},            option_arg, opt.recursiveoverride, 'postprocess',@str2numt );
    [opt.stopearly,         option_arg] =                   tt.parsem( {'stop_early','stop-after-first-fail'},      option_arg, opt.stopearly,  'postprocess',@str2numt );
    [opt.unattended,        option_arg] =                   tt.parsem( {'unattended'},                              option_arg, opt.unattended, 'postprocess',@str2numt );

    assert( isempty(option_arg), 'runttests:option', 'Unknown option(s) given.' );

    opt.verbose = numcast( opt.verbose );
    opt.dry = numcast( opt.dry );


    % parse options 'subfolder', 'doctest', 'test' which can be given also the very beginning
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if( numel(varargin) >= 1 && strcmpi(varargin{1}, 'subfolder') );
        opt.subfolder = 1;
        varargin(1) = []; end;

    if( numel(varargin) >= 1 );
        switch lower( varargin{1} );
            case {'test','unittest'};
                opt.doctest = 0;
                varargin(1) = [];
            case 'doctest';
                opt.doctest = 1;
                varargin(1) = [];
            otherwise;
                end; end;  % do nothing;

    % parse searchterm
    %%%%%%%%%%%%%%%%%%%%
    if( numel(varargin) == 0 );
        if( isempty(opt.search_parent_dir) );
            opt.search_parent_dir = true; end;
        if( opt.doctest );
            opt.searchterm = {'*'};
        else;
            opt.searchterm = {'test*','*test'}; end;
    else;
        if( isempty(opt.search_parent_dir) );
            opt.search_parent_dir = false; end;
        opt.searchterm = cell( 1, numel(varargin) );
        for i = 1:numel( varargin )
            assert( ischar(varargin{i}) || isstring(varargin{i}), 'runttests:searchterm', 'Only strings can be given as searchterms.' );
            opt.searchterm{i} = char( varargin{i} ); end; end;

    % postprocess searchterms
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    for i = 1:numel( opt.searchterm );
        assert( ischar(opt.searchterm{i}) || isstring(opt.searchterm{i}), 'runttests:options', 'All arguments must be strings.' );
        opt.searchterm{i} = char( opt.searchterm{i} );
        n = numel( opt.searchterm{i} );
        if( n >= 1 && opt.searchterm{i}(end) == '*' );
            continue; end;
        if( n >= 2 && strcmp(opt.searchterm{i}(end-1:end), '.m') );
            continue; end;
        opt.searchterm{i} = [opt.searchterm{i} '.m']; end;

    % set additional fields in opt
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    opt.starttime_test = tic;
    if( isempty(opt.shuffle) );
        opt.shuffle = any( [opt.searchterm{:}] == '*' ); end;
    
    % test if there some options which could not get parsed
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % for i = 1:numel( varargin );
    %     if( (ischar( varargin{i} ) || isstring( varargin{i} )) && ...
    %         strcmp( varargin{i}(1), '-' ) ...
    %       );
    %         error( 'runttests:options', 'Could not parse some option: %s\n', varargin{i} ); end; end;
            
        
end

function [ out ] = numcast( in );
    try;
        if( isnumeric(in) );
            out = in;
            return; end; 
    catch me;  %#ok<NASGU>
        end;
    
    try;
        out = str2double( in );
        if( ~isnan(out) );
            return; end;
    catch me;  %#ok<NASGU>
        end;
end

function [ arg ] = str2numt( arg );
% wrapper for str2num
% only calls function, when argument is of type char or string
    if( ischar(arg) || isstring(arg) );
        arg = str2num( arg );  %#ok<ST2NM>
    end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
