function [ varargout ] = overload_wrapper( arg1, arg2, varargin )
% Calls the overloaded function. For expensive functions, checks first whether this is necessary.
%
% arg2 must be one of: 'path', 'debug'
% arg1 must be any function name OR nan, or the returned value from a call with nan
% varargin are the arguments which shall be passed to function funcname
%
% lastchange = overload_wrapper( NaN, name )
%   Returns the time the last time an overload of `name` was called
%   If overload `name` was not called before, returns -inf
%
% flag = overload_wrapper( time, name )
%   Returns whether the last time (a double) an overload of `name` was called was later than `time`
%   If overload `name` was not called before, returns true
%
% [ varargout ] = overload_wrapper( string, name, varargin )

    persistent lastchange;
    persistent handle;

    
    if( isequaln(arg1, nan) );
        try;
            varargout{1} = lastchange.(arg2); 
        catch me;  %#ok<NASGU>
            varargout{1} = -inf; end;
        return; 
    elseif( isnumeric(arg1) );
        try;
            varargout{1} = lastchange.(arg2) > arg1;
        catch me;  %#ok<NASGU>
            varargout{1} = true; end;
        return; end;
            
    assert( isstring(arg1) || ischar(arg1) );
    arg1 = char( arg1 );

    if( nargout >= 1 );
        varargout = cell( 1, nargout ); end;
    

    if( isempty(handle) );
        handle = struct; 
        lastchange = struct; 
        lastchange.path = now;  %#ok<TNOW1>
        lastchange.debug = now; end;  %#ok<TNOW1>
    
    if( ~isfield(handle, arg1) );
        handle.(arg1) = getmatlabfunction( arg1 ); end;
    
    if( nargout == 0 );
        [w_msg, w_id] = lastwarn;
        handle.(arg1)( varargin{:} );
        [~, w_id_after] = lastwarn;
        switch w_id_after;
            case {'MATLAB:dispatcher:nameConflict'};
                lastwarn( w_msg, w_id );
            otherwise;
                % do nothing
            end;
    else;
        [ varargout{:} ] = handle.(arg1)( varargin{:} ); end;
    
    lastchange.(arg2) = now;  %#ok<TNOW1>
    
end

function TTEST_dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
