function [ varargout ] = warning( varargin );
% Shadowing of warning() in order to
%   1) store meta data of warning
%   2) save all (most) warnings thrown
%
% All possible standard calls as defined for matlab-warning:
%       warning( msg )
%       warning( msg, A )
%       warning( warnID, ___ )
%       warning( state )
%       warning(state, warnID )
%       warning
%       warnStruct = warning
%       warning( warnStruct )
%       warning( state, mode )
%       warnStruct = warning( state, mode )
%
% Additional calls:
%   warning( ['--temp',[duration] ], ___ )        changes warning settings temporarily, duration is either a number or a string representing a number
%   warning( ['--once', ___ )         throws the warning (identified by its id) only once (until the function is cleared).
%   warning( '--reset' )              resets the warning state prior to the last warning( 'off' ) call
%   [ w ] = warning( nan )          returns the saved warnings, in backwards order
%   [ w ] = warning( nan, nan )     returns the saved warnings, in backwards order and deletes all saved warnings
%   [ w ] = warning( --clear )      same as warning( nan, nan )
%   [ w ] = warning( --get )        same as warning( nan )
%
% Restrictions:
%   Does not save calls from warning() or workspacefunc()
%   May not save warnings thrown from builtins
%
% Output:
%   msg     first return value of matlab-warning
%           (the undocumented second return value is not returned)
%   w       cell array, the saved warnings in the format
%
% Written by: tommsch, 2020

%            2021-05-06, tommsch         Added option 'temp'
%            2021-08-18, tommsch         Automatically restore warnings after a `warning off`
%            2023-06-06, tommsch         Added support for input type of `message` (Undocumented)
% Changelog: 

    persistent warning_container;
    persistent warning_laststate;
    persistent timer_container;
    persistent last_warning_time;

    persistent isoctave;
    if( isempty (isoctave) );
        isoctave = logical( exist ('OCTAVE_VERSION', 'builtin') ); end;

    MAXNUM = 500;
    if( isoctave );
        builtin( 'warning', 'off', 'Octave:shadowed-function' );
    else;
        builtin( 'warning', 'off', 'MATLAB:dispatcher:nameConflict' ); end;

    if( isempty(warning_container) );
        warning_container = {}; end;
    if( isequaln( varargin, {nan} ) || ...
        isequal(varargin, {'--get'}) ||...
        numel(varargin) == 1 && ~ischar(varargin{1}) && ~isstring(varargin{1}) && ~isstruct(varargin{1}) && ~isa(varargin{1}, 'message') ...
      );
        varargout{1} = warning_container;
        return;
    elseif( isequaln( varargin, {nan,nan} ) || ...
            isequal(varargin, {'--clear'}) ||...
            numel(varargin) == 2 && ~ischar(varargin{1}) && ~isstring(varargin{1}) && ~isstruct(varargin{1}) && ~isa(varargin{1}, 'message') ...
           );
        varargout{1} = warning_container;
        warning_container = {};
        return; end;

    ds = dbstack;
    if( numel(varargin) >= 1 && isa(varargin{1}, 'message') );
        varargin = [varargin{1}.Identifier '%s', [varargin{1}.Arguments{:}] varargin(2:end)]; end;
    if( numel(ds) >= 2 && (isequal(ds(2).name, 'workspacefunc') || ...
        isequal(ds(2).name, 'warning')) ...
      );
        % do nothing
    else;
        if( numel(ds) >= 2 );
            ds = ds(2:end);
        else;
            ds = []; end;

        if( numel(varargin) == 0 );
            warning_container = [ {{'SAVE', ds}} warning_container ];

        elseif( isstruct(varargin{1}) );
            warning_container = [ {{'LOAD', varargin{1}, ds}} warning_container ];
        % elseif( numel(varargin) == 1 && isequal(varargin{1},'off') );
            % warning_laststate = builtin( 'warning' );
%         elseif( ~isoctave && numel(varargin) <= 1 && isequal(varargin{1}, 'off') );
%             warning_container = [ {{varargin{1:end} ds}} warning_container ];
%             startdelay = 10;
%             t = timer( 'TimerFcn', ['warning(''ttest_temp_back'',' num2str(size(timer_container,1)+1) ');'], 'StartDelay',startdelay );
%             timer_container{end+1,2} = t;
%             timer_container{end,1} = builtin( 'warning' );
%             start( t );
%             return;

        elseif( numel(varargin) <= 2 && ...
                (isequal(varargin{1}, 'off')   || isequal(varargin{1}, 'on')   || isequal(varargin{1}, 'query')   || isequal(varargin{1}, 'error'))   || ...
                (isequal(varargin{1}, '-off')  || isequal(varargin{1}, '-on')  || isequal(varargin{1}, '-query')  || isequal(varargin{1}, '-error'))  || ...
                (isequal(varargin{1}, '--off') || isequal(varargin{1}, '--on') || isequal(varargin{1}, '--query') || isequal(varargin{1}, '--error')) ...
              );
            if( isequal(varargin{1}, 'on') );
                last_warning_time = []; end;
            warning_container = [ {{varargin{1:end} ds}} warning_container ];
            if( isoctave );
                try;
                    TTEST_disableWarnings();
                catch me;  %#ok<NASGU>
                    persistent TTEST_disableWarnings_warning_printed;  %#ok<TLEV>
                    if( isempty(TTEST_disableWarnings_warning_printed) );
                        TTEST_disableWarnings_warning_printed = true;
                        fprintf( 'Could not disable superfluous Octave warnings. This may happen when TTEST is installed the first time.\n' ); end; end; end;

        elseif( isequal(varargin{1}, 'temp') || isequal(varargin{1}, '-temp') ||isequal(varargin{1}, '--temp'));  % temporarily change warnings
            if( isempty(timer_container) );
                timer_container = cell( 0, 2 ); end;
            warning_container = [ {{varargin{1:end} ds}} warning_container ];
            startdelay = 2;
            varargin(1) = [];
            if( numel(varargin) >= 1 && (ischar(varargin{1}) || isstring(varargin{1})) );
                val = str2double( char(varargin{1}) );
                if( ~isnan(val) );
                    varargin(1) = [];
                    startdelay = val; end;
            elseif( numel(varargin) >= 1 );
                varargin(1) = [];
                startdelay = varargin{2}; end;
            t = timer( 'TimerFcn', ['warning(''ttest_temp_back'',' num2str(size(timer_container, 1)+1) ');'], 'StartDelay',startdelay );
            timer_container{end+1,2} = t;
            timer_container{end,1} = builtin( 'warning' );
            start( t );
            builtin( 'warning', varargin{:} );
%             allwarningoffflag = false;
            return;

        elseif( isequal(varargin{1}, 'ttest_temp_back') );  % called by timer object to reenable warning, not to be used by the user
            warning_container = [ {{varargin{1:end} ds}} warning_container ];
            delete( timer_container{varargin{2},2} );
            builtin( 'warning', timer_container{varargin{2},1} );
            return;

        elseif( isequal(varargin{1}, 'once') || isequal(varargin{1}, '-once') || isequal(varargin{1}, '--once') );
            persistent once_container;  %#ok<TLEV>
            if( isequal(once_container, []) );
                once_container = {}; end;
            if( ~any( strcmp(varargin{2}, once_container) ) );
                once_container{end+1} = varargin{2};
                varargin = varargin(2:end);  %#ok<NASGU>
            else;
                return; end;

        elseif( isequal(varargin, {'reset'}) || isequal(varargin, {'-reset'}) || isequal(varargin, {'--reset'}) );
            if( isempty(warning_laststate) );
                warning_laststate = builtin( 'warning' ); end;
            warning_container = [ {{'RESET', ds}} warning_container ];
            varargin = {warning_laststate}; %#ok<NASGU>

        elseif( numel(varargin) >= 1 && ...
                ~isa(varargin{1}, 'message') && ... 
                (numel(varargin) == 1 || ~any(strfind(varargin{1}, ':'))) ...
              );
            warning_container = [{{ 'TTEST:NOID', sprintf( varargin{1:end} ), ds }} warning_container];

        elseif( numel(varargin) >= 2 && ~isa(varargin{1}, 'message') );
            warning_container = [{{ varargin{1}, sprintf( varargin{2:end} ), ds }} warning_container];

        else
            warning_container = [{{ varargin{1}, 'message', ds }} warning_container];
            end; end;

    if( numel(warning_container) >= 2*MAXNUM );
        warning_container = warning_container( 1:MAXNUM ); end;

    if( nargout == 0 );
        str = evalc( 'builtin( ''warning'', varargin{:} )' );
    else;
        [str, msg] = evalc( 'builtin( ''warning'', varargin{:} )' );
        varargout{1} = msg; end;

    if( ~isoctave && numel(str) >= 4000 );  % matlab has html code in warnings
        str = [str(1:1900) ' [...] ' str(end-1900:end)];
    elseif( isoctave && numel(str) >= 2000 );
        str = [str(1:1400) ' [...] ' str(end-1400:end)]; end;
    [~, lid] = lastwarn();
    if( isempty(str) );
        % do nothing
    elseif( numel(str) <= numel( 'Warning: ' ) + 6);
        str = ['[' 8 'Warning id: ' lid ']' 8 newline];
    elseif( numel(str) < numel( 'Warning:' ) + numel(lid) );
        str = ['[' 8 'Warning id: ' lid ']' 8 newline str]; end;
    fprintf( '%s', str );

    %% Safety check against Matlab functions
	woff.identifier = 'all';
    woff.state = 'off';
    w = builtin( 'warning' );
    if( isequal(w, woff) );
        flag = true;
        if( isempty(last_warning_time) );
            last_warning_time = tic;
        elseif( numel(ds) >= 1 && ~isempty(last_warning_time) );
            try;
                name = which( ds(1).name );
                flag = ~any( strfind( name, matlabroot ) );
            catch me;  %#ok<NASGU>
                end; end;
        if( flag && toc(last_warning_time) >= 10 );
            filename = '';
            idx = find( cellfun( 'prodofsize', warning_container ) == 2 );
            for i = idx
                if( isequal(warning_container{i}{1}, 'off') && numel(warning_container{i}) >= 2 );
                    filename = [warning_container{i}{2}(1).name ' @ ' warning_container{i}{2}(1).file];
                    break; end; end;
            last_warning_time = tic;
            fprintf( 2, '\nWARNING: All warnings were disabled.\n  Warnings are now enabled again by TTEST.\n' );
            warning( 'on' );
            %builtin( 'warning', 'on' );
            if( ~isempty(filename) );
                fprintf( 2, '  Function which disabled all warnings: %s\n', filename ); end; end; end;

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCMLR>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

