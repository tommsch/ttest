function clc( enableflag_ );
% disableable clc
% clc( enableflag_ )
%
% Input:
%   enableflag_     if `true`, subsequently calls to `clc()` behave like the builtin clc() (i.e. clear the console)
%                   if `false`, subsequently calls to `clc()` print a message that clc was called.
%                   if `nan`, nothing happens
%
% Written by: 27.02.2024, tommsch,

    if( nargin == 1 && isnan(enableflag_) );
        return; end;

    persistent enableflag
    if( isempty(enableflag) );
        enableflag = true; end;
    
    if( nargin == 1 );
        if( enableflag_ == false );
            enableflag = 0;
        elseif( enableflag_ == true );
            enableflag = 1; 
        else;
            error( 'TTEST:clc', 'Wrong usage of `clc`.' ); end;
        return; end;

    if( isequal(enableflag, 0) );
        enableflag = -1;
        fprintf( 'Call to `clc()` was intercepted by `TTEST`.\n' );
    elseif( isequal(enableflag, -1) );
        fprintf( 'clc() ' );
    else;
        builtin( 'clc' ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
