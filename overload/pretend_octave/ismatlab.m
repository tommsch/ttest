function [ ret ] = ismatlab()
    % returns always false
    ret = false;
end

function dummy; end  %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
