function [ ret ] = isoctave()
    % returns always true
    ret = true;
end

function dummy; end  %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
