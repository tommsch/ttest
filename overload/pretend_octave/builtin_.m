function [ varargout ] = builtin( varargin );
% wrapper function for cd, which resets warnings about overloaded builtin functions
    persistent handle;
    if( numel(varargin) == 1 && isa(varargin{1}, 'function_handle') );
        handle = varargin{1};
        fprintf( 2, 'Set handle for builtin.\n' );
        return; end;
    if( nargout >= 0 );
        varargout = cell( 1, nargout ); end;
    %oldpath = pwd;
    %cleanpath = onCleanup( @() cd(oldpath) );
    %cd( 'C:\Program Files\MATLAB\R2023b\toolbox\matlab\lang' );
    
    %disp( handle );
    [ varargout{:} ] = handle( varargin{:} );
end

function TTEST_dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
