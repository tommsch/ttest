function [ ret ] = exist( varargin );
% wrapper function for cd, which resets warnings about overloaded builtin functions

    
    switch nargin;
        case 0;
        case 1;
            if( isequal(varargin{1}, 'OCTAVE_VERSION') );
                ret = true;
                return; end;
        case 2;
            if( isequal(varargin{1}, 'OCTAVE_VERSION') );
                ret = strcmpi(varargin{2}, 'builtin');
                return; end;
        otherwise;
            end;  % do nothing
    ret = builtin( 'exist', varargin{:} );
end

function TTEST_dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
