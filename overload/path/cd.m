function [ varargout ] = cd( varargin );
% wrapper function for cd, which resets warnings about overloaded builtin functions

    [w_msg, w_id] = lastwarn();
	if( numel(varargin) >= 1 && numel(varargin) <= 2 &&  isequaln(varargin{1}, nan) );
		varargout{1} = [];
		return; end;

    if( nargout >= 0 );
        varargout = cell( 1, nargout ); end;
		
    [ varargout{:} ] = builtin( 'cd', varargin{:} );
    lastwarn( w_msg, w_id );
end

function TTEST_dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
