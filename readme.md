
# TTEST Suite
### *Test framework for scientific people and scientific software*

##
See the full documentation here:
[./doc/readme.md](./doc/readme.md)

## Authors:
- *Thomas Mejstrik*,
  > - [NuHAG](https://nuhag.univie.ac.at/), Faculty of Mathematics, University of Vienna, Universitaetsring 1, 1010 Vienna, Austria
  > - [Dimetor](https://www.dimetor.com/) GmbH, Wiedner Hauptstraße 24/15, 1040 Vienna, Austria,<tommsch@gmx.at>
- *Clara Hollomey*
  > - [ARI](https://www.oeaw.ac.at/isf/), Austrian Academy of Sciences, Wohllebengasse 12-14, 1040, Vienna, Austria

## Overview

The *TTEST* framework is a unit testing framework, especially designed for scientific software.

*TTEST* is designed along the following guidelines:
  - A test should be easy to write, to foster the writing of tests
  - A test should be easy to write, to prevent bugs in the test suites
  - A test should be easy to understand and to design
  - A test run should not stop just because one test fails, allowing to fix multiple bugs in a single test run
  - When a test fails, as much information as possible should be provided
  - Tests should be portable between Matlab versions and Octave
  - The test suite shall not replace Matlab features and work with most Matlab test styles. 

*In particular, to write a test should not require more knowledge than what is needed to write the program.* Therefore, writing unit tests with *TTESTs* requires no knowledge of
- classes, regular expressions, tables, structs, functional programming, handles, try/catch blocks, anonymous functions, cell arrays, functions

Of all currently maintained Matlab and Octave unit test frameworks (these are: *TTEST*, *MOxUnit* and *Matlab's built in unit test framework*, *TTEST*
- is the fastest, 
- needs least boiler plate code,
- has the most assertions,
- takes most care of isolating unit tests from each other.
- can test scripts, local and sub functions,
- has utilities for caching results for integration tests,
 - adds support for injection testing and partly for design by contract
See far below for the systematic comparison proofing the claims.

Aspects where TTEST falls behind are:
- code coverage reports,
- mocking, performance testing, test fixtures, usage of external data, 
- parallelization,
- coding standard tests.
