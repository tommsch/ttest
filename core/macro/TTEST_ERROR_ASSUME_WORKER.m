function [ ret ] = TTEST_ERROR_ASSUME_WORKER( id, ret, errid, errmsg );  %#ok<INUSD>
% TTEST_ERROR_ASSUME_WORKER( [errid], [errmsg] )
% Default function called when a PRECONDITION_ fails
%
% See also: TTEST_ERROR_EXPECT_WORKER

    if( nargin <= 0 || isempty( errid ) );
        errid = 'TTEST:assertion:failed';
        errmsg = 'TTEST:assertion failed.';
    elseif( nargin <= 1 || isempty( errmsg ) );
        errmsg = errid; end;
    
    errmsg = ['precondition test failed.' newline errid newline errmsg];
    errid = 'TTEST:assume_failed';
    error( errid, errmsg );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
