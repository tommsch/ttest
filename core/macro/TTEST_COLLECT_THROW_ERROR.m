function [ throw_exc ] = TTEST_COLLECT_THROW_ERROR( me );
    
    if( isoctave && ...
        numel(me.stack) >= 2 && ...
        strcmp(me.stack(1).name, 'assert') ...
      );
        idx = 2;
    else;
        idx = 1; end;
    throw_exc = {'t', {me.identifier}, {me.message}, me.stack(idx).line, {me.stack(idx).name}, {me.stack(idx).file}, 'Error'};
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
