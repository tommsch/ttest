function [ opt, varargin ] = ttest_drawer_opt_parsem( opt, varargin )
% Parses general options, valid for all drawers
% [ opt, varargin ] = ttest_drawer_opt_parsem( opt, varargin )
%
%            tommsch, 2022-02-24, Added option 'type', 'postprocess'
% Changelog: 
    
    [opt.minnorm,varargin]    =   tt.parsem( {'minnorm','minmag','min_magnitude'}, varargin,           [], 'asserte', @isnumeric );
    [opt.maxnorm,varargin]    =   tt.parsem( {'maxnorm','maxmag','max_magnitude'}, varargin,          [], 'asserte', @isscalar );
    [normval,varargin] = tt.parsem( {'norm'}, varargin,  [], 'asserte', @isnumeric );
    if( ~isempty(normval) );
        assert( isempty(opt.minnorm) && isempty(opt.maxnorm), 'GIVEN:opt', 'If ''norm'' is given, options  ''minnorm'' and ''maxnorm'' must not be given.' );
        opt.minnorm = normval;
        opt.maxnorm = normval; end;
    assert( all(opt.minnorm<opt.maxnorm), 'GIVEN:opt', '''minnorm'' must be strictly smaller than ''maxnorm''.' );
    
    [opt.minval,varargin]        =   tt.parsem( {'min','minval','min_value'}, varargin,              [], 'asserte', @isnumeric );
    [opt.maxval,varargin]        =   tt.parsem( {'max','maxval','max_value'}, varargin,              [], 'asserte', @isnumeric );
    [valval,varargin] = tt.parsem( {'val'}, varargin,  [], 'asserte', @isnumeric );
    if( ~isempty(valval) );
        assert( isempty(opt.minval) && isempty(opt.maxval), 'dopt_parsem:val', 'If ''val'' is given, options  ''minval'' and ''maxval'' must not be given.' );
        opt.minval = valval;
        opt.maxval = valval; end;
    assert( all(opt.minval<opt.maxval), 'GIVEN:opt', '''minval'' must be strictly smaller than ''maxval''.' );
    
    [opt.minsze,varargin]     =   tt.parsem( {'minsize','minsze','min_size'}, varargin,  [], 'asserte', @isnumeric );
    [opt.maxsze,varargin]     =   tt.parsem( {'maxsize','maxsze','max_size'}, varargin,  [], 'asserte', @isnumeric );
    [szeval,varargin] = tt.parsem( {'sze'}, varargin,  [], 'asserte', @isnumeric );
    if( ~isempty(szeval) );
        assert( isempty(opt.minsze) && isempty(opt.maxsze), 'dopt_parsem:sze', 'If ''sze'' is given, options  ''minsze'' and ''maxsze'' must not be given.' );
        opt.minsze = szeval;
        opt.maxsze = szeval; end;
    len = min( numel(opt.minsze), numel(opt.maxsze) );
    assert( all(opt.minsze(1:len)<=opt.maxsze(1:len)), 'GIVEN:opt', '''minsze'' must be smaller than ''maxsze''.' );
    
    [opt.allownan,varargin]   =   tt.parsem( {'allownan','nan','allow_nan'}, varargin,   [], 'expecte', @(x) isnumeric(x) || islogical(x) );        
    [opt.allowinf,varargin]   =   tt.parsem( {'allowinf','inf','allow_infinity'}, varargin,   [], 'expecte', @(x) isnumeric(x) || islogical(x) );        
    [opt.allowcp,varargin]    =   tt.parsem( {'allowcomplex','allowcp','complex','cp'}, varargin, [], 'expecte', @(x) isnumeric(x) || islogical(x) );        
    [opt.allowneg,varargin]   =   tt.parsem( {'allownegativ','allowneg','negative','neg'}, varargin, [], 'expecte', @(x) isnumeric(x) || islogical(x) );
    [opt.allowsparse,varargin] =  tt.parsem( {'allowsparse','allowsp','sparse','sp'}, varargin, [], 'expecte', @(x) isnumeric(x) || islogical(x) );
    [finite,varargin]         =   tt.parsem( {'finite'}, varargin );
    if( finite );
        opt.allownan = false;
        opt.allowinf = false; end;
    
    [opt.type,varargin]       =   tt.parsem( {'type'}, varargin, [], 'expecte', @(x) ischar(x) || isstring(x) );
    [opt.postprocess,varargin] =  tt.parsem( {'post','postprocess'}, varargin, 1, 'expecte', @(x) isequal(x,0) || isequal(x,1) || islogical(x) );

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
