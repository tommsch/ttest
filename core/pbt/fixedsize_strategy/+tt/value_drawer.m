function [ ex_state, raw ] = value_drawer( state, opt , set );

    ex_state = ttest_state_c( 'raw', [] );
    
    len = numel( set );
    
	if( state.num_draw < len );
		idx = 1 + state.num_draw;
	else;
        idx = randi( len ); end;

    if( iscell(set) );
        raw = set{opt.perm(idx)}; 
    else;
        raw = set(opt.perm(idx)); end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
