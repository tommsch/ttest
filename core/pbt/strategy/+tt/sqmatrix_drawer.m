function [ ex_state, raw ] = sqmatrix_drawer( state, opt );

	%XX needs to draw nan values as well
    
    ex_state = ttest_state_c( 'raw', [] );

    num_draw = state.num_draw;
    % get size
    while( true );
        if( isempty(opt.minsze) && isempty(opt.maxsze) );  % both minsz and maxsz not given
            minsze = floor( sqrt(num_draw) );
            maxsze = ceil( 2^(sqrt(num_draw)) );
        elseif( ~isempty(opt.minsze) && isempty(opt.maxsze) ) ;  % minsz given
            minsze = opt.minsze;
            maxsze = opt.minsze +  floor( sqrt(num_draw) );
        elseif( ~isempty(opt.maxsze) && isempty(opt.minsze) );  % maxsz given
            minsze = floor( sqrt(num_draw) );
            maxsze = opt.maxsze;
        else;
            minsze = opt.minsze;
            maxsze = opt.maxsze;
            end;  % do nothing
        assert( numel(minsze) == 1 && numel(maxsze) == 1, 'sqm_drawer:input', '''minsze'' and ''maxsze'' must be given as scalars.' );
        maxsze = max( minsze, maxsze );

        sz = tt.randcauchy( 1, minsze, min(maxsze, 2^24), num_draw/30 );
        assert( minsze <= sz && sz <= maxsze, 'TTEST:fatal', 'Programming error.' );
        
        if( prod(sz)<1e3 );
            break; end; end;
    
    % degenerate cases
    if( isequal(sz,0) );
        raw = zeros( 0, 0 );
        return; end;
    
    if( isempty(opt.type) );
        type = mod( round( state.lvl*1000 ), 44 ) + 1;
    else;
        type = opt.type; 
        idx = type == '_';
        type(idx) = []; end;
    
    switch lower( type );
        case {1,'zeros','zero','0'};
            raw = zeros( sz );
        case {2,'ones','1','one'};
            raw = ones( sz );
        case {3,'randn','random'};
            raw = randn( sz );       
%         case {5,6,'array'};
%             raw = randn( sz );
%             raw = tt.array_drawer( state, opt );
%             raw = array_drawer( state, opt );  % XX missing
        case {4,'toeplitz'};
            if( randi(2) == 1 );
                val1 = tt.randx( 1, max(sz) );
                val2 = tt.randx( 1, max(sz) );
                val2(1) = val1(1);
            else;
                val1 = tt.randx( 1, max(sz) );
                val2 = val1; end;            
            raw = toeplitz( val1(1:sz), val2(1:sz) );
        case {5,'circul','circulant'};
            val = randi( randi(100), 1, sz ) - randi(50);
            try;
                iend = randi(3);
                for i = 1:iend;
                    val = conv( val(1:end-2), 1/4*[1 2 1] ); end;
            catch me;  %#ok<NASGU>
                end;
            if( isempty(val) );
                raw = randn( sz );
            else;
                raw = gallery( 'circul', val );
                raw = raw(1:sz,1:sz); end;
        case {6,'cycol'};
            raw = gallery( 'cycol', sz, round(rand*sz)+1 );                  
        case {7,'compan','companion'};
            raw = compan( tt.randx(1,sz+1) );
        case {8,'hadamard'};
            raw = [];
            err = lasterror();  %#ok<LERR>
            while( isempty(raw) );
                try;
                    raw = hadamard( sz ); 
                catch me;  %#ok<NASGU>
                    sz = sz + 1; end; end;
            lasterror( err );  %#ok<LERR>
            if( sz < minsze || sz > maxsze );
                raw = tt.randx( minsze ); end;
        case {9,'hilb','hilbert'};
            raw = hilb( sz );
        case {10,'invhilb','invhilbert','inversehilbert'};
            raw = invhilb( sz );
        case {11,'magic'};
            raw = magic( sz );
        case {12,'pascal'};
            switch randi(3);
                case 1; raw = pascal( sz );
                case 2; raw = pascal( sz, 1 );
                case 3; raw = pascal( sz, 2 ); end;
        case {13,'rosser','wilkinson'};
            if( sz == 8 );
                raw = rosser;
            else;
                raw = wilkinson( sz ); end;
        case {14,'binomial'};
            if( ismatlab );
                raw = gallery( 'binomial', sz );
            else;
                raw = randn( sz ); end;
        
        case {15,'chebspec','chebyshevspectral'};
            raw = gallery( 'chebspec', sz, randi(2)-1 );
        case {16,'chebvand','chebyshev','chebyshevvandermond'};
            raw = gallery( 'chebvand', sz );
        
        case {17,'clement'};
            raw = gallery( 'clement', sz, randi(2)-1);
        
        % 'condex' skipped
        case {18,'condex'};
            switch sz;
                case {1,2}; raw = tt.randx( sz );
                case 3; raw = gallery( 'condex', 3, 2, abs(tt.randx) );
                case 4; raw = gallery( 'condex', 4, 1, abs(tt.randx) );
                otherwise; raw = gallery( 'condex', sz, 4, abs(tt.randx) ); end;
        
        case {19,'dramadah'};
            
            raw = gallery( 'dramadah', sz, randi(3) );
            if( size(raw,1)~=size(raw,2) );
                raw = gallery( 'dramadah', sz ); end;
        
        case {20,'frank'};
            raw = gallery( 'frank', sz, randi(2)-1 );
        case {21,'gcdmat'};
            raw = gallery( 'gcdmat', sz );
        case {22,'gearmat'};
            raw = gallery( 'gearmat', sz );
        case {23,'grcar'};
		    switch sz;  % fix for octave
			    case 0; raw = [];
				case 1; raw = [1];  %#ok<NBRAK>
				case 2; raw = [1 1; -1 1];
				otherwise; raw = gallery( 'grcar', sz ); end;
        case {24,'invol'};
            raw = gallery( 'invol', sz );
        case {25,'ipjfact'};
            if( sz==1 );
                raw = 2;
            else;
                raw = gallery( 'ipjfact', sz ); end;
        case {26,'kahan'};
            raw = gallery( 'kahan', sz );
        case {27,'lauchli'};
            raw = randn( sz );
            % raw = gallery( 'lauchli', sz );
            % XX move lauchli to matrix, lauchli is a (n+1) x n matrix
        case {28,'lehmer','totallynonnegative'};
            raw = gallery( 'lehmer', sz );
        case {29,'lesp'};
            if( sz==1 );
                raw = -5;
            else;
                raw = gallery( 'lesp', sz ); end;
        case {30,'lotkin'};
            raw = gallery( 'lotkin', sz );
        case {31,'minij'};
            raw = gallery( 'minij', sz );
        case {32,'neumann'};
            check = round(sqrt(sz))^2;
            if( sz>=2 && check==sz )
                raw = full( gallery( 'neumann', sz ) );
            else;
                raw = tt.randx( sz ); end;
        case {33,'orthog'};
            if( ismatlab );
                raw = real( gallery( 'orthog', sz, randi(6) ) );
            else;
                raw = real( gallery( 'orthog', sz, randi(5) ) ); end;
        case {34,35,'parter'};  % octave needs those special case handling
                if( sz==0 );
                    raw = [];
                elseif( sz==1 );
                    raw = inf;
                else;
                    raw = gallery( 'parter', sz ); end;
        % case {35,'poisson'};  % change value of 'parter' if this gets readded
        %    raw = gallery( 'poisson', sz );
        case {36,'redheff','redheffer'};
            raw = gallery( 'redheff', sz );
        case {37,'riemann'};
            raw = gallery( 'riemann', sz );
        case {38,'ris'};
            if( sz<=1 );
                raw = zeros( sz );
            else;
                raw = gallery( 'ris', sz ); end;
        case {39,'smoke'};
            raw = real( gallery( 'smoke', sz ) );
        case {40,'toeppen'};
            raw = full( gallery( 'toeppen', sz ) );
        case {41,'tridiag'};
            raw = full( gallery( 'tridiag', sz ) );
        case {42,'triw'};
            raw = full( gallery( 'tridiag', sz ) );
        % case {43,44,45,46,'tgallery'};
        %    raw = tgallery( tt.randcauchy(1,1,inf), sz, 1, tt.randx, 'nocell', 'verbose',-1 );
        %    sz1 = size( raw, 1 );
        %    sz2 = size( raw, 2 );
        %    if( sz1<sz2 ); 
        %        raw = raw(1:sz1,1:sz1);
        %    elseif( sz2<sz1 ); 
        %        raw = raw(1:sz2,1:sz2); end;
        %    if( sz1<opt.minsze ); 
        %        raw(opt.minsze,opt.minsze) = 0;
        %    elseif( sz2>opt.maxsze ); 
        %        raw = raw(1:opt.maxsze,1:opt.maxsze); end;
        case {43,'consecutive'};
            raw = reshape( 1:prod(sz*sz), [sz sz 1] );
        case {44,'hankel'};
            val1 = tt.randx( 1, sz );
            if( randi(2)==1 );
                val2 = tt.randx( 1, sz );
            else;
                val2 = zeros( 1, sz ); end;
            val2(1) = val1(end);
            raw = hankel( val1, val2 );
        case {45,'diagonal'};
            val = tt.randx( 1, sz );
            raw = diag( val );
  
        otherwise;
            error( 'TTEST:fatal', 'Wrong input for sqmatrix_drawer. Input: %s', num2str(lower(type)) ); end;
        
        
	raw = full( raw );

    if( opt.invertible );
        idx = ~isfinite( raw );
        raw(idx) = opt.invertible .* sign(raw(idx));
       
        [U,S,V] = svd( raw );
        if( ~isvector(S) );
            S = diag( S ); end;
        idx = abs( S )<=opt.invertible;
        S(idx) = opt.invertible .* sign(S(idx));
        S = diag( S );
        raw = U*S*V';
   
    elseif( opt.hermitian );
        raw = (raw + raw')./2;
   
    elseif( opt.antihermitian);
        raw = (raw - raw')./2;
        
    % further transformations
    if( opt.postprocess );
        elseif( type>3 );
            if( randi(10) == 1 );
                raw = triu( raw ); end;
            if( randi(4) == 1 );
                raw = fliplr( raw ); end;        
            if( randi(4) == 1 );
                raw = flipud( raw ); end;                
            if( randi(4) == 1 );
                raw = raw.'; end;
            if( randi(6) == 1 );
                raw = round( raw*randi( 10 ) )/randi( 10 ); end;
            if( randi(20) == 1 && all(isfinite(raw(:))) && ~issparse(raw) && sz<100 );
                raw = pinv( raw ); end; end;
    
    if( isequal(opt.allowinf, true) && state.lvl > .9 && rand > .7 );
        raw( rand(size(raw)) > state.lvl ) = (rand-.5)*inf; 
    else;
        raw( isinf(raw) ) = randi( 10 ); end;
    if( isequal(opt.allownan, true) && state.lvl > .9 && rand > .7 );
        raw( rand(size(raw)) > state.lvl ) = nan; 
    else;
        raw( isnan(raw) ) = randi( 10 ); end; end;
        
    % sparse is missing
    if( ~isempty(opt.allowsparse) && isequal(opt.allowsparse, false) );
        raw = double( raw ); end;
    if( islogical(raw) );
        raw = double( raw ); end;    
    while( true );
        if( ~isempty(opt.minnorm) && norm(raw) < opt.minnorm ); 
            raw = raw/norm(raw)*opt.minnorm; 
            continue; end;
        if( ~isempty(opt.maxnorm) && norm(raw) > opt.maxnorm ); 
            raw = raw/norm(raw)*opt.maxnorm;
            continue; end;
        if( ~isempty(opt.minval) && min(raw(:)) < opt.minval );
            raw(raw < opt.minval) = opt.minval;
            continue; end;
        if( ~isempty(opt.maxval) && max(raw(:)) > opt.minval );
            raw(raw > opt.maxval) = opt.maxval;continue; end;
        break; end;
    
    szeout = size( raw );
    assert( szeout(1) == szeout(2) && minsze <= szeout(1) && szeout(1) <= maxsze, 'TTEST:fatal', 'Programming error.' );
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
