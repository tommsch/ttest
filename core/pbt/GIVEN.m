function [ ret ] = GIVEN( varargin );

%% %%%%%%%%%%%
% get test_id
%%%%%%%%%%%%%%
if( numel(varargin) >= 1 && isa(varargin{1}, 'ttest_id_c') );
    id = varargin{1};
    varargin(1) = [];
elseif( evalin( 'caller', 'exist(''ttest_id'',''var'');' ) ); %id not given
    id = evalin( 'caller', 'ttest_id;' );
else;
    id = ttest_id_c( 0 ); end;

[opt, ret, str, varargin] = parse_input( id, varargin{:} );

[ret, str] = rerun_old_test( opt, ret, str );

%% run new tests
if( isa(varargin{end}, 'char') );  
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % everything given as string
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % cannot be put into subfunction due to use of evalin/assignin
    func = varargin{end};
    stname = varargin(1:end-1);
    
    st = cell( size(stname) );
    for i = 1:numel( st );
        st_lookup = stname{i};
        if( ischar(st_lookup) );
            st_lookup = stname{i};
            while( st_lookup(end) == '_' || ...
                   st_lookup(end) >= '0' && st_lookup(end) <= '9' ...
                 );
                st_lookup(end) = []; end;
            st{i} = TTEST( 0, 'pbt', st_lookup );
        elseif( isa(st_lookup, 'ttest_strategy_c') );
            error( 'TTEST:GIVEN', 'This mixed form where the strategies are given as variables and the command is given as string is not possible.' );
        else;
            error( 'TTEST:GIVEN', 'Wrong arguments given,' ); end; end;
    
    assert( numel(unique(stname)) == numel(stname), 'TTEST:GIVEN', 'Strategies with the same name must not be used multiple times. Append to the strategies numbers or underscores if necessary.' );
    
    wh = evalin( 'caller', 'whos;' );
    names = cell( 1, numel(wh) );
    [names{:}] = wh.name;
    for i = 1:numel( stname );
        if( any(strcmp(names, stname{i})) );
            fprintf( 2, 'Variable present in workspace which has the same name as an argument.\n  Variable name: %s\n. Type ''D'' to delete the variable or anything else to abort the test: ', stname{i} ); 
            ask = input( '' ,'s');
            if( strcmp(ask, 'D') );
                % do nothing. Variable is deleted afterwards
            else;
                error( 'TTEST:GIVEN', 'Test aborted.' ); end; end; end;
    clearstr = ['clear ' sprintf( '%s ', stname{:} )];
    
    timings = 0;  % we cannot start with empty array, since then timings(end) throws an error in the first round
    ret = true;
    try;
        exlist = {};
        for numtest = 1:opt.maxexample;
            if( sum(timings) + median(timings) >= opt.maxtime );
                break; end;
            starttic = tic;
            str = GIVEN_sprintf_example_beforeloop( str, opt );
            ex = cell( 1, numel(st) );
            
            try;            
                for j = 1:MAXNUM_TRY_DRAW;
                    for i = 1:numel( st );
                        ex{i} = st{i}.example();
                        assignin( 'caller', stname{i}, ex{i} ); end;
                    exhash = thash( ex );
                    if( ~any(strcmp(exlist, exhash)) );
                        exlist = [exlist exhash];  %#ok<AGROW>
                        break; end; end;
                if( j == MAXNUM_TRY_DRAW );
                    break; end;
                str = GIVEN_sprintf_example_afterloop( str, opt, ex );

                rngstate = rng();

                ret = evalin( 'caller', func );
                if( ~ret );
                    str = GIVEN_sprintf_failedtest( str, opt, ex );
                    h = evalin( 'caller', ['@() ' func ';' ] );
                    str_ = saveexample_wrapper_stringstyle( opt.name, h, ex, rngstate );
                    str = [str str_];  %#ok<AGROW>
                    break; end;
            catch me;
                ret = 0;
                str = [str 'Error thrown. Test failed.' newline me2str(me)];  %#ok<AGROW>
                break; end;

            timings(numtest) = toc( starttic ); end;  %#ok<AGROW>
           
    catch me;  %#ok<NASGU>
        end;  % do nothing. this is just for cleanup of assignin-ed variables.
    
    if( ~isempty(stname) );
        evalin( 'caller', clearstr ); end;
    
    
elseif( isa(varargin{end}, 'function_handle') );
    if( numel(varargin) == 1 );
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % implicit form, only function handle given
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % get variable names
        func = varargin{1};
        [ varname, ~, ~ ] = variablename( func );
        varname = varname{1};
        st = cell( 1, numel(varname) );
        for i = 1:numel( varname );
            lookupname = strtrim( varname{i} );
            while( true );
                if( lookupname(end) == '_' );
                    lookupname(end) = []; 
                    continue; end;
                if( lookupname(end) >= '0' && lookupname(end) <= '9' );
                    lookupname(end) = []; 
                    continue; end;
                break; end;
            lookupst =  TTEST( 'global', 'pbt', lookupname );
            if( isempty(lookupst) );
                % check if variables are present in callers workspace
                check = evalin( 'caller', ['exist( ''' varname{i} ''', ''var'' );'] );
                if( check );
                    st{i} = deepcopy( evalin( 'caller', ['' varname{i}; ''] ) ); 
                else;
                    error( 'TTEST:GIVEN', 'Found no strategy with shorthand %s, nor a variable with this name in the callers workspace.', varname{i} ); end; 
            else;
                st{i} = deepcopy( TTEST( 'global', 'pbt', lookupname ) ); end; end;
            


        [ret_, numtest, str_, opt] = GIVEN_explicit( func, opt, st{:} ); 
        
        ret = ret && ret_;
        str = [str str_];
        
    elseif( all(cellfun('isclass', varargin(1:end-1), 'ttest_strategy_c') | cellfun('isclass', varargin(1:end-1), 'char')) );
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % implicit form, function handle and strategies given
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [ret_, numtest, str_, opt] = GIVEN_explicit( varargin{end}, opt, varargin{1:end-1} );
        ret = ret && ret_;
        str = [str str_];
        
    else;
        error( 'TTEST:GIVEN', 'Wrong arguments given.' ); end;
else;
    error( 'TTEST:GIVEN', 'Wrong arguments given.' ); end;



if( opt.verbose >= 1 );
    fprintf( 2, '%s', str ); end;
if( ~ret );
    EXPECT_FAIL( id, 'Parametrized test failed.' ); end;
if( ret && numtest < opt.minexample );
    ret = false;
    EXPECT_FAIL( id, 'Too less examples generated. Test failed.' ); end;

end

function [ ret, numtest, str, opt ] = GIVEN_explicit( func, opt, varargin );

    str = '';
    ret = true;

    %% parse input
    st = varargin;
    
    for i = 1:numel( st );
        if( isa(st{i},'char') );
            st{i} = deepcopy( TTEST( 'global', 'pbt', st{i} ) ); end; end;
    
    % get variable names
    [ ~, ~, varidx ] = variablename( func );
    varidx = varidx{1};
    try;
        if( isoctave );
            err = lasterror(); end;
        if( isempty(varidx) && nargin(func) == 1 );  % case of function handle (i.e. not anonymous function, e.g. @isinf instead of @(x) isinf(x) )
            varidx = 1; end;
    catch me;  % octave cannot use nargin for builtins
        if( any(strfind(me.message, 'built-in')) );
            lasterror( err );
            varidx = 1;
        else;
            error( 'TTEST:fatal', 'Some internals of Octave or Matlab seems to have changed and the code needs to be adapted.' ); end; end;
    
    ex = cell( 1, numel(st) );
    timings = 0;  % we cannot start with empty array, since then timings(end) throws an error in the first round
    exlist = {};
    
    assert( numel(varidx) == numel(st), 'TTEST:GIVEN', 'Number of strategies given is different from number of input arguments of function handle.' );
    
    for numtest = 1:opt.maxexample;
        if( sum(timings) + timings(end) >= opt.maxtime );
            break; end;
        starttic = tic;
        str = GIVEN_sprintf_example_beforeloop( str, opt );
        try;
            for j = 1:MAXNUM_TRY_DRAW;
                for i = 1:numel( st );
                    ex{i} = st{varidx(i)}.example(); end;
                exhash = thash( ex );
                if( ~any(strcmp(exlist, exhash)) );
                    exlist = [exlist exhash];  %#ok<AGROW>
                    break; end; end;
            if( j == MAXNUM_TRY_DRAW );
                break; end;
            str = GIVEN_sprintf_example_afterloop( str, opt, ex );

            rngstate = rng();

            ret = feval( func, ex{:} );
            if( ~ret );
                str = GIVEN_sprintf_failedtest( str, opt, ex );
                str_ = saveexample_wrapper_functionstyle( opt.name, func, ex, rngstate );
                str = [str str_];  %#ok<AGROW>
                break; end;
        catch me;
            ret = 0;
            str = [str 'Error thrown. Test failed.' newline me2str(me)];  %#ok<AGROW>
            break; end;
        
        timings(numtest) = toc( starttic ); end; %#ok<AGROW>

end

%% helper functions

function [ opt, ret, str, varargin ] = parse_input( id, varargin );
    % parse options
    [ opt.name,varargin ]        = tt.parsem( {'name'}, varargin, '' );  % prefix of name where to store failed examples
    if( isempty(opt.name) );
        opt.name = generate_name( varargin{:} ); end;
    [ opt.maxtime, varargin ]    = tt.parsem( {'maxtime','timeout'}, varargin, 30 );  % approximate maximum execution time
    [ opt.maxexample, varargin ] = tt.parsem( {'maxnumexample','maxexamples','maxexample','maxtest','max_examples'}, varargin, 200 );
    [ opt.minexample, varargin ] = tt.parsem( {'minnumexample','minexamples','minexample','mintest','min_satisfying_examples'}, varargin, 5 );
    [ opt.rng, varargin ]        = tt.parsem( {'rng','derandomize'}, varargin, [] );
    if( ~isempty(opt.rng) );
        rng( opt.rng ); end;
    opt.verbose = TTEST( id, 'verbose' );

    % parse properties, UNUSED CODE currently
    [ opt.prop.assoziative, varargin ] =    tt.parsem( {'assoziative','associative'}, varargin );
    [ opt.prop.commutative, varargin ] =    tt.parsem( {'commutative'}, varargin );
    [ opt.prop.constant, varargin ] =       tt.parsem( {'constant'}, varargin );    
    [ opt.prop.idempotent, varargin ] =     tt.parsem( {'idempotent'}, varargin );
    [ opt.prop.innerproduct, varargin ] =   tt.parsem( {'innerproduct','inner_product','scalarproduct','scaler_product'}, varargin );
    [ opt.prop.monotone, varargin ] =       tt.parsem( {'monotone'}, varargin );
    [ opt.prop.nilpotent, varargin ] =      tt.parsem( {'nilpotent'}, varargin );

    opt.prop.any = any( cell2mat( struct2cell( opt.prop ) ) );

    for i = 1:numel( varargin );
        if( isa(varargin{i},'string') );
            varargin{i} = char( varargin{i} ); end; end;

    ret = true;
    str = '';
end

function [ ret, str ] = rerun_old_test( opt, ret, str );
    try;
        [ret_, str_] = rerun_old_tests( opt );
        ret = ret && ret_;
        str = [str str_];
    catch me;
        warning( 'GIVEN:rerun', '\nRerunning old tests failed.' );
        str = [str newline 'Warning: GIVEN:rerun, Rerunning old tests failed. Error: ' newline me2str(me) newline ];
        ret = true; end;
end

function [ ret, str ] = rerun_old_tests( opt );
    str = '';
    ret = true;
    basepath = fileparts( which( 'TTEST' ) );
    path = fullfile( basepath, 'tmp', 'fail', opt.name );
    lst = dir( [path '*.mat'] );
    for i = 1:numel( lst );
        ct = load( fullfile( lst(i).folder, lst(i).name ) );
        ct = ct.ct;
        if( ismatlab );
            [str_, ret] = evalc( 'ct.run()' );  % for some unknown reasons, this does not work on Octave 9.1
        else;
            [str_, ret] = evalc( 'testhandle( ct.test, ct.rngstate );' ); end;  % this is more fragile. If something in the implementation changes, this will not work anymore.
        str = [str str_];  %#ok<AGROW>
        if( ~ret );
            str = [str 'Old example failed' newline];  %#ok<AGROW>
            str = GIVEN_sprintf_failedtest( str, opt, ct.ex );
            ct.numsucceed = 0;
        else;
            ct.numsucceed = ct.numsucceed + 1; end;
        if( ct.numsucceed == 3 );
            delete( fullfile( lst(i).folder, lst(i).name ) );
        else;
            if( ismatlab );
                save( fullfile( lst(i).folder, lst(i).name ), 'ct' );
            else;
                save( '-binary', fullfile( lst(i).folder, lst(i).name ), 'ct' ); end; end; end;
end

%% print functions

function [ str ] = GIVEN_sprintf_example_beforeloop( str, opt );
    if( opt.verbose >= 2 );
        fprintf( 'Generated example with arguments:\n' ); end;
end

function [ str ] = GIVEN_sprintf_example_afterloop( str, opt, ex );
    if( opt.verbose >= 2 );
        for i = 1:numel( ex );
            fprintf( '%s\n', TTEST_DISP( [], ex{i} )); end; end;
end

function [ str ] = GIVEN_sprintf_failedtest( str, opt, expandedarg_ );
    if( opt.verbose >= 1 );
        str = [str 'Test failed with example:' newline];
        str = [str TTEST_DISP( [], expandedarg_{:} ) newline]; end; 
end

%% save example

function [ str ] = saveexample_wrapper_functionstyle( name, func, ex, rngstate );
    ct.test = @() feval( func, ex{:} );
    ct.rngstate = rngstate;
    ct.ex = ex;
    str = saveexample( name, ct );
end

function [ str ] = saveexample_wrapper_stringstyle( name, handle, ex, rngstate );
    ct.test = handle;
    ct.rngstate = rngstate;
    ct.ex = ex;
    str = saveexample( name, ct );
end

function [ str ] = saveexample( prefix, ct );
    str = '';
    dd = dir( fullfile( fileparts(which('TTEST')), 'tmp', 'fail', [prefix '*.mat'] ) );
    if( ~isempty(dd) );
        return; end;

    ct.run = @() testhandle( ct.test, ct.rngstate );
    ct.numsucceed = 0;
    savefail = 'file';  % TTEST( id, 'savefail' );
    time = clock;  %#ok<CLOCK>
    time = num2str( round(time), '%02i' );
    time(time==' ') = [];
    varname = [prefix '_' time];

    switch savefail;
        case {'no','none',false,'false'};
            %do nothing
        case {'file','mat'};
            filename = fullfile( fileparts(which('TTEST')), 'tmp', 'fail', varname );  % directory where to save failed example
            if( ismatlab() );
                save( [filename '.mat'], 'ct' );
            else;
                save( '-binary', [filename '.mat'], 'ct' ); end;
            if( ismatlab() );
                str = ['<a href="matlab:' varname ' = load( ''' filename '.mat'' ); ' varname '.ct.run()">!! Rerun failed test. !!</a>']; end;
         case {'base'};
             assignin( 'base', varname, handle );
             if( ismatlab() );
                 str = ['<a href="matlab:evalin(''base'', ''' varname '.ct.run()'' )">Rerun failed test.</a>']; end;
        otherwise;
            error( 'TTEST_SAVEEXAMPLE:TTEST_SAVEFAIL', 'Wrong value for ''TTEST_SAVEFAIL''.' ); end;
end

function [ ret ] = testhandle( h, rngstate );
    rng( rngstate );
    ret = h();
end

%% Constants
function [ ret ] = MAXNUM_TRY_DRAW; ret = 30; end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
