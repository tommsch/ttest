function [] = MESSAGE( varargin );
% Prepares a message to be printed out when a test fails
% MESSAGE();
% MESSAGE( varname_1, ..., varname_n );
% MESSAGE( fprintf-arguments );
%
% Input:
%   varname_i               string, name of the variable to printed out when a test failes
%   fprintf-arguments       arguments to fprintf
%
% Written by: tommsch, 2021,

%               tommsch, 16-07-2024, Behaviour change: MESSAGE now accepts variable names, instead of the variables itself. MESSAGE is much faster now, and can be used before a function is defined
% Changelog:    
    
    type = parse_input_type( varargin{:} );
    
    switch type
        case 'clear';
            evalin( 'caller', 'clear ttest_message_error' );

        case 'fprintf';
            [str, errmsg] = sprintf( varargin{:} ); 
            if( ~isempty(errmsg) );
                error( 'TTEST:message', '%s', errmsg ); end;
            assignin( 'caller', 'ttest_message_error', str );

        case 'vars';
            assignin( 'caller', 'ttest_message_error', varargin );

        case '';
            error( 'TTEST:message', 'Input could not be parsed.' );

        otherwise;
            fatal_error; end;
end


function [ type ] = parse_input_type( varargin );

    if( numel(varargin) == 0 );
        type = 'clear';
        return; end;

    input_vars = (cellfun( 'isclass', varargin, 'char' ) | cellfun( 'isclass', varargin, 'string' )) & ...
                 cellfun( @isvarname, varargin );
    vars_given = all( input_vars );
    if( vars_given );
        type = 'vars';
        return; end;

    fprintf_given = numel(varargin) >= 1 && ...
                    (ischar( varargin{1} ) || isstring( varargin{1} ));
    if( fprintf_given );
        type = 'fprintf';
        return; end;

    type = '';

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
