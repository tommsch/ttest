function [ summary, ret ] = ttest_check_installation()
% Checks whether TTEST is installed
% Returns a struct, each entry corresponds to a test
% Value is true if test worked, false otherwise
%
%   umacro          upper case macros (e.g. EXPECT_EQ)
%   lmacro          lower case macros (e.g. tt.expect_eq)
%   matcher         matchers
%   thirdparty      functions in thirdparty folder
%   TTEST           checks whether TTEST can be called
%   OPTION          checks whether OPTIONS class is working
%   tmpfolder       checks whether folders for goldstandards, etc... are present
%   octavepath      checks whether shims for octave are installed correctly (and not installed in Matlab installations)
%   badpath         checks whether folders which should not be on the path are really not on the path
%

%#ok<*TRYNC>
    [e_msg, e_id] = lasterr();  %#ok<LERR>
    summary = true;
    ret = struct;
    [ret.badpath,       summary] = test_must_succeed_and_return_true( 'try; x = false; ttest_this_function_should_not_be_on_path; catch; x = true; end;', summary );

    [ret.octavepath,    summary] = test_must_succeed_and_return_true( ['if( exist (''OCTAVE_VERSION'', ''builtin'') );' newline ...
                                                                       '    x = contains( ''a'', ''a'' );' newline ...
                                                                       'else;' newline ...
                                                                       '    p = lower( path );' newline ...
                                                                       '    x = ~contains( path, [''utility'' filesep ''octave'' filesep ''6.3''] ); end' newline ...
                                                                      ], summary );    
    [ret.tmpfolder,     summary] = test_must_succeed_and_return_true( ['olddir = pwd; cleandir = onCleanup( @() cd(olddir) ); ' newline ...
                                                                       'cd( fileparts(which(''TTEST'')) ); ' newline...
                                                                       'cd tmp; cd fail; cd ..; cd goldstandard; cd(olddir); ' newline ...
                                                                       'x = 1;' newline ...
                                                                       ], summary );        
    [ret.OPTION,        summary] = test_must_succeed_and_return_true( 'x = OPTION( ''1'' ); x = isequal(x.id,''1'')', summary );
    [ret.TTEST,         summary] = test_must_succeed_and_return_true( 'x = TTEST( ''ver'' ); x = ischar( x );', summary );
    [ret.thirdparty,    summary] = test_must_succeed_and_return_true( 'x = tt.parsem( ''val'', {''a'',''b''}, 1 );', summary );
    [ret.matcher,       summary] = test_must_succeed_and_return_true( 'H = tt.Eq(2); x = ~H.m( 3 )', summary );
    [ret.lmacro,        summary] = test_must_succeed_and_return_true( 'x = ~tt.expect_eq( 2, 3 );', summary );
    [ret.umacro,        summary] = test_must_succeed_and_return_true( 'x = ~EXPECT_EQ( 2, 3 );', summary );
    lasterr( e_msg, e_id );  %#ok<LERR>
end

function [ ret, summary ] = test_must_succeed_and_return_true( cmd, summary );
    ret = false;
    try;
        evalc( cmd );
        assert( x == true ); 
        ret = true; 
        summary = summary && (x == true); 
    catch me;  %#ok<NASGU>
        summary = false;
        end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
