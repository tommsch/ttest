function TTEST_DELETE( filename, message, verbose );
% deletes the file with name 'filename'
% silently deletes files starting with 'message', default: '%TTEST_AUTOGENERATE'
    
    ismatlab = ~exist( 'OCTAVE_VERSION', 'builtin' );

    if( nargin <= 1 || isempty(message) );
        message = '%TTEST_AUTOGENERATE'; end;
    if( nargin <= 2 || isempty(verbose) );
        verbose = 1; end;
    
    if( ismatlab );
        recycle( 'on' ); end;
    filename_list = dir( filename );
    if( isempty(filename_list) );
        %fprintf( 2, 'TTEST_DELETE: Could not find file(s): %s. No changes to filesystem were done.\n', filename );
        return; end;
    for i = 1:numel( filename_list );
        filename = fullfile( filename_list(i).folder, filename_list(i).name );
        try;
            [e_msg, e_id] = lasterr();  %#ok<LERR>  % we cannot use `lasterror( err )` here, because the overload of `lasterror()` may not be on the path
            
            val = fileread( filename );
            silentdelete = numel( val ) >= numel( message ) && ...
                           strcmp( val(1:numel(message)), message );
        catch me;
            if( verbose >= 0 );
                if( isequal( me.identifier, 'MATLAB:fileread:cannotOpenFile' ) || ...
                    isequal( me.message, 'fileread: cannot open file' ) ...
                  );
                    lasterr( e_msg, e_id );  %#ok<LERR>  
                else;
                    warning( 'TTEST:DELETE', 'Some strange failure occured.\n Cannot delete file: %s', filename ); end; end;
            if( verbose >= 1 );
                warning( 'TTEST:DELETE', 'Cannot delete file: %s', filename ); end;
            return; end;
    
        if( ~silentdelete );
            fprintf( 'Do you want to delete the file: %s\n', filename );
            a = input( 'y/n: ', 's' ); 
            if( isequal(a, 'y') );
                silentdelete = true; end; end;
    
        if( silentdelete );
            delete( filename ); end; end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
