function [ varargout ] = clearat( varargin );
% Clear injected code using the at-functions
% [ varargout ] = clearat( 'in', n1, ..., nn );
% [ varargout ] = clearat( ['all'] );
% [ varargout ] = clearat( ['inputat'] );
%
% Input:
%   'in', n1, ..., nn       strings, function names where to inject the assign statement.
%   'all'                   if called with no arguments, or with argument 'all', all TTEST injections are cleared
%   'inputat'               Only temporal inputat injections are cleared
% Output:
%   out                     This is always set to []
%
% Notes: 
%   - The code injection is made via conditional breakpoints. 
%     The clearat function only deletes those breakpoints, which were set by a TTEST at-function
%   - For more information see assignat
%
% E.g.: 
%   evalat( 'in','evalat', 'at','%<LABEL>', 'eval','fprintf(''Hello World\n'')' ); 
%   evalat();  %prints out "Hello World"
%   clearat( 'in','evalat' ); %clear injected code
%
% See also: assignat, evalat, captureat, inputat, flowat, clearat
%
% Written by: tommsch, 2021-03-21

%               tommsch,    2024-09-05,     Behaviour change: Function now returns an empty array as output
% Changelog: 

    persistent dbname;  % database storing filenames
    if( isequal(dbname, []) );
        dbname = {}; end;
    
    if( nargout > 0 );
        varargout = {[]}; end;
    
    if( numel( varargin ) == 1 && ...
        (strcmp( varargin{1}, 'inputat' ) || strcmp( varargin{1}, '-inputat' )) ...
      );
        clearat_inputat();
        return;
    elseif( numel( varargin ) == 0 || ...
        numel( varargin ) == 1 && (strcmp( varargin{1}, 'all' ) || strcmp( varargin{1}, '-all' )) ...
      );
        [data, dbname] = clearat_all( dbname );
    elseif( numel( varargin ) == 2 && ...
            (strcmp( varargin{1}, 'save' ) || strcmp( varargin{1}, '-save' )) ...
          );
        dbname{end+1} = varargin{2};
        return;
    else;
        data = ttest_at_parse_input( varargin{:} ); end;  % parse input
    
    for i = 1:numel( data.in );  % loop through files
        db = dbstatus( data.in{i} );
        for j = 1:numel( db )
            if( numel(db(j)) == 0 || isempty(db(j).line) );
                continue; end;
            if( ismatlab );
                idx = false( 1, numel(db(j).line) );
                for k = 1:numel( db(j).line );
                    if( any(strfind(db(j).expression{k}, 'ttest')) );
                        idx(k) = true; end; end;
                db(j).line(idx) = [];
                db(j).expression(idx) = [];
                db(j).anonymous(idx) = []; 
            else;
                if( any(strfind(db(j).cond, 'ttest')) );
                    db(j).line = [];
                    db(j).cond = []; end; end; end;
        try;
            % we first remove all breakpoints, and set them again afterwards - This is a method compatible with Matlab and Octave
            if( ismatlab );
                dbclear( data.in{i} );
            else;
                current_dir = pwd;
                clean_dir_db = onCleanup( @() cd(current_dir) );
                [data_in_path, data_in_name] = fileparts( data.in{i} );
                cd( data_in_path );
                dbclear( data_in_name );
                clear clean_dir_db; end;
        catch me;
            % swallow potential wrong octave warnings
            if( strfind(me.message, 'unable to find function') );
                % do nothing
            else;
                fprintf( 2, '%s\n', me2str( me ) );
                end; end;
        if( numel([db.line]) > 0 );
            dbstop( db ); end; end;

end

function clearat_inputat();
    clear inputat; 
end

function [ data, dbname ] = clearat_all( dbname );  %#ok<INUSD>
    clearat_inputat();
    db = dbstatus();
    data.in = {db.file};
    dbname = {};
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
