function [ data ] = ttest_at_parse_input( varargin );

 %#ok<*AGROW>
    [data.in,      data.at,    data.varname, data.varvalue, ...
     data.evalcmd, data.input, data.capture, data.group, ...
     data.type ] = deal( {} );
    data.once = false;
 
    if( numel(varargin) == 0 );
        return; end;
    
    if( strcmpi( varargin{1}(end-1:end), 'in' ) );
        dash = varargin{1}(1:end-2);  % prefix before ''in'';
    elseif( strcmpi( varargin{1}(end-4:end), 'input' ) );
        dash = varargin{1}(1:end-5);  % prefix before ''in'';
    else;
        error( 'ttest_at:input', 'First option must be ''in'' or ''input''.' ); end;
    
    ndash = numel( dash );
    i = 1;
    while( i <= numel(varargin) );
        opt_i = lower( varargin{i}(ndash+1:end) );
        switch opt_i;
            case {'in','at','assign','eval','input','capture','group','type'};
                assert( numel( varargin{i} ) >= ndash && ...
                        isequal( varargin{i}(1:ndash), dash ), ...
                        'ttest:at', 'The prefix of all options must be the same.\n  Given prefix for ''in'' (which is ''%i'')\n  is not the same the prefix for option %i\n', dash, i );
                opt = opt_i;
            otherwise;
                i = i - 1;  % do not change opt;
                end; 
            
        switch opt;
            case 'in';
                assert( ischar(varargin{i+1}), 'ttest_at:in', 'Filename must be a string or character vector.' );
                data.in{end+1} = varargin{i+1};

            case 'at'; 
                if( isnumeric(varargin{i+1}) );
                    data.at = [data.at num2cell(varargin{i+1})];
                elseif( ischar(varargin{i+1}) );
                    data.at{end+1} = varargin{i+1};
                else;
                    error( 'ttest_at:at', 'Wrong format for line numbers. Must be either a string or number(s)' ); end;

            case 'assign';
                if( isstruct(varargin{i+1}) );
                    error( 'ttest_at:fatal', 'not yet implemented' );
                else;
                    assert( numel(varargin) >= i+2, 'ttest_at:assign', 'Input missing. Arguments must be given as pairs: varname/varvalue.' ); 
                    assert( isvarname(varargin{i+1}), 'ttest_at:varname', 'Variable name is not a proper name.' );
                    data.varname{end+1} = varargin{i+1};
                    data.varvalue{end+1} = varargin{i+2}; end;
                i = i + 1;

            case 'eval';
                if( ischar(varargin{i+1}) || isa(varargin{i+1},'function_handle') );
                    data.evalcmd{end+1} = varargin{i+1};
                else;
                    error( 'ttest_at:eval', 'Only strings and functon handles are possible.' ); end;

            case 'input';
                if( ischar(varargin{i+1}) );
                    data.input{end+1} = varargin{i+1}; 
                else;
                    error( 'ttest_at:string', 'Only strings are possible.' ); end;;

            case 'capture';
                if( isequal(varargin{i+1}, 0) || isequal(varargin{i+1}, '0') );
                    % do nothing == capture whole workspace
                elseif( ischar(varargin{i+1}) );
                    assert( isvarname(varargin{i+1}), 'ttest_at:varname', 'Variable name is not a proper name.' );
                    data.capture{end+1} = varargin{i+1}; 
                else;
                    error( 'ttest_at:string', 'Only strings or the value 0 are possible.' ); end;

            case 'group';
                if( isequal(varargin{i+1}, 0) || isequal(varargin{i+1}, '0') );
                    data.group{end+1} = '0'; 
                elseif(ischar(varargin{i+1}) );
                    assert( isvarname(varargin{i+1}), 'ttest_at:varname', 'Group name is not a proper name.' );
                    data.group{end+1} = varargin{i+1}; 
                else;
                    error( 'ttest_at:string', 'Only strings or the value 0 are possible.' ); end;

            case 'type';
                switch varargin{i+1};
                    % cases for flow function
                    case 'abort';   data.type{end+1} = 'abort';
                    case 'failure'; data.type{end+1} = 'failure';
                    case 'expect';  data.type{end+1} = 'expect';

                    % cases for input function
                    case {'t','temporal','g','global'}; data.type{end+1} = 'global';
                    case {'s','spatial','l','local'};   data.type{end+1} = 'local';

                    otherwise; error( 'ttest_at:flow_input', 'Wrong type' ); end;

            case 'once';
                data.once = varargin{i+1};
            
            otherwise;
                fatal_error; end;
        i = i + 2; end; 
    
    if( isempty(data.at) );
        data.at = {1}; end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 

%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
