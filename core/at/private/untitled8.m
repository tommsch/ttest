
clc
ct = ct + 1
rng( ct );
%[T,~,prop] = gallery_matrixset( 'kone_regression_17' )
[T, ~, prop] = gallery_matrixset( 'matlab', 'dim',[1 3], 'N',2, 'poslead','m', 'nonneg','n' )
%randomize_matrixset( T, 'poslead' )

matrixinfo( prop, 'compact' )
vdisp( T )
prop.name
size( T{1}, 1 )
%prop.has_invariant_kone
%%
figure(4)
nfo = invariant_kone( T, 'method','et', 'algorithm','K', 'maxsmpdepth',2, 'plot','polytopeallpoint', 'sym',0 );


B = [nfo.blockvar{1}.cyclictree.VV{:}];
idx = ~[nfo.blockvar{1}.cyclictree.has_children{:}];
n1 = double( polytopenorm( T{1}*B(:,idx), B, 'k' ) );
n2 = double( polytopenorm( T{2}*B(:,idx), B, 'k' ) );
max( [n1(2,:) n2(2,:)] )
prop.name
%%
figure(5)
clf; hold on;
plotm( B, 'b', 'Kone', 'hull', 'lw',2 )
plotm( T{1}*B, 'r', 'Kone', 'hull' )
plotm( T{2}*B, 'g', 'Kone', 'hull' )
