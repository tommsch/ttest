function [ varargout ] = SECTION( varargin )
% [] = SECTION( [id], [name], [options] )
% Starts a section, ends all open sections, and subsections
%
% See also: TESTCASE, SECTION, SUBSECTION, ENDTESTCASE, ENDSECTION, ENDSUBSECTION

    %% get ttest_id
    if( numel(varargin) >= 1 && isa(varargin{1}, 'ttest_id_c') );
        id = varargin{1};
        varargin(1) = [];
    elseif( evalin( 'caller', 'exist( ''ttest_id'', ''var'' );' ) );  % id not given
        id = evalin( 'caller', 'ttest_id;' );
    else;
        id = ttest_id_c( 0 ); end;

    

    %% parse input
    [ name, opt, fh ] = parse_section_option( varargin{:} );

    %% execute fh and return early if applicable
    if( ~isempty(fh) )
        if( isa(fh, 'function_handle') );
            [~, ret] = evalc( 'fh();' );
        elseif( ischar(fh) || isstring(fh) );
            [~, ret] = evalc( [fh ';'] );
        else;
            error( 'TTEST:section', 'Wrong options given to section command.' ); end;
        if( nargout >= 1 );
            varargout{1} = ret; end;
        if( ~ret );
            if( nargout == 0 );
                ASSUME_FAIL( id ); end;
            return; end; end;

    %% where are we?
    p = TTEST( id, 'pid' );
    pos = p.section;
        
    %% print output
    verbose = p.TTEST_VERBOSE;
    if( verbose >= 2 );
        fprintf( 'Section start: %s\n', name );
    elseif( verbose >= 1 );
        fprintf( '.' ); end;

    %% parse the output of where we are
    if( ~pos.section.on );
        % execute the teardown and setup code
        for fh = {opt.setup};
            if( isstring(fh{1}) || ischar(fh{1}) );
                evalin( 'caller', fh{1} );
            elseif( isa(fh{1}, 'function_handle') );
                assignin( 'caller', 'ttest_function_handle_e62b372993f8c39c9a43df6e8356e71b', fh{1} );
                evalin( 'caller', 'ttest_function_handle_e62b372993f8c39c9a43df6e8356e71b();' );
                evalin( 'caller', 'clear ttest_function_handle_e62b372993f8c39c9a43df6e8356e71b;' );
                end; end;

        savecell = {};
        TTEST( id, 'section', 'section','on',1, 'section','name',name );
        if( opt.vars );  % opt vars has to be processed here, because we use evalin
            vars = evalin( 'caller', 'who' );
            ws = [];
            if( numel(vars) >= 1 );
                ws(numel( vars )).name = [];
                ws(numel( vars )).value = []; end;
            for ii = 1:numel( vars );
                ws(ii).name = vars{ii};  %#ok<AGROW>
                ws(ii).value = evalin( 'caller', vars{ii} ); end;  %#ok<AGROW>
            savecell(end+1:end+3) = {'section','vars', ws }; end;
        
        store_section_data( id, opt, 'section', savecell );
        
    else;
        % if we are in some section or subsection, then we load the sectionsvars
        TTEST( id, 'section', 'section','on',1, 'subsection','on',0, 'section','name',name );

        if( opt.vars && isfield(pos.section, 'vars') );
            evalin( 'caller', 'clear;' );  % delete the caller workspace

            % load the sectionvars
            vars = pos.section.vars;
            for kk = 1:numel( vars );
                
                
                
                assignin( 'caller', vars(kk).name, vars(kk).value ); end; end; 
    
        load_section_data( opt, pos.section );

        % execute the teardown and setup code
        for fh = {pos.subsection.teardown, pos.section.teardown, ...
                  pos.section.setup };
            if( isstring(fh{1}) || ischar(fh{1}) );
                evalin( 'caller', fh{1} );
            elseif( isa(fh{1}, 'function_handle') );
                assignin( 'caller', 'ttest_function_handle_e62b372993f8c39c9a43df6e8356e71b', fh{1} );
                evalin( 'caller', 'ttest_function_handle_e62b372993f8c39c9a43df6e8356e71b();' );
                evalin( 'caller', 'clear ttest_function_handle_e62b372993f8c39c9a43df6e8356e71b;' );
                end; end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
