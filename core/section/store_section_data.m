function store_section_data( id, opt, sectiontype, savecell )
% stores the current "state" in the TTEST database
% description and format of stored data
%   basews      variables currently in base workspace
%               struct, key=variable-name, value=variable-values
%   db          ttest debug breakpoints
%               Format for Matlab: {dbstop-struct, time-of-last-call-to-debug}
%               Format for Octave: dbstop-struct
%   global      global variables
%               struct
%

    if( isconvertible_to_bool(opt.basews) && opt.basews );
        basews = get_base_ws();
        savecell(end+1:end+3) = {sectiontype, 'basews', basews }; end;

    if( opt.debug );
        db = get_ttest_breakpoints( dbstatus );
        if( ismatlab)
            db = {{db,overload_wrapper(nan, 'debug')}};
        else;
            % do nothing
            end;
        savecell(end+1:end+3) = {sectiontype, 'debug', db }; end;

    if( isempty(opt.figure) );
        savecell(end+1:end+3) = {sectiontype, 'figure', isempty( findall( 0, 'type','figure' ) ) };
    elseif( ~opt.figure );
        % do nothing
    elseif( opt.figure );
        fig_val = serialize_figures();
        savecell(end+1:end+3) = {sectiontype, 'figure', fig_val }; end;
    %savecell(end+1:end+3) = {sectiontype, 'figure', opt.fig};

    if( opt.global );
        gvars = who( 'global' );
        gws = store_global_ttest_gws_adhjkalshd37r278hjaksdf728zhdgjskhdfgasg( gvars );
        savecell(end+1:end+3) = {sectiontype, 'global', gws }; end;

    if( opt.path );
        p = {{path,overload_wrapper(nan, 'path')}};
        savecell(end+1:end+3) = {sectiontype, 'path', p };  end;

    if( opt.pwd );
        savecell(end+1:end+3) = {sectiontype, 'pwd', pwd }; end;

    if( opt.rng );
        savecell(end+1:end+3) = {sectiontype, 'rng', rng }; end;

    if( ~isequaln(opt.setup, nan) );
        savecell(end+1:end+3) = {sectiontype, 'setup', opt.setup}; end;

    if( ~isequaln(opt.teardown, nan) );
        savecell(end+1:end+3) = {sectiontype, 'teardown', opt.teardown }; end;

    if( opt.warning );
        savecell(end+1:end+3) = {sectiontype, 'warning', builtin( 'warning' ) }; end;


    if( ~isempty(savecell) );
        TTEST( id, 'section', savecell{:} ); end;

end

function [ basews ] = get_base_ws();
    basews = struct;
    bvars = evalin( 'base', 'who' );
    if( numel(bvars) >= 1 );
        basews(numel( bvars )).name = [];
        basews(numel( bvars )).value = [];
        for ii = 1:numel( bvars );
            basews(ii).name = bvars{ii};
            basews(ii).value = evalin( 'base', bvars{ii} ); end; end;
end

function [ db ] = get_ttest_breakpoints( db );
    % remove all non-ttest break points
    if( ismatlab );
        for i = numel( db ):-1:1;
            for j = numel( db(i).line ):-1:1;
                if( ~contains( db(i).expression{j}, 'ttest' ) );
                    db(i).line(j) = [];
                    db(i).expression(j) = [];
                    db(i).anonymous(j) = [];
                    end; end;
            if( isempty(db(i).line) );
                db(i) = []; end; end;
    else;
        for i = numel( db ):-1:1;
            if( ~contains( db(i).cond, 'ttest' ) );
                db(i) = []; end; end; end;
end


function [ fig ] = serialize_figures();
    h1 = findall( groot );
    fig = cell( 1, numel(h1) );
    for i = 1:numel( h1 );
        fig{i} = serialize( h1{i} ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
