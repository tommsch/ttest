function octave_section_warning( );
    persistent octave_bug_warning;
    err = lasterror();  %#ok<LERR>
    ver_check = isoctave() && verLessThan('octave', '8.0');
    lasterror( err );  %#ok<LERR>
    if( isempty(octave_bug_warning) && ver_check );
        octave_bug_warning = 1;
        warning( 'TTEST:Octave_bug', 'The variable containing the name of the current test suite cannot be restored in Octave.\n  Thus this variable is skipped. Nevertheless, this should not be a problem.\n  This is an Octave bug.\n  This warning is printed at most once.\n' );
        warning( 'off', 'TTEST:Octave_bug' ); end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
