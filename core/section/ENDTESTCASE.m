function ENDTESTCASE( varargin );
% [] = ENDTESTCASE( [id] )
% Ends all open testcases, sections, and subsections
% Clears teardown and setup functions of all open testcases, sections and subsections
%
% See also: TESTCASE, SECTION, SUBSECTION, ENDTESTCASE, ENDSECTION, ENDSUBSECTION

    %% get ttest_id
    if( numel(varargin) >= 1 && isa(varargin{1}, 'ttest_id_c') );
        id = varargin{1};
        varargin(1) = [];
    elseif( evalin( 'caller', 'exist(''ttest_id'',''var'');' ) );  % id not given
        id = evalin( 'caller', 'ttest_id;' );
    else
        id = ttest_id_c( 0 ); end;
    
    assert( numel(varargin) == 0, 'TTEST:SECTION', 'End-section functions only accept at most one argument of ttest_id_c (which is discarded currently).' );

    %% we do not care where we are, just close testcases, sections and subsections
    pos = TTEST( id, 'section', 'testcase','on',0 );
    assert( pos.testcase.on == true, 'TTEST:SECTION', 'No TESTCASE open. ENDTESTCASE can only be called, when a TESTCASE is open.' );

    %% clear workspace 
    evalin( 'caller', 'clear;' );

    %% load the testcase variables
    vars = pos.testcase.vars;
    for kk = 1:numel( vars );
        assignin( 'caller', vars(kk).name, vars(kk).value ); end;
        
    load_section_data( [], pos.testcase )

    % execute the teardown and setup code
    for fh = {pos.subsection.teardown, pos.section.teardown, pos.testcase.teardown, ...
             };
        if( isstring(fh{1}) || ischar(fh{1}) );
            evalin( 'caller', fh{1} );
        elseif( isa(fh{1}, 'function_handle') );
            assignin( 'caller', 'ttest_function_handle_e62b372993f8c39c9a43df6e8356e71b', fh{1} );
            evalin( 'caller', 'ttest_function_handle_e62b372993f8c39c9a43df6e8356e71b();' );
            evalin( 'caller', 'clear ttest_function_handle_e62b372993f8c39c9a43df6e8356e71b;' );
            end; end;

end        

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
