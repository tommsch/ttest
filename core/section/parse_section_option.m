function [ name, opt, fh ] = parse_section_option( varargin );
% Parses arguments passed to `TESTCASE`, `SECTION`, `SUBSECTION`
%
% Output:
%   name        the name of the section, i.e. the first argument of varargin
%   opt         struct
%   fh          something evalable or empty
%
% Written by: 2022, tommsch

    % set default values
    opt.basews =   false;
    opt.debug =    true;
    opt.figure =   [];
    opt.global =   true;
    opt.path =     true;
    opt.pwd =      true;
    opt.rng =      false;
    opt.setup =    nan;
    opt.teardown = nan;
    opt.vars =     true;
    opt.warning =  true;
    for i = numel( varargin )-1:-2:1;
        switch lower( varargin{i} );
            case {'base'};
                opt.basews = varargin{i+1};
                assert( isstruct( varargin{i+1} ) || ...
                        isconvertible_to_bool( varargin{i+1} ), ...
                        'runttTTESTests:section', 'The argument to %s must be a struct or convertible to a boolean.', varargin{i} );
                varargin(end-1:end) = [];
            case {'debug'};
                opt.debug= varargin{i+1};
                assert( isa( varargin{i+1}, 'struct' ) || ...
                        isconvertible_to_bool( varargin{i+1} ), ...
                        'TTEST:section', 'The argument to %s must be a struct or convertible to a boolean.', varargin{i} );
            case {'fig','figure'}
                opt.figure = varargin{i+1};
                assert( ischar(varargin{i+1}) || isstring(varargin{i+1}) || ...
                        isconvertible_to_bool( varargin{i+1} ), ...
                        'TTEST:section', 'The argument to %s must be a string or convertible to a boolean.', varargin{i} );
                varargin(end-1:end) = [];
            case {'global'};
                opt.global = varargin{i+1};
                assert( isstruct( varargin{i+1} ) || ...
                        isconvertible_to_bool( varargin{i+1} ), ...
                        'TTEST:section', 'The argument to %s must be a struct or convertible to a boolean.', varargin{i} );
            case {'path'};
                opt.path = varargin{i+1};
                assert( ischar(varargin{i+1}) || isstring(varargin{i+1}) || ...
                        isconvertible_to_bool( varargin{i+1} ), ...
                        'TTEST:section', 'The argument to %s must be a string or convertible to a boolean.', varargin{i} );
                varargin(end-1:end) = [];
            case {'pwd'};
                opt.pwd = varargin{i+1};
                assert( ischar(varargin{i+1}) || isstring(varargin{i+1}) || ...
                        isconvertible_to_bool( varargin{i+1} ), ...
                        'TTEST:section', 'The argument to %s must be a string or convertible to a boolean.', varargin{i} );
            case {'rng'};
                opt.rng = varargin{i+1};
                assert( isa(varargin{i+1}, 'struct') || ...
                        isconvertible_to_bool( varargin{i+1} ), ...
                        'TTEST:section', 'The argument to %s must be a string or convertible to a boolean.', varargin{i} );
            case {'setup','tearup'};
                opt.setup = varargin{i+1};
                assert( isa(varargin{i+1}, 'function_handle') || ...
                        ischar(varargin{i+1}) || isstring(varargin{i+1}) || ...
                        isempty(varargin{i+1}) || isequal(varargin{i+1}, 0), ...
                        'TTEST:section', 'The argument to ''%s'' must be a function handle or a string.', varargin{i} );
                varargin(end-1:end) = [];
            case {'teardown','setdown'};  % setup and teardown are not yet implemented correctly
                opt.teardown = varargin{i+1};
                assert( isa(varargin{i+1}, 'function_handle') || ...
                        ischar(varargin{i+1}) || isstring(varargin{i+1}) || ...
                        isempty(varargin{i+1}) || isequal(varargin{i+1}, 0), ...
                        'TTEST:section', 'The argument to ''%s'' must be a function handle or a string.', varargin{i} );
                varargin(end-1:end) = [];
            case {'ws','var','vars'};
                opt.vars = varargin{i+1};
                assert( isstruct( varargin{i+1} ) || ...
                        isconvertible_to_bool( varargin{i+1} ), ...
                        'TTEST:section', 'The argument to %s must be a struct or convertible to a boolean.', varargin{i} );
                varargin(end-1:end) = [];
            case {'warning'};
                opt.warning = varargin{i+1};
                assert( isstruct( varargin{i+1} ) && isfield(varargin{i+1}, 'identifier') && isfield(varargin{i+1}, 'state') || ...
                        isconvertible_to_bool( varargin{i+1} ), ...
                        'TTEST:section', 'The argument to %s must be a struct as returned by `warning()` or convertible to a boolean.', varargin{i} );
                varargin(end-1:end) = [];
            otherwise;
                % do nothing
                %error( 'TTEST:section', 'wrong option given. Unkown option: %s', varargin{i} );
                end; end;

    assert( numel(varargin) <= 2, 'TTEST:section', 'Only one additional argument is allowed.' );
    name = '';
    if( numel(varargin) >= 1 && ...
        (isstring(varargin{1}) || ischar(varargin{1})) ...
      );
        name = varargin{1};
        varargin(1) = []; end;
    if( isstring(name) );
        name = char( name ); end;

    fh = [];
    if( numel( varargin ) >= 1 && ...
        (isstring( varargin{1} ) || ischar( varargin{1} ) || isa( varargin{1}, 'function_handle') ) ...
      )
        fh = varargin{1};
        varargin(1) = []; end;  %#ok<NASGU>
    
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
