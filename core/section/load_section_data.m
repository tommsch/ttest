function load_section_data( opt, state );  %#ok<INUSL>  % opt is unused
% Restores the state to as stored in pos_type
%
% Input:
%   opt     option structed as parsed by parse_section_option
%   data    pos.(section) struct as stored in the TTEST database
%
% Written by: 2021, tommsch


    if( ~isfield(state, 'path') );  % check whether data contains information already
        return; end;

    if( ~isempty(state.basews) );
        evalin( 'base', 'clear;' );
        vars = state.basews;
        for kk = 1:numel( vars )
            assignin( 'base', vars(kk).name, vars(kk).value ); end; end;

    if( ~isequal(state.debug,[]) );
        if( ismatlab );
            if( overload_wrapper( state.debug{1}{2}, 'debug' ) )
                non_ttest_breakpoints = get_non_ttest_breakpoints( dbstatus );
                dbclear all;
                dbstop( non_ttest_breakpoints );
                dbstop( state.debug{1}{1} );
                end;
        else;
            db = dbstatus;
            if( ~isequal(db, state.debug) );
                non_ttest_breakpoints = get_non_ttest_breakpoints( db );
                for i = 1:numel( db );
                    [~, fn] = fileparts( db(i).file );
                    dbclear( fn ); end;
                dbstop( non_ttest_breakpoints );
                dbstop( state.debug ); end; end; end;

    if( ~isempty(state.figure) );
        if( isequal(state.figure, true) );
            close all hidden;
        elseif( isequal(state.figure, false) );
            
        else;
            warning( 'TTEST:figure', 'Figures cannot be restored currently.' ); end; end;
        
    if( ~isempty(state.global) );
        clearvars -global
        vars = state.global;
        for kk = 1:numel( vars )
            if( ~isempty(vars(kk).name) );
                eval( ['global ' vars(kk).name ';'] );
                assignin( 'caller', vars(kk).name, vars(kk).value );
            
            end; end; end;

    if( ~isempty(state.path) );
        if( overload_wrapper( state.path{1}{2}, 'path' ) );
            path( state.path{1}{1} ); end; end;

    if( ~isempty(state.pwd) );
        p = pwd;
        if( ~isequal(p,state.pwd) );
            cd( state.pwd ); end; end;  %#ok<MCCD>

    if( ~isempty(state.rng) );
        if( ~isequal(rng, state.rng) );
            rng( state.rng ); end; end;

    if( ~isempty(state.warning) );
        w = builtin( 'warning' );
        if( ~isequal(w,state.warning) );
            if( ismatlab );
                warning( state.warning );
            else;
                for i = 1:numel( state.warning );
                    warning( state.warning(i).state, state.warning(i).identifier ); end; end; end; end;

end

function [ db ] = get_non_ttest_breakpoints( db );
    if( ismatlab );
        for i = numel( db ):-1:1;
            for j = numel( db(i).line ):-1:1;
                if( contains( db(i).expression{j}, 'ttest' ) );
                    db(i).line(j) = [];
                    db(i).expression(j) = [];
                    db(i).anonymous(j) = [];
                    end; end;
            if( isempty(db(i).line) );
                db(i) = []; end; end;
    else;
        for i = numel( db ):-1:1;
            if( contains( db(i).cond, 'ttest' ) );
                db(i) = []; end; end; end;
end

function dummy; end   %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
