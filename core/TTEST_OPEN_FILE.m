function [ fid, cleanup ] = TTEST_OPEN_FILE( name, permission  );

    if( nargin<=1 || isempty(permission) );
        permission = 'r'; end;
    
    name_orig = name;
    fid = fopen( name, permission  );  % File to be written
    if( fid==-1 );
        name = which( name );
        idx = strfind( name, '%' );
        if( ~isempty(idx) );
            name(idx(1):end) = []; end;
        fid = fopen( which(name, 'all'), permission ); end;
    
    assert( fid>=3, 'open_file:cannotopenfile', 'Cannot open file %s', name_orig );
    cleanup = onCleanup( @() fclose( fid ) );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
