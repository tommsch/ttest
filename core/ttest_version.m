function [ ret ] = ttest_version
% returns the version number of TTEST
% Written by: tommsch, 2020
	ret = '3.2025.02.28';
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
