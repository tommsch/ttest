function [ str ] = TTEST_DISP( maxdisp, varargin );
% displays all inputs using disp, cuts of if textual representation is longer than maxdisp
% [ str ] = TTEST_DISP( maxdisp, varargin );
%
% E.g.: TTEST_DISP( [2;3;4;5], randn(200), 'abdsfasdf' )
%
% Written by: tommsch, 2020

%               2024-02-20, tommsch,   Behaviour change: Newline is added at begin of string if generated string contains a newline somewhere in the middle
%               2024-06,    tommsch,   Behaviour change: Does not accepts ttest-id as first parameter anymore due to performance reasons
% Changelog: 

    assert( nargin >= 1, 'TTEST:DISP', 'First argument to TTEST_DISP must be the maxmimum number of characters to be printed or empty.' );
    if( isempty(maxdisp) );
        maxdisp = 500; end;
    str = '';
    for i = 1:numel( varargin );
        vari = varargin{i};
        try;
            err = lasterror;  %#ok<LERR>
            if( ischar(vari) || iscell(vari) );
                newstr = strnewlinetrim( evalc('disp( vari )') );
            else;
                newstr = strnewlinetrim( repr( vari, '_', '-silent') ); end;
        catch me;  %#ok<NASGU>
            lasterror( err );  %#ok<LERR>
            try;
                err = lasterror();  %#ok<LERR>
                newstr = strnewlinetrim( evalc('disp( vari )') );
            catch me;  %#ok<NASGU>
                lasterror( err );  %#ok<LERR>
                newstr = ['Could not display variable %s' vari]; end; end;
        %newstr = strnewlinetrim( evalc('disp( vari )') );
        hasnewline = any( any( int8(newstr) == int8(newline) ) );
        if( isempty(vari) );
            if( iscell(vari) );
                newstr = '{}';
            else;
                newstr = '[]'; end;
        elseif( numel(newstr) > maxdisp );
            newstr(maxdisp+1:end) = [];
            newstr = [newstr ' ... Output truncated' newline]; end;  %#ok<AGROW>
        str = [str newstr newline];  %#ok<AGROW>
        if( hasnewline );
            str = [newline str];  %#ok<AGROW>
            if( ~isempty(str) );
                str(end) = []; end;  % remove last newline
        else;
            str = strtrim( str ); end; end;  % removes also last newline
    
end

function str = strnewlinetrim( str );
    while( ~isempty(str) );
        idx = [];
        if( numel(str) >= 2 && str(2) == newline );
            idx = [idx 2]; end;  %#ok<AGROW>
        if( str(end) == newline );
            idx = [idx numel(str)]; end;  %#ok<AGROW>
        str(idx) = [];
        if( isempty(idx) );
            break; end; end;
     str = deblank( str );
        
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
