function [ varargout ] = ttest_toolbox_anymatrix( varargin );
    switch varargin{1};
        case 'load';

        case 'unload';

        case 'ttest';
            varargout{1} = 1;
            
        case 'licpkg';
            varargout{1} = [];
            
        case 'ver';
            varargout{1} = [];
            
        case 'file';
            varargout{1} = 'anymatrix.m';
            
        case 'num';
            varargout{1} = '';
			varargout{2} = varargin{2};

        case 'handle';
            handle_check = true;
            evalc( '[A, R] = anymatrix(''core/beta'', 10)' );
            assert( false )  % XX TM Missing
            if( ~(A&&R) );
                handle_check = false; end;
             varargout{1} = handle_check;
            
        otherwise;
            error( 'ttest_ttoolbox_callback:fatal', 'wrong input' ); end;
end


function dummy; end  % #ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
