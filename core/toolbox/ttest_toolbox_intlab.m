function [ varargout ] = ttest_toolbox_intlab( varargin );
    switch varargin{1};
        case 'load';

        case 'unload';

        case 'ttest';
            varargout{1} = '1.0';
            
        case 'licpkg';
            varargout{1} = [];
            
        case 'ver';
            varargout{1} = [];
            
        case 'file';
            varargout{1} = 'intval.m';
            
        case 'num';
            majorct = 2;
            minor = 0;
            major = [];
            while( true )
                minorct = 0;
                while( true );
                    if( minorct==0 );
                        file = ['INTLAB_Version_' num2str(majorct) '.m'];
                    else
                        file = ['INTLAB_Version_' num2str(majorct) '_' num2str(minorct) '.m']; end; 
                    if( exist( file, 'file' ) == 2 );
                        minor = minorct;
                        major = majorct;
                        minorct = minorct + 1; 
                    else;
                        break; end;
                    minorct = minorct + 1; end;
                majorct = majorct + 1;
                if( minorct==0 );
                    break; end; end;
            if( ~isempty(major) );
                varargout{1} = [num2str(major) '.' num2str(minor)];
            else;
                varargout{1} = ''; end;
            varargout{2} = varargin{2};

        case 'handle';
            in = infsup( 0 , 1 );
            out = 1/in;
            expected = infsup( 1, inf );
            varargout{1} = isequal(out, expected);
            
        otherwise;
            error( 'ttest_ttoolbox_callback:fatal', 'wrong input' ); end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
