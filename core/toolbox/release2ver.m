function [ ver ] = release2ver( release )
% Translates Release strings to version strings
% Has to be maintained by hand
% E.g.: release2ver( 'R2020a' )
%
% Written by: tommsch, 2021-10-14

    switch release;
        case '1984';     ver = '1.0';
        case '1986';     ver = '2';
        case '1987';     ver = '3';
        case '1990';     ver = '3.5';
        case '1992';     ver = '4';
        case '1994';     ver = '4.2.3';
        case 'Volume 8'; ver = '5.0';
        case 'Volume 9'; ver = '5.1';
        case 'R9.1';     ver = '5.1.1';
        case 'R10';      ver = '5.2';
        case 'R10.1';    ver = '5.2.1';
        case 'R11';      ver = '5.3';
        case 'R11.1';    ver = '5.3.1';
        case 'R12';      ver = '6.0';
        case 'R12.1';    ver = '6.1';
        case 'R13';      ver = '6.5';
        case 'R13SP1';   ver = '6.5.1';
        case 'R13SP2';   ver = '6.5.2';
        case 'R14';      ver = '7';
        case 'R14SP1';   ver = '7.01';
        case 'R14SP1+';  ver = '7.0.1';
        case 'R14SP2';   ver = '7.0.4';
        case 'R14SP3';   ver = '7.1';
        case {'R2006a','2006a'};  ver = '7.2';
        case {'R2006b','2006b'};  ver = '7.3';
        case {'R2007a','2007a'};  ver = '7.4';
        case {'R2007b','2007b'};  ver = '7.5';
        case {'R2008a','2008a'};  ver = '7.6';
        case {'R2008b','2008b'};  ver = '7.7';
        case {'R2009a','2009a'};  ver = '7.8';
        case {'R2009b','2009b'};  ver = '7.9';
        case {'R2009bSP1','2009bSP1'}; ver = '7.9.1';
        case {'R2010a','2010a'};  ver = '7.10';
        case {'R2010b','2010b'};  ver = '7.11';
        case {'R2010bSP1','2010bSP1'}; ver = '7.11.1';
        case {'R2010bSP2','2010bSP2'}; ver = '7.11.2';
        case {'R2011a','2011a'};  ver = '7.12';
        case {'R2011b','2011b'};  ver = '7.13';                
        case {'R2012a','2012a'};  ver = '7.14';
        case {'R2012b','2012b'};  ver = '8.0';                
        case {'R2013a','2013a'};  ver = '8.1';
        case {'R2013b','2013b'};  ver = '8.2';                
        case {'R2014a','2014a'};  ver = '8.3';
        case {'R2014b','2014b'};  ver = '8.4';                
        case {'R2015a','2015a'};  ver = '8.5';
        case {'R2015aSP1','2015aSP1'}; ver = '8.5';
        case {'R2015b','2015b'};  ver = '8.6';
        case {'R2016a','2016a'};  ver = '9.0';
        case {'R2016b','2016b'};  ver = '9.1';
        case {'R2017a','2017a'};  ver = '9.2';
        case {'R2017b','2017b'};  ver = '9.3';
        case {'R2018a','2018a'};  ver = '9.4';
        case {'R2018b','2018b'};  ver = '9.5';
        case {'R2019a','2019a'};  ver = '9.6';
        case {'R2019b','2019b'};  ver = '9.7';
        case {'R2020a','2020a'};  ver = '9.8';
        case {'R2020b','2020b'};  ver = '9.9';
        case {'R2021a','2021a'};  ver = '9.10';
        case {'R2021b','2021b'};  ver = '9.11';
        case {'R2022a','2022a'};  ver = '9.12.0';
        case {'R2022b','2022b'};  ver = '9.13';
        case {'R2023a','2023a'};  ver = '9.14';
        case {'R2023b','2023b'};  ver = '23.2';
        otherwise; error( 'TTEST:TOOLBOX', 'Given Release String either wrong or not hardcoded yet.\n  Given release string: %s', release ); end; 
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
