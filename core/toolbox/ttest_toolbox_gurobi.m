function [ varargout ] = ttest_toolbox_gurobi( varargin );

	persistent gurobipath;
	if( isempty(gurobipath) );
		gurobipath = fileparts( which('gurobi') ); end;
    
    switch varargin{1};
		case 'load';
			addpath( gurobipath );

		case 'unload';
            assert( ~isempty(gurobipath), 'ttest:toolbox', 'I Could not determine path of gurobi. This means I will not be able to load it again.' );
			rmpath( gurobipath );

        case 'ttest';
            varargout{1} = 1;
            
        case 'licpkg';
            varargout{1} = [];
            
        case 'ver';
            varargout{1} = [];
            
        case 'file';
            varargout{1} = 'gurobi_setup.m';
            
        case 'num';
            model.A = sparse( [1 1 0; 0 1 1] );
            model.obj = [1 2 3];
            model.modelsense = 'Max';
            model.rhs = [1 1];
            model.sense = ['<' '<'];
            params.outputflag = 0;

            result = gurobi( model, params );
            varargout{1} = [num2str(result.versioninfo.major) '.' num2str(result.versioninfo.minor) '.' num2str(result.versioninfo.technical)];
			
			varargout{2} = varargin{2};

        case 'handle';
            handle_check = true;
            model.A = sparse( [1 1 0; 0 1 1] );
            model.obj = [1 2 3];
            model.modelsense = 'Max';
            model.rhs = [1 1];
            model.sense = ['<' '<'];
            params.outputflag = 0;

            result = gurobi( model, params );
            if( ~isequal(result.x,[1 0 1].') );
                handle_check = false; end;
             varargout{1} = handle_check;
            
        otherwise;
            error( 'ttest_ttoolbox_callback:fatal', 'wrong input' ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
