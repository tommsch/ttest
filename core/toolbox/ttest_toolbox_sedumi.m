function [ varargout ] = ttest_toolbox_sedumi( varargin );
    switch varargin{1};
        case 'load';

        case 'unload';

        case 'ttest';
            varargout{1} = 1;
            
        case 'licpkg';
            varargout{1} = [];
            
        case 'ver';
            varargout{1} = [];
            
        case 'file';
            varargout{1} = 'sedumi.m';
            
        case 'num';
            str = evalc( 'help sedumi' );
            idx1 = find( str<='9' & str>='0' | str=='.' );
            idx2 = find( diff(idx1)-1, 1 );
            idx1 = idx1(1:idx2);
            str = str(idx1);
            varargout{1} = str;
			varargout{2} = varargin{2};

        case 'handle';
                handle_check = true;
                AA = [ 4  4  4 -1 -4 -4
                              4  1 -4 -4 -4  4];
                bb = [ 2  1]';
                cc = [-4  0  0  0  0  0]';
                pars.fid = 0;
                K.l = 6;
                val = sedumi( AA, bb, cc, K, pars );
                if( norm( val - [0.250000000000000   0.200025626486528   0.178778846360113   0.211428602830903   0.242873139784766   0.333074182354149]' )>1e-12 ); 
                    handle_check = false; end;
                if( handle_check );  % test for sedumi bug
                    try;
                        A = [-1 0 4 0;-3 0 0 -5;-2 0 0 4;0 0 5 0;0 1 0 0];
                        b = [0 0 0 0 1].';
                        c = [-1 0 0 0].';
                        K.f = 0;
                        K.l = 1;
                        K.q = 3;
                        pars.fid = 0;
                        [~,x]= EXPECT_NO_THROW( @() sedumi( A, b, c, K, pars ), 'MATLAB:rankDeficientMatrix' );
                        if( norm(x(1))>1e-8 );
                            handle_check = false; end;
                    catch;
                        handle_check = false;
                        fprintf( 2, ['Your SeDuMi version contains a bug. You may want to do the following (although legacy code may break then): \n' ...
                                    '  In the function: ''sedumi/pretransfo.m'',\n'...
                                    '  line number:     ~144\n' ...
                                    '  change:          ''m = min(size(At));'' to ''m = size(At,1);''.\n'] ); end; end;
             varargout{1} = handle_check;
            
        otherwise;
            error( 'ttest_ttoolbox_callback:fatal', 'wrong input' ); end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
