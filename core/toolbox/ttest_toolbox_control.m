function [ varargout ] = ttest_toolbox_control( varargin );
    switch varargin{1};
        case 'load';

        case 'unload';

        case 'ttest';
            varargout{1} = 1;
            
        case 'licpkg';
            varargout{1} = tifh( ismatlab, [], 'control' );
            
        case 'ver';
            varargout{1} = tifh( ismatlab, [], 'control' );
            
        case 'file';
            varargout{1} = 'hsvd.m';
            
        case 'num';
			if( isoctave );
				err = lasterror(); end;
			ver_returned = ver( 'control' );
			if( isoctave );
				lasterror( err ); end;
            varargout{1} = ver_returned.Version;
			varargout{2} = varargin{2};
            
        case 'handle';
            varargout{1} = true;
            
        otherwise;
            error( 'ttest_ttoolbox_callback:fatal', 'wrong input' ); end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
