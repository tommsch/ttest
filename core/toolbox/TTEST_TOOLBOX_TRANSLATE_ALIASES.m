function [ name ] = TTEST_TOOLBOX_TRANSLATE_ALIASES( name );
    name(name == '_') = [];
    switch lower( name );
        case {'jsr','thejsrtoolbox','jsrlouvain' };                 name = 'jsrlouvain';
        case {'parallel','distcomp','distribcomputingtoolbox'};     name = 'parallel';
        case {'signalprocessing','signaltoolbox','signal'};         name = 'signal';
        case {'statistic','statistics','ml','machinelearning'};     name = 'statistic';
        case {'sym','symbolictoolbox','symbolic'};                  name = 'symbolic'; 
        case {'ttest'};                                             name = 'ttest'; 
        case {'optim','optimization','optimizationtoolbox'};        name = 'optim';
        case {'ipa','subdivision','ttoolboxes','ttoolbox'};         name = 'ttoolboxes';
        otherwise; % do nothing
        end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.