function [ varargout ] = ttest_toolbox_optim( varargin );
    switch varargin{1};
        case 'load';

        case 'unload';

        case 'ttest';
            varargout{1} = 1;
            
        case 'licpkg';
			if( ismatlab );
				varargout{1} = 'Optimization_Toolbox';
			else;
				varargout{1} = 'optim'; end;
            
        case 'ver';
			varargout{1} = 'optim';  % MATLAB R2019b and earlier
            
        case 'file';
            varargout{1} = 'linprog.m';
            
        case 'num';
			if( isoctave );
				err = lasterror(); end;
			ver_returned = ver( 'optim' );
			if( isoctave );
				lasterror( err ); end;
            
            varargout{1} = ver_returned.Version;
			varargout{2} = varargin{2};
            
        case 'handle';
            try;
                A = [1 1;1 1/4;1 -1;-1/4 -1;-1 -1;-1 1];  %#ok<NASGU>
                b = [2 1 2 1 -1 2].';  %#ok<NASGU>
                f = [-1 -1/3].';  %#ok<NASGU>
                x = nan;
                try;  % matlab up to R2021a
                    options = optimoptions( 'linprog','Display','off' );  %#ok<NASGU>
                    [~, x] = evalc( 'linprog( f, A, b, [], [], [], [], [], options );' );
                catch me;  %#ok<NASGU>
                    end;
                try;  % matlab at least up from R2023b
                    options = optimoptions( 'linprog','Display','off' );  %#ok<NASGU>
                    [~, x] = evalc( 'linprog( f, A, b, [], [], [], [], options );' );
                catch me;  %#ok<NASGU>
                    end;
                try;  % octave
                    [~, x] = evalc( 'linprog( f, A, b );' );
                catch me;  %#ok<NASGU>
                    end;
                varargout{1} = norm( x - [2/3; 4/3] ) <= 1e-9;
            catch me;  %#ok<NASGU>
                varargout{1} = false; end;
            
        otherwise;
            error( 'ttest_ttoolbox_callback:fatal', 'wrong input' ); end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
