function [ varargout ] = ttest_toolbox_matlab( varargin );
    switch varargin{1};
        case 'load';

        case 'unload';

        case 'ttest';
		    % may be used to test whether the version of this file is compatible with TTEST version
		    % the returned number should be the TTEST version number for which this file was written for
            varargout{1} = '1.0';
            
        case 'licpkg';
		    % the string which is needed for the functions licence (on matlab) or pkg (on octave)
            varargout{1} = 'MATLAB';
            
        case 'ver';
		    % the string which is needed for the function ver
            varargout{1} = 'matlab';
            
        case 'file';
		    % a file which is contained in the toolbox
            varargout{1} = 'plot.m';
            
        case 'num';
		    % must return the version number as a string, numbers separated by dots
			% varargin{2}     the input version number as passed by the user
			% varargout{1}    the actual version number as returned by the software, in the correct format
			% varargout{2}    the input version number as passed by the user, in the correct format
			varargout{2} = varargin{2};
            if( isequal( varargin{2}(1), 'R') );
                varargout{2} = release2ver( varargin{2} ); end;
			if( ismatlab );
				ver_returned = ver( 'matlab' );
			else;
				ver_returned = ''; end;
            varargout{1} = ver_returned.Version;
            
        case 'handle';
		    % used to test the toolbox on a specific example
			% must either return true or false
			% See ttest_toolbox_sedumi for an example
            varargout{1} = true;
            
        otherwise;
		    % not necessary to have this case
            error( 'ttest_ttoolbox_callback:fatal', 'wrong input' ); end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
