function TTEST_DELETE_LINE( name );
    [fid_read, closefid_read] = TTEST_OPEN_FILE( name, 'r' );  %#ok<ASGLU>

    output = '';
    flag = false;
    while( ~feof(fid_read) );
        line = fgetl( fid_read );
        if( ~isequal( line, -1 ) && ...
            contains( line, '%TTEST_AUTOGENERATE' ) ...
          );
            flag = true;
            continue; end
        output = [output newline line]; end;  %#ok<AGROW>
    if( flag );
        output = [output newline '% Deleted at least one TTEST line %TTEST_AUTOGENERATE' newline]; end;
    if( numel(output)>=1 && isequal(output(1), newline) );
        output(1) = []; end;
    clear closefid_read;


    [fid_write, closefid_write]= TTEST_OPEN_FILE( name, 'w+' );  %#ok<ASGLU>
    if( flag );
        numbytes = fprintf( fid_write, '%s', output );
        if( numbytes<numel(output) );
            warning( 'deleteTTESTlines:writefailure', 'It seems that deleteTTESTlines could not write to the file.'  ); end; end;
    clear closefid_write;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
