TTEST init section_experimental_2

%% 
TESTCASE( 'severities' );
if( SECTION( DISABLED ) );
    TODO_FAIL(); end;  % this should not get executed.

if( SECTION( @() false ) );
    TODO_FAIL(); end;  % this should not get executed.

if( SECTION( @() ENABLED( @false ) ) );
    TODO_FAIL(); end;  % this should not get executed.

if( SECTION( ENABLED ) );
else;
    TODO_FAIL(); end;  % this should not get executed.

if( SECTION( @() true ) );
else;
    TODO_FAIL(); end;  % this should not get executed.

if( SECTION( @() ENABLED( @true) ) );
else;
    TODO_FAIL(); end;  % this should not get executed.


%#ok<*NOSEL,*NOSEMI,*ALIGN>
