% test testrunner

TTEST init

cleandir = onCleanup_lazy( @(x) cd(x), cd( 'multiinit' ) );  %#ok<NASGU>

EXPECT_FALSE( @() runttests( 'test_multiinit_worker.m', '-recursiveoverride', '-strict' ) );



clear cleandir

%%
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>
