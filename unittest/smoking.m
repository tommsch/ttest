function smoking;
    fprintf( 'Starting smoking test\n' );

    path

    ASSERT_EQ( 2, 2 );
    try;
        evalc( 'ASSERT_EQ( 2, 3 );' );
        assert( false );
    catch me;
        assert( strcmp(me.identifier, 'TTEST:NotEq') );
        end;

    fprintf( 'Finished smoking test\n' );
end

%#ok<*NOSEL,*NOSEMI,*ALIGN>

