% test macro

id = TTEST( 'init' );
id_unused = TTEST( 'init', 'id_unusued' );

% check if we are in the right directiory
a = which( 'test_macro' );
assert( numel(a) >= 14 && strcmpi(a(1:end-13), pwd), 'test_macro', 'This function must be called from the ./unittest folder in the TTEST folder.' );
% preconditions

%% TRUE_boolean
TESTCASE( 'TRUE, FALSE' );
assert( tt.expect_true( true ) );
EXPECT_FALSE( tt.expect_true( false ) );
EXPECT_TRUE( true );

%% FAIL
assert( ~tt.expect_fail() );
assert( ~tt.expect_fail('text') );

EXPECT_FALSE( tt.expect_fail );
EXPECT_FALSE( tt.expect_fail('text') );

%% SUCCEED
assert( EXPECT_SUCCEED );
assert( ASSERT_SUCCEED );

EXPECT_TRUE(  tt.expect_succeed() );
EXPECT_TRUE(  tt.assert_succeed() );

%% TRUE
assert( EXPECT_TRUE( 1 ) );
EXPECT_TRUE( 1 );
EXPECT_FALSE( tt.expect_true( 2 ) );
ASSERT_TRUE( 1 );
EXPECT_THROW( @() ~tt.assert_true( 0 ), 'TTEST:NotTrue' );
EXPECT_NTHROW( 'EXPECT_TRUE( id, @true );' );
EXPECT_NTHROW( 'EXPECT_FALSE( id, @() false );' );
EXPECT_NTHROW( 'ASSERT_TRUE( id, @true );' );
EXPECT_THROW( @() tt.assert_true( [0 1] ), 'TTEST:noScalar' );

assert( ~tt.expect_true( false ) );
EXPECT_FALSE( tt.expect_true(0) );
EXPECT_FALSE( tt.expect_true(1,0,1) );
EXPECT_FALSE( tt.expect_true([0 1]) );

%% FALSE
EXPECT_FALSE( 0 );
ASSERT_FALSE( 0 );
EXPECT_THROW( @() tt.assert_false(1), 'TTEST:Notfalse' );

assert( ~tt.expect_false( true ) );
EXPECT_FALSE( tt.expect_false(0, 1, 0) );
EXPECT_FALSE( tt.expect_false(1) );

%% TRUTHY
EXPECT_TRUTHY( [1 2 3] );
EXPECT_FALSE( tt.expect_truthy( [] ) );
EXPECT_FALSE( tt.expect_truthy( [0 2 3] ) );
if( ismatlab() )
    EXPECT_THROW( @() tt.expect_truthy( nan ), 'MATLAB:nologicalnan', 'ttest:expect:truthy' );
    EXPECT_THROW( @() tt.expect_truthy( {} ), 'MATLAB:invalidConversion', 'ttest:expect:truthy' );
    end;

assert( ~tt.expect_falsy( true ) );
assert(  EXPECT_TRUTHY( inf ) );

%% FALSY
EXPECT_FALSY( [0 0 0] );
EXPECT_FALSY( [] );
EXPECT_FALSY( [1 0 0] );
EXPECT_THROW( @() tt.expect_falsy( id, @sum ), ':minrhs', 'Octave:invalid-fun-call' );

EXPECT_FALSE( tt.expect_falsy( [1 2] ) );

%% PRED
TESTCASE( 'PRED' );
EXPECT_PRED( @isempty, [] );
EXPECT_PRED( @isempty, {} );
EXPECT_PRED( @(a, b) a < b, 1, 2);
EXPECT_PRED( @(a, b, c) a + b + c == 0, 1, 2, -3 );

EXPECT_FALSE( tt.expect_pred( @isempty, 1 ) );
EXPECT_FALSE( tt.expect_pred( @(a, b) a < b, 2, 1) );
EXPECT_FALSE( tt.expect_pred( @(x) x >= 0 && x <= 2, 3 ) );

%% NPRED
EXPECT_NPRED( @isempty, 1 );

EXPECT_FALSE( tt.expect_npred( @isempty, [] ) );
EXPECT_FALSE( tt.expect_npred( @isempty, {} ) );
EXPECT_FALSE( tt.expect_npred( @(a, b) a < b, 1,2 ) );
EXPECT_FALSE( tt.expect_npred( @(a, b, c) a + b + c == 0, 1, 2, -3 ) );

%% EEQ
TESTCASE( 'COMP' );
EXPECT_EEQ( [2 3], [2 3] );
EXPECT_EEQ( [], [] );
EXPECT_EEQ( [], [], [] );
EXPECT_EEQ( 2 );
EXPECT_EEQ( 2, 2 );
EXPECT_EEQ( @sum, @sum );
EXPECT_NTHROW( @() EXPECT_EEQ() );
EXPECT_EEQ( nan );

EXPECT_FALSE( tt.expect_eeq( 1, true ) );
EXPECT_FALSE( tt.expect_eeq( 1, sparse(1) ) );
EXPECT_FALSE( tt.expect_eeq( 1, int32(1) ) );
EXPECT_FALSE( tt.expect_eeq( 97, 'a' ) );
EXPECT_FALSE( tt.expect_eeq( 2, 3 ) );
EXPECT_FALSE( tt.expect_eeq( [], {}) );
EXPECT_FALSE( tt.expect_eeq( @sum, @prod) );
EXPECT_FALSE( tt.expect_eeq( 2, 3 ) );
EXPECT_FALSE( tt.expect_eeq( @sum, struct ) );
EXPECT_FALSE( tt.expect_eeq( nan, nan ) );
EXPECT_FALSE( tt.expect_eeq( 1, [] ) );

%% EQ
EXPECT_EQ( 1, true );
EXPECT_EQ( 1, sparse(1) );
EXPECT_EQ( 1, int32(1) );
EXPECT_EQ( 97, 'a' );
EXPECT_EQ( [2 3], [2 3] );
EXPECT_EQ( [], [] );
EXPECT_EQ( [], [], [] );
EXPECT_EQ( 2 );
EXPECT_EQ( 2, 2 );
EXPECT_EQ( @sum, @sum );
EXPECT_NTHROW( @() EXPECT_EQ( id ) );
EXPECT_EQ( nan );

EXPECT_FALSE( tt.expect_eq( 2, 3 ) );
EXPECT_FALSE( tt.expect_eq( [], {} ) );
EXPECT_FALSE( tt.expect_eq( @sum, @prod ) );
EXPECT_FALSE( tt.expect_eq( 2, 3 ) );
EXPECT_FALSE( tt.expect_eq( @sum, struct ) );
EXPECT_FALSE( tt.expect_eq( nan, nan ) );
EXPECT_FALSE( tt.expect_eq( 1, [] ) );

%% NE
EXPECT_NE( @sum, @prod );
EXPECT_NE( 2 );
EXPECT_NE( 2, 3 );
EXPECT_NE( nan, nan );
EXPECT_NE( nan );

EXPECT_FALSE( tt.expect_ne( @sum, @sum ) );
EXPECT_FALSE( tt.expect_ne( 2, 2 ) );
EXPECT_FALSE( tt.expect_ne( 2, 3, 2 ) );
EXPECT_FALSE( tt.expect_ne( 1, int32(1) ) );

%% NEE
EXPECT_NEE( @sum, @prod );
EXPECT_NEE( 2 );
EXPECT_NEE( 2, 3 );
EXPECT_NEE( nan, nan );
EXPECT_NEE( nan );
EXPECT_NEE( 1, [] );
EXPECT_NEE( 1, int32(1) );

EXPECT_FALSE( tt.expect_nee( @sum, @sum ) );
EXPECT_FALSE( tt.expect_nee( 2, 2 ) );
EXPECT_FALSE( tt.expect_nee( 2, 3, 2 ) );

%% LT
EXPECT_LT( 2 );
EXPECT_LT( 2, 3 );
EXPECT_LT( 2, 3 );
EXPECT_LT( 2, 3, 4, 5 );

EXPECT_FALSE( tt.expect_lt( 2, 2 ) );
EXPECT_FALSE( tt.expect_lt( 3, 2 ) );
EXPECT_FALSE( tt.expect_lt( 2, 2 ) );
EXPECT_FALSE( tt.expect_lt( 3, 2 ) );
EXPECT_FALSE( tt.expect_lt( 3, 2, 3 ) );
EXPECT_FALSE( tt.expect_lt( [.5 .8], [1;1] ) );

%% LE
EXPECT_LE( 2 );
EXPECT_LE( 2, 2 );
EXPECT_LE( 2, 2, 2, 2 );
EXPECT_LE( 2, 3, 4, 5 );
EXPECT_LE( 2, 2, 3, 4, 10 );
EXPECT_LE( 2, 3 );
if( ismatlab() );
    EXPECT_THROW( @() tt.expect_le( 1, 2, 3, {} ), 'ttest:property', ':UndefinedFunction', 'ttest:expect:le' );
    EXPECT_THROW( @() tt.expect_le( @() @sum, {} ), 'ttest:property', ':UndefinedFunction', 'ttest:expect:le' );
    EXPECT_THROW( @() tt.assert_le( @() @sum, {} ), 'ttest:property', ':UndefinedFunction', 'TTEST:Notle' );
    end;
    
EXPECT_FALSE( tt.expect_le( 2, 2, 3, 4, 10, 9 ) );
EXPECT_FALSE( tt.expect_le( 3, 2 ) );
EXPECT_FALSE( tt.expect_le( 3, 2, 3 ) );


%% EE
EXPECT_EE( 2, [2 2] );
EXPECT_EE( 2, 2 );
EXPECT_EE( 2, int32(2) );
EXPECT_EE( 2, sparse(2) );

EXPECT_FALSE( tt.expect_ee( 2, [2 3] ) );
EXPECT_FALSE( tt.expect_ee( 2, 2, 1, 0 ) );
EXPECT_FALSE( tt.expect_ee( 2, 3 ) );
EXPECT_FALSE( tt.expect_ee( 3, 2 ) );
EXPECT_FALSE( tt.expect_ee( 2, 2, 1, 2 ) );
EXPECT_FALSE( tt.expect_ee( [1;2], [1 2] ) );
if( ismatlab );
    EXPECT_THROW( @() tt.expect_ee( [1;2], @sum ), 'MATLAB:UndefinedFunction',  'ttest:expect:ee' );
else;
    err = lasterror();  %#ok<LERR>
    EXPECT_FALSE( tt.expect_ee( [1;2], @sum ) );
    lasterror( err ); end;  %#ok<LERR>

%% GE
EXPECT_GE( 2, 2 );
EXPECT_GE( 2, 2, 1, 0 );
EXPECT_GE( 3, 2 );

EXPECT_FALSE( tt.expect_ge( 2, 3 ) );
EXPECT_FALSE( tt.expect_ge( 2, 2, 1, 2 ) );

%% GT
EXPECT_GT( 3, 2 );
EXPECT_GT( 3, 2, 1 );

EXPECT_FALSE( tt.expect_gt( 2, 2 ) );
EXPECT_FALSE( tt.expect_gt( 2, 3 ) );
EXPECT_FALSE( tt.expect_gt( 2, 2, 3 ) );
EXPECT_FALSE( tt.expect_gt( 2, 3, 2 ) );

%% ALMOST_EQ
EXPECT_ALMOST_EQ( single(1), 1 + eps(single(1)) );
EXPECT_ALMOST_EQ( single(1), 1 + eps(single(1)) );
EXPECT_ALMOST_EQ( single(1), 1 + eps(single(1)) );
EXPECT_ALMOST_EQ( 1, 1 + eps );
EXPECT_ALMOST_EQ( 1, 1 + 4*eps );
EXPECT_ALMOST_EQ( 1, 1 + eps, 1 + 2*eps );

EXPECT_FALSE( tt.expect_almost_eq( 1, 1+5*eps ) );
EXPECT_FALSE( tt.expect_almost_eq( 1, 1+2*eps, 1+5*eps ) );
EXPECT_FALSE( tt.expect_almost_eq( 1, [] ) );
EXPECT_FALSE( tt.expect_almost_eq( single(1), 1+6*eps(single(1)) ) );
err = lasterror();  %#ok<LERR>
EXPECT_FALSE( tt.expect_almost_eq( {} ) );
lasterror( err );  %#ok<LERR>

EXPECT_ALMOST_EQ( 1, 1 + eps );
EXPECT_ALMOST_EQ( 1, 1 + 4*eps );
EXPECT_ALMOST_EQ( 1, 1 + eps, 1 + 2*eps );

EXPECT_FALSE( tt.expect_almost_eq( 1, 1+5*eps ) );
EXPECT_FALSE( tt.expect_almost_eq( 1, 1+2*eps, 1+5*eps ) );

%% ALMOST_NE
EXPECT_ALMOST_NE( single(1), 1 + 5*eps(single(1)) );
EXPECT_ALMOST_NE( 1, 1 + 5*eps );

EXPECT_FALSE( tt.expect_almost_ne( single(1), 1+eps(single(1)) ) );
EXPECT_FALSE( tt.expect_almost_ne( single(1), 1+eps(single(1)) ) );
EXPECT_FALSE( tt.expect_almost_ne( single(1), 1+eps(single(1)) ) );
err = lasterror();  %#ok<LERR>
EXPECT_FALSE( tt.expect_almost_ne( {} ) );
lasterror( err );  %#ok<LERR>
EXPECT_FALSE( tt.expect_almost_ne( 1, 1+eps ) );
EXPECT_FALSE( tt.expect_almost_ne( 1, 1+4*eps ) );
EXPECT_FALSE( tt.expect_almost_ne( 1, 1+2*eps, 1+5*eps ) );
EXPECT_FALSE( tt.expect_almost_ne( 1, 1+eps, 1+2*eps ) );
EXPECT_FALSE( tt.expect_almost_ne( 1, [] ) );

%% NEAR
EXPECT_NEAR( 100, 1.1 );
EXPECT_NEAR( 1, 2, 1.1 );
EXPECT_NEAR( inf, inf, 0 );
EXPECT_THROW( @() tt.assert_near( emptytype, 2, 0 ), 'TTEST:NotNear', 'MATLAB:UndefinedFunction', 'TTEST:NOID' );

EXPECT_FALSE( tt.expect_near( 1, 2, 0.9 ) );
EXPECT_FALSE( tt.expect_near( 1, 2, 3, 1.5 ) );


%% NNEAR
EXPECT_NNEAR( 100, 1.1 );
EXPECT_NNEAR( 1, 2, 0.9 );

EXPECT_FALSE( tt.expect_nnear( 1, 2, 1.1 ) );
EXPECT_FALSE( tt.expect_nnear( 1, 2, 3, 1.5 ) );
EXPECT_FALSE( tt.expect_nnear( 1, 1.1, 2, 1.5 ) );

%% OUTPUT
TESTCASE( 'OUTPUT' );
EXPECT_OUTPUT( @() max( [1 3 2] ), 3 );
EXPECT_OUTPUT( @() max( [1 3 2] ), 3, 2 );

EXPECT_FALSE( tt.expect_output( @() max( [1 3 2] ), 4, 2 ) );
EXPECT_FALSE( tt.expect_output( @() max( [1 3 2] ), 3, int32(2) ) );

%% RANGE
TESTCASE( 'RANGE' );
EXPECT_RANGE( [0 3] );
EXPECT_RANGE( 2, [0 3] );
EXPECT_RANGE( 3, [0 3] );

EXPECT_FALSE( tt.expect_range( 2, [3 5] ) );
EXPECT_FALSE( tt.expect_range( 4, 6, [3 5] ) );

%% STREQ
TESTCASE( 'STRINGS' );
EXPECT_THROW( @() tt.expect_streq( 'a5', @sum ), 'MATLAB:minrhs', 'Octave:invalid-fun-call', 'ttest:expect:streq' );
EXPECT_STREQ( '23' );
EXPECT_STREQ( '23', '23', '23' );
EXPECT_STREQ( 'ab', 'ab' );
EXPECT_STREQ( '', '' );

EXPECT_FALSE( tt.expect_streq( 1,1 ) );
EXPECT_FALSE( tt.expect_streq( 1 ) );
EXPECT_FALSE( tt.expect_streq( 'ab', 'aB' ) );
EXPECT_FALSE( tt.expect_streq( 'ab', 'ac' ) );
EXPECT_FALSE( tt.expect_streq( '23', '' ) );
EXPECT_FALSE( tt.expect_streq( [], '' ) );
EXPECT_FALSE( tt.expect_streq( 97, 'a' ) );

%% STRNE
EXPECT_STRNE( '23' );
EXPECT_STRNE( 'ab', 'aB' );
EXPECT_STRNE( 'AAA', 'BBB', 'aaa' );
EXPECT_STRNE( 'ab', 'ac' );
EXPECT_STRNE( '23', '' );

EXPECT_FALSE( tt.expect_strne(1) );
EXPECT_FALSE( tt.expect_strne(1, 2) );
EXPECT_FALSE( tt.expect_strne('23', '23') );
EXPECT_FALSE( tt.expect_strne('ab', 'ab') );
EXPECT_FALSE( tt.expect_strne('AAA', 'BBB', 'AAA') );
EXPECT_FALSE( tt.expect_strne('23', '23', '23') );
EXPECT_FALSE( tt.expect_strne('','') );

%% STRCASEEQ
EXPECT_STRCASEEQ( '23' );
EXPECT_STRCASEEQ( '23', '23' );
EXPECT_STRCASEEQ( '23', '23', '23' );
EXPECT_STRCASEEQ( 'ab', 'ab' );
EXPECT_STRCASEEQ( 'ab', 'aB' );
EXPECT_STRCASEEQ( '', '' );

EXPECT_FALSE( tt.expect_strcaseeq(1) );
EXPECT_FALSE( tt.expect_strcaseeq(1, 2) );
EXPECT_FALSE( tt.expect_strcaseeq('ab', 'ac') );
EXPECT_FALSE( tt.expect_strcaseeq('23', '') );

%% STRCASENE
EXPECT_STRCASENE( '23' );
EXPECT_STRCASENE( 'ab', 'ac' );
EXPECT_STRCASENE( '23', '' );

EXPECT_FALSE( tt.expect_strcasene( 1 ) );
EXPECT_FALSE( tt.expect_strcasene( 1, 2 ) );
EXPECT_FALSE( tt.expect_strcasene( '23', '23' ) );
EXPECT_FALSE( tt.expect_strcasene( '23', '23', '23' ) );
EXPECT_FALSE( tt.expect_strcasene( 'ab', 'ab' ) );
EXPECT_FALSE( tt.expect_strcasene( 'ab', 'aB' ) );
EXPECT_FALSE( tt.expect_strcasene( 'AAA', 'BBB', 'AAA' ) );
EXPECT_FALSE( tt.expect_strcasene( 'AAA', 'BBB', 'aaa' ) );
EXPECT_FALSE( tt.expect_strcasene( '', '' ) );

%% STRLEEQ
str_n = sprintf( '\n' );  %#ok<SPRINTFN>
str_rn = sprintf( '\r\n' );
EXPECT_STRLEEQ( str_n, str_rn );
EXPECT_FALSE( tt.expect_strleeq( str_n, [ str_rn ' '] ) );

%% STRLENE
str_n = sprintf( '\n' );  %#ok<SPRINTFN>
str_rn = sprintf( '\r\n' );
tt.expect_strlene( str_n, [ str_rn ' '] );
EXPECT_FALSE( tt.expect_strlene( str_n, str_rn ) );


%% FILE_EQ
TESTCASE( 'FILE' );
EXPECT_FILE_EQ( 'EXPECT_EQ.m' );
EXPECT_FILE_EQ( 'test_macro.m' );
EXPECT_FILE_EQ( 'test_macro.m', 'test_macro.m' );

EXPECT_FALSE( tt.expect_file_eq( 'filamskamgkag.m' ) );
EXPECT_FALSE( tt.expect_file_eq( 'EXPECT_EQ.m', 'ASSERT_EQ.m' ) );

%% SUBSET
TESTCASE( 'SETS' );
EXPECT_SUBSET( 1 );
EXPECT_SUBSET( 1, 1 );
EXPECT_SUBSET( [1 2], [1 2 3], [0 1 2 3 4] );
EXPECT_SUBSET( {1 2}, {1 2 3}, {0 1 2 3 4} );
EXPECT_SUBSET( [1], [1 2], [1 3 5 2] );

EXPECT_FALSE( tt.expect_subset( [1 2], [1 2 3], [0 1 2 3 4], [2 3 4 5] ) );
EXPECT_FALSE( tt.expect_subset( {1 2}, {1 2 3}, {0 1 2 3 4}, {2 3 4 5} ) );
EXPECT_FALSE( tt.expect_subset( [1], [1 2], [1 3 5 ] ) );

%% SUPERSET
EXPECT_SUPERSET( [1 2], [1], [1] );
EXPECT_SUPERSET( {[1] [2] [1 2] [1;2]}, {[1],[2]});

EXPECT_FALSE( tt.expect_superset( [1 2 3 4], [1 2 3 5] ) );
EXPECT_FALSE( tt.expect_superset( {[1;2]},{[1 2]} ) );

%% NUMEL
EXPECT_NUMEL( 4 );
EXPECT_PRODOFSIZE( 4 );
EXPECT_NUMEL( [1 2 3], [3 4 5], 3 );
EXPECT_NUMEL( [1 2 3], [2 3 4], 3 );
EXPECT_THROW( @() tt.expect_numel( [1 2 3], [2 3 4] ), 'TTEST:input', 'ttest:expect:numel' );

EXPECT_FALSE( tt.expect_numel( [1 2 3], [2 3 4 4], 4 ) );
EXPECT_FALSE( tt.expect_numel( [1 2 3], [3 4 5], 4 ) );

%% ISEMPTY
EXPECT_ISEMPTY( [] );
EXPECT_ISEMPTY( );
EXPECT_EMPTY( [], '', {} );

EXPECT_FALSE( tt.expect_isempty( [1] ) );
EXPECT_FALSE( tt.expect_empty( 2 ) );
EXPECT_FALSE( tt.expect_empty( @sum ) );
EXPECT_FALSE( tt.expect_empty( struct ) );

%% LENGTH
EXPECT_LENGTH( randn( 10, 20 ), 20 );

EXPECT_FALSE( tt.expect_length( randn( 10, 20 ), 10 ) );

%% SIZE
EXPECT_SIZE( randn( 10, 1 ), 10 );
EXPECT_SIZE( randn( 10, 20 ), [10 20] );

EXPECT_FALSE( tt.expect_size( randn( 10, 20 ), [10 10] ) );
EXPECT_FALSE( tt.expect_size( randn( 10, 20 ), 10 ) );

%% FIELD
TESTCASE( 'TYPES' );
s.f1 = 'a';
EXPECT_FIELD( s );
EXPECT_FIELD( s, 'f1' );

EXPECT_FALSE( tt.expect_field( s, 'f2' ) );
EXPECT_FALSE( tt.expect_field( 10, 'f2' ) );

%% SCALAR
EXPECT_SCALAR( 10, 20 );
EXPECT_SCALAR( @sum );
EXPECT_SCALAR(  );

EXPECT_FALSE( tt.expect_scalar( 2, [10 10] ) );

%% ISA
EXPECT_ISA( cell(0), cell(1), 'cell' );
EXPECT_ISA( 2, 'double' );
EXPECT_CLASS( 2, 'double' );

%% TYPE
EXPECT_TYPE( figure(1), figure(2) );
EXPECT_TYPE( double(1), double(2) );

EXPECT_FALSE( tt.expect_type( 1, @sum ) );
EXPECT_FALSE( tt.expect_type( double(1), single(2) ) );
close all

%% HANDLEEQ
EXPECT_HANDLEEQ( figure(1), figure(1) );

EXPECT_FALSE( tt.expect_handleeq( figure(1), figure(2) ) );
EXPECT_FALSE( tt.expect_handleeq( @sin, figure(1) ) );
close all

%% EXIST
TESTCASE( 'EXIST' );
var_uas9d = 1;
EXPECT_EXIST( 'var_uas9d', 'var' );
EXPECT_EXIST( 'OPTION', 'class' );

EXPECT_FALSE( tt.expect_exist( 'OPTION', 'variable' ) );

%% THROW_NTHROW
TESTCASE( 'THROW, NTHROW' );
cleanPath = onCleanup_lazy( @(x) path(x), path );
[w_msg, w_id] = lastwarn;
err = lasterror;  %#ok<LERR>

%for h = { @() evalc( 'addpath( ''../overload/warning/'' )' ), ...  % first time run with TTEST-warning, use evalc to surpress output
%          @() evalc( 'rmpath( ''../overload/warning/'' )' ) ...  % relative location of folder where TTEST-warning is located
%        };
%    h{1}();

for i = 1:2;
    if( i == 1 );
        TTEST overload warning on
        %evalc( 'addpath( ''../overload/warning/'' )' );  % first time run with TTEST-warning  % use evalc to surpress output  % relative location of folder where TTEST-warning is located
    else;
        TTEST overload warning off
        %evalc( 'rmpath( ''../overload/warning/'' )' );  % second time run with Matlab-warning
        end;
    MESSAGE( 'i = %i (i.e. %s `warning` is used)\n', i, tifh( i == 1, @() 'TTEST', @() 'Matlab' ) );
    
    SECTION( 'Matlab / TTEST specific tests' );    
    if( i == 1 );
        % TTEST-warning
        EXPECT_THROW( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'test:id2' );
        
        EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'test:id2' );
        EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'test:id2', 'test_macro.m' );
        
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1' ) );
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'FALSE_FILE' ) );
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id2' ) );
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id2', 'FALSE_FILE' ) );
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'test:id2', 'FALSE_FILE' ) );
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg'), warning('test:id3','some msg')}, 'test:id1', 'test:id2' ) );
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg'), warning('test:id3','some msg')}, 'test:id1', 'test:id2', 'FALSE_FILE' ) );
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), terror('test:id1', 'some msg')}, 'test:id1', 'terror' ) );
        
        EXPECT_FALSE( tt.expect_nthrow( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1' ) );
        EXPECT_FALSE( tt.expect_nthrow( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'EXPECT_NTHROW' ) );
        EXPECT_FALSE( tt.expect_nthrow( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'FALSE_FILE' ) );
        EXPECT_FALSE( tt.expect_nthrow( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id2' ) );
        EXPECT_FALSE( tt.expect_nthrow( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id2', 'EXPECT_NTHROW' ) );
        EXPECT_FALSE( tt.expect_nthrow( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id2','FALSE_FILE' ) );
        EXPECT_FALSE( tt.expect_nthrow( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'test:id2', 'FALSE_FILE' ) );
    elseif( i == 2 );
        % Matlab-warning
        EXPECT_THROW( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id2' );
        EXPECT_THROW( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id2', 'FALSE_FILE' );  % filename cannot be checked
        EXPECT_THROW( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'test:id' );
        EXPECT_THROW( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'test:id2', 'FALSE_FILE' );
        EXPECT_THROW( @() {warning('test:id1', 'some msg'), terror('test:id1', 'some msg')}, 'test:id1', 'terror' );  % function name not available in warning
        
        EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id2' );
        EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id2', 'EXPECT_NTHROW' );
        EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id2',-1, 'FALSE_FILE' );  % this succeeds, since the line number cannot be checked
        EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id2', 'FALSE_FILE' );  % this succeeds, since the filename cannot be checked
        
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1' ) );
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'EXPECT_NTHROW' ) );
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'FALSE_FILE' ) );
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg'), warning('test:id3','some msg')}, 'test:id1', 'test:id2' ) );
        EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg'), warning('test:id3','some msg')}, 'test:id1', 'test:id2', 'FALSE_FILE' ) );
        
        EXPECT_FALSE( tt.expect_nthrow( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1' ) );
        EXPECT_FALSE( tt.expect_nthrow( @() {warning('test:id1', 'some msg'), warning('test:id2', 'some msg')}, 'test:id1', 'EXPECT_NTHROW' ) );
    else;
        error( 'test:macro', 'fatal error' ); end;

    SECTION( 'Side effect tests' );
    EXPECT_THROW( 'a = twarning();', 'twarning:noid' );  % this sets a=1  % This test does not work on Octave 7.1
    EXPECT_EQ( a, 1  );
    
    a = 0;
    EXPECT_FALSE( tt.expect_throw( 'a = twarning();', 'wrong:id' ) );
    EXPECT_EQ( a, 1 );
    
    EXPECT_NTHROW( 'a = 1; a = 2; ' );
    EXPECT_EQ( a, 2 );
    EXPECT_NTHROW( '2;' );
    EXPECT_NTHROW( @() {1 2}, 'test:wrn1' );
    
    evalc( 'warning( ''test:wrn1'', ''test:wrn1'' );' );  % surpress output
    EXPECT_FALSE( tt.expect_throw( @() {}, 'test:wrn1' ) );    
    
    SECTION( 'General tests' );
    EXPECT_THROW( 'twarning()', ':noid' );
    EXPECT_THROW( @() twarning(), ':noid' );
    EXPECT_THROW( 'adsasfasdasda ajkdwaklsd;', ':UndefinedFunction' );
    EXPECT_THROW( 'adsasfasdasda ajkdwaklsd;', 'asd:asd', ':UndefinedFunction');
    EXPECT_THROW( @() error('err:id', 'throw'), 'ttest_msgthrow','Too many' );
    EXPECT_THROW( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:' );
    EXPECT_THROW( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'm','test:id1', 'm','test:id2' );
    EXPECT_THROW( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1', 'test:id2' );
    EXPECT_THROW( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'm', 'test:id1', 'm', 'test:id2', 'terror' );
    EXPECT_THROW( @() {warning('test:id1', 'some msg'), terror('test:id1', 'some msg')}, 'test:id1' );
    EXPECT_THROW( @() {terror('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1' );
    EXPECT_THROW( @() {terror('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1', 'terror' );
    EXPECT_THROW( @() {warning('test:id1', 'some msg'), warning('test:id1', 'some msg')}, 'test:id1' );
    
    EXPECT_NTHROW( '0;' );
    EXPECT_NTHROW( '' );
    EXPECT_NTHROW( ';' );
    EXPECT_NTHROW( 'adsasfasf a asdkasd;', 'asd:asd', 'MATLAB:UndefinedFunction', 'Octave:undefined-function' );
    EXPECT_NTHROW( @() error('test:err', 'some msg'), 'test:err', 'test:err2' );
    EXPECT_NTHROW( @() error('test:err', 'some msg'), 'test:err', 'test:err2', [0 1 580:600] );  % reported line number here may differ from Matlab version to Matlab versn
    EXPECT_NTHROW( @() error('test:err', 'some msg'), 'test_macro.m' );
    EXPECT_NTHROW( DISABLED('onoctave'), @() error('some msg'), '@()error(''some msg'')', 'test_macro' );  %#ok<ERTAG>
    EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), warning('test:id1', 'some msg')}, 'test:id1' );
    EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1', 'test:id2' );
    EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1', 'test:id2' );
    EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'o', 'test:id1', 'o', 'test:id2', 'terror.m' );
    EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1', 'o', 'test:id2', 'terror.m' );
    EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), terror('test:id1', 'some msg')}, 'test:id1' );
    EXPECT_NTHROW( @() {warning('test:id1', 'some msg'), terror('test:id1', 'some msg')}, 'test:id1', 'o', 'terror.m' );
    EXPECT_NTHROW( @() {terror('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1' );
    EXPECT_NTHROW( @() {terror('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1', 'terror' );
    EXPECT_NTHROW( @() nullary );
    EXPECT_NTHROW( 'nullary' );
    
    EXPECT_FALSE( tt.expect_throw( '0;', 'abc' ) );
    EXPECT_FALSE( tt.expect_throw( 'twarning();', 'asd:asd' ) );
    EXPECT_FALSE( tt.expect_throw( @() twarning() ) );
    EXPECT_FALSE( tt.expect_throw( 'thisisnotavalidcomand', 'wrong_id' ) );
    EXPECT_FALSE( tt.expect_throw( '0;', 'wrong_message' ) );
    EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1' ) );
    EXPECT_FALSE( tt.expect_throw( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'm', 'test:id1', 'm', 'test:id2', 0, 'terror' ) );
    EXPECT_FALSE( tt.expect_throw( @() {terror('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1', 'FALSE_FILE' ) );
    EXPECT_FALSE( tt.expect_throw( @() {terror('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id2' ) );
    EXPECT_FALSE( tt.expect_throw( @() {terror('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id2', 'terror' ) );
    
    EXPECT_FALSE( tt.expect_nthrow( 'adsasfasf a asdkasd;' ) );
    EXPECT_FALSE( tt.expect_nthrow( @() error('test:err', 'some msg'), 'test:err', -1 ) );  % line number -1 is never contained
    EXPECT_FALSE( tt.expect_nthrow( @() error('test:err', 'some msg'), 'test:err2', 'testTTEST' ) );
    EXPECT_FALSE( tt.expect_nthrow( @() error('test:err', 'some msg') ) );
    EXPECT_FALSE( tt.expect_nthrow( @() error('test:err', 'some msg') ) );
    EXPECT_FALSE( tt.expect_nthrow( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1' ) );
    EXPECT_FALSE( tt.expect_nthrow( @() {warning('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1', 'EXPECT_NTHROW' ) );
    EXPECT_FALSE( tt.expect_nthrow( @() {terror('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id1', 'FALSE_FILE' ) );
    EXPECT_FALSE( tt.expect_nthrow( @() {terror('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id2' ) );
    EXPECT_FALSE( tt.expect_nthrow( @() {terror('test:id1', 'some msg'), terror('test:id2', 'some msg')}, 'test:id2', 'terror' ) );
    end

TTEST overload warning on
%evalc( 'addpath( ''../overload/warning/'' )' );  % relative location of folder where TTEST-warning is located

lastwarn( w_msg, w_id );
lasterror( err );  %#ok<LERR>

SECTION
    [w_msg_0, w_id_0] = lastwarn();
    w_msg_1 = 'asdkjla';
    w_id_1 = 'asfiawt:asfioawf';
    lastwarn( w_msg_1, w_id_1 );
    EXPECT_FALSE( tt.expect_throw( @() 1 ) );
    [w_msg_2,w_id_2] = lastwarn;
    EXPECT_EQ( w_msg_1, w_msg_2 );
    EXPECT_EQ( w_id_1, w_id_2 );
    lastwarn( w_msg_0, w_id_1 );

%% TOOLBOX
TESTCASE( 'TOOLBOX' );
if( ismatlab );
    v = ver( 'matlab' );  %#ok<VERMATLAB>
    v = v.Version;
    EXPECT_TOOLBOX( 'matlab', ['<=' v] );
    EXPECT_TOOLBOX( 'matlab', ['>=' v] );
    EXPECT_TOOLBOX( 'matlab', ['==' v] );
    EXPECT_TOOLBOX( 'matlab', '<=100.100' );
    EXPECT_TOOLBOX( 'matlab', '>=9.0' );
    EXPECT_TOOLBOX( 'matlab', '>=9.0' );
    EXPECT_TOOLBOX( 'matlab', '>=8.100' );
    EXPECT_TOOLBOX( 'matlab', '<=100.1' );
    EXPECT_TOOLBOX( 'matlab', '>=R2018a' );
    
    EXPECT_FALSE( tt.expect_toolbox( 'matlab', ['<' v], OPTION('verbose',0) ) );
    EXPECT_FALSE( tt.expect_toolbox( 'matlab', ['>' v], OPTION('verbose',0) ) );
    EXPECT_FALSE( tt.expect_toolbox( 'matlab', '=9.100', OPTION('verbose',0) ) );
    EXPECT_FALSE( tt.expect_toolbox( 'matlab', '<=9.0', OPTION('verbose',0) ) );
    EXPECT_FALSE( tt.expect_toolbox( 'matlab', '>=100.100', OPTION('verbose',0) ) );
else;
    EXPECT_TOOLBOX( 'octave', '>=8.0' );
    end;


%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK,*NBRAK1,*NBRAK2>
