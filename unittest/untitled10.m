clc
condset( 'ct', 0)
while( true )
    ct = ct + 1
    rng( ct );
    try;
        T = gallery_matrixset( 'signal' );
    catch me;
        end;
    if( size(T{1}, 1) >= 5 && norm( T{1} - eye(size(T{1}/T{1}(1))) ) >= 2 && ...
        (allm(T{1}>=0) || allm(T{1}<=0 )) );
        break; end; end;
imagesc( T{1} )
colorbar

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>
