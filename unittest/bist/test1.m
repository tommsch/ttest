function [ x, y, z ] = test1( bar )  %#ok<STOUT,INUSD>
  % some amazing code
end

%!assert (test1 (1))
%!assert (test1 (1:10))
%!assert (test1 ("on"), "off")
%!error <must be positive integer> test1 (-1)
%!error <must be positive integer> test1 (1.5)

%!demo
%! %% see how cool test1() is:
%! test1([1:100])