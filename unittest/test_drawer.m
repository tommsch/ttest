% test drawer
% These tests check the pbt generator functions
%#ok<*ASGLU>
 
TTEST init

% preconditions 
cd private
pbtrepeat_h = @pbtrepeat;
cd ..

%%
TESTCASE
[ state, st, me ] = pbtrepeat_h( tt.array, 50, 200 );
EXPECT_ISEMPTY( me );

%%
TESTCASE
[ state, st, me ] = pbtrepeat_h( tt.bool, 50, 200 );
EXPECT_ISEMPTY( me );

%%
TESTCASE
[ state, st, me ] = pbtrepeat_h( tt.float, 50, 200 );
EXPECT_ISEMPTY( me );

%%
TESTCASE
[ state, st, me ] = pbtrepeat_h( tt.matrix, 50, 200 );
EXPECT_ISEMPTY( me );

%%
% TESTCASE
% [ state, st, me ] = pbtrepeat( tt.namevalue, 50, 200 );
% EXPECT_ISEMPTY( state );

%%
TESTCASE
[ state, st, me ] = pbtrepeat_h( tt.sqmatrix, 50, 200 );
EXPECT_ISEMPTY( me );


%%
%{
rng( state )
st = tt.sqmatrix;
st.example
%}