TTEST init


%% teardown

TESTCASE( 'setup', @() xunit_func( 42 ), 'teardown', @() xunit_func( 'plus' ) )
    EXPECT_EQ( xunit_func(), 42 );
    SECTION(  'setup', @() xunit_func( 'plus' ), 'teardown', @() xunit_func( 'times2' ) );
        EXPECT_EQ( xunit_func(), 43 );
    SECTION();
        EXPECT_EQ( xunit_func(), 87 );
    SECTION();
        EXPECT_EQ( xunit_func(), 175 );
    ENDSECTION();
    EXPECT_EQ( xunit_func(), 350 );
    SECTION();
        EXPECT_EQ( xunit_func(), 350 );
    ENDSECTION();
    SUBSECTION( 'setup', @() xunit_func( 5 ), 'teardown', @() xunit_func( 'plus' ) );
            EXPECT_EQ( xunit_func(), 5 );
ENDTESTCASE();
EXPECT_EQ( xunit_func(), 7 );
TESTCASE();
    EXPECT_EQ( xunit_func(), 7 );
ENDTESTCASE();
EXPECT_EQ( xunit_func(), 7 );

TESTCASE( 'setup',[], 'teardown',[] );
SECTION( 'setup',[], 'teardown',[] );
SUBSECTION( 'teardowntest', 'teardown', @() xunit_func( 666 ), 'setup', @() xunit_func( 42 ) );
    EXPECT_EQ( xunit_func(), 42 );
ENDTESTCASE();
EXPECT_EQ( xunit_func(), 666 );


%% teardown local workspace

TESTCASE( 'setup', @() assignin( 'caller', 'ttest_a_e3d977affa01ecccc3bfeddc2a89e870', 10 ) )
    EXPECT_EQ( ttest_a_e3d977affa01ecccc3bfeddc2a89e870, 10 );
ENDTESTCASE()
EXPECT_EQ( ttest_a_e3d977affa01ecccc3bfeddc2a89e870, 10 );

%% conditional
TESTCASE( 'conditional' );
    SECTION();
        a_cond = 1;
    EXPECT_FALSE( SECTION( @() false ) );
        EXPECT_EQ( a_cond, 1 );
    EXPECT_TRUE( SECTION( @() true ) );
        EXPECT_EXIST( 'a_cond', 'no' );
ENDTESTCASE();

%% tt
a_tt = 1;
x_tt = 10;
TESTCASE( 'tt1' );
    EXPECT_EQ( a_tt, 1 );
    b_tt = 1;  %#ok<NASGU>
TESTCASE( 'tt2' );  % section 't1' is implicitely closed
    EXPECT_EQ( a_tt, 1 );
    EXPECT_EXIST( 'b_tt', 0 );
    b_tt = 3; 
    SECTION( 'tt2' );  % section 's1' is nested in section 't1'
        EXPECT_EQ( a_tt, 1 );
        EXPECT_EQ( b_tt, 3 );
        b_tt = 4; 
        c_tt = 4;  %#ok<NASGU>
    SECTION( 'tt2' );  % unnamed section is implicitely closed
        EXPECT_EQ( a_tt, 1 );
        EXPECT_EQ( b_tt, 3 );
        EXPECT_EXIST( 'c', 0 );
        c_tt = 5; d_tt = 6;
        SUBSECTION( 'tt3' );
            EXPECT_EQ( a_tt, 1 );
            EXPECT_EQ( b_tt, 3 );
            EXPECT_EQ( c_tt, 5 );
            EXPECT_EQ( d_tt, 6 );
            d_tt = 7;
        ENDSUBSECTION  % we are back in 's1' as it was before opening of 's3'
        EXPECT_EQ( c_tt, 5 );
        EXPECT_EQ( d_tt, 6 );
        % ENDSUBSECTION( 's3' );  % would throw an error, since no subsection is open
TESTCASE( 'tt4' );  % closes everything
    EXPECT_EQ( a_tt, 1 );
    EXPECT_EXIST( 'b_tt', 0 );
ENDTESTCASE;
EXPECT_EQ( a_tt, 1 );
b_tt = 2;  % we are outside of all sections here
TESTCASE( 'tt5' );
    EXPECT_EQ( a_tt, 1 );
    EXPECT_EQ( b_tt, 2 );
ENDTESTCASE();

%% t2
a_t2 = 1;
SUBSECTION();
SUBSECTION();
    EXPECT_EQ( a_t2, 1 );
ENDTESTCASE();

%% t3
a_t3 = 1;
TESTCASE();
    b_t3 = 2;
    SUBSECTION();
        EXPECT_EQ( a_t3, 1 );
        EXPECT_EQ( b_t3, 2 );
    ENDSUBSECTION();
    EXPECT_EQ( a_t3, 1 );
    EXPECT_EQ( b_t3, 2 );
ENDTESTCASE();

%% t4
a_t4 = 1;  %#ok<NASGU>
SUBSECTION();
    b_t4 = 2;
    a_t4 = 10;
    EXPECT_EQ( a_t4, 10 );
TESTCASE();
    EXPECT_EXIST( 'a_t4', 'var' );
    EXPECT_EXIST( 'b_t4', 'no' );
ENDTESTCASE();

%% t5
SUBSECTION();
    ttest_variable_c_asdkq28r0nas90xcvnm_c = 3;
SECTION();
    ttest_variable_c_asdkq28r0nas90xcvnm_b = 2;
TESTCASE()
    ttest_variable_c_asdkq28r0nas90xcvnm_a = 1;
    EXPECT_EXIST( 'ttest_variable_c_asdkq28r0nas90xcvnm_c', 0 );
    EXPECT_EXIST( 'ttest_variable_c_asdkq28r0nas90xcvnm_b', 0 );
    EXPECT_EQ( ttest_variable_c_asdkq28r0nas90xcvnm_a , 1 );
ENDTESTCASE();
EXPECT_EXIST( 'ttest_variable_c_asdkq28r0nas90xcvnm_a', 0 );
EXPECT_EXIST( 'ttest_variable_c_asdkq28r0nas90xcvnm_b', 0 );
EXPECT_EXIST( 'ttest_variable_c_asdkq28r0nas90xcvnm_c', 0 );
    

%% t6
a_t6 = 1;
SECTION();
    c_t6 = 3;
ENDTESTCASE();

EXPECT_EXIST( 'a_t6', 'var' );
EXPECT_EXIST( 'c_t6', 'no' );

%% t7
a_t7 = 1;
SUBSECTION();
    c_t7 = 3;
ENDTESTCASE();

EXPECT_EQ( a_t7, 1 );
EXPECT_EXIST( 'c_t7', 'no' );

%% t8
a_t8 = 1;
SUBSECTION();
    c_t8 = 3;
ENDSECTION();

EXPECT_EQ( a_t8, 1 );
EXPECT_EXIST( 'c_t8', 'no' );
ENDTESTCASE();

%% t9
a_t9 = 1;
SECTION();
    c_t9 = 3;
ENDSECTION();

EXPECT_EQ( a_t9, 1 );
EXPECT_EXIST( 'c_t9', 'no' );
ENDTESTCASE();

%% t10
a_t10 = 1;
SUBSECTION();
    c_t10 = 3;
ENDSUBSECTION();

EXPECT_EQ( a_t10, 1 );
EXPECT_EXIST( 'c_t10', 'no' );
ENDTESTCASE();

%% t11
a_t11 = 123;  %#ok<NASGU>
TESTCASE();
a_t11 = 234;
TESTCASE();
TESTCASE();
    EXPECT_EQ( a_t11, 123 );
ENDTESTCASE();
EXPECT_EQ( a_t11, 123 );

%% t12
a_t12 = 1;
TESTCASE();
    b_t12 = 2;
    SUBSECTION()
        c_t12 = 3;
    SECTION();
        EXPECT_EQ( a_t12, 1 );
        EXPECT_EQ( b_t12, 2 );
        EXPECT_EXIST( 'c_t12', 'no' );
ENDTESTCASE();        

%% restore
global ttest_a_global_restore  %#ok<GVMIS>
ttest_a_global_restore = 1;  %#ok<NASGU>
TESTCASE( 'global' );
    SECTION( '1a' );
        ttest_a_global_restore = 2;
    SECTION( '1b' );    
        EXPECT_EQ( ttest_a_global_restore, 1 );
        
TESTCASE( 'path' );
    SECTION( '1a'   );
        addpath( 'at' )
    SECTION( '1b' );
        EXPECT_FALSE( onpath( fullfile('TTEST', 'unittest', 'at') ) );
        
TESTCASE( 'warning' );
    SECTION( '1a' );
        warning( 'off', 'TTEST:sectiontest' );
    SECTION( '1b' );    
        w = warning;
        idx = arrayfun( @(i) strcmp(w(i).identifier, 'TTEST:sectiontest'), 1:numel(w) );
        EXPECT_TRUE( ~any(idx) );  
        
TESTCASE( 'pwd' );
    SECTION( '1a' );
        cd cache
    SECTION( '1b' );
        p = pwd;
        EXPECT_FALSE( numel(p) >=  5 && isequal(p(end-4:end),'cache') );  
ENDTESTCASE();

db_before_debug_test = dbstatus();
TESTCASE( 'debug' );
    SECTION( '1a' );
        dbstop test_section 1;
    SECTION( '1b' );
        db = dbstatus;
        idx = arrayfun( @(i) strcmp(db(i).name, 'test_section'), 1:numel(db) );
        EXPECT_TRUE( any(idx) );
        dbclear test_section
    SECTION( '1c' );
        dbstop in test_section at 1 if isequal( cwd, 'ttest_' );
    SECTION( '1d' );
        db = dbstatus;
        idx = arrayfun( @(i) strcmp(db(i).name, 'test_section'), 1:numel(db) );
        EXPECT_TRUE( ~any(idx) );
        dbclear test_section
ENDTESTCASE();
dbstop( db_before_debug_test );
clear db_before_debug_test
    
TESTCASE( 'rng1' );
    SECTION( '1a' );
        TTEST( 'var', 'rand', randi(10000) );
    SECTION( '1b' );
        EXPECT_NE( randi(10000),  TTEST( 'var', 'rand' ) );

TESTCASE( 'base' );
    num = num2str( randi(1000000) );
    name = ['ttest_x_hajskdh7q2gha' num];
    SECTION( '1a' );
        assignin( 'base', name, 10 );
    SECTION( '1b' );
        wh = evalin( 'base', 'who' );
        idx = arrayfun( @(i) strcmp(wh{i},name), 1:numel(wh) );  % Note: this test fails when executed from the command line, since then the caller workspace and the base workspace coincide
        EXPECT_TRUE( any(idx) );
        evalin( 'base', ['clear ' name] );
ENDTESTCASE();

%%




%#ok<*NOSEL,*NOSEMI,*ALIGN>
