% test matcher

TTEST init
TTEST v 0

% AllOf, Compose, etc... are tested at the end of the file

%% BeginsWith
assert(  EXPECT_THAT( 'Thomas', tt.BeginsWith('') ) );
assert(  EXPECT_THAT( 'Thomas', tt.BeginsWith('Th') ) );
assert(  EXPECT_THAT( 'Thomas', tt.BeginsWith('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', tt.BeginsWith('t') ) );

%% Contains
assert( EXPECT_THAT( [1 2 3], tt.Contains(2) ) );
assert( EXPECT_THAT( [1 2 3], tt.Contains(2) ) );

%% DoubleEq
assert(  EXPECT_THAT( 1+eps, tt.DoubleEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps, tt.DoubleEq(1) ) );
assert( ~EXPECT_THAT( nan, tt.DoubleEq(nan) ) );
assert(  EXPECT_THAT( 1, tt.DoubleEq(1+eps) ) );

%% EndsWith
assert(  EXPECT_THAT( 'Thomas', tt.EndsWith('') ) );
assert(  EXPECT_THAT( 'Thomas', tt.EndsWith('mas') ) );
assert(  EXPECT_THAT( 'Thomas', tt.EndsWith('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', tt.EndsWith('S') ) );

%% Each
assert( EXPECT_THAT( [1 2 3], tt.Each([1 2 3]) ) );

%% Eq
assert(  EXPECT_THAT( 1, tt.Eq(1) ) );
assert( ~EXPECT_THAT( 2, tt.Eq(1) ) );
assert( ~EXPECT_THAT( nan, tt.Eq(nan) ) );
assert(  EXPECT_THAT( [2 3], tt.Each(tt.Eq([2 3])) ) );
assert( ~EXPECT_THAT( [2 3], tt.Eq([2 3]) ) );
assert(  EXPECT_THAT( [2 3], tt.Each(tt.Eq([2 3])) ) );

%% FloatEq
assert(  EXPECT_THAT( 1+eps, tt.FloatEq(1) ) );
assert(  EXPECT_THAT( 1+5*eps, tt.FloatEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps(single(1)), tt.FloatEq(1) ) );
assert( ~EXPECT_THAT( nan, tt.FloatEq(nan) ) );

%% Ge
assert(  EXPECT_THAT( 2, tt.Ge(0) ) );
assert(  EXPECT_THAT( 1, tt.Ge(0) ) );
assert(  EXPECT_THAT( 0, tt.Ge(0) ) );
assert( ~EXPECT_THAT( -1, tt.Ge(0) ) );

%% Gt
assert(  EXPECT_THAT( 1, tt.Gt(0) ) );
assert( ~EXPECT_THAT( 0, tt.Gt(0) ) );
assert( ~EXPECT_THAT( -1, tt.Gt(0) ) );

%% HasSubstr
assert(  EXPECT_THAT( 'Thomas', tt.HasSubstr('') ) );
assert(  EXPECT_THAT( 'Thomas', tt.HasSubstr('hom') ) );
assert( EXPECT_THAT( 'Thomas', tt.HasSubstr('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', tt.HasSubstr('H') ) );

%% Id
assert(  EXPECT_THAT( 6, tt.Ge(5,tt.Id) ) );
assert( ~EXPECT_THAT( 0, tt.IsTrue(tt.Id) ) );
assert(  EXPECT_THAT( 10, tt.Ge(5,tt.Id) ) );

%% IsEmpty
assert(  EXPECT_THAT( [], tt.IsEmpty() ) );
assert( ~EXPECT_THAT( 0, tt.IsEmpty() ) );
assert(  EXPECT_THAT( [], tt.IsEmpty() ) );

%% IsEq
assert( EXPECT_THAT( {0}, tt.IsEq({0}) ) );
assert( EXPECT_THAT( [2 3], tt.IsEq([2 3]) ) );

%% IsFalse
assert(  EXPECT_THAT( 0, tt.IsFalse() ) );
assert( ~EXPECT_THAT( 1, tt.IsFalse() ) );

%% IsNan
assert(  EXPECT_THAT( nan, tt.IsNan() ) );
assert( ~EXPECT_THAT( 1, tt.IsNan() ) );
assert(  EXPECT_THAT( [1 2], tt.Not(tt.Each(tt.IsNan)) ) );

%% IsSubsetOf
assert( EXPECT_THAT( [1 2 3], tt.IsSubsetOf([1 2 3 4]) ) );

%% SubsetOf
assert( EXPECT_THAT( [1 2 3], tt.SubsetOf([1 2 3 4]) ) );

%% IsSupersetOf
assert( EXPECT_THAT( [1 2 3], tt.IsSupersetOf([1 2]) ) );

%% SupersetOf
assert( EXPECT_THAT( [1 2 3], tt.IsSupersetOf([1 2]) ) );

%% IsTrue
assert(  EXPECT_THAT( 1, tt.IsTrue() ) );
assert( ~EXPECT_THAT( 0, tt.IsTrue() ) );
assert(  EXPECT_THAT( ~false, tt.IsTrue ) );

%% Le
assert(  EXPECT_THAT( 1, tt.Le(2) ) );
assert(  EXPECT_THAT( 0, tt.Le(0) ) );
assert( ~EXPECT_THAT( -1, tt.Le(-2) ) );

%% Lt
assert(  EXPECT_THAT( 1, tt.Lt(2) ) );
assert( ~EXPECT_THAT( 0, tt.Lt(0) ) );
assert( ~EXPECT_THAT( -1, tt.Lt(-2) ) );

%% MatcherRegEx

%% NanSensitiveDoubleEq
assert(  EXPECT_THAT( 1+eps, tt.NanSensitiveDoubleEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps, tt.NanSensitiveDoubleEq(1) ) );
assert(  EXPECT_THAT( nan, tt.NanSensitiveDoubleEq(nan) ) );
assert(  EXPECT_THAT( [3 nan], tt.Each(tt.NanSensitiveDoubleEq([3 nan])) ) );

%% NanSensitiveFloatEq
assert(  EXPECT_THAT( 1+eps, tt.NanSensitiveFloatEq(1) ) );
assert(  EXPECT_THAT( 1+5*eps, tt.NanSensitiveFloatEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps(single(1)), tt.FloatEq(1) ) );
assert(  EXPECT_THAT( nan, tt.NanSensitiveFloatEq(nan) ) );

%% NanSensitiveNear
assert(  EXPECT_THAT( 1.2, tt.NanSensitiveNear(1,0.5) ) );
assert( ~EXPECT_THAT( 1.6, tt.NanSensitiveNear(1,0.5) ) );
assert( ~EXPECT_THAT( nan, tt.NanSensitiveNear(1,0.5) ) );
assert( ~EXPECT_THAT( nan, tt.NanSensitiveNear(1,nan) ) );
assert(  EXPECT_THAT( nan, tt.NanSensitiveNear(nan,0.5) ) );
assert(  EXPECT_THAT( nan, tt.NanSensitiveNear(nan,nan) ) );
assert(  EXPECT_THAT( nan, tt.NanSensitiveNear(nan,3) ) );

%% NanSensitiveSingleEq
assert(  EXPECT_THAT( 1+eps, tt.NanSensitiveSingleEq(1) ) );
assert(  EXPECT_THAT( 1+5*eps, tt.NanSensitiveSingleEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps(single(1)), tt.FloatEq(1) ) );
assert(  EXPECT_THAT( nan, tt.NanSensitiveSingleEq(nan) ) );

%% Ne
assert(  EXPECT_THAT( false, tt.Ne(true) ) );
assert(  EXPECT_THAT( 1, tt.Ne(2) ) );
assert(  EXPECT_THAT( 1, tt.Ne(0) ) );
assert( ~EXPECT_THAT( 0, tt.Ne(0) ) );
assert(  EXPECT_THAT( nan, tt.Ne(nan) ) );

%% Near
assert(  EXPECT_THAT( 1.2, tt.Near(1,0.5) ) );
assert( ~EXPECT_THAT( 1.6, tt.Near(1,0.5) ) );
assert( ~EXPECT_THAT( nan, tt.Near(1,0.5) ) );
assert( ~EXPECT_THAT( nan, tt.Near(1,nan) ) );
assert( ~EXPECT_THAT( nan, tt.Near(nan,0.5) ) );
assert( ~EXPECT_THAT( nan, tt.Near(nan,nan) ) );
assert(  EXPECT_THAT( 8, tt.Near(10,3) ) );

%% Not
assert(  EXPECT_THAT( false, tt.IsFalse() ) );
assert( ~EXPECT_THAT( 1, tt.Not(tt.Ne(2)) ) );
assert( ~EXPECT_THAT( 1, tt.Not(tt.Ne(0)) ) );
assert(  EXPECT_THAT( 0, tt.Not(tt.Ne(0)) ) );
assert( ~EXPECT_THAT( nan, tt.Not(tt.Ne(nan)) ) );

%% NumelIs
assert( ~EXPECT_THAT( [1 2 3], tt.NumelIs(4) ) );
assert(  EXPECT_THAT( [1 2 3], tt.NumelIs(3) ) );
assert(  EXPECT_THAT( [1 2 3], tt.Not(tt.NumelIs(2)) ) );

%% NotEmpty
assert(  EXPECT_THAT( 0, tt.NotEmpty() ) );
assert( ~EXPECT_THAT( [], tt.NotEmpty() ) );
assert(  EXPECT_THAT( [1 2], tt.NotEmpty ) );

%% SingleEq
assert(  EXPECT_THAT( 1+eps, tt.SingleEq(1) ) );
assert(  EXPECT_THAT( 1+5*eps, tt.SingleEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps(single(1)), tt.SingleEq(1) ) );
assert( ~EXPECT_THAT( nan, tt.SingleEq(nan) ) );

%% StartsWith
assert(  EXPECT_THAT( 'Thomas', tt.StartsWith('') ) );
assert(  EXPECT_THAT( 'Thomas', tt.StartsWith('Th') ) );
assert(  EXPECT_THAT( 'Thomas', tt.StartsWith('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', tt.StartsWith('t') ) );

%% StrCaseEq
assert(  EXPECT_THAT( 'Thomas', tt.StrCaseEq('thOMAS') ) );
assert(  EXPECT_THAT( 'Thomas', tt.StrCaseEq('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', tt.StrCaseEq('Thoma') ) );

%% StrCaseNe
assert( ~EXPECT_THAT( 'Thomas', tt.StrCaseNe('thOMAS') ) );
assert( ~EXPECT_THAT( 'Thomas', tt.StrCaseNe('Thomas') ) );
assert(  EXPECT_THAT( 'Thomas', tt.StrCaseNe('Thoma') ) );

%% StrEq
assert( ~EXPECT_THAT( 'Thomas', tt.StrEq('thOMAS') ) );
assert(  EXPECT_THAT( 'Thomas', tt.StrEq('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', tt.StrEq('Thoma') ) );

%% StrNe
assert(  EXPECT_THAT( 'Thomas', tt.StrNe('thOMAS') ) );
assert( ~EXPECT_THAT( 'Thomas', tt.StrNe('Thomas') ) );
assert(  EXPECT_THAT( 'Thomas', tt.StrNe('Thoma') ) );

%% Special Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% AllOf
assert(  EXPECT_THAT( [2 3], tt.Each(tt.AllOf(tt.Ge(2), tt.Le(3))) ) );
assert( ~EXPECT_THAT( 3,     tt.AllOf(tt.Ge(0), tt.Le(2)) ) );
assert(  EXPECT_THAT( [],    tt.AllOf() ) );
assert(  EXPECT_THAT( 5,     tt.AllOf() ) );
assert(  EXPECT_THAT( [],    tt.Each(tt.AllOf(tt.Ge(2))) ) );
assert(  EXPECT_THAT( 5,     tt.AllOf(tt.Ge(2)) ) );
assert(  EXPECT_THAT( 5,     tt.AllOf(tt.Ge(2), tt.Le(6)) ) );
assert(  EXPECT_THAT( 5,     tt.AllOf(tt.Ge(2), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6)) ) );
assert( ~EXPECT_THAT( 5,     tt.AllOf(tt.Ge(2), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(0)) ) );
assert(  EXPECT_THAT( 5,     tt.AllOf(tt.Ne(2), tt.Ne(6)) ) );
 
%% AnyOf
assert(  EXPECT_THAT( [2 3], tt.Each(tt.AnyOf(tt.Ge(3),tt.Le(2))) ) );
assert( ~EXPECT_THAT( [],    tt.AnyOf() ) );
assert( ~EXPECT_THAT( 5,     tt.AnyOf() ) );
assert(  EXPECT_THAT( [],    tt.Not(tt.AnyOf()) ) );
assert(  EXPECT_THAT( 5,     tt.AllOf(tt.Ge(2)) ) );
assert(  EXPECT_THAT( 5,     tt.AllOf(tt.Ge(2), tt.Le(6)) ) );
assert(  EXPECT_THAT( 5,     tt.AllOf(tt.Ge(2), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6)) ) );
assert( ~EXPECT_THAT( 5,     tt.AllOf(tt.Ge(2), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(6), tt.Le(0)) ) );
assert(  EXPECT_THAT( 5,     tt.AllOf(tt.Ne(2), tt.Ne(6)) ) );


%% Compose
assert(  EXPECT_THAT( [2 3], tt.Each(tt.AnyOf(tt.Ge(3),tt.Le(2))) ) );
assert( ~EXPECT_THAT( 2,     tt.Compose(tt.Not(),tt.Ge(1)) ) );
assert(  EXPECT_THAT( 2,     tt.Compose(tt.Not(),tt.Not(),tt.Ge(2)) ) );
assert(  EXPECT_THAT( 3,     tt.Compose( tt.AllOf(tt.Ge(4),tt.Le(4)), tt.Unary(@(x)x+3), tt.IsTrue() ) ) );
assert(  EXPECT_THAT( 0,     tt.Compose(tt.Eq(3),tt.Compose(@(x)x+1,@(x)x+1),@(x)x+1) ) );

%% Matcher
tt.Matcher( 'Odd', @(x) mod(x,2) );
assert( EXPECT_THAT( 3, Odd ) );
Even = tt.Matcher( @(x) ~mod(x,2) );
assert( EXPECT_THAT( 2, Even ) );

%% Thread
assert(  EXPECT_THAT( [1 2], [],        tt.Contains(tt.Thread(tt.IsEmpty())) ) );
assert( ~EXPECT_THAT( [1 2], [1],       tt.Contains(tt.Thread(tt.IsEmpty())) ) );  %#ok<NBRAK>
assert(  EXPECT_THAT( [2 3 4], [1 2 3], tt.Each(tt.Thread(tt.NumelIs(3))) ) );
assert( ~EXPECT_THAT( [2 3 4], [1 3],   tt.Each(tt.Thread(tt.NumelIs(3))) ) );

%% Unary
assert( EXPECT_THAT( [1 2 3], tt.Unary(@(y) numel(y)==3) ) );
assert( EXPECT_THAT( 1, tt.DoubleEq(sin(1),tt.Unary(@sin)) ) );

%% WhenSorted
assert( EXPECT_THAT( [3 1 2], tt.Each(tt.WhenSorted([1 2 3])) ) );


%% postprocessing
% Set errorflag to false
TTEST errflag false

%#ok<*NOSEL,*NOSEMI,*ALIGN>
