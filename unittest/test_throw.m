% test TTEST THROW
% These tests check macro calls which must throw

TTEST( 'init' );
throw_id = TTEST( 'init', 'throw_id' );
%TTEST v 0

% preconditions 

%% ASSERT
EXPECT_TRUE( function_throw( throw_id ) );
TTEST( throw_id, 'errorflag', false );
TTEST( throw_id, 'runttests_checked', true );

%#ok<*NOSEL,*NOSEMI,*ALIGN>
