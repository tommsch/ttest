% test_time

TTEST init

%% TIMETESTS
TESTCASE( 'TIMETESTS' );
    % warning( 'off', 'TTEST:Maxtime' );

    if( tt.expect_toolbox('parallel') );
        SECTION( 'MINTIME' );
            EXPECT_MINTIME( @() secloop(10), 2 );
            for i = 1:10;
                val = randi(20) + 3;
                EXPECT_MINTIME( @() secloop(val), val-2 );
            end; end;
            
    SECTION( 'MAXTIME' );    
        EXPECT_MAXTIME( @() secloop(2), 8 );
        EXPECT_MAXTIME( '1', 2 );
        EXPECT_FALSE( tt.expect_maxtime( @() pause(5), 1 ) );
        EXPECT_MAXTIME( @() pause(2), 10 );
        EXPECT_FALSE( tt.expect_maxtime( 'badcall', 0.00001 ) );
        evalc( 'EXPECT_MAXTIME( @() terror, 2 );' );  % throwing errors after function started counts as successful            

%     SECTION( 'DEFINED' );
%         assert( ~EXPECT_DEFINED( @() ajsd ) );
%         assert( EXPECT_DEFINED( @() 2 ) );
        
    if( tt.expect_toolbox('parallel') );
        SECTION( 'MINTIME' );
            EXPECT_MINTIME( @() secloop(10), 2 );
            for i = 1:100
                val = randi(20) + 3;
                EXPECT_MINTIME( @() secloop(val), val-2 );
            end; end;
        
    SECTION( 'Timetests' );
        EXPECT_FALSE( tt.expect_maxtime( @() secloop(1), .1 ) );
        
        EXPECT_FALSE( tt.expect_maxtime( @() secloop(1), .1 ) );
        

   
%%
ENDTESTCASE();


%#ok<*NOSEL,*NOSEMI,*ALIGN>
