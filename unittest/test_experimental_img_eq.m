TTEST init


if( isoctave );
    ASSUME_TRUE( contains( available_graphics_toolkits, 'qt' ) ); end;

ASSUME_FAIL();  % This test is disabled until EXPECT_IMG_EQ works reliable

%% IMG_EQ
TESTCASE( 'IMG_EQ' );
SECTION( 'two_round_test' );

    % XX use `set(groot, 'DefaultFigureVisible', 'off')` in TESTCASE, so that figures are invisible by default
    if( isoctave() );
        warning( 'off', 'Octave:delete:no-such-file' );
        err = lasterror();  %#ok<LERR>
        end;
    delete( 'img/*.png' );
    delete( [TTEST('base') filesep 'tmp' filesep 'img' filesep '*.png'] );
    
    if( isoctave() );
        graphics_toolkit('qt'); end;  % getframe is only implemented for graphics_toolkit ("qt"), not for "fltk" and "gnuplot"  XX Maybe move this to _IMG_EQ

    MESSAGE( 'iteration' );
    for iteration = 1:2;  % in the first round the images are generated. In the second they are compared
        % XX Tests where something fails are missing
        %wait_duration = 0;

        rng( 1 );
        EXPECT_IMG_EQ( @() imagesc(randn(40, 40)), [TTEST('base') '/unittest/img/%v_img_eq_imagesc1000_%o.png'] );
        EXPECT_IMG_EQ(     imagesc(randn(40, 40)), 'img/%v_img_eq_imagesc2000_%o.png' );
        EXPECT_IMG_EQ( @() plot(randn(1, 40)),     'img/%v_img_eq_plot1000_%o.png' );
        EXPECT_IMG_EQ(     plot(randn(1, 40)),     'img/%v_img_eq_plot2000_%o.png' );
        close all hidden;

        rng( 1 );
        f10 = figure( 10 ); set_position( f10, [500, 400] );
        EXPECT_IMG_EQ( @() plot(randn(1, 40)),     'img/%v_img_eq_nofig_%o.png', '-nofig' );
        close( f10 );
        rng(1);
        f20 = figure( 20 ); set_position( f20,  [500, 400] );
        EXPECT_IMG_EQ( @() plot(randn(1, 40)),     'img/%v_img_eq_nofig_%o.png', '-nofig' );
        close all hidden;
        
        rng( 1 );
        %pause(.1);
        EXPECT_IMG_EQ( @() imagesc(randn(40, 40)), 'img/%v_img_eq_imagesc3000_%o.png' );
        rng( 1 );
        %pause(.1);
        EXPECT_IMG_EQ( @() imagesc(randn(40, 40)), 'img/%v_img_eq_imagesc5000_%o.png' );
        %pause(.1);
        EXPECT_IMG_EQ( 'img/%v_img_eq_imagesc3000_%o.png', 'img/%v_img_eq_imagesc5000_%o.png' );
        close all hidden;
        
        rng( 1 );
        f2 = figure( 2 );
        EXPECT_NTHROW( @() plot( rand(3,10), 'r-' ) );  % _NTHROW necessary for Octave 9.1
        rng( 1 );
        f3 = figure( 3 );
        EXPECT_NTHROW( @() plot( rand(3, 10), 'r-' ) );
        EXPECT_IMG_EQ( f2, f3, '-nofig' );
        close all hidden;
        
%        TTEST v 0
%        EXPECT_IMG_EQ( @() plot([1 2 3]), @() plot([1 2 3]), 'img/%s_img_eq_plot_plot_file.png' );
%        EXPECT_IMG_EQ( @() plot([1 2 3]), @() plot([1 2 4]), 'img/%s_img_eq_plot_plot_file.png', OPTION('not') );
%        EXPECT_IMG_EQ( @() plot([1 2 3]), @() plot([1 2 3]) );
%        EXPECT_IMG_EQ( @() plot([1 2 3]), @() plot([1 2 4]), @() plot([1 2 4]), OPTION('not') );
%        TTEST v 1
%        close all
    
        end;

SECTION( 'regression' );
    EXPECT_IMG_EQ( @() plot( [1 2], 'k', 'linewidth',1 ), @() plot( [1 2], 'k', 'linewidth',10 ) );



%#ok<*NOSEL,*NOSEMI,*ALIGN>
