% test testrunner

TTEST init
cleandir1 = onCleanup_lazy( @(x) cd(x), cd( 'testrunner' ) );  %#ok<NASGU>

%% freename
if( ismatlab );
    EXPECT_EQ( tt.freename( 'freename', 'free', 1 ), 'free10' );
    EXPECT_EQ( tt.freename( 'freename', 'free', 2 ), 'free10' );
    EXPECT_EQ( tt.freename( 'freename', 'newfile', 2 ), 'newfile01' );
    end;

%% makefilelist

%% maketestlist

%% no_options
DISABLED_clear_TTEST_vars();
EXPECT_TRUE( runttests( '-recursiveoverride' ) );
EXPECT_EQ( TTEST( 'ttest_id_test2',                   'var','test2',1000 ),                   'executed' );
EXPECT_EQ( TTEST( 'ttest_id_doctest_unittest',        'var','doctest_unittest',1010 ),        'executed' );
EXPECT_EQ( TTEST( 'ttest_id_doctest_doctest',         'var','doctest_doctest',1020 ),         [] );
EXPECT_EQ( TTEST( 'ttest_id_functionbasedtest_test1', 'var','functionbasedtest_test1',1030 ), 'executed' );
EXPECT_EQ( TTEST( 'ttest_id_functionbasedtest_test2', 'var','functionbasedtest_test2',1040 ), 'executed' );


%% doctest
DISABLED_clear_TTEST_vars();
EXPECT_TRUE( runttests( 'doctest', '-recursiveoverride' ) );
EXPECT_EQ( TTEST( 'ttest_id_test2_doctest',    'var','test2_doctest',1050 ),    'executed' ); 
EXPECT_EQ( TTEST( 'ttest_id_func',             'var','func',1060 ),             'executed' );
EXPECT_EQ( TTEST( 'ttest_id_doctest_unittest', 'var','doctest_unittest',1070 ), []);
EXPECT_EQ( TTEST( 'ttest_id_doctest_doctest',  'var','doctest_doctest',1080 ),  'executed' );
%TODO_EQ( TTEST( 'ttest_id_doctest_multiline_doctest', 'var','doctest_multiline_doctest',666 ), 'doctest_multiline_doctest' );


%% only_one_doctest
DISABLED_clear_TTEST_vars();
EXPECT_TRUE( runttests( 'doctest', 'func', '-recursiveoverride' ) );
EXPECT_EQ( TTEST( 'ttest_id_func',             'var','func',1090 ),            'executed' );
EXPECT_EQ( TTEST( 'ttest_id_doctest_unittest', 'var','doctest_unittest',1100 ), [] );
EXPECT_EQ( TTEST( 'ttest_id_doctest_doctest',  'var','doctest_doctest',1110 ),  [] );

%% only_one_unittest
DISABLED_clear_TTEST_vars();
EXPECT_TRUE( runttests( 'unittest', 'func2', '-recursiveoverride' ) );
EXPECT_EQ( TTEST( 'ttest_id_func2', 'var', 'func2', 1120 ), 'executed' );
EXPECT_EQ( TTEST( 'ttest_id_test2', 'var', 'test2', 1130 ), [] );

%% recursive
DISABLED_clear_TTEST_vars();
EXPECT_TRUE( runttests( 'recursive' ) );
EXPECT_EQ( TTEST( 'ttest_id_test2',                   'var','test2',1140 ),                     'executed' );
EXPECT_EQ( TTEST( 'ttest_id_doctest_unittest',        'var','doctest_unittest',1150 ),          'executed' );
EXPECT_EQ( TTEST( 'ttest_id_doctest_doctest',         'var','doctest_doctest',1160 ),           [] );
EXPECT_EQ( TTEST( 'ttest_id_recursivetest_unittest',  'var','recursivetest_unittest',1170 ),    'executed' );
EXPECT_EQ( TTEST( 'ttest_id_recursivetest_doctest',   'var','recursivetest_doctest',1180 ),     [] );
EXPECT_EQ( TTEST( 'ttest_id_func_subfolder_unittest', 'var','func_subfolder_unittest',1190 ),   [] );
EXPECT_EQ( TTEST( 'ttest_id_func_subfolder_doctest',  'var','func_subfolder_doctest',1200 ),    [] );

%% recursive_doctest
DISABLED_clear_TTEST_vars();
EXPECT_TRUE( runttests( 'recursive', 'doctest', '-recursiveoverride' ) );
EXPECT_EQ( TTEST( 'ttest_id_test2_doctest',           'var','test2_doctest',1210 ),            'executed' );
EXPECT_EQ( TTEST( 'ttest_id_func',                    'var','func',1220 ),                     'executed' );
EXPECT_EQ( TTEST( 'ttest_id_doctest_unittest',        'var','doctest_unittest',1230 ),         [] );
EXPECT_EQ( TTEST( 'ttest_id_doctest_doctest',         'var','doctest_doctest',1240 ),          'executed' );
EXPECT_EQ( TTEST( 'ttest_id_recursivetest_unittest',  'var','recursivetest_unittest',1250 ),   [] );
EXPECT_EQ( TTEST( 'ttest_id_recursivetest_doctest',   'var','recursivetest_doctest',1260 ),    'executed' );
EXPECT_EQ( TTEST( 'ttest_id_func_subfolder_unittest', 'var','func_subfolder_unittest',1270 ),  [] );
EXPECT_EQ( TTEST( 'ttest_id_func_subfolder_doctest',  'var','func_subfolder_doctest',1280 ),   'executed' );

%% empty folder
cleandir2 = onCleanup_lazy( @(x) cd(x), cd( 'runttests_empty' ) );  %#ok<NASGU>
EXPECT_THROW( @() runttests( '-recursiveoverride', '-search_parent_dirs',false ), 'runttests:notests' );
clear cleandir2

%% nested
EXPECT_THROW(  @() runttests( 'nested1' ), 'runttests:runtime' );
EXPECT_THROW(  @() runttests( 'nested2' ), 'runttests:runtime' );
EXPECT_NTHROW( @() runttests( 'nested3' ), 'ttest:runttests:before' );

%% bad filename
EXPECT_THROW( @() runttests( 'test_thisfiledoesnotexist' ), 'runttests:badfile','runttests:notests' );

%%
clear cleandir2
clear cleandir1
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>

%%

