% test TTEST R2017b

% Tests which need at least R2017b to work, since this version is not supported anymore, this is no additional restrictions

% preconditions   
TTEST init
TTEST v 0

%% test EQ
assert( EXPECT_EQ( 'a', 'a' ) );
assert( EXPECT_EQ( "a", "a" ) );
assert( EXPECT_EEQ( 'a', 'a' ) );
assert( EXPECT_EEQ( "a", "a" ) );
if( ismatlab && ~verLessThan('matlab', '9.7') );
    assert(  EXPECT_EQ( 'a', "a" ) );
    assert(  EXPECT_EQ( "a", 'a' ) );
    assert( ~EXPECT_EEQ( 'a', "a" ) );
    assert( ~EXPECT_EEQ( "a", 'a' ) );
end;

%% test NE
assert( ~EXPECT_NE('a','a') );
assert( ~EXPECT_NE("a","a") );
assert( ~EXPECT_NEE('a','a') );
assert( ~EXPECT_NEE("a","a") );
if( ismatlab && ~verLessThan('matlab', '9.7') );
    assert( ~EXPECT_NE('a',"a" ) );
    assert( ~EXPECT_NE("a",'a' ) );
    assert( EXPECT_NEE('a',"a" ) );
    assert( EXPECT_NEE("a",'a' ) );
end;

%% test EE
assert( EXPECT_EE('a','a') );
assert( EXPECT_EE("a","a") );
assert( ~EXPECT_NN('a','a') );
assert( ~EXPECT_NN("a","a") );
if( ismatlab && ~verLessThan('matlab', '9.7') );
    assert( EXPECT_EE('a',"a" ) );
    assert( EXPECT_EE("a",'a' ) );
    assert( ~EXPECT_NN('a',"a" ) );
    assert( ~EXPECT_NN("a",'a' ) );
end;

%% test STREQ
if( ismatlab && ~verLessThan('matlab', '9.7') );
    assert( EXPECT_STREQ( '23', "23" ) );
    assert( EXPECT_STREQ( 'ab', "ab" ) );
    assert( ~EXPECT_STREQ( 'ab', "aB" ) );
    assert( ~EXPECT_STREQ( 'ab', "ac" ) );
end;
assert( EXPECT_STREQ( "23", "23" ) );
assert( EXPECT_STREQ( "ab", "ab" ) );
assert( ~EXPECT_STREQ( "ab", "aB" ) );
assert( ~EXPECT_STREQ( "ab", "ac" ) );


%% test STRNEQ
if( ismatlab && ~verLessThan('matlab', '9.7') );
    assert( ~EXPECT_STRNE( '23', "23" ) );
    assert( ~EXPECT_STRNE( 'ab', "ab" ) );
    assert( EXPECT_STRNE( 'ab', "aB" ) );
    assert( EXPECT_STRNE( 'ab', "ac" ) );
end;    
assert( ~EXPECT_STRNE( "23", "23" ) );
assert( ~EXPECT_STRNE( "ab", "ab" ) );
assert( EXPECT_STRNE( "ab", "aB" ) );
assert( EXPECT_STRNE( "ab", "ac" ) );

%% test STRCASEEQ
if( ismatlab && ~verLessThan('matlab', '9.7') );
    assert( EXPECT_STRCASEEQ( '23', "23" ) );
    assert( EXPECT_STRCASEEQ( 'ab', "ab" ) );
    assert( EXPECT_STRCASEEQ( 'ab', "aB" ) );
    assert( ~EXPECT_STRCASEEQ( 'ab', "ac" ) );
end;    
assert( EXPECT_STRCASEEQ( "23", "23" ) );
assert( EXPECT_STRCASEEQ( "AB", "ab" ) );
assert( EXPECT_STRCASEEQ( "ab", "aB" ) );
assert( ~EXPECT_STRCASEEQ( "ab", "ac" ) );

%% test STRCASENEQ
if( ismatlab && ~verLessThan('matlab', '9.7') );
    assert( ~EXPECT_STRCASENE( '23', "23" ) );
    assert( ~EXPECT_STRCASENE( 'ab', "ab" ) );
    assert( ~EXPECT_STRCASENE( 'ab', "aB" ) );
    assert( EXPECT_STRCASENE( 'ab', "ac" ) );
end;
assert( ~EXPECT_STRCASENE( "23", "23" ) );
assert( ~EXPECT_STRCASENE( "AB", "ab" ) );
assert( ~EXPECT_STRCASENE( "ab", "aB" ) );
assert( EXPECT_STRCASENE( "ab", "ac" ) );

%% test NO_THROW
[w_msg, w_id] = lastwarn;
err = lasterror; %#ok<LERR>

assert( EXPECT_NO_THROW( "2;" ) );
assert( ~EXPECT_NO_THROW( "adsasfasf a asdkasd;" ) );
assert( ~EXPECT_NO_THROW( "adsasfasf a asdkasd;", "asd:asd", "MATLAB:UndefinedFunctiom", "Octave:undefined-functiom" ) );
assert( EXPECT_NO_THROW( "adsasfasf a asdkasd;", "asd:asd", "MATLAB:UndefinedFunctio", "Octave:undefined-functio" ) );

assert( ~EXPECT_THROW( "0;") );
assert( EXPECT_THROW( "twarning();", 'twarning:noid' ) );
assert( ~EXPECT_THROW( "twarning();", "asd:asd" ) );
assert( EXPECT_THROW( "twarning();", "asd:asd", "twarning:noid" ) );

assert( ~EXPECT_THROW( "0;" ) );
assert( ~EXPECT_THROW( "adsasfasdasda ajkdwaklsd;" ) );
assert( ~EXPECT_THROW( "adsasfasdasda ajkdwaklsd;", "asd:asd" ) );
assert( EXPECT_THROW( "adsasfasdasda ajkdwaklsd;", "asd:asd", "MATLAB:UndefinedFunction", "Octave:undefined-function" ) );
lastwarn( w_msg, w_id );
lasterror( err );  %#ok<LERR>

%% postprocessing
% Set errorflag to true
TTEST errflag false

%#ok<*NOSEL,*NOSEMI,*ALIGN>
