% test severity

TTEST init

%% DISABLED
assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 3, DISABLED )'), ...
                     tt.HasSubstr( 'skipped' ) ) );
assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 2, DISABLED(0) )'), ...
                     tt.Not( tt.HasSubstr('skipped') ) ) );
if( ismatlab );
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 2, DISABLED(''ismatlab'') )'), ...
            tt.HasSubstr( 'skipped' ) ) );
else;
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 2, DISABLED(''ismatlab'') )'), ...
            tt.Not( tt.HasSubstr('skipped') ) ) ); end;

%% ENABLED
if( ismatlab );
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 2, ENABLED(ismatlab) )'), ...
                         tt.Not( tt.HasSubstr('skipped') ) ) );
else;
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 2, ENABLED(ismatlab) )'), ...
                         tt.HasSubstr( 'skipped' ) ) ); end;

%% TODOED
assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 3, TODOED(@() true) )'), ...
                     tt.HasSubstr('>>This is a TODO test<<') ) );
assert( EXPECT_THAT( evalc( 'EXPECT_EQ( 2, 3, TODOED(@() false) )' ), ...  % this test must on multiple lines, because otherwise the string which is tested appears in the error message
                     tt.Not( tt.HasSubstr( '>>This is a TODO test<<' ) ) ) );

%% is
assert( EXPECT_EQ( ismatlab, ~isoctave ) );
assert( EXPECT_EQ( ispc, ~isunix, ENABLED( @()~ismac ) ) );

%% postprocessing
% Set errorflag to false
TTEST errflag false

%#ok<*NOSEL,*NOSEMI,*ALIGN>

