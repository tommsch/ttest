function [ ret ] = xunit_func( val )
    persistent value;
    if( nargin == 1 );
        if( isequal(val, 'plus') );
            value = value + 1;
        elseif( isequal(val, 'times2') );
            value = 2*value;
        else;
            value = val; end; end;
    ret = value;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
