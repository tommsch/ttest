TTEST init


%% getfigure
TESTCASE( 'getfigure' );
f = figure( 10 );
g = getfigure( f );
EXPECT_HANDLEEQ( f, g );
close all

f = figure( 2 );
[~, x] = EXPECT_NTHROW( @() plot( rand(4, 4) ), '__legend_handle__' );
g = getfigure( x );
EXPECT_HANDLEEQ( f, g );
close all

f = figure( 'Name','f9', 'Visible','off' );
ax = gca;
figure( 20 );
[~, x] = EXPECT_NTHROW( @() plot( ax, rand(2,4) ), '__legend_handle__' );
g = getfigure( x );
EXPECT_HANDLEEQ( f, g );
close all

f = figure( 'Name','fff', 'IntegerHandle','off' );
g = getfigure( f );
EXPECT_HANDLEEQ( f, g );
close all

f = figure( 'Name','ggg' );
g = getfigure( f );
EXPECT_HANDLEEQ( f, g );
close all

f1 = figure( 'Name','f1', 'IntegerHandle','off' );
f2 = figure( 2 );
f3 = figure( 'Name','f1' );
f4 = figure( 'Name','f1', 'IntegerHandle','off' );
EXPECT_HANDLEEQ( getfigure(f1), f1 );
EXPECT_HANDLEEQ( getfigure(f2), f2 );
EXPECT_HANDLEEQ( getfigure(f3), f3 );
EXPECT_HANDLEEQ( getfigure(f4), f4 );
close all hidden

%% set_position
TESTCASE( 'setposition' );
% just tests whether there are no errors
figure( 10 );
[ flag ] = set_position( [1000 1000] );
EXPECT_THAT( flag, tt.IsFinite );
close( 10 )

[ flag ] = set_position( [20 20 200 200] );
EXPECT_THAT( flag, tt.IsFinite );
close all hidden

f13 = figure( 13 );
set_position( f13, [500, 400] );
close all hidden


%#ok<*NOSEL,*NOSEMI,*ALIGN>
