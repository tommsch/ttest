% preconditions

TTEST init

%% contains
EXPECT_TRUTHY( contains( '*', {'*','?'} ) );
EXPECT_TRUTHY( contains( '*_*_*?', {'*_*','?'} ) );
EXPECT_TRUTHY( contains( '*_*_*?', '*_*' ) );
EXPECT_FALSY(  contains( 'a', {'*','?'} ) );
EXPECT_FALSY(  contains( 'a_*_a!', {'*_*','?'} ) );
EXPECT_FALSY(  contains( '*_b_', '*_*' ) );

%% endsWith
str = {'a.doc','a.tar.gz','b.m';
       'b.ppt','c.png','a.gz'};
pat = '.gz';
TF = endsWith( str, pat );
EXPECT_EQ( TF, [0 1 0;0 0 1] );

str = 'abstract.doc';
EXPECT_TRUTHY( endsWith(str, '.doc') );
EXPECT_FALSY( endsWith(str, '.docx') );
EXPECT_FALSY( endsWith(str, 'o') );

%% fflush
EXPECT_NTHROW( @() fflush(stderr) );
EXPECT_NTHROW( @() fflush(stdout) );
EXPECT_NTHROW( @() fflush(0) );

%% getpid
pid = getpid();
EXPECT_CLASS( pid, 'double' );
EXPECT_THAT( pid, tt.IsIntegral() );
EXPECT_EQ( getpid(), getpid() );

%% index
EXPECT_EQ( index( 'Teststring', 'z' ), 0 );
EXPECT_EQ( index( 'Teststring', 't' ), 4 );
EXPECT_EQ( index( 'Teststring', 't', 'last' ), 6 );

%% isAlways
EXPECT_TRUTHY( isAlways(true) );

%% ostrsplit
EXPECT_EQ( ostrsplit ('a,b,c', ','), {'a','b','c'} );
EXPECT_EQ( ostrsplit (['a,b' ; 'cde'], ','), {'a', 'b', 'cde'} );

%% OCTAVE_VERSION
EXPECT_ISA( OCTAVE_VERSION(), 'char' );

%% printf
EXPECT_STREQ( @() printf('123'), @() fprintf('123') );

%% puts
EXPECT_STREQ( @() puts('123'), @() fprintf('123') );

%% readdir
EXPECT_NTHROW( @() readdir('.') );

%% rng
rng(0)
a = rand;
rng(0);
b = rand;
EXPECT_EQ( a, b );

%% stderr
EXPECT_NTHROW( @() fprintf( stderr, 'lorem ipsum' ) );
EXPECT_STREQ( @() fprintf( stderr, 'abc' ), @() fprintf( 'abc' ) );

%% stdout
EXPECT_NTHROW( @() fprintf( stdout, 'lorem ipsum' ) );
EXPECT_STREQ( @() fprintf( stdout, 'abc' ), @() fprintf( 'abc' ) );

%% startsWith
str = {'a.doc','a.tar.gz','b.m';
       'b.ppt','c.png','a.gz'};
pat = 'a.';
TF = startsWith( str, pat );
EXPECT_EQ( TF, [1 1 0;0 0 1] );

str = 'abstract.doc';
EXPECT_TRUTHY( startsWith(str, 'abst') );
EXPECT_FALSY( startsWith(str, '.bst') );
EXPECT_FALSY( startsWith(str, 'b') );




