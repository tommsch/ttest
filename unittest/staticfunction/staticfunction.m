function staticfunction();
% function which is a static workspace 
%   - it includes tests for TTESTS
%   - is also used as target to test certain functions

    if( false );
        fprintf( '\n===================================\nTyping Matcher.m:\n===================================\n' );  %#ok<UNRCH>
        type Matcher.m
        fprintf( '\n===================================\nPath:            \n===================================\n' );
        path; 
        fprintf( '\n===================================\n' );
        end;  % XX DEBUG
    i = 1;  %#ok<NASGU>
    function [ ret ] = nestedfunction3  %#ok<DEFNU>
        %fprintf( 'in nested function\n' );
        ret = 3;
    end

    2;
    i = 2;  %#ok<NASGU>

    ttest_id = TTEST( 'init', 'staticfunction' );
    TTEST v 0
    EXPECT_FAIL( 'This message should not be printed.' );
    TTEST v 1
    TTEST( ttest_id, 'runttests_checked', true );
    
    eq5 = tt.Matcher( @(x) x == 5 );
    EXPECT_THAT( 5, eq5 );
    
    [~, a] = EXPECT_NTHROW( @() inv(0), ':singularMatrix' );
    
    EXPECT_EQ( a, inf );
    %EXPECT_EQ( a, 0 );  % Octave v6 returns 0 ??
    
    b = 0;
    EXPECT_NTHROW( 'b = inv(0)', ':singularMatrix' );
    EXPECT_EQ( b, inf );
    %EXPECT_EQ( b, 0 );  % Octave v6 returns 0 ??
    
end

function ret = localfunction2 %#ok<DEFNU>
    %fprintf( 'in localfunction2' );
    ret = 2;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
