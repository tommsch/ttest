function [ ret ] = function_throw( id );  %#ok<INUSD>

    % This function tests macro calls which throw
    ret = true;
    h = localfunctions;
    err = lasterror();  %#ok<LERR>
    [w_msg, w_id] = lastwarn();
    for i = 1:numel( h );
        lasterror( err );  %#ok<LERR>
        lastwarn( w_msg, w_id );
        if( ~ret );
            return; end;
        try;
            evalc( 'h{i}( id );' );
            ret = false;
            fprintf( 'ERROR: Macro call %i did not throw.\n==================\n', i );
            return;
        catch me;
            [~, w_id_after] = lastwarn();
            switch i;
                case {1,2,3,4,5,6};
                    ret = ret && (any( strcmp( me.identifier, {'MATLAB:unassignedOutputs'} )) || contains( me.message, 'undefined in return list' ) );
                    ret = ret && any( strcmp( w_id_after, {'TTEST:nargout'} ) );
                otherwise; fatal_error; 
                end; end; end;  
    lasterror( err );  %#ok<LERR>
    lastwarn( w_msg, w_id );
end

%#ok<*ASGLU>
%function f;  EXPECT_SUCCEED(); end  % For Debugging purposes. This function does not throw
function [] = f1( id );   [ret, a, b] =     EXPECT_TRUE( id, 1 );           end 
function [] = f2( id );   [ret, a] =        EXPECT_FAIL( id );              end
function [] = f3( id );   [ret, a] =        EXPECT_FAIL( id, "Text" );      end
function [] = f4( id );   [ret, a, b, c] =  EXPECT_PRED( id, @sum, [2 3] ); end
function [] = f5( id );   [ret, a, b, c] =  EXPECT_ALMOST_NE( id, 1 );      end
function [] = f6( id );   [ret, x1, x2] =   EXPECT_EQ( id, 2 );             end

% function dummy; end  %#ok<DEFNU>  % no dummy function, because we use call all localfunctions here
%#ok<*NOSEL,*NOSEMI,*ALIGN>
