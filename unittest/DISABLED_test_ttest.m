% preconditions

TTEST init

% %% init
% TESTCASE( 'init' );
% 
% oldid = ttest_id;
% evalc( 'EXPECT_FAIL();' );
% str = evalc( 'TTEST init' );
% EXPECT_THAT( str, tt.HasSubstr('Warning') );
% 
% EXPECT_EQ( oldid, ttest_id );
% str = evalc( 'TTEST init-hard' );
% EXPECT_NE( oldid, ttest_id );
% newid = TTEST( 'id', 10 );
% EXPECT_NTHROW( 'TTEST init-last' );
% EXPECT_EQ( newid, ttest_id );
% 
% %ASSUME_FAIL( 'This tests currently fails totally, and is thus disabled.' )

%% verbose
TESTCASE verbose
ASSERT_PRED( @(x) ~isempty(x), evalc( 'EXPECT_EQ( 2, 3 );' ) ); 
ASSERT_PRED( @(x)  isempty(x), evalc( 'EXPECT_EQ( 2, 2 );' ) );

EXPECT_NTHROW( @() TTEST( 'v' ) );
EXPECT_NTHROW( @() TTEST( 'v', 1 ) );
EXPECT_EQ( 1, TTEST( 'v', 0 ) );
EXPECT_EQ( 0, TTEST( 'v' ) );

return 

% %% clear_all
% TTEST ttest_id_89124190jaksdjk var variable10 10
% TTEST global v 54321
% TTEST todo ass
% str = evalc( 'TTEST p' );
% EXPECT_THAT( str, tt.HasSubstr('TODO') );
% EXPECT_THAT( str, tt.HasSubstr('ASSERT') );
% EXPECT_THAT( str, tt.HasSubstr('54321') );
% val = TTEST( 'todo' );
% EXPECT_THAT( char(val{1}), tt.HasSubstr('ASSERT') ); 
% 
% EXPECT_EQ( TTEST( 'ttest_id_89124190jaksdjk', 'var','variable10',nan ), [] );
% EXPECT_EQ( TTEST( 'global', 'v' ), 1 );
% val = TTEST( 'todo' );
% EXPECT_PRED( @(x) numel(x) == 2, val );
% EXPECT_THAT( char(val{1}), tt.Not(tt.HasSubstr('ASSERT')) );

%% var
TESTCASE var
%%% local
EXPECT_EQ( TTEST( 'var', 'var1', 1 ), [] );
EXPECT_EQ( TTEST( 'var', 'var1', 2 ), 1 );
TTEST( 'clearvar', 'var1' );
EXPECT_EQ( TTEST( 'var', 'var1', 1 ), [] );

%%% global
unusedvarname = 'ttest_ajkslf_jkal_a125jkoasf90jklawf90q3j';  % To make sure that this is not defined by accident
EXPECT_EQ( [], TTEST( 'global', 'var', unusedvarname, 1 ) );
EXPECT_EQ( 1, TTEST( 'global', 'var', unusedvarname, 2 ) );
EXPECT_NTHROW( @() TTEST( 'global', 'clearvar', unusedvarname ) );

%% wrong_input
%%% local
EXPECT_THROW( 'TTEST( ''wronginput'' )', 'TTEST:argin' );
EXPECT_NTHROW( 'TTEST( ''var'' )', 'TTEST:var' );
EXPECT_NTHROW( 'TTEST( ''exp'' )', 'TTEST:todoexpass' );

%%% global
EXPECT_THROW( @() TTEST( 'global', 'wronginput' ), 'TTEST:argin' );
EXPECT_NTHROW( @() TTEST( 'global', 'var' ), 'TTEST:var' ); 
EXPECT_NTHROW( @() TTEST( 'global', 'exp' ), 'TTEST:todoexpass' );

%% todoexpass
%TTEST v 0
TTEST ass exp
% we test whether TTEST ass exp works. I.e. failing ASSERT tests shall not call assert
EXPECT_NTHROW( 'ASSERT_EQ( 2, 3 )' );
EXPECT_NTHROW( @() ASSERT_EQ(ttest_id, 2, 3) );

TTEST todo ass
% we test whether TODO_ tests now throw an error
EXPECT_THROW( 'TODO_EQ( 2, 3 )', 'TTEST:NotEq' );
EXPECT_THROW( @() TODO_EQ(ttest_id, 2, 3), 'TTEST:NotEq' );

TTEST todo exp
str = evalc( 'TODO_EQ( 2, 2 );' );  % This must not produce screen output
EXPECT_EMPTY( str );
% make standard values again
TTEST todo todo
TTEST exp exp
TTEST ass ass
%TTEST v 1

TTEST( 'experr', @(a, b, c, d) fprintf( 'OK' ) );
str = evalc( 'EXPECT_FAIL;' );
EXPECT_STRCONTAINS( str, 'OK' );

%% tellme
EXPECT_NTHROW( @() TTEST( 'tellme' ) );


%#ok<*NOSEL,*NOSEMI,*ALIGN>


