% test TTEST GIVEN

TTEST init

% preconditions 

%% basictest
TESTCASE
a = 100; assert( GIVEN( 'sqm1_', 'EXPECT_NE( sqm1_, a )' ) );
assert( GIVEN( @(flt) EXPECT_SCALAR( flt ) ) );
assert( GIVEN( tt.float('nan', 0), @(x) EXPECT_EQ( x, x.' ) ) );
flt = tt.float( 'nan',0 );
assert( GIVEN( flt, @(x) EXPECT_EQ( x, x ) ) );
assert( GIVEN( 'flt', @(x) EXPECT_SCALAR( x ) ) );

assert( GIVEN( 'sqm', 'arr', 'sqm', @(x, z, y) EXPECT_ISA( x, y, z, 'double' ) ) );

%% multiple
TESTCASE
tt.float( 'nan',0, 'name','fltr' );
assert( GIVEN( 'fltr1_', 'fltr2_', 'EXPECT_TRUE( fltr1_ <= fltr2_ || fltr1_ >= fltr2_ )' ) );
assert( GIVEN( 'fltr', 'fltr', @(x, y) EXPECT_TRUE( x >= y || x <= y ) ) );
assert( GIVEN( @(fltr1, fltr2) EXPECT_TRUE( fltr1 >= fltr2 || fltr1 <= fltr2 ) ) );

%% rounddown
TESTCASE( 'roundown' );
    TTEST v 0
    cd pbt
    assert( ~GIVEN( @(flt) flt >= pbtfail('rounddown', flt) ) );
    st = tt.float( 'allowneg',false, 'nan',0, 'name','fltp' );
    assert( GIVEN( @(fltp) fltp >= pbtfail('rounddown', fltp) ) );
    assert( GIVEN( st, @(x) x >= pbtfail('rounddown', x) ) );
    TTEST v 1
ENDTESTCASE();

%% function_handle
assert( GIVEN( tt.float('min',inf, 'nan',0),  @isinf, 'minnumexample',1 ) );

%% handle behaviour of class
% XX unit test missing

%% maxtime
TTEST v 0

t1 = tic; 
assert( ~GIVEN( @() tt.void( @() pause(0.5), true ), 'maxtime',2 ) );
t2 = toc( t1 );
assert( t2<2.6 );

t1 = tic; 
assert( ~GIVEN( 'tt.void( @() pause(0.5), true )', 'maxtime',2 ) );
t2 = toc( t1 );
assert( t2<2.6 );

TTEST v 1
%% maxexample
TTEST v 0

t1 = tic;
assert( ~GIVEN( @() tt.void( @() pause(0.5), true ), 'maxexample',3 ) );
t2 = toc( t1 );
assert( t2<2 );

t1 = tic;
assert( ~GIVEN( 'tt.void( @() pause(0.5), true )', 'maxexample',3 ) );
t2 = toc( t1 );
assert( t2<2 );

TTEST v 1

%% regression tests
st1 = tt.value( {@sqrt} );
st2 = tt.value( {randn(1), randn(2), randn(3), randn(4)} );
GIVEN( @(st1,st2) EXPECT_NEAR( st1(st2),  sqrt(st2), 5e-14 ) );

%% postprocessing
% Set errorflag to false
TTEST errflag false

%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>
