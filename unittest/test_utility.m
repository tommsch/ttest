% test TTEST utility

TTEST init

% preconditions

%% allfunctionhandle
if( ismatlab )
TESTCASE( 'allfunctionhandle' );
    cd allfunctionhandle
    SECTION( 'static function' );
        warning off TTEST:nested
        allfunctionhandle( 'staticfunction', 'local', 'nested',1, 'v',-1, 'assign' );
        EXPECT_NTHROW( @() assert(staticfunction_nestedfunction3() == 3) );
        EXPECT_NTHROW( @() assert(localfunction2() == 2) );

    SECTION( 'script function' );
        allfunctionhandle( 'scriptfunction', 'v',-1, 'assign' );

    SECTION( 'wrong name' );
        EXPECT_THROW( @() allfunctionhandle( 'this_function_does_not_exist_ajskdlau89qh3ajf83' ), 'allfunctionhandle:function' );
        EXPECT_THROW( @() allfunctionhandle( 'scriptfunction', 'this_subfunction_does_not_exist_ajskdlau89qh3ajf83' ), ...
            'm', 'allfunctionhandle:subfunction', 'o', 'allfunctionhandle:name' );

ENDTESTCASE();
else;
    TODO_FAIL( 'allfunctionhandle does not work on Octave yet.' );
end;

%% castws
%%% stress test
TESTCASE( 'castws' );
    SECTION( 'stresstest' );
    for ttest_type = {'int8','uint16','single'};
        for var = {3, struct('m',struct('m',1)), {int32([2 3 4]) {int32([3 2])}}, single( 3200 ), logical([10 10 10])};
            warning off castws:doublecast
            var = var{1};  %#ok<FXSET>
            nerr = castws( ttest_type{1} );
            EXPECT_EQ( nerr, 0 ); end; end;


ENDTESTCASE();

%% cwd
TESTCASE
EXPECT_NTHROW( @() cwd( 'plotm' ) );
EXPECT_NTHROW( @() cwd( 'error', 'e' ) );
EXPECT_NTHROW( @() cwd( 'plot', '-rec', 1 ) );
EXPECT_NTHROW( @() cwd( 'plot', '-rec', '1' ) );
EXPECT_NTHROW( @() cwd( 'plot', '-rec', 'eager', '1' ) );
EXPECT_NTHROW( @() cwd( 'EXPECT*' ) );  % this has the nice side effect, that the files are checked for parse errors on octave
EXPECT_NTHROW( @() cwd( '-back' ) );  % this has the nice side effect, that the files are checked for parse errors on octave
EXPECT_NTHROW( @() cwd( '-back' ) );  % this has the nice side effect, that the files are checked for parse errors on octave


%% evallazy
if( ismatlab );
    allfunctionhandle( 'evallazy', 'assign', 'v',0 );
    EXPECT_EMPTY( variablename( ) );
    EXPECT_EQ( variablename( @()1 ), {{}} );
    EXPECT_EQ( variablename( @(x)x ), {{'x'}} );
    [vn,an,i] = variablename( @(x,y) x, @(z,x)z );
    EXPECT_EQ( vn, {{'x','y'},{'z','x'}} );
    EXPECT_EQ( an, {'x','y','z'} );
    EXPECT_EQ( i, {[1 2],[3 1]} );

    %%% evallazy_handle
    a = 1; h = @(b) a+b; a = 2; b = 4; %#ok<NASGU>
    EXPECT_EQ( evallazy( h ), 5 );

    a = 1; h = @(b,c) a+b; a = 2; b = 4; %#ok<NASGU>
    try;
        evallazy( h );
        assert( false );
    catch me;
        assert( strcmp(me.identifier, 'evallazy:missing') ); end;

    a = 1; h = @(b) a+b; b = 10; a = 20; st.b = 0; %#ok<NASGU>
    EXPECT_EQ( evallazy( h, st ), 1 );


    a = 1; h = @(b,c) a+b; b = 10; a = 20; st.b = 0;
    try;
        evallazy( h, st );
        assert( false );
    catch me;
        assert( strcmp(me.identifier,'evallazy:missing') ); end;

    EXPECT_EQ( evallazy( @() 2 ), 2 );
end;

%% filetype
TESTCASE
cd test_filetype
if( ismatlab );
    EXPECT_EQ( 'class',    filetype( 'filetype_class' ) ); end;
EXPECT_EQ( 'function', filetype( 'filetype_function1' ) );
EXPECT_EQ( 'function', filetype( 'filetype_function2' ) );
EXPECT_EQ( 'function', filetype( 'filetype_function3' ) );
EXPECT_EQ( 'function', filetype( 'filetype_function4' ) );
EXPECT_EQ( 'script',   filetype( 'filetype_script1' ) );
EXPECT_EQ( 'script',   filetype( 'filetype_script2' ) );

%% isequalproperty
TESTCASE( 'isequalpropert' );
    clear x
    EXPECT_TRUE( isequalproperty( [], [] ) );
    EXPECT_TRUE( isequalproperty( @sum, @prod ) );
    EXPECT_TRUE( isequalproperty( sparse([1 2],[1 1],[-1 -2],2,2,10), sparse([1 2], [1 1], [-1 -2], 2, 2, 20) ) );
    EXPECT_TRUE( isequalproperty( ttest_id_c(1), ttest_id_c(2) ) );

    EXPECT_FALSE( isequalproperty( zeros(1,0), zeros(0,1) ) );
    EXPECT_FALSE( isequalproperty( zeros(1,0), zeros(0,1) ) );
    EXPECT_FALSE( isequalproperty( single(2), double(2) ) );
    EXPECT_FALSE( isequalproperty( [], {} ) );

    x.id = 'ttest_id_1';
    ret = EXPECT_NTHROW( @() isequalproperty( struct(ttest_id_c(1) ), x),'MATLAB:structOnObject', 'Octave:classdef-to-struct' );
    EXPECT_TRUE( ret );
    EXPECT_FALSE( isequalproperty( ttest_id_c(1), x ) );
ENDTESTCASE();

%% onCleanup_lazy
TESTCASE
    EXPECT_NTHROW( @() {onCleanup_lazy( @warning, warning('off','symbolic:mldivide:RankDeficientSystem') )} );  % here the return value is stored
    EXPECT_THROW( @() onCleanup_lazy( @warning, warning('off','symbolic:mldivide:RankDeficientSystem') ), 'onCleanup_lazy:argout', TODOED('onoctave') );  % here the return value is not stored, thus the function must throw

%% repr
TESTCASE( 'repr' );
    SECTION;
    A = randn( 2, 1, 1, 3, 1, 4, 5 );
    eval( repr(A, 'B') );
    EXPECT_EQ( A, B );

    SECTION;
    A = @(x) sin(x);
    eval( repr(A, 'B') );
    EXPECT_EQ( A(2.414), B(2.414) );
    
    EXPECT_STREQ( @() repr( 'k', '_' ), '''k''' );

    %SECTION;
    %A = 1/-inf;
    %eval( repr(A, 'B') );
    %EXPECT_EQ( 1/B, -inf );  % does not work, because mat2str converts -0 to +0


%% secloop
% XX These two tests currently fail for an unknown reason,
% XX but only when the whole test suite is executed with no debugger attached
% EXPECT_MAXTIME( DISABLED('onoctave'), @() secloop(2), 8 );
% EXPECT_MINTIME( DISABLED('onoctave'), @() secloop(10), 2 );

%% subset
EXPECT_TRUE( subset( [1 2], [1 2 3] ) );
EXPECT_TRUE( subset( [1 1;1 2].', [1 1;1 2;2 3].' ) );
EXPECT_TRUE( subset( {[1],[1 2]}, {[1],[1 2],[1;2]} ) );  %#ok<NBRAK>
EXPECT_FALSE( subset( [1 2 3], [1 2]  ) );
EXPECT_FALSE( subset( [1 1;1 2;2 3].', [1 2;2 3].' ) );
EXPECT_FALSE( subset( {[1],[1 2]}, {[1],[1;2]} ) );  %#ok<NBRAK>
if( ismatlab );
    EXPECT_THROW( @() subset([1 2]',[1 2 3]'), ':AandBColnumAgree' );
else;
    EXPECT_THROW( @() subset([1 2]',[1 2 3]'), 'ismember: number of columns in A and B must match' );
end;
EXPECT_TRUE( subset({1 2},{1 2 3}) );
EXPECT_TRUE( subset( {nan},{nan} ) );

%% terror
EXPECT_THROW( @() terror('id:id'), 'id:id' );


%% variable
h = @(st1, st2) EXPECT_NEAR (st1 (st2), sqrt (st2), 5e-14);
n = variablename( h );
EXPECT_EQ( n, {{'st1', 'st2'}} );



%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>
