% test_experimental

TTEST init

%% complexity
TESTCASE( 'complexity' );
    warning( 'off', 'complexity:x' );
    
%     EXPECT_EQ( complexity( 10*ones([1 1000])+.1*rand(1,1000), 'v',0 ), '1' );
    EXPECT_EQ( complexity( abs((1:100)+randn(1,100)), 'v',0 ), 'n' );
    
    x = 1:1000;
    EXPECT_EQ( complexity( x, abs(0.1 + log(x).*(1 + rand(size(x)))), 'v',0 ), 'logn' );
    EXPECT_EQ( complexity( x, abs(0.1 + x.*log(x).*(1 + rand(size(x)))), 'v',0 ), 'nlogn' );
    EXPECT_EQ( complexity( [x; abs(0.1 + x.^2.*(1 + rand(size(x))))], 'v',0 ), 'n2' );
    
    x = 1:100;
    EXPECT_EQ( complexity( [x; abs(0.1 + x.^3 + 1000*rand(size(x)))], 'v',0 ), 'n3' );
    EXPECT_EQ( complexity( [x; abs(0.1 + x.^4 + 1000*rand(size(x)))], 'v',0 ), 'n4' );
%    TODO_EQ( complexity( abs(0.1 + 2.^(x/10 + .0001*rand(size(x)))), 'v',0 ), '2n' );
    EXPECT_EQ( complexity( abs(0.1 + gamma((1:15) + abs(0.01*randn(1, 15)))), 'v',0 ), 'n!' );
    EXPECT_ANYOF( complexity( abs(0.1 + gamma(1:15) + abs(cumprod(3*randn(1, 15)))), 'v',0 ), {'2n','n!'} );



    
%%
ENDTESTCASE();


%#ok<*NOSEL,*NOSEMI,*ALIGN>
