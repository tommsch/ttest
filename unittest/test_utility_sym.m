% test TTEST utility_sym

TTEST init

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );

% preconditions

%% castws
%%% stress test
TESTCASE( 'castws' );

    if( tt.expect_toolbox('symbolic', OPTION('load')) );
        SECTION( 'sym test' );
        var = struct( 'm', struct('m', 1) );
        try;
            castws( 'sym', 'v',-1 );
            EXPECT_ISA( var.m.m, 'sym' );

            var = @() true;
            nerr = castws( 'double', 'v',-1 );
            EXPECT_ISA( var, 'function_handle' );
            EXPECT_EQ( nerr, 1 );

            nerr = castws( 'double', 'v',-1, 'skip',{'ttest_','var'} );
            EXPECT_EQ( nerr, 0 );
        catch me;
            EXPECT_FAIL( 'castws test failed. Thrown error:\n%s', me2str(me) );
            end; end;
ENDTESTCASE();

%% subset
TESTCASE( 'subset' );
syms xx
EXPECT_FALSE( subset( xx+[0:2], xx+[1:3] ) );  %#ok<NBRAK>

%% repr
TESTCASE( 'repr' );

    SECTION;
    A = {struct {1,2;3 4}, [2 3];
         sym(2) [], {}};
    [~, strB] = EXPECT_NTHROW( @() repr(A, 'B'), 'repr:sym' );
    eval( strB );
    EXPECT_EQ( A, B );

%#ok<*NOSEL,*NOSEMI,*ALIGN>
