%TTEST_AUTOGENERATE
% This file is auto-generated. Do not modify it. Changes may be overwritten.
% Class which represents the ttest_id-s

classdef filetype_class
    properties
    end
	methods
        function [ obj ] = filetype_class()
		    error( 'filetype_test:fatal', 'This function must not be executed.' );
        end
    end
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
