TTEST init section_experimental_1

%%
TESTCASE( 'cell array of function handles' );
    err = lasterror();  %#ok<LERR>
    if( SECTION( {
            @() EXPECT_SUCCEED();
            @() EXPECT_TRUE( ~tt.expect_fail() );
            @() error( 'tt:cell_array_of_function_handles', 'Test must throw.' );
            } ) ); end;
        [err_msg, err_id] = lasterr();  %#ok<LERR>
        EXPECT_STR_EQ( err_id, 'tt:cell_array_of_function_handles' );
    ENDSECTION;
    lasterror( err );  %#ok<LERR>



%#ok<*NOSEL,*NOSEMI,*ALIGN>
