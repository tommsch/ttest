% test TTEST logging

TTEST init
id_unused = TTEST( 'init', 'id_unused' );

% preconditions 

%% MESSAGE
TESTCASE( 'TCMESSAGE' );
var_a = 42; 

MESSAGE( 'var_a' );
str = evalc( 'EXPECT_FAIL( id_unused );' );  % The TESTCASE name cannot be included into this string because we evalc the argument
EXPECT_THAT( ...  % these tests must be on multiple lines, otherwise the string which is tested against is included in the error message
    str, ...
    tt.AllOf(tt.HasSubstr( 'var_a' ), tt.HasSubstr( '42' )) ...
           );

MESSAGE( 'TEXT%i', 1 );
str = evalc( 'EXPECT_FAIL( id_unused );' );
EXPECT_THAT( ...
    str, ...
    tt.HasSubstr( 'TEXT1' ) ...
           );

MESSAGE();
str = evalc( 'EXPECT_FAIL( id_unused );' );
EXPECT_THAT( ...
    str, ...
    tt.Not(tt.HasSubstr( 'TEXT1' )) ...
           );

EXPECT_THROW( ...
    DISABLED( 'onoctave' ), ...
    @() MESSAGE( 'TEXT', a ), ...
    ' format', 'undefined near line', 'Unrecognized function or variable' ...
            );

TTEST( id_unused, 'errflag', false )

%% TRACE
TESTCASE( 'TRACE' );
if( ismatlab )
    
    varname = 'tracestring';
    MESSAGE( 'somemessage\n' );
    str = evalc( 'TRACE( @() EXPECT_EQ( 2, 3 ), varname );' );
    EXPECT_THAT( id_unused, ...
                 str, ...
                 tt.AllOf( tt.HasSubstr( 'varname' ), ...
                           tt.HasSubstr( 'tracestring' ), ...
                           tt.Not( tt.HasSubstr( 'somemessage' ) ) ) );

    str = evalc( 'TRACE( @() EXPECT_EQ( 2, 3 ) );' );
    EXPECT_THAT( str, tt.AllOf(tt.HasSubstr( 'somemessage' )) );
    MESSAGE();
    str = evalc( 'EXPECT_FAIL( id_unused );' );
    EXPECT_THAT( str, tt.Not(tt.HasSubstr( 'somemessage' )) );
else;
    TODO_FAIL( '''TRACE'' not working yet on Octave.' );
end;

TTEST( ttest_id_c(0), 'errflag', false )
TTEST( id_unused, 'errflag', false )

ENDTESTCASE();

%%

%#ok<*NOSEL,*NOSEMI,*ALIGN>
