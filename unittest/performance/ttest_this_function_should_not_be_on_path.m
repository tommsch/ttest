function [ varargout ] = ttest_this_function_should_not_be_on_path( varargin );
	warning( 'TTEST:path', ['This function should not be on the Matlab path.\n' ...
                          '  You most likely added all TTEST folders to the Matlab path by hand,\n' ...
						  '  instead of installing TTEST using the cttest_check_installationommand `TTEST install`.\n' ...
						  '  Remove all TTEST folders from the Matlab path and execute `TTEST install` in the folder where `TTEST.m` is located.'] );
    varargout = cell( 1, nargout );
end
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
