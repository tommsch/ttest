function generator();
% generates all unittests for the performance test
 
    % pre processing
    wold = warning;
    clean_warning = onCleanup( @() warning(wold) );
    warning( 'off', 'MATLAB:MKDIR:DirectoryExists' );
    
    olddir = pwd;
    cleandir = onCleanup( @() cd(olddir) );
    cd( fileparts( which( mfilename ) ) );
 
    % generate files
    generator_ttest_matlab_comment();
    generator_ttest_section();
    generator_ttest_function();
    
    generator_xunit4();

    generator_moxunit();

    generator_matlab_script();
    generator_matlab_function();
    generator_matlab_class();
    
end

%% generators
function generator_ttest_matlab_comment;
    cleanfid = onCleanup( @() close_all_fid );
    cleandir = mkdir_cd( 'ttest_matlab_comment' );  %#ok<NASGU>
    
    mkdir_cd _1024_1
    for n = 1:1024;
        fid = fopen( ['test' num2str(n) 'test.m'], 'wt' );
        fprintf( fid, '%% testPart1\n    EXPECT_EQ( 2, 2 );\n' ); end;
    
    mkdir_cd .. _1_1024
    fid = fopen( 'test1test.m', 'wt' );
    for n = 1 : 1024;
        fprintf( fid, ['%% testPart' num2str(n) '\n    EXPECT_EQ( 2, 2 );\n'] ); end;
    
    % mkdir_cd .. A
    % fid = fopen( 'test1test.m', 'wt' );
    % fprintf( fid, '%% testPart1\n' ); 
    % for n = 1 : 1024;
    %     fprintf( fid, '    EXPECT_EQ( 2, 2 );\n' ); end;
    
    mkdir_cd .. _Empty
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid, '%% testPart1\n' );
    
end

function generator_ttest_function;
    cleanfid = onCleanup( @() close_all_fid );
    cleandir = mkdir_cd( 'ttest_function' );  %#ok<NASGU>
    
    mkdir_cd _1024_1
    for n = 1:1024;
        fid = fopen( ['test' num2str(n) 'test.m'], 'wt' );
        fprintf( fid, 'function testPart1;\n    EXPECT_EQ( 2, 2 );\nend\n' ); end;
    
    mkdir_cd .. _1_1024
    fid = fopen( 'test1test.m', 'wt' );
    for n = 1 : 1024;
        fprintf( fid, ['function testPart' num2str(n) ';\n    EXPECT_EQ( 2, 2 );\nend\n'] ); end;
    
    % mkdir_cd .. A
    % fid = fopen( 'test1test.m', 'wt' );
    % fprintf( fid, 'function testPart1;\n' ); 
    % for n = 1 : 1024;
    %     fprintf( fid, '    EXPECT_EQ( 2, 2 );\n' ); end;
    % fprintf( fid, 'end\n' );
    
    mkdir_cd .. _Empty
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid, 'function testPart1;\nend\n' );
    
end

function generator_ttest_section;
    cleanfid = onCleanup( @() close_all_fid );
    cleandir = mkdir_cd( 'ttest_section' );  %#ok<NASGU>
    
    mkdir_cd _1024_1
    for n = 1:1024;
        fid = fopen( ['test' num2str(n) 'test.m'], 'wt' );
        fprintf( fid, 'TTEST init;\nTESTCASE( ''test1'' );\n    EXPECT_EQ( 2, 2 );\n' ); end;
    
    mkdir_cd .. _1_1024
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid, 'TTEST init;\n' ); 
    for n = 1 : 1024;
        fprintf( fid, ['TESTCASE( ''testpart' num2str(n) ''' );\n    EXPECT_EQ( 2, 2 );\n'] ); end;
    
    % mkdir_cd .. A
    % fid = fopen( 'test1test.m', 'wt' );
    % fprintf( fid, 'TTEST init;\nTESTCASE( ''testpart1'' );\n' ); 
    % for n = 1 : 1024;
    %     fprintf( fid, '    EXPECT_EQ( 2, 2 );\n' ); end;    
    
    mkdir_cd .. _Empty
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid, 'TTEST init;\n' ); 
    
end

function generator_xunit4;
    cleanfid = onCleanup( @() close_all_fid );
    cleandir = mkdir_cd( 'xunit4' );  %#ok<NASGU>
    
    mkdir_cd _1024_1
    for n = 1:1024;
        fid = fopen( ['test' num2str(n) 'test.m'], 'wt' );
        fprintf( fid, ['function tests = test' num2str(n) 'test;\n    tests = buildFunctionHandleTestSuite( localfunctions );\nend\n' ...
                       'function testPart1;\n    assertEqual( 2, 2 );\nend\n'] ); end;
    
    mkdir_cd .. _1_1024
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid, 'function tests = test1test;\n    tests = buildFunctionHandleTestSuite( localfunctions );\nend\n' ); 
    for n = 1 : 1024;
        fprintf( fid, ['function testPart' num2str(n) ';\n    assertEqual( 2, 2 );\nend\n'] ); end;
    
    % mkdir_cd .. A
    % fid = fopen( 'test1test.m', 'wt' );
    % fprintf( fid, 'function tests = test1test;\n    tests = buildFunctionHandleTestSuite( localfunctions );\nend\n' ); 
    % fprintf( fid, 'function testPart1;\n' );
    % for n = 1 : 1024;
    %     fprintf( fid, '    assertEqual( 2, 2 );\n' ); end;    
    % fprintf( fid, 'end\n' );
    
    mkdir_cd .. _Empty
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid, 'function tests = test1test;\n    tests = buildFunctionHandleTestSuite( localfunctions );\nend\n' ); 
    
end

function generator_moxunit;
    cleanfid = onCleanup( @() close_all_fid );
    cleandir = mkdir_cd( 'moxunit' );  %#ok<NASGU>
    
    mkdir_cd _1024_1
    for n = 1:1024;
        fid = fopen( ['test' num2str(n) 'test.m'], 'wt' );
        fprintf( fid, [
                'function test_suite = test' num2str(n) 'test;' newline ...
                'try;' newline ...
                '    test_functions = localfunctions();' newline ...
                'catch;' newline ...
                '    end;' newline ...
                'initMoxTestSuite;' newline ...
                'end' newline newline ...
                'function test_2_2;' newline ...
                '    assertEqual( 2, 2 );' newline ...
                'end'] ); end;
    
    mkdir_cd .. _1_1024
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid,  [
                'function test_suite = test1test;' newline ...
                'try;' newline ...
                '    test_functions = localfunctions();' newline ...
                'catch;' newline ...
                '    end;' newline ...
                'initMoxTestSuite;' newline ...
                'end' newline newline] ); 
    for n = 1 : 1024;
        fprintf( fid, ['function testPart' num2str(n) ';\n    assertEqual( 2, 2 );\nend'] ); end;
    
    % mkdir_cd .. A
    % fid = fopen( 'test1test.m', 'wt' );
    % fprintf( fid,  [
    %             'function test_suite = test1test;' newline ...
    %             'try;' newline ...
    %             '    test_functions = localfunctions();' newline ...
    %             'catch;' newline ...
    %             '    end;' newline ...
    %             'initMoxTestSuite;' newline ...
    %             'end' newline newline ...
    %             'function testPart' newline] ); 
    % for n = 1 : 1024;
    %     fprintf( fid, '    assertEqual( 2, 2 );\n' ); end;    
    % fprintf( fid, 'end\n' );
    
    mkdir_cd .. _Empty
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid,  [
                'function test_suite = test1test;' newline ...
                'try;' newline ...
                '    test_functions = localfunctions();' newline ...
                'catch;' newline ...
                '    end;' newline ...
                'initMoxTestSuite;' newline ...
                'end' newline] );
    
end

function generator_matlab_script;
    cleanfid = onCleanup( @() close_all_fid );
    cleandir = mkdir_cd( 'matlab_script' );  %#ok<NASGU>
    
    mkdir_cd _1024_1
    for n = 1:1024;
        fid = fopen( ['test' num2str(n) 'test.m'], 'wt' );
        fprintf( fid, '%% test1\n    assert( 2 == 2 );\n' ); end;

    mkdir_cd .. _1_1024
    fid = fopen( 'test1test.m', 'wt' );
    for n = 1 : 1024;
        fprintf( fid, ['%%%% test' num2str(n) '\n    assert( 2 == 2 );\n'] ); end;
    
    % mkdir_cd .. A
    % fid = fopen( 'test1test.m', 'wt' );
    % fprintf( fid, ['%%%% test' num2str(n) '\n'] );
    % for n = 1 : 1024;
    %     fprintf( fid, '    assert( 2 == 2 );' ); end;
    
    mkdir_cd .. _Empty
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid, '%% test\n' );
    
end

function generator_matlab_function;
    cleanfid = onCleanup( @() close_all_fid );
    cleandir = mkdir_cd( 'matlab_function' );  %#ok<NASGU>
    
    mkdir_cd _1024_1
    for n = 1:1024;
        fid = fopen( ['test' num2str(n) 'test.m'], 'wt' );
        fprintf( fid, ['function tests = test' num2str(n) 'test;\n    tests = functiontests( localfunctions );\nend\n' ...
                       'function testPart1( testCase );\n    verifyEqual( testCase, 2, 2 );\nend\n'] ); end;
    
    mkdir_cd .. _1_1024
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid, 'function tests = test1test;\n    tests = functiontests( localfunctions );\nend\n' ); 
    for n = 1 : 1024;
        fprintf( fid, ['function testPart' num2str(n) '( testCase );\n    verifyEqual( testCase, 2, 2 );\nend\n'] ); end;
    
    % mkdir_cd .. A
    % fid = fopen( 'test1test.m', 'wt' );
    % fprintf( fid, 'function tests = test1test;\n    tests = functiontests( localfunctions );\nend\n' ); 
    % fprintf( fid, 'function testPart1( testCase );\n' );
    % for n = 1 : 1024;
    %     fprintf( fid, '    verifyEqual( testCase, 2, 2 );\n' ); end;    
    % fprintf( fid, 'end\n' );
    
    mkdir_cd .. _Empty
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid, 'function tests = test1test;\n    tests = functiontests( localfunctions );\nend\n' ); 
    
end

function generator_matlab_class;
    cleanfid = onCleanup( @() close_all_fid );
    cleandir = mkdir_cd( 'matlab_class' );  %#ok<NASGU>
    
    mkdir_cd _1024_1
    for n = 1:1024;
        fid = fopen( ['test' num2str(n) 'test.m'], 'wt' );
        fprintf( fid, [
                'classdef test' num2str(n) 'test < matlab.unittest.TestCase' newline ...
                '    methods(Test)' newline ...
                '        function testPart1( testCase );' newline ...
                '            testCase.verifyEqual( 2, 2 );' newline ...
                '        end' newline ...
                '    end' newline ...
                'end' newline] ); end;
    
    mkdir_cd .. _1_1024
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid, [
                'classdef test1test < matlab.unittest.TestCase' newline ...
                '    methods(Test)' newline ...
                ] );
    for n = 1:1024
        fprintf( fid, [
                '        function testPart' num2str(n) '( testCase );' newline ...
                '            testCase.verifyEqual( 2, 2 );' newline ...
                '        end' newline] ); end;
    fprintf( fid, [
                '    end' newline ...
                'end' newline] );
    
    % mkdir_cd .. A
    % fid = fopen( 'test1test.m', 'wt' );
    % fprintf( fid, [
    %             'classdef test1test < matlab.unittest.TestCase' newline ...
    %             '    methods(Test)' newline ...
    %             '        function testPart1( testCase );' newline ...
    %             ] );
    % for n = 1 : 1024;
    %     fprintf( fid, [
    %             '            verifyEqual( testCase, 2, 2 );\n'] ); end;    %#ok<NBRAK2>
    % fprintf( fid, [
    %             '        end' newline ...
    %             '    end' newline ...
    %             'end' newline] );
    
    mkdir_cd .. _Empty
    fid = fopen( 'test1test.m', 'wt' );
    fprintf( fid, [
                'classdef test1test < matlab.unittest.TestCase' newline ...
                '   methods(Test)' newline ...
                '       end' newline ...
                'end' newline] ); 
        
end


%% helper

function make_gitignore();
    [fid, cleanup] = fopen_safe( '.gitignore', 'wt' );  %#ok<ASGLU>
    fprintf( fid, '*.*' );
end

function [ cleandir ] = mkdir_cd( varargin );
    if( nargout == 1 );
        olddir = pwd;
        cleandir = onCleanup( @() cd(olddir) ); end;
    
    for i = 1:numel( varargin );
        mkdir( varargin{i} );
        cd( varargin{i} ); 
        fid = fopen( '.gitignore', 'wt' );
        fprintf( fid, '*test.m\n' );
        make_gitignore(); end;
end

function close_all_fid();
    fids = fopen( 'all' );
    for i = fids;
        try;
            fclose( i );
        catch me;  %#ok<NASGU>
            end; end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD,*MCMFL>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

