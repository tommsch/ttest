function benchmark
 %#ok<*AGROW>

    % pre processing
    olddir = pwd;
    cleandir = onCleanup( @() cd(olddir) );
    cd( fileparts( which( mfilename ) ) );
    
    frameworksdir = pwd;
    frameworks = dir;
    for f = numel( frameworks ):-1:1;
        cd( frameworksdir );
        if( ~frameworks(f).isdir );
            continue; end;
        if( strcmp( frameworks(f).name, '.' ) || strcmp( frameworks(f).name, '..' ) );
            continue; end;
        
        cd( frameworks(f).name );
        suitesdir = pwd;        
        
        if( contains(frameworks(f).name, 'matlab') );
            runner = {@runtests};
        elseif( contains(frameworks(f).name, 'ttest') );
            TTEST clear all
            %runner = {@runtests, @runttests};
            runner = {@runttests};
        elseif( contains(frameworks(f).name, 'moxunit') );
            runner = {@moxunit_runtests};
        elseif( contains(frameworks(f).name, 'xunit4') );
            runner = {@runxunit};
        else;
            fprintf( 'Unkown unit test framework encountered.\n  Framework is skipped' ); 
            continue; end;
        
        
        for r = 1:numel( runner );
            fprintf( 'Benchmarking: %s, Runner: %s\n', frameworks(f).name, strtrim(evalc('disp(runner{r})')) );
            suites = dir;
            for s = 1:numel( suites );
                if( ~suites(s).isdir );
                    continue; end;
                if( strcmp( suites(s).name, '.' ) || strcmp( suites(s).name, '..' ) );
                    continue; end;

                cd( suites(s).name );
                try;
                    fprintf( 'Suite: %s', suites(s).name );
                    tic; evalc( 'runner{r}();' ); T(f,s,1) = toc; 
                    fprintf( ', First run:  %7.2f', T(f,s,1) );
                    tic; evalc( 'runner{r}();' ); T(f,s,2) = toc;
                    fprintf( ', Second run: %7.2f\n', T(f,s,2) );
                catch me;
                    fprintf( ', Error while benchmarking. This benchmark is aborted.\nThrown error:%s\n', me2str(me) );
                    end;

                cd( suitesdir );
            end; end;
        fprintf( '\n' );
        
        end;
    
    
end


function dummy; end  %#ok<>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

%% Ryzen 3600, Matlab R2023b
%{
Benchmarking: xunit4, Runner: @runxunit
Suite: A, First run:     0.52, Second run:    0.03
Suite: E, First run:     0.07, Second run:    0.02
Suite: F, First run:    28.72, Second run:    5.60
Suite: S, First run:     3.99, Second run:    3.19

Benchmarking: ttest_section, Runner: @runtests
Suite: A, First run:     0.71, Second run:    0.42
Suite: E, First run:     0.19, Second run:    0.18
Suite: F, First run:    57.49, Second run:   48.76
Suite: S, First run:     1.62, Second run:    1.59
Benchmarking: ttest_section, Runner: @runttests
Suite: A, First run:     0.32, Second run:    0.29
Suite: E, First run:     0.14, Second run:    0.12
Suite: F, First run:    26.42, Second run:   20.13
Suite: S, First run:     2.11, Second run:    2.14

Benchmarking: ttest_matlab_comment, Runner: @runtests
Suite: A, First run:     0.46, Second run:    0.42
Suite: E, First run:     0.19, Second run:    0.18
Suite: F, First run:    46.63, Second run:   38.36
Suite: S, First run:     0.45, Second run:    0.43

Benchmarking: ttest_function, Runner: @runtests
Suite: A, First run:     0.09, Second run:    0.05
Suite: E, First run:     0.05, Second run:    0.03
Suite: F, First run:    17.51, Second run:    7.91
Suite: S, First run:     0.15, Second run:    0.12
Benchmarking: ttest_function, Runner: @runttests
Suite: A, First run:     0.33, Second run:    0.32
Suite: E, First run:     0.14, Second run:    0.12
Suite: F, First run:    15.99, Second run:    6.13
Suite: S, First run:     1.74, Second run:    1.64

Benchmarking: moxunit, Runner: @moxunit_runtests
Suite: A, First run:     0.63, Second run:    0.04
Suite: E, First run:     0.03, Second run:    0.01
Suite: F, First run:    25.42, Second run:    9.46
Suite: S, First run:     0.19, Second run:    0.01

Benchmarking: matlab_script, Runner: @runtests
Suite: A, First run:     0.29, Second run:    0.25
Suite: E, First run:     0.19, Second run:    0.17
Suite: F, First run:    46.37, Second run:   36.06
Suite: S, First run:     9.20, Second run:    9.07

Benchmarking: matlab_function, Runner: @runtests
Suite: A, First run:     1.17, Second run:    1.12
Suite: E, First run:     0.05, Second run:    0.04
Suite: F, First run:    58.38, Second run:   44.95
Suite: S, First run:    16.84, Second run:   16.64

Benchmarking: matlab_class, Runner: @runtests
Suite: A, First run:     1.23, Second run:    1.13
Suite: E, First run:     0.07, Second run:    0.04
Suite: F, First run:   138.81, Second run:   94.32
Suite: S, First run:     6.35, Second run:    5.87
%}

%% Ryzen 3600, R2020a
%{
Benchmarking: xunit4, Runner: @runxunit
Suite: A, First run:     0.20, Second run:    0.03
Suite: E, First run:     0.03, Second run:    0.01
Suite: F, First run:     5.00, Second run:    1.54
Suite: S, First run:     0.36, Second run:    0.31

Benchmarking: ttest_section, Runner: @runtests
Suite: A, First run:     9.83, Second run:    1.00
Suite: E, First run:     0.66, Second run:    0.48
Suite: F, First run:    52.72, Second run:   48.46
Suite: S, First run:     2.13, Second run:    2.03
Benchmarking: ttest_section, Runner: @runttests
Suite: A, First run:     0.54, Second run:    0.33
Suite: E, First run:     0.19, Second run:    0.17
Suite: F, First run:    16.04, Second run:   15.24
Suite: S, First run:     2.56, Second run:    2.52

Benchmarking: ttest_matlab_comment, Runner: @runtests
Suite: A, First run:     0.65, Second run:    0.61
Suite: E, First run:     0.39, Second run:    0.40
Suite: F, First run:    42.73, Second run:   39.08
Suite: S, First run:     0.64, Second run:    0.61

Benchmarking: ttest_function, Runner: @runtests
Suite: A, First run:     0.36, Second run:    0.27
Suite: E, First run:     0.26, Second run:    0.25
Suite: F, First run:    79.20, Second run:   73.60
Suite: S, First run:     0.34, Second run:    0.34
Benchmarking: ttest_function, Runner: @runttests
Suite: A, First run:     0.42, Second run:    0.35
Suite: E, First run:     0.18, Second run:    0.20
Suite: F, First run:     8.46, Second run:    3.61
Suite: S, First run:     0.47, Second run:    0.42

Benchmarking: moxunit, Runner: @moxunit_runtests
Suite: A, First run:     0.15, Second run:    0.03
Suite: E, First run:     0.03, Second run:    0.01
Suite: F, First run:     9.24, Second run:    6.84
Suite: S, First run:     0.07, Second run:    0.01

Benchmarking: matlab_script, Runner: @runtests
Suite: A, First run:     0.58, Second run:    0.47
Suite: E, First run:     0.39, Second run:    0.38
Suite: F, First run:    42.40, Second run:   38.17
Suite: S, First run:    11.49, Second run:   11.28

Benchmarking: matlab_function, Runner: @runtests
Suite: A, First run:     1.76, Second run:    1.16
Suite: E, First run:     0.23, Second run:    0.19
Suite: F, First run:    30.93, Second run:   26.95
Suite: S, First run:     6.74, Second run:    6.58

Benchmarking: matlab_class, Runner: @runtests
Suite: A, First run:     1.37, Second run:    1.10
Suite: E, First run:     0.22, Second run:    0.20
Suite: F, First run:   228.03, Second run:  378.78
Suite: S, First run:     8.68, Second run:    5.98
%}

% Ryzen 3600, Octave 8.1
%{
Benchmarking: ttest_section, Runner: @runttests
Suite: A, First run:     2.45, Second run:    2.41
Suite: E, First run:     1.71, Second run:    1.71
Suite: F, First run:   866.78, Second run:  855.48
Suite: S, First run:    24.54, Second run:    5.39

Benchmarking: ttest_function, Runner: @runttests
Suite: A, First run:     2.82, Second run:    2.79
Suite: E, First run:     1.57, Second run:    1.58
Suite: F, First run:   567.96, Second run:  565.50
Suite: S, First run:     3.00, Second run:    2.90

Benchmarking: moxunit, Runner: @moxunit_runtests
Suite: A, First run:     0.38, Second run:    0.32
Suite: E, First run:     0.32, Second run:    0.32
Suite: F, First run:   270.25, Second run:  269.10
Suite: S, First run:     0.32, Second run:    0.31
%}