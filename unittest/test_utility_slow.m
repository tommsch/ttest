% test TTEST dependencies

TTEST init
if( isoctave );
    return; end;


%% dependencies test
TESTCASE();
    TTEST path overload off
    MESSAGE( 'fun' );
    for fun = {'dumpws','cwd','filetype','isoctave','ismatlab','sectionname','terror','variablename'};        
        [dep, tb] = matlab.codetools.requiredFilesAndProducts( fun{1} );
        if( ~EXPECT_NUMEL( tb', dep', 1 ) );
            disp( dep ); end; end;
    TTEST path overload on

%#ok<*NOSEL,*NOSEMI,*ALIGN>
