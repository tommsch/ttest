function [ state, st_out, me ] = pbtrepeat( st_in, rounds, depth )
    
    if( nargin<=1 );
        rounds = 30; end;
    if( nargin<=2 );
        depth = 100; end;
    
    depth = ceil( depth/rounds );
	st = deepcopy( st_in );
    state = [];
    me = [];
    st_out = [];
    try;
        for i = 1:rounds;
            
            for j = 1:i*depth
                state = rng();
                st_i = deepcopy( st );
                st_i.example();
            end; end; 
    catch me;
        st_out = deepcopy( st_i );
        end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.