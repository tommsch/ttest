% test TTEST

TTEST init

% preconditions 

%% fail
[w_msg, w_id] = lastwarn;
EXPECT_STRCONTAINS( @() tt.expect_fail, 'Expect test failed.' ) ;
lastwarn( w_msg, w_id );

%% succeed
tt.expect_succeed;

%% true
EXPECT_TRUE( tt.expect_true( 1 ) );
EXPECT_TRUE( tt.expect_true( 1, 1, 1 ) );
EXPECT_TRUE( tt.expect_true( single(1) ) );
EXPECT_TRUE( tt.expect_true( true ) );

EXPECT_FALSE( tt.expect_true( [true true] ) );
%EXPECT_FALSE( tt.expect_true( 2 ) );  % Would be nice to have to allow only 0/1-values if they are not logical
EXPECT_FALSE( tt.expect_true( 0 ) );
EXPECT_FALSE( tt.expect_true( false ) );
EXPECT_FALSE( tt.expect_true( nan ) );
EXPECT_FALSE( tt.expect_true( [0 0] ) );
EXPECT_THROW( @() tt.assert_true([0 1]), 'ttest:assert:true', 'TTEST:noScalar' );

%% false
tt.expect_false( 0 );
EXPECT_FALSE( tt.expect_false( nan ) );

%% truthy
tt.expect_truthy( [1 2 3] );

%% falsy
tt.expect_falsy( [0 0 0] );

%% pred
tt.expect_pred( @isempty, [] );

%% nthrow
tt.expect_throw( @() sum, 'MATLAB:minrhs', 'Octave:invalid-fun-call', 'o','Texinfo ' );  %#ok<LTARG>



%#ok<*NOSEL,*NOSEMI,*ALIGN>
