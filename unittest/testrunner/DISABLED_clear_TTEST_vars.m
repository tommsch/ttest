function [] = DISABLED_clear_TTEST_vars();
    % delete stuff from functions which are called by this function
    TTEST hardinit doctest_unittest
    TTEST hardinit doctest_doctest
    TTEST hardinit func2
    TTEST hardinit test2
    TTEST hardinit test2_doctest
    TTEST hardinit functionbasedtest_test1
    TTEST hardinit functionbasedtest_test2
    TTEST hardinit recursivetest_unittest
    TTEST hardinit recursivetest_doctest
    TTEST hardinit func_subfolder_unittest
    TTEST hardinit func_subfolder_doctest
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
