function functionbasedtest
 %#ok<*DEFNU>
 
function test1
    TTEST init functionbasedtest_test1
    TTEST var  functionbasedtest_test1 executed

function test2
    TTEST init functionbasedtest_test2
    TTEST var  functionbasedtest_test2 executed
