% test macro sym

TTEST init

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );


%% TRUTHY
EXPECT_TRUTHY( sym(1) );
EXPECT_TRUTHY( sym(inf) );

%% FALSY
EXPECT_THROW( @() EXPECT_FALSY(@sum), 'sym:isAlways', 'MATLAB:minrhs', 'Octave:invalid-fun-call' );

%%% ALMOST_EQ
EXPECT_ALMOST_EQ( sym(2), 2 );

%% almost_eq

MESSAGE( 'rx', 'ix', 'ry', 'iy', 'type' )
for rx = [-nan -inf 0 inf nan];
for ix = [-nan -inf 0 inf nan];
for ry = [-nan -inf 0 inf nan];
for iy = [-nan -inf 0 inf nan];
    type = rand_oneof( @double, @vpa, @sym );
    x = type( rx + 1i*ix );
    y = type( ry + 1i*iy );
    
    if( isnan(x) || isnan(y) );
        ASSERT_FALSE( tt.expect_almost_eq( x, y ) );
    elseif( isequal( x, y ) );
        ASSERT_TRUE( tt.expect_almost_eq( x, y ) );
    else;
        ASSERT_FALSE( tt.expect_almost_eq( x, y ) );
        end;
    end; end; end; end;

%% SUPERSET
syms xx;
EXPECT_SUPERSET( {xx,xx+1},{xx,xx+1} );

%% postprocessing
% Set errorflag to false
TTEST errflag false

%#ok<*NOSEL,*NOSEMI,*ALIGN>
