function [ fig ] = getfigure( obj );
% (experimental) Returns the figure of a graphics object
% [ fig ] = getfigure( obj )
% If the object belongs to more than one figure (I do not know whether this is possible), then an error is thrown
% If the object does not belong to a figure, then `[]` is returned
%
% Input:
%   obj     anything
%
% Output:
%   fig     figure handle
%
% Example:
%   getfigure( plot( rand(1, 5) ) )
%
% Written by: tommsch, 2024-03-27

    assert( nargin == 1, 'getfigure:input', 'Exactly one input argument must be given.' );
    if( isa( obj, 'matlab.ui.Root' ) );
        fig = obj.CurrentFigure;
        return;
    elseif( isoctave() && isequal(obj, 0) );
        fig = gcf();
        return; end;
    if( isoctave );
        if( any( ~ishghandle( obj ) ) );
            fig = [];
            return; end; end;
    figs = ancestor( obj, 'figure' );
    if( numel(figs) > 1 );
        fig = figs{1};
        for i = 2:numel( figs );
            assert( isequal(fig, figs{i}) , 'TTEST:getfigure', 'More than one figure was found.' ); end;
    else;
        fig = figs; end;

    return; 

    if( numel(figs) == 1 );
        fig = figs(1);
        return; end;

    if( isoctave );
        Name = {};
        if( iscell(figs) );
            Number = [figs{:}];
        else;
            Number = figs; end;
    else;
        Name = cell( 1, numel(figs) );
        Number = zeros( 1, numel(figs) );
        if( ~iscell(figs) );
            figs = {figs}; end;
        for i = 1:numel( figs );
            if( ~isempty(figs{i}) );
                if( ~isempty(figs{i}.Number) );
                    Number(i) = figs{i}.Number; 
                else;
                    Name{i} = figs{i}.Name; end; end; end; end;
    idx_Name_empty = cellfun( 'prodofsize', Name ) == 0;
    Name(idx_Name_empty) = [];
    Number(Number == 0) = [];
    Name = unique( Name );
    if( ~isempty(Name) && isempty(Name{1}) );
        Name = {}; end;
    Number = unique( Number );
    assert( numel(Name) + numel(Number) <= 1, 'TTEST:getfigure', 'More than one figure was found.' );
    if( numel(Number) );
        fig = figure_lazy( Number(1) );
    elseif( numel(Name) );
        fig = findall( groot, 'Type', 'figure', 'Name',Name{1} );
        set( 0, 'CurrentFigure', fig{1} );
    else;
        fig = []; end;
    

end

function [ varargout ] = figure_lazy( fig );
    % sets focus to figure with number fig, but does not bring it to the front in case it already exists
    if( nargin == 0 );
        figure;
    else;
        try;
            set( 0, 'CurrentFigure', fig );
        catch me;  %#ok<NASGU>
            figure( fig ); end; end;
    
    if( nargout >= 1 );
        varargout = cell( 1, nargout );
        varargout{1} = gcf; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
