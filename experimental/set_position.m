function [ flag ] = set_position( varargin );
% tries to set the position of a figure window
% Matlab unfortunately tries to be smart when this is done,
% and the resulting place of the figure may not the one provided
% [ flag ] = set_position( [lx ly] );
% [ flag ] = set_position( [x y lx ly] );
%
% Input:
%   x, y    position of the figure
%   lx, ly  x and y size of the figure
%
% Output:
%   flag    is trueif function suceeded
%
% Written by: tommsch, 2024


    if( isscalar(varargin{1}) && ishandle(varargin{1}) && numel(varargin) == 2 );
        fig = varargin{1};
        pos_orig = varargin{2};
    elseif( isnumeric(varargin{1}) );
        fig = gcf;
        pos_orig = varargin{1};
    elseif( numel(varargin) == 2 );
        fig = varargin{1};
        pos_orig = varargin{2};
    else;
        error( 'setposition:input', 'Input could not be parsed.' ); end;
    if( numel(pos_orig) == 2 );
        pos_new = [0 0 pos_orig];
    else;
        pos_new = pos_orig; end;

    set( fig, 'Position',pos_new );
    resize_current = get( fig, 'Resize' );
    clean_resize = onCleanup( @() set( fig, 'Resize',resize_current ) );
    pos_after = get( fig, 'Position' );
    if( numel(pos_orig) == 2 );
        flag = isequal( pos_orig, pos_after(3:4) );
    else;
        flag = isequal( pos_orig, pos_after ); end;
end

%#ok<*NOSEL,*NOSEMI,*ALIGN>
