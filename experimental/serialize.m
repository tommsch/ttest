function [ ret ] = serialize( obj );
% serializes objects
    if( ismatlab );
        ret = getByteStreamFromArray( obj );
    else
        ret = uint8( save( '-7', '-', 'obj' ) );
        end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.  
%#ok<*NOSEL,*NOSEMI,*ALIGN>