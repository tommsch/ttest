function [ str ] = complexity( varargin );
% (experimental) Guesses to wich complexity class an algorithm belongs
% [ str ] = complexity( xy, [options] );
% [ str ] = complexity( [x], y, [options] );
%
% Input:
%   xy      2xN arary, first row: Input size, second row: Measured time
%   x       1xN array, default: 1:numel(y) Input size
%   y       1xN array, Measured time  
%
% Options:
%   'filter',bool   logical, default=true, filters input values
%   'verbose',val   integer, verbose level
%
% Output
%   str     string which describes complexity
%               1, logn, n, nlogn, n2, n3, n4, 2n, n!

    [fun, name] = make_functions();
    [xy, opt] = parse_input( varargin{:} );
    [xy, ymin] = preprocess( xy, opt );
    [b, rel_err] = estimate( xy, fun, name, opt );
    str = do_output( xy, ymin, fun, b, rel_err, name, opt );

end

function [ xy, opt ] = parse_input( varargin );
    
    [opt.weight, varargin] =    tt.parsem( {'weighted','weight'}, varargin, '1+x' );
    [opt.verbose, varargin] =   tt.parsem( {'verbose','v'}, varargin, 1, 'expect',@isnumeric ); %Verbose level
    [opt.filter, varargin] =    tt.parsem( {'filter','f'}, varargin, 'default' );
    [opt.all, varargin] =       tt.parsem( {'all'}, varargin );
    
    if( numel(varargin) == 1 && size(varargin{1}, 1) == 1 && isnumeric(varargin{1}) );
        y = varargin{1};
        x = 1:numel(y);
        varargin(1) = [];
    elseif( numel(varargin) == 2 && isnumeric(varargin{1}) && isnumeric(varargin{2}) );
        x = varargin{1};
        y = varargin{2};
        varargin(1:2) = [];
    elseif( numel(varargin) == 1 && size(varargin{1}, 1) == 2 && isnumeric(varargin{1}) );
        x = varargin{1}(1,:);
        y = varargin{1}(2,:);
        varargin(1) = []; end;
    tt.parsem( varargin, 'test' );
    
    xy = [x;y];
    
    
end

function [ xy, ymin ] = preprocess( xy, opt );
    nanidx = any( isnan( xy ), 1 );
    if( any(nanidx) );
        warning( 'complexity:nan', 'NaNs encountered in input data. Those values will be filtered out.' ); end;
    xy(:,nanidx) = [];
    
    posidx = all( xy>0, 1 );
    if( ~all(posidx) );
        warning( 'complexity:x', 'All x and y values must be strictly positive. All non-positive values will be filtered out.' ); end;
    xy(:,~posidx) = [];

    [~, idx] = sort( xy(1,:) );
    xy = xy(:,idx);
    
    doshift = true;
    switch opt.filter;
        case {0,false,'','none'};
            doshift = false;
        case {'cummax','limsup','max'};
            xy(2,:) = cummax( xy(2,:) );
        case {'default','liminf','cummin','min'};
            xy(2,end:-1:1) = cummin( xy(2,end:-1:1) );
        case {'movmedian','movmed','median','med'};
            xy(2,:) = movmedian( xy(2,:), 3 );
            if( size(xy, 2)>=1 );
                xy(:,1) = []; end;
            if( size(xy, 2)>=1 );
                xy(:,end) = []; end;        
        case {'movmean','mean'};
            xy(2,:) = movmean( xy(2,:), 3 );
        otherwise;
            error( 'complexity:filter', 'Wrong value passed to option with name `filter`.' ); end;
        
    ymin = min( xy(2,:) );
    if( doshift );
        xy(2,:) = xy(2,:) - ymin; end;
end

function [ fun_, name_ ] = make_functions();
    persistent fun;
    persistent name;
    
    if(  isempty(fun) );
        i = 0;
        while( true );
            i = i + 1;
            switch i;
                case 1;  % 1
                    name.long{i} = 'O( 1 )      ';  % all names must have the same length
                    name.short{i} = '1';
                    fun{i} = @(b,x) b.*ones( size(x) );
                case 2;  % logn
                    name.long{i} = 'O( logn )   ';
                    name.short{i} = 'logn';
                    fun{i} = @(b,x) b*(log( x + 1 ));
                case 3;  % n
                    name.long{i} = 'O( n )      ';
                    name.short{i} = 'n';
                    fun{i} = @(b,x) b*(x);
                case 4;  % nlogn
                    name.long{i} = 'O( n logn ) ';
                    name.short{i} = 'nlogn';
                    fun{i} = @(b,x) b*((x+1).*log(x+1));
                case 5;  % n^2
                    name.long{i} = 'O( n^2 )    ';
                    name.short{i} = 'n2';
                    fun{i} = @(b,x) b*(x.^2);
                case 6;  % n^3
                    name.long{i} = 'O( n^3 )    ';
                    name.short{i} = 'n3';
                    fun{i} = @(b,x) b*(x.^3);
                case 7;  % n^4
                    name.long{i} = 'O( n^4 )    ';
                    name.short{i} = 'n4';
                    fun{i} = @(b,x) b*(x.^4);
                case 8;  % exp(n)
                    name.long{i} = 'O( 2^n )    ';
                    name.short{i} = '2n';
                    fun{i} = @(b,x) b*(2.^x - 1);
                case 9;  % n!
                    name.long{i} = 'O( n! )     ';
                    name.short{i} = 'n!';
                    fun{i} = @(b,x) b*(gamma( x + 1 ) - 1);
                case 10;  % n^(1/2);
                    name.long{i} = 'O( n^{1/2} )';
                    name.short{i} = 'n1/2';
                    fun{i} = @(b,x) b*(x.^(1/2));
                case 11;  % n^(1/3);
                    name.long{i} = 'O( n^{1/3} )';
                    name.short{i} = 'n1/3';
                    fun{i} = @(b,x) b*(x.^(1/3));
                otherwise;
                    break; end; end; end;  % leave loop
        
    name_ = name;
    fun_ = fun;
end

function [ b, rel_err ] = estimate( xy, fun, name, opt );
    b = zeros( 1, numel(fun) );
    err = zeros( 1, numel(fun) );
    for i = 1:numel( fun );
        val = xy(2,:)./fun{i}(1, xy(1,:));
        val(isnan(val)) = 1;
        nanidx = isnan( val );  % after applying `fun` it could happen that there are new NaN values
        if( any(nanidx) );
            warning( 'complexity:fun', 'NaN values occured in estimating coefficient for O(%s)', name.long{i} ); end;
        switch opt.weight;
            case 'x';
                b(i) = mean( val(~nanidx).*(xy(1,~nanidx)) ) / mean( (xy(1,~nanidx)) );  % use (1+x) values as weight            
            case '1+x';
                b(i) = mean( val(~nanidx).*(1+xy(1,~nanidx)) ) / mean( (1+xy(1,~nanidx)) );  % use (1+x) values as weight
            case {0,false,'','none'};
                b(i) = mean( val(~nanidx) );  % use (1+x) values as weight
            otherwise;
                error( 'complexity:weight', 'Wrong value given for option ''weight''.' ); end;
        err(i) = sqrt( sum( abs( xy(2,:) - fun{i}(b(i), xy(1,:)) ).^2 ) ); 
        end;
        
    rel_err = sqrt( numel(xy(2,:)) ) * (err / sum( abs(xy(2,:)) ));
    rel_err(isnan(rel_err)) = inf;
    idx_finite = isfinite( b );
    rel_err(~idx_finite) = inf;
    %for i = 1:numel( b );
    %    rel_err(i) = correction{i}( rel_err(i) ); end;        
end

function [ str ] = do_output( xy, ymin, fun, b, rel_err, name, opt );

    [~, err_idx] = sort( rel_err );  % sorted index
    str = name.short{err_idx(1)};
    
    if( opt.verbose >= 1 );
        fprintf( '|  coefficient   |   err%%    |  complexity  |\n' );
        fprintf( '|---------------:|----------:|--------------|\n' );
        for i = 1:numel( err_idx );
            rel_err_i = rel_err(err_idx(i));
            if( rel_err_i<1 && ~isnan(b(err_idx(i))) || opt.verbose>=2 || opt.all );
                fprintf( '|  %+13.6e | %6.2i  | %s |\n', b(err_idx(i)), 100*rel_err_i, name.long{err_idx(i)} ); end; end; end;

    if( opt.verbose >= 2 );
        clf; hold on;
        plot( xy(1,:), xy(2,:)+ymin, 'k.' );
        for i = 1:numel( b );
            rel_err_i = rel_err(err_idx(i));
            if( rel_err_i < 1 && ~isnan( b(err_idx(i)) ) || ...
                opt.verbose >= 3 ...
              );
                switch i;
                    case 1; format = 'r-';
                    case 2; format = 'r--';
                    case 3; format = 'r-.';
                    case 4; format = 'r:';
                    case 5; format = 'b--'; 
                    case 6; format = 'b-.'; 
                    case 7; format = 'b:'; 
                    otherwise; format = 'k:';
                end;
                yi = fun{err_idx(i)}(b(err_idx(i)), xy(1,:));
                if( ~isequal(size(yi, 2), size(xy, 2)) );
                    continue; end;
                plot( xy(1,:), yi + ymin, format );  
            end; end;
        if( ismatlab );
             warning( 'off', 'MATLAB:legend:IgnoringExtraEntries' ); end;
        legend( ['data', name.long(err_idx(:))], 'Location','eastoutside' );
        if( contains( name.short{err_idx(1)}, {'n!','2n'} ) || ...
            opt.verbose >= 3 ...
          );
            set( gca, 'YScale', 'log' ); end;
        
        try;
            xm = [min( xy(1,:) ) max( xy(1,:) )];
            ym = [min( xy(2,:) ) max( xy(2,:) )];
            xborder = log(abs(xm(2)/xm(1)))/2;
            yborder = min( log(abs(ym(2)/ym(1)))/2, abs(max(ym))/2 );
            axis( [xm(1) - xborder 
                   xm(2) + xborder
                   max( 0, ymin + min( xy(2,:) ) - max( xy(2,:)/2 ) ) - yborder
                   ymin + max( xy(2,:) ) - min( xy(2,:)/2 ) + yborder
                  ] );
        catch me; %#ok<NASGU>
            end; end;

    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 
%#ok<*NOSEL,*NOSEMI,*ALIGN>
