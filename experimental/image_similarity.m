function [ score ] = image_similarity( A, B )
    score = 1;

    % Resize images to a common size
    common_size = min(size(A), size(B));
    Ar = imresize( A, common_size );
    Br = imresize( B, common_size );

    tic; ssim_s = ssim_score( Ar, Br ); toc
    tic; psnr_s = psnr_score( Ar, Br ); toc
    tic; svd_s = svds_score( A, B ); toc
    tic; dftm_s = dft_magnitude_score( Ar, Br ); toc
    tic; surf_s = speededup_robust_features( A, B ); toc
    tic; hog_s = histogram_of_oriented_gradients( A, B ); toc
    tic; bovw_s = bag_of_visual_words( A, B ); toc
    tic; histogram_s = pixel_intensity_values_histograms( A, B ); toc
    tic; avg_s = avg_pixel_intensity_values( A, B ); toc

    fprintf( 'ssim score: %f\npsnr score: %f\nsvd score: %f\ndft score: %f\nsurf score: %f\nhog score: %f\nbovw score: %f\nhistogram score: %f\navg_score: %f\n', ssim_s, psnr_s, svd_s, dftm_s, surf_s, hog_s, bovw_s, histogram_s, avg_s );
end

function [ score ] = ssim_score( Ar, Br );
    score = (ssim( Ar, Br ) + 1)/2;
end

function [ score ] = psnr_score( Ar, Br );
    mse = sum(sum((Ar - Br).^2)) / numel(Ar);
    max_intensity = max(Ar(:));
    psnr = 10 * log10(max_intensity^2 / mse);
    score = min(max((psnr / 100), 0), 1);
end

function [ score ] = svds_score( A, B );
    [~, sa] = svd( A, 'econ' );
    [~, sb] = svd( B, 'econ' );
    l1 = min( size(sa, 1), numel(sb, 1) );
    l2 = min( size(sa, 2), numel(sb, 2) );
    sa = sa(1:l1,1:l2);
    sb = sb(1:l1,1:l2);
    score = dot(sa, sb) / (norm(sa) * norm(sb));
end

function [ score ] = dft_magnitude_score( Ar, Br );
    dft_A = fft2( double(Ar) );
    dft_B = fft2( double(Br) );
    magnitude_spectrumA = abs( dft_A );
    magnitude_spectrumB = abs( dft_B );
    score = (corr2( magnitude_spectrumA, magnitude_spectrumB ) + 1)/2;
end

function [ score ] = speededup_robust_features( A, B );
    pointsA = detectSURFFeatures( A );
    pointsB = detectSURFFeatures( B );
    [featuresA, surf_valid_pointsA] = extractFeatures( A, pointsA );
    [featuresB, surf_valid_pointsB] = extractFeatures( B, pointsB );
    index_pairs = matchFeatures( featuresA, featuresB );
    num_matches = size( index_pairs, 1 );
    max_possible_matches = min( surf_valid_pointsA.Count, surf_valid_pointsB.Count );
    score = num_matches / max_possible_matches;
end

function [ score ] = histogram_of_oriented_gradients( A, B );
    featuresA = extractHOGFeatures( A );
    featuresB = extractHOGFeatures( B );
    l = min( numel(featuresA), numel(featuresB) );
    featuresA = featuresA(1:l);
    featuresB = featuresB(1:l);
    similarity = dot( featuresA, featuresB );
    normA = norm( featuresA );
    normB = norm( featuresB );
    score = (similarity / (normA * normB) + 1) / 2;
end

function [ score ] = bag_of_visual_words( A, B );
    % Bag of Visual Words (BoVW) model
    points1 = detectSURFFeatures( A );
    points2 = detectSURFFeatures( B );
    [featuresA, ~] = extractFeatures( A, points1 );
    [featuresB, ~] = extractFeatures( B, points2 );
    bovw_num_clusters = round( min( size(A) )/2 );  % Number of clusters (adjust as needed)
    [idxA, ~] = kmeans( featuresA, bovw_num_clusters );
    [idxB, ~] = kmeans( featuresB, bovw_num_clusters );
    histogramA = histcounts( idxA, 1:bovw_num_clusters + 1 );
    histogramB = histcounts( idxB, 1:bovw_num_clusters + 1 );
    similarity = dot( histogramA, histogramB );
    normA = norm( histogramA );
    normB = norm( histogramB );
    score= (similarity / (normA * normB) + 1) / 2;
end

function [ score ] = pixel_intensity_values_histograms( A, B );
    num_bins = size( A, 1 );;
    histogramA = imhist( A, num_bins );
    histogramB = imhist( B, num_bins );
    intersection = min( histogramA, histogramB );
    union = max( histogramA, histogramB );
    similarity = sum(intersection) / sum(union);
    score = similarity;
end

function [ score ] = avg_pixel_intensity_values( A, B );
    intensityA = mean( A );
    intensityB = mean( B );
    similarity = 1 ./ (1 + mean(abs(intensityA - intensityB)));
    score = similarity;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
