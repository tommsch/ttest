function [ h ] = monitor_console( id )
% (experimental)
% Captures all output to the console
%
% Usage:
%   h = monitor_console();
%   % come code
%   str = h();  % str contains the console output occured during execution of `%some code`
%  
% Written by: tommsch, 2025-01-10

%               tommsch,    2025-01-10,     New experimental function: `monitor_console`
% Changelog:    

    assert( nargout == 1, 'monitor_console:nargout', 'The return value of this function must be stored' );
    if( nargin == 0 );
        id = '0'; end;
    id = num2str( id );
    if( ~isvarname(id) );
        [~, id] = fileparts( id ); end;
    assert( isvarname(id), 'monitor_console:id', 'The passed `id` is not a valid one.' );
    caller_name = '';
    db = dbstack;
    if( numel(db) >= 2 );
        caller_name = db(2).name; end;
    [~, caller_name] = fileparts( caller_name );
    base = fileparts( which( 'TTEST' ) );

    filename = fullfile( base, 'tmp', 'diary', [caller_name '_' id '.log'] );
    if( exist(filename, 'file') );
        delete( filename ); end

    diary( filename );
    h = @() check_diary( filename );
end

function [ content ] = check_diary( filename );
    diary off;  % Stop logging
    content = evalc( ['type( ''' filename ''' )'] );
    if( numel(content)>=1 && strcmp( content(end), newline ) );
        content(end) = []; end;
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>  % cd for mcc

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
