function [fid, cleanup] = fopen_safe( varargin );
% wrapper for fopen, which returns its cleanup function too
    assert( nargout == 2, 'fopen_safe:nargout', 'This function must have too output arguments.' );
    fid = fopen( varargin{:} );
    if( fid == -1 );
        cleanup = [];
    else;
        cleanup = onCleanup( @() fclose(fid) ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>