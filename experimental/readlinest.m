function [ S ] = readlinest( filename );
% simple implementation of readlines, returning cell array of char-array
% [ S ] = readlinest( filename )
    filetext = fileread( filename );
    line_ending = {'\n','\r','\r\n'};
    S = strsplit( filetext, line_ending )';
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
