function [ str, err ] = get_line( varargin );
% returns the nth line of a file
% [ str, err ] = get_line();
% [ str, err ] = get_line( linenumber );
% [ str, err ] = get_line( linenumber, filename );
%
% Input:
%   linenumber  int, default = value of dbstack()(3).line, the line to be returned
%   filename    string, default = value of dbstack()(3).file, the function which shall be read
%
% Output:
%   str         str, the `linenumber`-th line of file `filename`
%               (experimental behaviour) If linenumber is too big OR file could not be opened, then an empty string is returned
%   err         If two output arguments are requested, than an error is signified by setting err to a non-zero value. No warnings/errors are thrown.
%
% Written by: tommsch, 2024-07-01

    str = '';
    err = 0;
    if( numel(varargin) == 0 );
        ds = dbstack();
        if( numel(ds) <= 2 );
            if( nargout <= 1 );
                error( 'get_line:input', 'If no filename is given, then this function can only be called from a function.' );
            else;
                err = 1;
                return; end; end;
        filename = ds(3).file;
        linenumber = ds(3).line;
    elseif( numel(varargin) == 1 );
        ds = dbstack();
        assert( numel(ds) >= 3 );
        filename = ds(3).file;
        linenumber = varargin{1};
    elseif( numel(varargin) == 2 && ...
            (ischar( varargin{1} ) || isstring( varargin{1} )) ...
          );
        filename = varargin{1};
        linenumber = varargin{2};
    elseif( numel(varargin) == 2 && ...
            (ischar( varargin{2} ) || isstring( varargin{2} )) ...
          );
        filename = varargin{2};
        linenumber = varargin{1};
    else;
        if( nargout <= 1 );
            error( 'get_line:input', 'Could not parse input.' );
        else;
            err = 1;
            return; end;
    end;
    if( linenumber <= 0 );
        if ( nargout <= 1 );
            error( 'get_line:line_numer', 'Line number must be positiv' );
        else;
            err = 1;
            return; end; end;
    
    fid = fopen( filename );
    if( fid == -1 );
        filename = which( filename );
        fid = fopen( filename ); end;
    if( fid ~= -1 );
        close_file = onCleanup( @() fclose(fid) ); end;
    if( isequal(fid, -1) );
        if( nargout <= 1 );
            error( 'get_line:file', 'Could not find/open file %s', filename ); 
        else;
            err = 1;
            return; end; end;

    for i = 1:linenumber;
        tline = fgetl( fid );
        if( isa(tline, 'double') && isequal(tline, -1) );
            if( nargout <= 1 );
                error( 'get_line:line_numer', 'Linenumber to big' );
            else;
                err = 1;
                return; end; end; end;
    str = tline;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
