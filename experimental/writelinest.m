function writelinest( lines, filename );
% simple implementation of writelines
% writelinest( filename )
    
    for i = 1:numel( lines );
        lines{i}(end+1) = newline; end;
    lines = [lines{:}];

    fid = fopen( filename, 'w' );
    if( fid == -1 );
        error( 'writelinest:fopen', 'Could not open file for writing: %s', filename );
        end;
    fid_clean = onCleanup( @() close_fid( fid ) );
    fprintf( fid, '%s', lines );
end

function close_fid( fid );
    if( fid ~= -1 );
        try;
            fclose( fid );
        catch me; %#ok<NASGU>
            end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
