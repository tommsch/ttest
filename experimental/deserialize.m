function [ obj ] = deserialize( ret );
% serializes objects
    if( ismatlab );
        obj = getArrayFromByteStream( ret );
    else
        
    end

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.  
%#ok<*NOSEL,*NOSEMI,*ALIGN>