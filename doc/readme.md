
# TTEST Suite
### *Unittest framework for scientific people and scientific software*
Thomas Mejstrik, Clara Hollomey, *TTEST - Unittest framework for scientific people and scientific software*, gitlab.com/tommsch/TTEST, 2024

## Authors:
- *Thomas Mejstrik*,
    > - [NuHAG](https://nuhag.univie.ac.at/), Faculty of Mathematics, University of Vienna, Universitaetsring 1, 1010 Vienna, Austria
- *Clara Hollomey*
    > - [ARI](https://www.oeaw.ac.at/isf/), Austrian Academy of Sciences, Wohllebengasse 12-14, 1040, Vienna, Austria

## Changelog

### **v3.2025.02.28**
- exception ids of lower-case tt.assert_ functions changed
- `TTEST install`: New installation option -clean
- New functions: (experimental) `monitor_console`
- `runntests`: Behaviour change: If no skipterms are given (starting with '-'), then 'disable' is a skipterm, Behaviour change: Skip terms are case-insensitive, Better support for recursive calls of runttests, Better support for test suites with multiple `TTEST init` calls
- `cwd`: Bugfix for paths with/without trailing filesep character
- `dumpws`: Added new output option '-var'
- `repr`: new option '-clipboard'

### **v3.2024.10.31**
- End-of-line-logging is now officially supported
- Improved `MAXTIME` test
- `cwd`: Option `'eager'` is deprecated
- `TTEST`: Behaviour change: When called without arguments, the help text is printed, Bugfix when called with `ttest_id` only
- `TTEST_DISP`: Behaviour change: Does not accepts `ttest_id` as first parameter anymore due to performance reasons
- `clearat`:  Function now returns an empty array as output
- `inputat`: Behaviour change: The input style `inputat( '-in', '-input','some string' )` is not supported anymore
- `runttests`: Behaviour change: function based tests must not take nor return arguments; Improved finding of test suites. Now runttests also searches in super-folders, if it cant find tests in the current folder, Added option: `'no_search_parent_dirs'`, Better parsing of command line options when given in "shell-format", More robust behaviour when runttests executes an m-file which is by chance not a TTEST test suite, 
- New macros `_EQN`/`_EEQN`/`_NEN`/`_NEEN` for equality tests of data with NaNs


### **v3.2024.09.02**
- Bugfixes in `_MAXTIME` tests
- Removed `_DOUBLE_EQ`, `_FLOAT_EQ` - Use `_ALMOST_EQ` instead
- `ASSUME_` macros are now experimental
- `inputat`: Behaviour change: The input style `inputat( '-in', '-input','some string' )` is not supported anymore

### **v3.2024.08.01**
- Added matchers: `IsIntegral`, `IsReal`, `IsNumeric`,  `IsFloat`, `IsPositive`, `IsNonnegative`, `IsNonNegative`, `IsZero()`, `IsNonpositive`, `IsNonPositive`, `IsNegative`, `IsFinite`, `IsNan`, `IsInf`, `IsInfinity`, `IsInfinite`, `IsPrime`, `IsInteger`
- Improved search results in `cwd`
- Added unit tests for shims
- added aliases `_STR_EQ`, `_STR_NE`, `_TYPE_EQ`, `_TYPEEQ`, `_STRCASE_EQ`, `_STRCASE_NE`
- Added thirdparty functions: `isvpa` and `issymstrict`, for checks in macro `_ISA`
- Experimental behaviour change: Macros for which function handles do not make sense, automatically evaluate all passed function handles (but not evalable strings). These are:  `_TRUE`, `_FALSE`, `_TRUTHY`, `_FALSY`, number comparison macros. (`_LT`, ...) and String macros (`_STREQ`)
- Added experimental function `can_execute_parfor`
- Behaviour change: `MESSAGE` now takes names of variables, instead of the variables, and thus can be used even before the variables are defined
- Added new type of macros: `ASSUME_` tests. If they fail, the test suite is aborted, but the test run does not count as failed.
- Removed some old shims
- Added experimental macros: `_IMG_EQ`
- Deprecated macros: `DOUBLE_EQ`, `FLOAT_EQ`, `SINGLE_EQ`
- Removed installation option: `-experimental`
- Sections: Bugfix, Added experimental support for setup and teardown functions
- New function: `is_parfor_worker_available`
- `issymstrict`, `isvpa`: Behaviour change. Function now returns an array instead of a scalar. This change is necessary, since Matlab allows vpa's and sym's in the same array.
- `onCleanup_lazy`: Greatly improved practicallity of this function
- `runttests`: Greatly improved option `'repeat'`. Behaviour change:  If more than one filename is passed, then 'shuffle' defaults to false.
- `cwd`: Improved search results. Added experimental feature with files names `cwd_paths`
- `repr`: Added repring of symbolic stuff, Experimental behaviour change, string input means variable name now, repr without argumts repres the `ans` variable

### **v3.2024.04.02**
- New shim for Octave `ver`
- refactored file and order structure, and thus a major version number change
- removed support for Octave 6.2, 6.3. Currently supported are: Octave 6.4, Octave 8.1, Matlab R2020a, Matlab R2023b
- improved uninstalling behaviour of TTEST, removed option to update `TTEST` from Matlab/Octave within
- improved handling of exceptions thrown during a testrun with `runttests`
- Behaviour change in `TTEST_DISP`
- Improved overload of `warning`, now accepts inputs of type `message` (Some undocumented Matlab thingy)
- Better handling of breakpoints set during debugging a testrun
- Improvements in `cwd`, now accepts wildcards, line numbers, searches more places, can jump to the folder of the currently opened file in the editor
- `dumpws`: Behaviour change of flag `overwrite`, new options `disp` and `get`, `ans` is not dumped anymore, better readable output
- Bugfix in `sysinfo`
- New experimental functions: `deserialize`, `serialize`, `isconvertible_to_bool`
- New experimental macro: `_IMG_EQ` for comparing matlab figures with some gold standard. Works better on Matlab than on Octave currently
- Newly added third party functions: `scl`, `wwhich`, `isolate_axes`, `crop_borders`
- Behaviourchange: Lower case macros do not throw a warning when: a test fails AND the macro is called with at least one output argument
- Added option 'check' for `TTEST`, which does a check of the installation (it calls `ttest_check_installation`)
- `file2txt`: Behaviour change. Prints to screen if nargout == 0. Printout handles html tags correctly (i.e. they will most likely not get rendered)

### **v2.2023.04.19**
- New TTEST option `update` (experimental)
- lowercase macros can now be disabled using `TTEST lcmacro off`
- Better error messages for `_EQ` tests,
- New macros: `_NEMPTY` (experimental), `_TYPE`, `_EEQ`, `_NEE`, `_NN`
- New matchers: `NanSensitiveEq`
- Behaviour change: `_EQ` test does not test underlying properties, `_EEQ` does.
- Update of `tt.parsem`
- Behaviour change: `allfunctionhandle`: local functions are not queried anymore, when a different type of function shall be queried. If local functions shall also be queried, the option `'local'` must be appended
- `cwd`: Added rudimentary possibility to add user defined search paths; Added option aliases with `--` at the front; Minor bugfix; Function should find more files now
- Behaviourchange: `isequalproperty`: Behaviour change: Added test whether values are complex valued; Behaviour change: At least two inputs must be given; Behaviour change: Function now iterates over the elements of structs and cell arrays
- `makefilefilst`: Bugfix for folders without tests

### **v2.2022.03.28**
- Several bugs fixed which were introduced in v2.2022.03.24

### **v2.2022.03.24**
- New version scheme
- Added syntax completion hints (using `functionSignatures.json`) for various functions
- Breaking change: Namespace `ttest.` is renamed to `tt.` . This change was necessary due to multiple Octave bugs.
- Defect fix: `TODO_` tests now also return `false` when a test failed. Otherwise the return value of a `TODO_` test is meaningless.
- Assertions: New experimental options `'execute'`, new assertion `_TOOLBOX`
- `TTEST.m`: Added options: `'uninstall'` (uninstalling TTEST), `overload` (en/disabling TTEST overloads)
- `runttests.m`: Files to be skipped by tests can be given with `':'` prefix too.
Behaviour change: Tests are executed in random order by default (Option: `'shuffle'`)
Tests are executed in differen order each hour.
- `warning.m`: Automatically restore warnings after a `warning off`
- `cwd.m`: Added rudimentary possibility to add user defined search paths
- PBT Drawers: Added experimental options `'type'` and `'postprocess'`
- Experimental: New severity `APPLIED`, works as `ENABLED` but does not produce a warning
- New functions: `cwd`, 

### **v 0.9, 3. September 2021**
- Initial public version.




## Table of Contents

[[_TOC_]]

## Overview

The *TTEST* framework is a unit testing framework, especially designed for scientific software.

*TTEST* is designed along the following guidelines:
  - A test should be easy to write, to foster the writing of tests
  - A test should be easy to write, to prevent bugs in the test suites
  - A test should be easy to understand and to design
  - A test run should not stop just because one test fails, allowing to fix multiple bugs in a single test run
  - When a test fails, as much information as possible should be provided
  - Tests should be portable between Matlab versions and Octave
  - The test suite shall not replace Matlab features and work with most Matlab test styles. 

*In particular, to write a test should not require more knowledge than what is needed to write the program.* Therefore, writing unit tests with *TTESTs* requires no knowledge of
- classes, regular expressions, tables, structs, functional programming, handles, try/catch blocks, anonymous functions, cell arrays, functions

Of all currently maintained Matlab and Octave unit test frameworks (these are: *TTEST*, *MOxUnit* and *Matlab's built in unit test framework*, *TTEST*
- is the fastest, 
- needs least boiler plate code,
- has the most assertions,
- takes most care of isolating unit tests from each other.
- can test scripts, local and sub functions,
- has utilities for caching results for integration tests,
 - adds support for injection testing and partly for design by contract
See far below for the systematic comparison proofing the claims.

Aspects where TTEST falls behind are:
- code coverage reports,
- mocking, performance testing, test fixtures, usage of external data, 
- parallelization,
- coding standard tests.

**Easy writing:** 
A standalone test using the *TTESTs* usually looks like the following `EXPECT_EQ( 2, 3 )`, which produces the output
```matlab
>> EXPECT_EQ( 2, 3 );
lhs ~= rhs (but they should be @isequal), where
lhs = 2
rhs = 3
``` 
This line is already a full featured test, which 
1. does not need much code
2. does what it promises to do

**Informative and unstoppable:**
Usually tests are grouped together in a test file, in which case more informative output is produced than in the stand-alone example above. Assume we put the above tests into the file `testexample.m`
```matlab
%% test integers
EXPECT_EQ( 2, 3 );
%% test error
%%% wrong function name
EXPECT_THROW( 'thisisnotavalidcomand', 'wrong:id' )
``` 
 and run it with Matlabs `runtests` . Then, the following output is produced
```
lhs ~= rhs (but they should be @isequal), where
lhs = 2
rhs = 3
In: testexample.m:2
Test name:  test integers
=====================================
       
Mandatory exception was not thrown.
  An exception should throw one of the id(s):
      {'wrong:id'}
  But thrown exception has id:
      {'MATLAB:UndefinedFunction'}
  In: testexample.m:5
  Test name:  test error
  Subtest name:  wrong function name
=====================================  
```
Here `testexample.m:2` and `testexample.m:2` are hyperlinks to the lines in the file of the failed test. Note also that we grouped the tests and assigned names to the groups and subgroups, here: `test integer` and `test error/wrong function name`. Note that, there are even more possibilities for logging in *TTESTs*. This examples just presents a basic and simple one.

Also note that, the second test is executed, although the first failed. 

**Portability and Backwards compatibility:**
The *TTESTs* should run on all Matlab versions from R2016b onwards and on the latest Octave version. The interface of the *TTESTs* is unlikely to be changed, except for features marked as *(experimental)*.

**Matlab functionality:**
The *TTESTs* are supposed to complement Matlabs testing framework, which is extremely limited for script based tests.

We decided that the *TTESTs* shall not interact with Matlabs testing suites at all by default, because Matlab changes the interface of its functions rapidly. If we would allow interaction of the *TTESTs* with Matlab, the *TTESTs* would most likely be broken after one year.

Nevertheless, one can define error handlers which make it easy to integrate the *TTESTs* into any testing framework, including Matlabs.

### Installation
Copy the files from this archive to a folder called `TTEST` and run `TTEST install`. *You must have write permission in that folder!*. The new path definition is saved automatically. After a new version of *TTESTs* was installed, call `TTEST install update`.

The *TTESTs* depend on some third party functions, which are all shipped with *TTESTs*. Thus, no additional downloads are necessary.

# In depth Documentation
## Test macros
The *TTESTs* offer a wide variety of test macros, named like that due to historical reasons, which should cover most needs. 

### Variable types and symbolic computations
All test macros (should) work with at least the following data types:
- `double`, `single`
- `uint8`, ..., `int64`
- `sym`, `vpa` (Where possible we try to distinguish between these two qualitatively totally different types. Unfortunately this is not always possible due to their implementation in Matlab)
- user defined classes

If you find a macro which does not work with a particular type, but logically it should, please file a bug report.

Most macros take an arbitrary numbers of arguments, testing multiple values at the same time. For example, given three implementations of `sin`, one can write a test
```
x = 1
EXPECT_ALMOST_EQ( sin1(x), sin2(x), sin3(x) );
```

### `TODO_`, `EXPECT_`, `ASSERT_` and `ASSUME_` tests
1. Each test macro is available in four forms: `TODO`, `EXPECT`, `ASSERT` and `ASSUME`. 
     - `TODO_` - tests
 These are for somewhat special use cases, of which one is the the following: You are working on a feature, but found a bug which belongs to another feature which also needs some work, and thus, cannot or should not be fixed at the moment. Writing a `TODO_` test for this bug will remind you, but not brake the unit tests. Furthermore, even if a `TODO` test succeeds, a message is printed. Thus, one will not forget to change the test to an `EXPECT_` or `ASSERT_` test.
 
    -  `EXPECT_`-  tests:
   This is the standard test and should be used most of the time
   
    -  `ASSERT_` - tests:
   Used when there is no point in continuing a test after some condition failed. For example, when the test requires some external database, which is not available at the time of the test run
 
 - `ASSUME_` - tests: (experimental) 
   Used when a test suite (e.g. a file) needs some preconditions. If an `ASSUME` tests fail, the test suite is aborted, but the test run does not count as failed.

 
2. Each  test macro is available in the global namespace with uppercase letters (e.g. `EXPECT_EQ`), and in the namespace `tt.` with lowercase letters (e.g. `tt.expect_eq`) are. The uppercase macros are intended to be used in tests. The lower case macros have less features but are much faster, and can be used in production code. Furthermore, 
  - *(experimental)* lowercase macro can be disabled by calling `TTEST lcmacro off`, which render all lowercase macros nearly to a Nop. 
  - *(experimental)* some `expect_` lowercase macros additionally exist in a version without `expect_` prefix (e.g. `tt.eq`).

We continue with describing all test macros. For brevity, we omit the prefixes `TODO_`, `EXPECT_` and `ASSERT_` in the following, and also only describe the upper case macros

### Generic tests
| Name                                   | Short explanation   |
| -------------------------------------- | --------------------|
| `_FAIL( ['text'])`                     | always fails; when `'text'` is given, `'text'` is printed |
| `_SUCCEED()`                           | always succeeds; for positively minded people |
| `_TRUE`/`_FALSE( x1, ..., xn )`        | tests whether `xi` is a scalar which evaluates to `true`/`false`. |
| `_TRUTHY`/`_FALSY( x1, ..., xn )`     | tests whether `xi` evaluates to `true`/`false` |
| `_PRED`/`_NPRED( pred, x1, ..., xn )` | tests whether `pred( x1, ..., xn )` is fulfilled, respectively not fulfilled |

<!--TRUE-->
**`_TRUE`/`_FALSE`/`_TRUTHY`/`_FALSY`** These tests check whether the arguments are `true` or `false`. The first two macros expect a scalar. The latter two not. Be aware that Matlab evaluates an empty variable to `false` in an `if` statement to false, which is logically wrong. In other words, the following test succeeds `EXPECT_FALSY( [] );`, whereas the test `EXPECT_FALSE( [] )` throws a warning (and fails), because `[]` is not a scalar.

<!--PRED-->
**`_PRED`/`_NPRED`** These tests check whether the arguments fulfil a given predicate. The predicate must be given as the first argument. For example, this test succeeds `EXPECT_NPRED( @isequal, 2, 3 )`

### Comparison tests
| Name                                       | Short explanatation |
| ------------------------------------------ | --------------------|
| `_EQ`/`_EEQ`/`_EQN`/`_EEQN( x1, ..., xn )` | tests pairwise equality for all `i+1 = j` using `isequal` |
| `_NE`/`_NEE`/`_NEN`/`_NEEN( x1, ..., xn )` | tests pairwise for non equality for all `i ~= j` |
| `_LT`/`_LE`/`_EE`/`_GE`/`_GT`/`NN( x1, ..., xn )`   | tests `xi` `<`/`<=`/`==`/`>=`/`>`/`~=` `x(i+1)` |
| `_ALMOST_EQ( x1, ..., xn )`  | tests whether values are pairwise at most 4 ULP's different |
| `_ALMOST_NE`/`_DOUBLE_NE`/`_FLOAT_NE`/`_SINGLE_NE( x1, ..., xn )`  | like above, but tests for non equality |
| `_NEAR`/`_NNEAR( x1, ..., xn, bd )`          | tests whether values are pairwise near to each other, respectively not |
| `_RANGE( x1, ..., xn, range )`               | tests whether values are in the range defined by `range` (including endpoints) |
| `_PLUSZERO`/`_MINUSZERO( x1, ..., xn )` | tests whether the values `xi` are `+0` or `-0` |
| `_OUTPUT( h, x1, ..., xn )` | tests whether the function `h` produces results equal to `x1` to `xn`


<!--EQ-->
**`_EQ`/`_EEQ`/`_NE`/`_NEE`/`_EQN`/`_EEQN`/`_NEN`/`_NEEN`** These tests check whether all given arguments are equal or pairwise unequal. The check is made via `isequal`. The versions `_EEQ` and `_NEE` also check underlying properties, this is whether the data is complex valued and the `size`, `sparsity` and `class` is checked (everything only when applicable).
Note that `_EQ( nan, nan )` returns false. If a NaN test is necessary, one can use the matchers `NanSensitiveEq` or `DoubleNanSensitiveEq`. The versions with trailing `___N` test using `isequaln`.
Note that, only the `_NE` and `_NEE` tests check the arguments pairwise. The `_EQ` and `_EEQ` tests assume that "equality" is a transitive relation.
There are some aliases: `NEQ` = `NE`, `NEEQ` = `NEE`, `NEQN` = `NEN`, `NEEQN` = `NEEN`

<!--LT-->
**`_LT`/`_LE`/`_EE`/`_GE`/`_GT`/`_NN`** These tests check whether the given arguments are less-or-equal, less-than, greater-or-equal, greater-than, unequal, respectively, using `<`, `<=`,`==`, `>=`, `>`. E.g. the this test succeeds `EXPECT_LE( 1, 2, 2, 4 )`, whereas this test fails `EXPECT_LE( 1, 2, 2, 1 )`.  Therefore, the arguments must be comparable using these operators. If the result of a comparison is not a scalar, `all` is applied to the result (except for the experimental`NN` test where `any` is used.
Furthermore, these tests do not check the type of the arguments. E.g. this test succeeds `EXPECT_LE( 2, sym(3) )`.
*(experimental)* The macros `EE` and `NN` are experimental.

<!--NEAR-->
**`_NEAR`/`_ALMOST_EQ`** These tests check whether a list of arguments are pairwise numerically near to each other. For the tests `_NEAR`, the maximum difference `maxdiff` must be given as the last argument, e.g. this test fails `EXPECT_NEQ( 2, 3, 0.1 )`. For the tests `_ALMOST_EQ`/`_DOUBLE_EQ`/`_SINGLE_EQ`/`_FLOAT_EQ` , the maximum difference `maxdiff` for two elements `x1` and `x2` is four 4 ULPs. More precisely,
```matlab
ei = eps( cast(norm(xi,inf),class(x1)) );
ej = eps( cast(norm(xj,inf),class(x1)) );
maxdiff = 4 * max( ei, ej  );
```	
Finally, two elements `xi`and `xj` are near to each other if
```matlab
d = xi - xj;
norm( d(:), inf ) < maxdiff
```
Thus, in order to use this test, the arguments must work together with the operators `eps`,`cast`,`minus`, `norm(_,inf)` and `(:)`.
Older versions of `TTEST` had the functions `_DOUBLE_EQ`/ `_SINGLE_EQ`/`_FLOAT_EQ` which were removed in v2.2024.09.01.

<!--OUTPUT-->
**`_OUTPUT( h, x1, ..., xn )`** This test takes a function handle or evalable string `h` and executes it, demaing `n` output arguments. The results are then compared for equality to the arguments `x1` to `xn`.

### String tests
| Name                                     | Short explanatation |
| ---------------------------------------- | --------------------|
| `_STREQ`/`STR_EQ`/`_STRNE`/`STR_NEQ( x1, ..., xn )`   | tests whether strings `xi` are equal. The is no distinction made between char arrays (e.g. `'text'`) and strings (e.g. `"text"`) |
| `_STRCASEEQ/_STRCASENE( x1, ..., xn )` | similar to `_STREQ/_STRNE`, but ignores the case of the arguments |
| `_STRLEEQ/_STRLENE( x1, ..., xn )`     | similar to `_STREQ/_STRNE`, but treats Windows and Unix line endings as equal |
| `_STRCONTAINS( x1, ..., xn )`          | *(experimental)* tests whether `xi+1` is a substring of `xi` |

*Note that strings are only supported (in an at least half sensible way) from Matlab R2018a onwards. Furthermore, Octave and Matlab treat strings differently. Thus, it is best to avoid strings completely.*
<!--STR-->
**`_STREQ`/`_STRNE`/`_STRCASEEQ`/`_STRCASENE`/`_STRLEEQ`/`_STRLENE`** These tests check whether the arguments are character arrays or strings and whether they are equal or unequal using `strcmp` or  `strcmpi`. The two latter macros convert line endings to Unix style (`\n`) prior comparison. The macros with `CASE` in their name do not consider the case of the string, e.g. this test fails `EXPECT_STREQ( 'a', 'A' )`, whereas this test succeeds `EXPECT_STRCASEEQ( 'a', 'A' )`. The macros do not distinguish between strings and character-arrays, e.g. this test succeeds: `EXPECT_STREQ( 'a', "a" )`. If one wants to distinguish between strings and chars, one can use an `EXPECT_EQ` test instead or test the type of the arguments explicitly using an `_ISA` test.

If a function handle is given, then the function evaluated and the (captured) text output is used for the test. E.g. the following test succeeds `EXPECT_STREQ( @() fprintf('OK'), 'OK' )`.

### File tests
| Name                      | Short explanatation |
| ------------------------- | --------------------|
| `_FILE_EQ( x1, ..., xn )` | *(experimental)* tests whether the files `xi` are equal | 

The behaviour of this function will change in future releases.

### Image tests
| Name                        | Short explanatation                     |
| --------------------------- | --------------------------------------- |
| `_IMG_EQ`                   | Checks whether images are equal         |

<!--IMG_EQ-->
**`_IMG_EQ( ... )`** *(experimental)* takes as arguments: filename, figure handle, function handle, numeric array

### Special tests

| Name                                     | Short explanatation |
| ---------------------------------------- | --------------------|
| `_SUBSET`/`_SUPERSET( x1, ..., xn )`     | tests whether `xi` is a subset/superset of `x(i+1)`, where `xi` is an array or a cell array | 
| `_NUMEL`/`_PRODOFSIZE( x1, ..., xn, N )` | tests whether all `xi` have `N` elements |
| `_ISEMPTY`/`_EMPTY( x1, ..., xn )`       | tests whether all `xi` are empty |
| `_LENGTH( x1, ..., xn, L )`              | tests whether the length of all `xi` is equal to `L` |
| `_SIZE( x1, ..., xn, S )`                | tests whether the size of all `xi` is equal to the vector `S`  |
| `_FIELD( x1, ..., xn, F1,..., Fm )`      | tests whether all structs `xi` have the fields `F1` to `Fm` |
| `_SCALAR( x1, ..., xn )`                 | tests whether all `xi` are a scalar |
| `_ISA`/`_CLASS( x1, ..., C )`            | tests whether all `xi` are of class `C` |
| `_TYPEEQ`/`EQTYPE`/`TYPE_EQ`( x1, ..., xn )`                   | tests whether all `xi` are of the same type |                       
| `_HANDLEEQ`/`_HANDLE_EQ( x1, ..., xn )`  | tests whether all `xi` are the same handle instance |
| `_EXIST( id1, ..., idn, thing)`          | *(experimental)* tests whether identifiers 'idi' exist and are `thing` as defined for `exist` |
| `_ANYOF( x, {val1,...,valn} )`          | *(experimental)* tests whether x isequal to any of `vali` |

<!--FIELD-->
**`_FIELD( x1, ..., xn, F1,..., Fm )`** tests whether all `xi` are structs and whether all `xi` have the fields with names `F1` to `Fm`. It is possible not to give any `Fi` in which case it is only checked whether all `xi` are structs. For example the following test succeeds
```matlab
st.a = 1;
st.b = 2;
EXPECT_FIELD( st, 'a', 'b' );
```

<!--SUBSET-->
**`_SUBSET`/`_SUPERSET`** tests whether a set is a subset/superset of  another set. These tests also work for cell array. For example, the following test succeeds 
```matlab
EXPECT_SUBSET( {[1]}, {[1],'ab'}, {[1],[1 2],'ab'} )
```

<!--TYPE-->
**`_TYPE`** tests whether all `xi` are of the same type, i.e. have the same size, data-type, sparsity, complex-valuedity

<!--EXIST-->
**`_EXIST( id1, ..., idn, thing)`** *(experimental)* tests the type of an identifier. The identifier has to be given as a string, the type has to be given as a number. The following numbers are possible:
| Thing           | Meaning                                   |
| --------------- | ----------------------------------------- |
|   -1, 'any'     | identifier can by anything                |
|    0, 'no'      | identifier does not exist                 |
|    1, 'var'     | identifier is a variable in the workspace |
|    2, 'file'    | identifier is a file or a directory       |
|    5, 'builtin' | identifier is a built-in function but not a class |
|    7, 'dir'     | identifier is a directory                 |
|    8, 'class'   | identifier is a class                     |

For example, the following tests succeed
```
a = 1;
EXPECT_EXIST( 'a', 1 );
EXPECT_EXIST( 'b', 0 );
```

### Death tests (Exception tests)
| Name                                     | Short explanatation |
| ---------------------------------------- | --------------------|
| `_THROW( cmd, exceptions )`              | tests whether `cmd` throws the warnings or errors defined by `exceptions` |
| `_NTHROW/_NO_THROW( cmd, exceptions  )`  | tests whether `cmd` throws no warning nor error, except those defined by `exceptions` |

<!--THROW-->
**`_NO_THROW`/`_NTHROW`** These functions test the absence or existence of warnings and errors during execution of a command `cmd`. For this to work, the command has to be passed as a string, e.g. 
```matlab
EXPECT_THROW( 'inv(0)', 'MATLAB:singularMatrix' )
```
or as a function handle taking no arguments
```matlab
EXPECT_THROW( @() inv(0), 'MATLAB:singularMatrix' )
```        
If the command `cmd` is given as a string, it is executed via `evalin`, i.e. it is executed in the callers workspace and thus has access to the callers workspace variables and is allowed to create new variables. If the command `cmd` is given as a string, then the following is possible:
```matlab
EXPECT_THROW( 'a = inv(0);', 'MATLAB:singularMatrix' );
EXPECT_EQ( a, inf );
```
If the command is given as a function handle `@() cmd`, it is executed as `cmd();'`. In particular, "string-calls" and "function handle calls" may have different namespace scope.

A **`_THROW`** test succeeds if all of the defined warnings/errors are thrown, but no others (See below how to specify multiple exceptions). The restriction that the expected exception(s) must be defined is made to avoid false positives. For example, for the evalable string `{error('test:err')}` one would usually expect that an error with id `'test:err'`is thrown. This is wrong! Instead an error with id `MATLAB:maxlhs` is thrown:
```matlab
>> {error('test:err')}
Error using error
  Too many output arguments.'
```
Thus, if we would not check the thrown error (as it is done in most unit test frameworks), the following test would succeed, although it must fail
```matlab
EXPECT_THROW( '{error(''test:err'')' );  % string form
EXPECT_THROW( @() {error('test:err') );  % function handle form
```

An **`_NTHROW`**  test succeeds when no warning/errors *or only those* warnings/errors are thrown which are specified as extra arguments. For example, this test succeeds `EXPECT_NTHROW( '2+2' );`, as does this test `EXPECT_NTHROW( 'inv(0)', 'MATLAB:singularMatrix' );`

#### How to specify exceptions
The arguments `exceptions` define the mandatory or expected warnings/errors. One can specify 
1. Line numbers where exceptions are thrown
2. Exceptions ids,
3. Exception messages
4. Function names where exceptions are thrown
5. File names where exceptions are thrown

In general, one can just pass the thing one wants to test and it is not necessary to specify which of the above is given, since the `_THROW` and `_NTHROW` macros identify the arguments. For example the following tests whether an exception is thrown in file `calculus.m`, function `rsqrt`, has id `id:xx` and is thrown between line 10 and 100
```matlab
EXPECT_THROW( 'rsqrt(1)', 'calculus.m', 'rsqrt', 'id:xx', 10:100 );
```

Identification of the arguments use the following rules:
1. Every integer or integer-vector is considered to represent line numbers, the integers must be greater equal `-1`. The value of `-1` is sometimes reported for function handles, but it seems that this changes rapidly with each Matlab Release.
2.  Every string of the following format is considered to be an exception id:
    - which only consists of parts, each of which is either empty or starts with a letter and only consisting of letters, numbers, underscore, and dash, where the parts are divided by `:` or
    - starting with `'ttest_id'` (where `'ttest_id'` is stripped of from the string)
3. Every string of the following format is considered to be a file name:
    - ending on `'.m'`, `'.p'`, `'/'`, `'\'` or 
    - starting with `'/'`, `'~/'`, `'\'`, `'A:\'`, ..., `'Z:\'`, or
    - starting with `'ttest_file'` (where `'ttest_file'` is stripped from the string)
4. Every string of the following format is considered to be a function name:
    - beginning with an alphabetic character, and containing only letters, numbers, or underscores, or
    - starting with `'ttest_fun'` (where `'ttest_fun'` is stripped from the string)
5. Every string of the following format is considered to be an exception message:
    - not in a format above, or
    - starting with `'ttest_msg'`  (where `'ttest_msg'` is stripped from the string)

For example, the following test succeeds, since it expects a warning/error of id `'test:err'` or `'test:err2'` thrown at one of the lines `0,5,6,7,8,9,10`.
```matlab
EXPECT_NTHROW( @() error('test:err','some msg'), ...
    'test:err','test:err2', 0,[5:10] );
```
The parsing rules are quite straight-forward and not surprising, except for the following:
1. The string `'Message'` will be identified as a *function name*  and not as a message. To change  this behaviour, either prepend `'ttest_msg'` or add a space `' '` (if admissible), thus write `'ttest_msgMessage'` or `'Message '`.*[0]*
2. If a function is called via a function handle, the function name then is the *string of the function handle*. For example, the following test succeeds: 
``` matlab
EXPECT_NTHROW( @() error('some msg'), '@()error(''some msg'')' )
``` 

*[0]* We admit that this is not the most beautiful solution of this ambiguity problem and are working on a better solution. 

A thrown exception is matched with a specified  exception
if at least one of the following matches, and none of the following does not match (when given):
1. one line number
2. one exception id, or part of an id, where the comparison is case-insensitive and all `'-'` characters are ignored
3. one thrown message, or part of a message
4. one function name
5. one file name, or part of a file name

Two use cases for this would be the following:  If one writes a test suite for Octave and Matlab, the thrown exceptions will have different ids. By specifying both of them, the test suite is portable. For example, the following test succeeds on Matlab and Octave
```matlab
EXPECT_THROW( @() inv(0), 'Octave:singular-matrix', MATLAB:singularMatrix' )
``` 
Respectively, maybe the thrown exceptions will have different ids, but have the same part after the `:` character. By specifying only the latter part, the test suite is portable. For example, the following test succeeds
```matlab
EXPECT_THROW( @() warning('pre:id'), ':id' )
```
#### Catching multiple exceptions *(experimental)*
Using the vanilla Matlab-`warning` it is not possible to check for multiple exceptions, since Matlab only stores the last thrown warning, For example, the following test succeeds although two warnings are thrown and we only expect the second one. Thus, actually this test should fail:
```matlab
EXPECT_THROW( ...
    @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, ...
       'test:id2' );
```
To overcome this problem (and to remedy the need to distinguish between *Warnings* and *Errors* which is not so easy in Matlab), the *TTESTs* come shipped with an overloaded `warning`  function.
Note though that,
- it is not necessary to use the *TTEST*-`warning`, but then some features are not available to the user and
- some Matlab functions do not call `warning` when generating a warning, seemingly mostly built ins, in which case a `_THROW` test may yield not the desired result.

To specify multiple exceptions, one just puts all exceptions as an argument.
For example, this test succeeds using the *TTEST* warning function.
```matlab
EXPECT_THROW( ...
    @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, ...
       'm', 'test:id2', 'o', 'test:id1' ) );
```

If one needs to further specify the exceptions one can put the identifier `mandatory`/`m` or `optional`/`o` in front of the exception specification. A test succeeds, if all of the mandatory exceptions are thrown, and no exceptions are thrown which are either mandatory or optional. For example the following test succeeds with the *TTEST*-warning function:
```matlab
EXPECT_THROW( ...
    @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, ...
       'm', 'test:id2', 'o', 'test:id1' ) );
```

***(experimental)*** Currently the overload of `warning` is added to the Matlab path, and thus, always active. This seems to be a good idea and so far makes no problems at all. Nevertheless, we may change this in a future release such that the overload is only active when the test suite is executed via the `runttests` test runner.

#### How to specify number of output arguments
If the function under test behaves differently when called with a different number of output arguments, one can use the following strategies to test it
- Specify additional output arguments (see below): E.g.  `[ret,x1,x2] = EXPECT_NTHROW( @() function )` tests whether the call `[x1,x2] = function()` does not throw. The variable `ret` contains the outcome of the test, i.e. `true` or `false`. If one is not interested in the values `x1`, `x2` one can replace them with a tilde `~`.
- Use the string-form: E.g. `EXPECT_NTHROW( '[x1,x2] = function() );` .

For example, the following tests succeed
```matlab
[ret,~,~] = EXPECT_THROW( @() sum(1), ':maxlhs' );  % function handle form
EXPECT_THROW( '[~,~] = sum(1)', ':maxlhs' );  % string form
```

#### Notes:
- **Octave Compatibility:** Octaves rules for exception ids are less strict than Matlabs. If it is necessary to specify an Octave Exception id, one may prefix the id with the string `ttest_id`. E.g. the id `'Octave:singular-matrix'` can be specified as `'ttest_idOctave:singular-matrix'`.
- **Side effects** If an expected exception is thrown in a death test (i.e. in a `_NTHROW` or `_THROW` test), then the state of `lastwarn` and `lasterr` is not altered.



### Performance tests *(experimental)* *(Matlab only)* 
| Name                 | Short explanatation |
| -------------------- | --------------------|
| `_MAXTIME( cmd, t )` | tests whether `cmd` terminates faster than in `t` seconds. |
| `_MINTIME( cmd, t )` | tests asynchronously whether `cmd` does not terminate in `t` seconds. |
These tests are experimental and Matlab only, since they use the parallel toolbox of Matlab which has no counterpart in Octave so far.

<!--MAXTIME-->
**`_MAXTIME( cmd, t )`** *(experimental)* *(Matlab only)* Tests whether `cmd`, given as a string or a function handle, returns faster than `t` seconds. This test macro can be used for functions which may not terminate in case of present bugs. 
This test macro starts a parallel pool and runs the command on one worker. After `t` seconds the test is aborted. If `cmd` throws an error before the time runs out, the test is considered to be successful. When the `cmd` could not be started, the test counts as failed, e.g. `EXPECT_MAXTIME( @asdasf, 1 )` fails.
Note: Currently this macro is blocking, but in a future release we change it to a non-blocking macro (like in `_MINTIME`) when the timeout `t` is large.

<!--MINTIME-->
**`_MINTIME( cmd, t )`** *(experimental)* *(Matlab only)* Tests whether `cmd`, given as a string or a function handle, does not return in less than `t` seconds. The command `cmd` is executed via a `timer`, thus this macro does not block. As a consequence, this macro always returns `true` and the outcome of the test is only printed to the screen later on.

### Flow tests
| Name                        | Short explanatation                     |
| --------------------------- | --------------------------------------- |
| `_FLOW`                     | Checks the control flow of a function   |

<!--FLOW-->
**`_FLOW`** This macro checks the control flow of program and works together with the function `flowat`. It is discussed in Section *Legacy Code/Code injection/flow_at*

### Other tests
| Name                        | Short explanatation                     |
| --------------------------- | --------------------------------------- |
| `_TOOLBOX`                  | Checks whether a toolbox is available |
| `_IMG_EQ`                   | (*experimental*) Checks whether images are equal |

<!--TOOLBOX-->
**`_TOOLBOX( ... )`** checks whether a toolbox is available, i.e. installed, licenced (for Matlab) and working. For Matlab this means, the toolbox is checked out, for Octave this means the package is loaded when this test is executed. As arguments can either be given a shorthand (*control, gpu, gurobi, intlab, jsrlouvain, ltfat, matlab, octave, parallel, sedumi, signal, symbolic, ttest, ttoolboxes, versoft*), or the name which would need to be passed to `ver` to check for the availability of a toolbox.


### Matcher tests

| Name                        | Short explanatation |
| --------------------------- | --------------------|
| `_THAT( x, matcher )`       | tests whether `x` satisfies a `matcher`. See below for informations about matchers |

<!--THAT-->
**`_THAT( ... )`** Matcher tests take an argument an a matcher, and check whether the argument matches the matcher. They are a popular way of writing unit tests where the code somewhat reads like English language *[0]*. We give an instructive example how to use them (The test executed as printed will mostly likely fail, because all matchers are in the namespace `ttest` *[1]*).
```matlab
>> EXPECT_THAT( 3, AllOf(Ge(0),Le(2)) );
Argument does not fulfil matcher, but it should, where
matcher = greater equal 0  and less equal 2 
Argument 1 = 3
```    
In principle, all `_THAT` tests with matchers can be constructed using  a `_PRED` test and an anonymous function, e.g. the above test is equivalent to 
```matlab
>> EXPECT_PRED( @(x) x>=0 && x<=2, 3 )
Value does not fulfil predicate, but it should, where
Predicate = @(x) x>=0 && x<=2
Value 1 = 3
```
But, this test does not produce such a nice error description in words.  Although, one can even mix these two styles  and write
```matlab
>> EXPECT_PRED( AllOf(Ge(0),Le(2)), 3 );
Value does not fulfil predicate, but it should, where
Predicate = greater equal 0  and less equal 2

Value 1 = 3
```

By convention, Matchers start with an upper-case letter and belong to the namespace `tt.*`. In the following list of implemented matchers, we will denote the free  argument with `x`. In the column **Example** we present a `_THAT` test which succeeds.

*[0]* Our implementation of matchers does not read that fluent due to restrictions of the Matlab language: A returned object may not directly used further. E.g. the following is impossible in Matlab `[0 1 2 3](2)`, one has instead to write `x=[0 1 2 3]; x=x(2);`. In languages where such constructs are possible, fluent interfaces are much easier to write. See for example *Fluent Assertions* for Java where this test is written as as `Number.Should().BeGreaterOrEqualTo(0).And().BeLessOrEqualTo(2);`.

*[1]* Namespaces are a way to logically group functions. Unfortunately, Matlabs implementation of namespaces is mediocre and Octaves support even more. In order to execute a function `func` residing in a namespace `ns` one has the following options
-  Prepend the function name with `ns.`. In our example: `ns.func()`, respectively the examples this means to write 
    ```matlab
    EXPECT_PRED( tt.AllOf(tt.Ge(0),tt.Le(2)), 3 );
    ```
    This way, the code can get easily very verbose. Unfortunately, in Octave this is the only way.
- Import the used functions into the global namespace using the command `import` plus the full function name. In our example: `import ns.func`. Afterwards the function `func` can be called with its name `func` directly. For the example with the matchers this means writing
    ```matlab
    import tt.Ge
    import tt.Le
    import tt.Allof
    EXPECT_PRED( AllOf(Ge(0),Le(2)), 3 );
    ```
- Import all functions from the namespace into the global namespace using the command `import` plus the namespace name and an asterisk. In our example: `import ns.*`. Afterwards all functions in the namespace `ns` can be called with their name directly. For the example with the matchers this means writing
    ```matlab
    import tt.*
    EXPECT_PRED( AllOf(Ge(0),Le(2)), 3 );
    ```


## Matchers
### Generic comparison matchers
|              Matcher                 |            Description           |   Example  |
| ------------------------------------ | -------------------------------- | ---------- |
| `IsTruthy`/`IsFalsy()`               | tests whether `x` implicitly evaluates to `true`/`false`          | `_THAT( [0 1 5], IsFalsy() )` |
| `IsEq( a )`                          | returns `isequal( x, a )`         | `_THAT( {0}, IsEq({0}) )` |
| `Ne`/`Eq`/`Ge`/`Gt`/`Le`/`Lt( a )`   | tests `x ~=`/`==`/`>=`/`>`/`<=`/`< a` |  `_THAT( 2, Ge(0) )` |
| `Not`/`IsFalse()`                    | returns `~logical( x )`          | `_THAT( false, IsFalse() )` |
| `IsTrue()`                           | returns `logical( x )`           | `_THAT( ~false, IsTrue )` |
| `Id()`                               | returns `x`                      | `_THAT( 10, Ge(5,Id) )` |
| `NanSensitiveEq( a )`               | returns `isequaln( x, a )`       | `_THAT( [nan],NanSensitiveEq([nan]) )` | 
| `DoubleEq`/`FloatEq`/`SingleEq( a )` | test whether `x` is double/single-equal to `a` |  `_THAT( 1, DoubleEq(1+eps) )` |
| `NanSensitiveDoubleEq`/ `...( a )`   |  test whether `x` is double/single-equal to `a`, treating two `nan`s as equal |  `_THAT( [3 nan], Each(NanSensitiveDoubleEq([3 nan])) )` |
| `Near( a, bd )`                      | tests whether the difference between `x` and `a` is less than `bd` |  `_THAT( 8, Near(10,3) )` |
| `NanSensitiveNear( a, bd )`          | like `Near`, treating two `nan`s as equal |   `_THAT( nan, NanSensitiveNear(nan,3) )` |


Note that there is a subtle difference in between the behaviour of the `Eq` matcher and the `_EQ` test macro. The `_EQ` test macro uses `isequal` for comparison, whereas the `Eq` matcher uses `==`. Thus,
these tests are equivalent,
```matlab
EXPECT_EQ( [2 3], [2 3] )
EXPECT_THAT( [2 3], IsEq([2 3]) )
EXPECT_THAT( [2 3], Each(Eq([2 3])) )
```    
whereas the test
```matlab
EXPECT_THAT( [2 3], Eq([2 3]) )
```     
throws a warning, because `Eq([2 3])([2 3])`  (pseudocode) does not evaluate to a scalar.

### Number matchers
Most of these matchers are self-explanatory, so we omit a description.
|              Matcher         |         Description        |   Example  |
| ---------------------------- | -------------------------- | ---------- |
| `IsIntegral()`               | tests whether argument has an integral value (not type) | `_THAT( [0 .5], IsIntegral() )` |
| `IsReal()`                   |  |
| `IsNumeric()`                |  |
| `IsFloat()`                  |  |
| `IsPositive()`               |  |
| `IsNonnegative`/`IsNonNegative()`   |  |
| `IsZero()`                   |  |
| `IsNonpositive`/`IsNonPositive()`   |  |
| `IsNegative()`               |  |
| `IsFinite()`                 |  |
| `IsNan()`                    | tests `isnan( x )` |  `_THAT( [1 2], Not(Each(IsNan)) )` |
| `IsInf`/`IsInfinity`/`IsInfinite()` |  |
| `IsPrime()`                  |  tests whether argument is a prime number |
| `IsInteger()`                |  (*experimental*) tests whether argument has integer type | |

### String matchers

The following matchers operate both on character arrays and strings.

|       Matcher          | Description                        |  Example |
| ---------------------- | ---------------------------------- | -------- |
| `HasSubstr( a )`       | tests  whether `x` has a substring `a`                   | `_THAT( 'tommsch', HasSubstr('omm') )` |
| `MatchesRegex( a )`    | tests  whether `x` matches the regular expression `a`    |
| `Regex( a )`           | finds where  `x` matches the regular expression `a`    |
| `StartsWith`/`BeginsWith( a )` | tests  whether `x` starts with a substring `a`           | `_THAT( 'tommsch', StartsWith('tomm') )` |
| `EndsWith( a )`        | tests  whether `x` ends with a substring `a`             | `_THAT( 'tommsch', EndsWith('sch') )` |
| `StrEq`/`StrNe( str )` | tests for string equality/inequality, | `_THAT( 'tommsch', StrNe('apple') )` |
| `StrCaseEq`/`StrCaseNe( str )` | tests for string equality/inequality, ignoring case | `_THAT( 'tommsch'], StrCaseEq('TOMMSCH') )` |

### Container matchers

|       Matcher                 | Description                       | Example |
| ----------------------------- | --------------------------------- | ------- |
| `IsEmpty()`                   | tests `isempty( x )`              | `_THAT( [], IsEmpty() )` |
| `NotEmpty()`                  | tests `~isempty( x )`             |  `_THAT( [1 2], NotEmpty() )` |
|  `Each`/`All_()`              | evaluates `all( x(:) )`           | `_THAT( [1 2 3], Each([1 2 3]) )` |
|  `Contains`/`Any_()`          | evaluates `any( x(:) )`           | `_THAT( [1 2 3], Contains(2) )` |
|  `IsSubsetOf`/`SubsetOf( a )` | tests if `x` is a subset of `a`   | `_THAT( [1 2 3], IsSubsetOf([1 2 3 4]) )` |
|  `IsSupersetOf`/`SupersetOf( a )`   | tests if `x` is a superset of `a`  | `_THAT( [1 2 3], IsSupersetOf([1 2]) )`          |
|  `NumelIs( a )`               | evaluates `numel( x )` | `_THAT( [1 2 3], NumelIs(3) )` |
|  `NnzIs( a )`                 | evaluates `nnz( x )` | `_THAT( [1 0 3], NnzIs(2) )` |
|  `WhenSorted( a )`            | evaluates `sort( x )`, `x` must be an array  | `_THAT( [3 1 2], WhenSorted([1 2 3]) )` |

**`Each`/`All_`/`Contains`/`Any`** The functions `Each` and `Contains` are not named `All` and `Any`, since Matlab does not  necessarilly distinguish between case when looking up for a function, even though the documentation tells otherwise. Thus, a function `All` could shadow Matlabs function `all`.  If one does not like these names, one can use the aliases `All_` and `Any_`*[0]*. 

*[0]* To further remedy this danger,  all Matchers are in the namespace `tt.*`.  The test macros `TODO_`, `EXPECT_` and `ASSERT_` are not in a namespace because it is unlikely that the latter shadow any functions. 

### Special purpose matchers

|       Matcher             | Description                        |  Example  |
| ------------------------- | ---------------------------------- | --------- |
|  `Unary( a )`             | evaluates `a(x)`, where `a` is a unary function          | `_THAT( [1 2 3], Unary(@(y) numel(y)==3) )` |
|  `AllOf( a1, ..., an )`   | evaluates `a1(x) & ... & an(x)`    | `_THAT( [2 3], Each(AllOf(Ge(2),Le(3))) )` |
|  `AnyOf( a1, ..., an )`   | evaluates `a1(x) | ... | an(x)`    | `_THAT( [2 3], Each(AnyOf(Ge(3),Le(2))) )` |
|  `NoneOf( a1, ..., an )`  | evaluates `~(a1(x) | ... | an(x))` | `_THAT( 2, NoneOf(1,5) )` |
|  `Compose( a1, ..., an )` | evaluates `a1(a2(...(an(x))...))`  | `_THAT( [2 3], Each(AnyOf(Ge(3),Le(2))) )` |
|  `Thread( m )`            | yields a matcher taking arbitrary number of arguments and applies the matcher `m` to all arguments | `_THAT( [1 2], [], Contains(Thread(IsEmpty())) )` |
|  `Description( m, txt )`  | replaces the description of the matcher `m` by  `txt`. | `_THAT( 2, Description(3,'The number is three.') )` |
|  `IsClass`/`Class( c )`  | checks wether `x` is of type `c` |

### Comparison matchers (experimentally)
The matchers `AllOf`, `AnyOf` and `NoneOf` can be used in comparisons in the following way
```matlab
2 >= AnyOf( 1, 3 )
tt.AllOf( 1, 3, 5 ) <= tt.AnyOf( 0, 6 )
EXPECT_EQ( 2, AnyOf(2,3) )
```

### Binary matchers (experimentally)
Those matchers are not meant to be used in `_THAT` tests, but in `_NEAR` tests as last argument.
|       Matcher             | Description                               |  Example  |
| ------------------------- | ----------------------------------------- | --------- |
|  `AbsoluteError( e )`             | returns `@(x,y) abs(x-y)<e`    | `_NEAR( 1, 1.01, AbsoluteError(.1) )` |

The following is unfortunately not possible at the moment
```matlab
% EXPECT_NEAR( 1, AnyOf(1.1,2), AbsoluteError(.2) ); % Not possible
```
### User defined matchers

|       Matcher           | Description                                  |  Example |
| ----------------------- | -------------------------------------------- | -------- |
|  `Matcher( 'name', h )` | Generates a matcher `h(x)` with name `name`  | `Matcher( 'Odd', @(x) mod(x,2) );` |
|  `name = Matcher( h )`  | Generates a matcher `h(x)` with name `name`  | `Odd = Matcher( @(x) mod(x,2) );`  |

Note that the form `Matcher( 'name', h )` uses `assignin`, and thus cannot be used in static workspace. In static workspaces thus one has to use `name = Matcher( h )`.



### Output arguments
The first output of a test macro is always a logical indicating whether a test failed or succeeded *[0]*.  The latter outputs are the evaluated input arguments. E.g. `[ret,x1,x2] = EXPECT_EQ( 2, sum([1 1]) );` yields `ret = false; x1 = 2; x2 = 2;`.  

Defining superfluous output arguments raises an error, except for the special cases described below.

*[0]* Since TODO_` tests always return `true`, the first output argument of a `TODO_` test is always `true`.

**Special cases:**
- `[ret,a1,...,an,d] = _ALMOST_EQ( x1, ..., xn )` returns as `n+2nd` output the maximal delta used for comparison (and similar for the `_ALMOST_NE` macro). 
For example `[ret,x1,x2,de] = EXPECT_ALMOST_NE( 2, 3 )` typically yields `ret==true`, `x1==2`, `x2==3`, `de==1.7764e-15`.

- The `_THROW`/`_NTHROW`/`_MAXTIME` macros return the evaluated arguments.
For example 
    ```matlab
    [ret,x1] = EXPECT_NTHROW( @() sum([2 3]) );  % yields `ret==true, x1==5. 
    [ret,x1] = EXPECT_NTHROW( 'sum([2 3])' )
    ```
  If a death test fails, then the additional output arguments may not be assigned properly and default to `[]`. For example `[ret,a] = EXPECT_MAXTIME( @() eig(randn(100000)), 1 )` usually yields `ret==false`, `a==[]`.
- The experimental test macros `_FILE_EQ` may return the files contents with modified line endings and caps.
- The  experimental test macros `_MINTIME` test macros do not return additional values

### Test macro options
*(experimental)* A certain set of options can be passed to test macros using the `OPTIONS(__)` function. The option can be given at any position. Lower case macros do not except options.
|       Option         | Description                               |
| -------------------- | ----------------------------------------- | 
|  `'not'`             | Test result is inverted                   |
|  `'verbose',val`     | Partly overrides verbosity level          |
|  `'execute'`         | Evaluates function handles prior the test |

**`'not'`** For example, the following test succeeds:
```matlab
EXPECT_EQ( 2, 3, OPTION('not') );
```
**`'execute'`** (experimental) If this option is given, function handles and evalable strings are evaluated before the assertions are executed.
```matlab
EXPECT_EQ( @() 2, @() 2, OPTION('execute') );
```


## The three styles of assertions
Each test macro exists in three styles
1. Upper case macros, as defined in the section above. E.g.: `EXPECT_EQ`, `ASSERT_LE`
2. *(experimental)* Class methods macros. To use them, one has to define a test object via `ttest_test_c` beforehand. The macros are in lower case then. E.g.:
    ```
    obj = ttest_test_c(1);
    obj.expect_eq( 2, 3 );
    ```
3. Lower case macros. They all rely in the namespace `tt.` They are faster than the other two,have less dependencies (in fact, they should be stand-alone), but offer less features too. In particular, test result summary is not available with them.

## Legacy Code / Scientific code
A lot of software written at university, is not meant to be used again later in time and/or used by people other than the original programmer *[0]*. Thus, some reasons why unit testing is important, are not that important for scientific code. On the other hand, most unit test frameworks ignore the needs of scientific software and/or of scientific people and *[1]*
- mandate one specific style of programming,
- do not provide the correct tools.

The tools described in this section are meant to alleviate this problem. Most of them are of no use for well structured code. But, as we know and often observe, a lot of code is not well structured - even code which should be. May it be because it is code from before-unit-testing-times, so called *legacy code*, or code from non-programmers, or fast hacked code. Nevertheless, it is of high importance that the code does what it should do and compute the things that it should do.

Thus, these tools purpose lies in
- refactoring code
- debugging code
- testing badly written code

*[0]* Although, the latter argument is weak, because we implicitly write code for our *future-me*.
*[1]* This let some people even think that unit testing is not possible for scientific or non-object oriented code - which is an obvious nonsense.

###  Pre / Postconditions *(experimental)*

**Preconditions**
The pre-condition functions can be used to test pre-conditions of functions, i.e. invariants of the input values. These functions are similar to the test macros but do not support all features of the upper case macros. In particular, the following is not supported
- Custom error and success handling
- Changing severity level using `DISABLED`, etc...
- Gold standard tests using `CACHE` (see below)
- Property based testing using `GIVEN`

This is, because these functions shall be as self-contained as possible. Thus, features which would tie those functions tightly to the *TTEST* framework are removed.

**Postconditions **
*(experimental)* *(partially Matlab only)* The postcondition function on the other hand can be used to test postconditions, i.e. invariants which must hold when the function returns. For that, it captures the workspace at the moment the function `POSTCONDITION` is executed, so that input and output variables can be compared. We describe its use (and those of a precondition) with an example:
```matlab
function [ ret ] = sortbackwards( v );
    tt.assert_isa( v, 'numeric' );  % (*)
    POSTCONDITION( @(ret) numel(ret)==numel(v) );  % (**)
    v = sort( v );
    ret = v(end:-1:1);
```
In line `(*)` a pre-condition is tested. This test is equivalent with `assert( isa(v,'numeric')`, but using the lower-case macros it is easier to express the intent of the check, and, in case of failure the error message is more descriptive. Also, by using the precondition functions one-of errors are less likely to happen.
For the postcondition, in Line `(**)` the variable `v` is captured, but not the variable `ret`. When the function returns, the value of `ret` at return time is used in the check of the postcondition.

The `DISABLED` and `ENABLED` functions work also for post-conditions.


#### Important Notes
- Currently, only the `function_handle`-form for post-conditions work, and not a `string`-version. Due to restrictions of the Matlab language it is unlikely that a `string` version will be implemented.
- The `POSTCONDITION` function accesses variables which are already destroyed. The Matlab documentation is somehow vague in this aspect, but this behaviour is intrinsically dangerous. The function has been well tested and seems to work on Matlab, *but not on Octave*.
If the function does not work in a certain situation one can also use the output-style of `POSTCONDITION` in the following way: Assigning the `POSTCONDITION` call to a variable and manually clear it before return, e.g.:
    ```matlab
    function [ ret ] = sortbackwards( v );
        tt.assert_isa( v, 'numeric' );  % (*)
        pc = POSTCONDITION( @(ret) numel(ret)==numel(v) );  % (**)
        v = sort( v );
        ret = v(end:-1:1);
        clear pc
    ```
- `POSTCONDITION` uses `assignin`, and thus, the usual restrictions apply. 1) The version without output argument does not work in a static workspace. 2) If the version without output is used, then no variables starting with `ttest_` must be present in the callers workspace.

### Code injection via *at* functions
Similar to Matlabs *in* functions, `assignin` and `evalin`, the *TTESTs* bring some *at* functions, which can be used to inject code into `m`-files, namely:
| Name        | Description           |
| ----------- | --------------------- |
| `assignat`  | assigns a variable |
| `captureat` | captures the value of variables |
| `evalat`    | runs code |
| `flowat`    | checks the control flow of a program |
| `returnat`  | returns from the function |
| `inputat`   | pipes keyboard input to the next `input` command |
| `clearat`   | removes all injected code from a file |

#### General usage description
- The *at* functions accept options in the `name, value, value, ...` format. The first option-name for all `at` functions must be **`'in'`** (except `clearat`). If there would happen collisions between option-names and values, one can prefix the option name `in` with any prefix (e.g. `'--in'`), in which case all other option-names need to have the same prefix.

- All *at* functions (except `clearat`) accept the option **`'at'`** which specifies the position where code shall be injected. One can pass 
  - nothing: Code is injected on the first executable line
  - an integer or a vector of integers: Code is injected on the given line numbers  
  - a string or char array: Code is injected on every line which contains the given string 
  If the specified line is not executable, the code is injected on the next following executable line.


**`'assign'`, `'eval'`, `'input'`, `'capture'`, `'group'`, `'type'`** are special options for the various `at` functions and its usage is described below.

#### Detailed description
- **`assignat`** assigns or creates variables. Clearly, the latter may not work in static workspaces. The following example uses the function `eval2nd` in `/utilities/`, which just evaluates the second input named `second`.
    ```matlab
    assignat( 'in','eval2nd', 'assign', 'second',@() 10 );
    eval2nd( @()20,@()20 )  % yields 10
    clearat  % deletes all injected code
    ```

- **`captureat`** captures variables and workspaces. The variables to be captured are defined using the option `'capture'`. Multiple variables can be captured. When no variable is defined, the whole workspace is captured. To capture the same variable multiple times, the option `'group',name` can be used. To retrieve the captured variables, `captureat` has to be called without arguments. The following example uses the function `capture_function` in the folder `/unittest/at/`.
    ```matlab
    captureat( '-in','capture_function', '-at','<La20>', '-capture','a', '-group','La10' );
    captureat( '-in','capture_function', '-at','<La30>', '-capture','a', '-group','La20' );
    capture_function()
    ws = captureat();
    EXPECT_EQ( ws.La10.a, 10 );
    EXPECT_EQ( ws.La20.a, 20 );
    clearat in captureat  % deletes injected code `captureat`
    ```

- **`evalat`** runs the code, given by the options `'eval'` . The code can be given as either an evalable string or as a function handle. The following example uses again the function `eval2nd` in `/utility`.
    ```matlab
    evalat( 'in','eval2nd', 'eval','fprintf(''Hello World\n'')' );
    eval2nd( @()20,@()20 )  % prints out 'Hello World'
    clearat  % deletes injected code
    
    evalat( 'in','eval2nd', 'eval',@() fprintf('Hello World\n') );
    eval2nd( @()20,@()20 )  % prints out 'Hello World'
    clearat  % deletes injected code
    ```
  The following restrictions apply:
    - The string is evaluated using `evalin`, and thus, not all commands may succeed. 
    - The function handle must return at least one argument and must not throw an error.

- **`flowat`** records the control flow of a program. Flow points can be set and the recorded flow point can be checked using a `_FLOW` test macro. Three types of control points can be defined
  - `abort` If such a line is hit, the program is allowed to abort and no further control points are recorded.
  - `failure` If such a line is hit, the `_FLOW` test fails, the program is allowed to abort and no further control points are recorded.
  - `expect` If such a line is not hit, the `_FLOW` test fails.

  The following example uses the function `flowat_function` in `/unittest/at/`.
    ```matlab
    flowat( 'in','flowat_function', 'at','<START>', 'type','expect' );
    flowat( 'in','flowat_function', 'at','<1a>', 'type','failure' );
    EXPECT_FLOW( 'flowat_function(0)' );  % calls flowat_function()
    clearat all  % deletes all injected code
    ```

  If one wants to examine the recorded flow by hand, the function `flowat()` called with no arguments returns the record. Thus, the above example can also be written like this:
    ```matlab
    flowat( 'in','flowat_function', 'at','<START>', 'type','expect' );
    flowat( 'in','flowat_function', 'at','<1a>', 'type','failure' );
    flowat_function( 0 );
    fl = flowat();
    EXPECT_FLOW( fl );
    clearat all
    ```

  To record multiple flows at once, one can use the `'group'` option as for the `captureat` function.

- **`returnat`** is a convenience wrapper for `flowat` where the type defaults to `abort`. 

- **`inputat`**  generates keyboard input for the next `input` command, and thus, makes the `input` command non-blocking, i.e. a script will not halt when `input` is used. Therefore, by using `inputat` one can automatically unit test functions and scripts which would rely on user input. For this to work, *TTEST* comes with an overloaded `input` function which resides in the folder `/overload/input/`. The overloaded function behaves exactly like Matlabs `input` function.
One can choose between two different injection mechanisms, `local` and `global`, to be defined via `'type',type`.
  -   type: `'local'`/`'l'`/`'spatial'` (default): The next call to `input` occurring in the function which got injected will get input injected
  -   type: `'global'`/`'g'`/`'temporal'`: The next call to input (from anywhere) will get input injected. 
  -  When no function names are given (via option `'in'`), the type `'global'` is chosen unconditionally.
 
  The text to be injected is given via the `'input'` option.

  Multiple injections can be made, which are resolved in a FIFO manner, except that `global` injections are always chosen before `local` injections.

  The following example uses the function `inputat_function` in `/unittest/at/`.
    ```matlab 
    inputat( '-input','ABC' );  % no filename given, type is 'global'
    str = input('','s');  % yields: 'ABC'
    
    inputat( '-in','input_function', '-at','<0>', '-input','2+2', '-type','local' );
    input_function(false);  % yields: 4
    ``` 

  `local` injections can be removed via `clearat <functionname>`, whereas `global` injections may only be removed via `clearat <functionname>` followed by `clearat inputat`, respectively shorter `clearat all`, or even shorter via `clearat`.

  **Note:** If the overloaded `input`  function cannot be used for some reason, one may use the third party function `inputemu` In the `tt.*` namespace (i.e. its full name is `tt.inputemu`). This function emulates keypresses, but is hard to use and *depends on the currently selected keyboard scheme*.


- **`clearat`** removes injected code from a file. E.g.: `clearat in eval2nd` removes all injected code in the file `eval2nd.m`. The function `clearat` has additional calling syntaxes. Namely, one can omit in most cases the first, otherwise mandatory, argument `in`, note though, this calling style should not be used in functions or scripts. Additionally
  -  `clearat all`/`clearat` removes all injected code in any file and global injections by `inputat`
  - `clearat inputat` removes only global injections by `inputat`
  

## `CACHE` / Gold standard tests
`CACHE` maps a file to a variable in Matlab in a transparent way. Its interface make it perfectly suited for, so called, *gold standard unit tests*. These are tests, where the outcome of a function is compared with a saved value. Usually, they are used in the following situations:
- The output of the algorithm is not known exactly and one tests whether new versions of the algorithm still produce reasonable results. Such tests are usually called *regression tests*.
- The output of the function is highly complex (e.g. a picture) and cannot be stored directly in the source code.

Basic usage:
```matlab
EXPECT_NEAR( CACHE('rho.mat'), my_rho([1 2;1 2]), 1e-9 );
```
Here, the function `my_rho` computes the spectral radius of a matrix. If this test is executed the first time, a file with name `my_rho.mat` is generated in the current working directory with value `my_rho([1 2;1 2])`. In subsequent runs, the content of the file is compared with the result `my_rho([1 2;1 2])`. Thus, in order to use `CACHE`, one needs write permissions in the current working directory.

### Usage in test macros
Apart from instantiating a `CACHE` object as a single step, it can also be instantiated directly in a test macro. For example, the following two are equivalent:
```matlab
res = superdifficult( b )
cache = CACHE( res, 'superdifficult.mat' );
EXPECT_NEAR( cache.var, res );

% is equivalent to

EXPECT_NEAR( CACHE('superdifficult.mat'), superdifficult(b) );
```
In particular, the specification of `.var` to access the variables value is not necessary in a test macro.

The `CACHE` object takes the first argument of the test macro which is not a `CACHE` object to initialize its value. Thus, at most one uninitialized `CACHE` object can be used in a test macro. E.g. the following works
```matlab
CACHE( 1, 'name1' );
EXPECT_EQ( 1, CACHE('name1'), CACHE('name2') );
``` 
whereas, this throws an error
```matlab
EXPECT_EQ( 1, CACHE('name3'), CACHE('name4') );
```
### Using `CACHE` objects for file access
Changes to the value of a `CACHE` object directly map to the underlying file. Thus, it is possible to use it to store value on disk. Although, since a variable is easily deleted or changed unintentionally, we think such usage will lead to great frustration and should be avoided. Nevertheless, we give an example how this can be done
```matlab
x = CACHE( `xyz.mat` );  % generates cache object `x` and creates file `xyz.mat`
x.var = 10;  % sets value of x to 10, and writes it directly to the disk
clear x;  % clears variable x, does not delete the file `xyz.mat`
y = CACHE( `xyz.mat` );  % loads the file `xyz.mat`. Value of y is 10
```
If one wants to prohibit that changes to the variable are directly written to the disk, the option `private` shall be used. To later force writing, the function `savemat` must be used. Again, we give an explanatory example
```matlab
x = CACHE( 20, `xyz.mat`, 'private' );  
    % generates cache object `x`, creates file `xyz.mat` 
    % value of x is 20, value in file is 20
x.var = 10;   % value of x is 10, value in file is 20
x.savemat();  % value of x is 10, value in file is 10
```
**Note:** Currently, it is not possible to use a constructed cache object in another environment, i.e. across different Matlab/Octave versions or operating systems.

#### Construction of `CACHE` objects
There are roughly three possibilities to construct a `CACHE` object.
- `cache = CACHE( name, [options] );` maps the file `name` to the variable `cache`
  - `name` can be given as full/relative path and w/o file extension. If no file extension is given, `.mat` is used.
  - If `name` does not exist, it is created with the value `ttest_nfo('empty')`, which represents uninitialized stuff.
  - There must exist only one variable in the file and its name is of no significance.
- `cache = CACHE( value, 'name', [options] );` 
  - as above, but stores the value `value` in the file when the file does not exist yet.
- `cache = CACHE( _, 'varname',varname, [options] )` 
  - as above, but stores the variable with name `varname` in the `mat` file, regardless of its real name. It is recommended that `varname` always coincides with the real variable name.

Possible options are
  - `'verbose',value` defines the verbosity level, default: `1`. At verbosity level `0` only severe errors are printed/thrown. At verbosity level `-1` nothing is printed, but errors may still be thrown.
  - `'private'` described above

#### Member variables and methods
We briefly describe the (public) internals of a `CACHE` object.
- `cache.var` is the value of the variable
- `cache.opt.verbose` verbose level, can be changed after instantiating the `CACHE` object.
- `cache.opt.private` private flag, can be changed after instantiating the `CACHE` object.
- `cache.filename` full path to the underlying file
- `cache.varname` name of the variable in the file
- `cache.remove()` deletes (!) the corresponding file from the disk
- `cache.savemat()` saves the value of the underlying variable to disk. This is done automatically, except when the `CACHE` is opened with the `'private'` flag passed.

## Sections
Sections are another way to group unit tests and make unit tests mutually independent. On Octave this is the preferred way, since no sensible built in test runner is available. The idea  of sections is developed in *[0]*. Its advantage over xUnits approach of having test fixtures is that with sections the preparatory steps needed for a unit tests are near to the actual unit test.

*TTEST* offers three nested level of sections: `TESTCASE`, `SECTION` and `SUBSECTION`, the latter name being inspired by Latex. Note though that with *"section"* we refer to all three of them. Whenever a section ends, the state is set back as it was when this section was entered. 
With state we mean *[1]*
- the workspace, i.e. all variables currently in scope,
- the Matlab path,
- global workspace, i.e. all global variables,
- the state of the global random number generator and
- the working directory
- breakpoints set by *TTEST*

We give a small explanatory example (the comments describe the values of the variables at the various places)
```matlab
TTEST init  % create TTEST environment
a = 1;
TESTCASE;
    % a==1
    b = 3; 
    SECTION;
        % a==1, b==3
        b = 4; c = 4;
    SECTION;
        % a==1, b==3, c is undefined
TESTCASE;
    % a==1, b and c are undefined
```

It is also possible to end a section without opening a new one, by using the functions `ENDTESTCASE`, `ENDSECTION`, `ENDSUBSECTION`. We give again an explanatory example
```matlab
a = 1;
TESTCASE;
    % a==1;
    a = 2;
    b = 1;
ENDTESTCASE;
% a==1, b is undefined
```

**Notes:**
- All sections take a first positional string argument determining the name of the section. If a test macro in a section fails, the sections name is printed, e.g. `TESTCASE( 'sectionname' );`
- *(experimentally)* The option `'base',true` can be passed to a section, in which case also the base workspace is set back to its original state. 
- A call to `TTEST init` is not necessary, but helps to avoid some pitfalls. `TTEST init` creates a *TTEST* environment in which the sections are placed. If no environment is created, sections in different parts of a program may be mixed.
- In the examples only `TESTCASE` and `SECTION` is used, but the reader will clearly figure out the behaviour of `SUBSECTION` herself.
- It is allowed to directly start with a `SECTION` or `SUBSECTION`, and thus omitting a `TEST` and/or `SECTION`. Some people call this the *article-style*.
- Closing a section before opening one yields an error.


*[0]* [Catch2, Unit Test Framework for C++.](https://github.com/catchorg/Catch2/blob/devel/docs/test-cases-and-sections.md) Note though that, *TTESTs* realisation of this idea is slightly different from Catch2`s.
*[1]* The behaviour when the Matlab path is restored when using *TTEST* sections differs from the behaviour when Matlab resets the Matlab path for unit tests written in Matlabs unit test framework. For the latter, Matlab only resets the path when the whole test suite ends, but may not reset the path in between when a new test starts. Since this behaviour leads to interdependent tests, we deem it as a design failure and do not copy the behaviour of Matlab in this regard. Something similar applies to the current working directory.


## Test runner
*TTEST* features the test runner `runttests`, due to various reasons (see Section *State of the art of unit testing in Matlab*).
`runttests` supports two types of test execution
- *`'test'` - mode:*  In this mode, all `*.m` files in the current directory whose filename starts or ends with `'test'` (case insensitive) are executed. This is the default behaviour and the option `'test'` usually does not need to be given.
- *`'doctest'`-mode:* In this mode, all files in the current directory are parsed, and only the lines starting with `%TT` are executed. For this mode the option `'doctest'` must be given.

One can also specify which files shall be executed. E.g. `runttests test_abc` only runs the file `test_abc.m`. The wildcard `*` is supported. E.g. `runttests abc*` executes all files in the current directory whose filename starts with `abc`.

*(experimental)* Additionally, as the very first argument the option `'recursive'` is allowed, in which case the test runner also searches in subdirectories for files. All subdirectories should be in the Matlab path when the `'recursive'` option is used.

If one wants to doctest a file, whose filename happens to be `test.m`, then one has to use the syntax `runttests doctest test`. Similarly, if one wants to executes a file, whose filename happens to be `doctest.m`, then one hast to use the syntax `runttests test doctest`.

### When is a test run a success
A test run fails, when at least
- one uncaught error is thrown, or
- a *TTEST* test macro fails

Additionally (in a future release), the test run *may* also fail when a caught, but unexpected, exception is thrown. This may seem to restrictive, but it turns out that otherwise test run may succeed, although they obviously should fail -- mostly due to third party code.

### Test styles
The *TTEST* test runner `runttests` supports three types of file layouts: 
- Script based unit tests,
- Function based unit tests
#### Script based unit tests
In this mode, the script is just executed. A sample test script file looks like:
```matlab
SECTION( 'test' );
    EXPECT_EQ( 2, 2 );
SECTION( 'test2' );
    EXPECT_EQ( 3, 3 );
```
#### Function based unit tests
This is more or less the same as the Matlab style function based unit test, except that one can omit most of the boiler plate code and just write
``` matlab
function test1()
    EXPECT_EQ( 2, 2 );
function test2()
    EXPECT_EQ( 3, 3 );    
``` 
Note, there must not be a breakpoint in the first function and the order of execution of the functions is not defined.
The functions must not take any arguments, nor return any arguments. They can be named arbitrarily.

### Restrictions
- In *doctests*, private functions usually cannot be used, since they get commented out.

***(experimental)*** Currently the overloads of `warning` is added to the Matlab path, and thus, always active. This seems to be a good idea and so far makes no problems at all. Nevertheless, we may change this in a future release such that the overload is only active when the test suite is executed via the `runttests` test runner.

## Property based tests
The *TTESTs* bring a very easy to use interface for property based tests. These are tests which check properties instead of specific input-output pairs. The most easy usage of a property based test reads *[0]*
```matlab
GIVEN( 'flt', 'EXPECT_EQ( flt, flt.'' )' );  % string form
GIVEN( @(flt) EXPECT_EQ( flt, flt.' ) );   % implicit form
GIVEN( tt.float, @(x) EXPECT_EQ( x, x.' ) );  % explicit form
```

*(experimental*) There is also an experimental mixed string forms, which most likely will work, although its rational is in question:
```matlab
GIVEN( 'flt', @(flt) EXPECT_EQ( flt, flt.' ) );  % mixed form
```

In the examples, `flt` is a shorthand for floating point numbers. The test, given as last argument is then checked with 20 random (but carefully chosen) floating point numbers. For the explicit form, no shorthand is used but the name of the strategy *[1]*. All strategies reside in the namespace `tt.*`. For each randomly generated floating point number it is checked whether it is equal to its transpose. As for test macros, both a function handle form and a string form can be used. In function handle form the argument `'flt'` can be incorporated into the function handle itself. 

Note, the string version uses `assignin` and evaluates the passed function in the callers workspace. Thus as usual, this does not work in static workspaces and no variables with the shorthand name must must be present in the callers workspace, in the example: `flt`. If such a variable already exists, an error is thrown. If this poses problems, one can add underscores and numbers to the shorthand. For example, the above test can be written as
```matlab
GIVEN( 'flt1_', 'EXPECT_EQ( flt1_, flt1_.'' )' );  % save string form
```
*[0]* Note, these tests will fail due to `NaN`, which is a valid floating point number. 

*[1]* A more descriptive name for *strategies* would be *generators*, but we tried to mimic the API of the widely used Hypothesis package for Python *[3]*. Also the name `GIVEN` is taken from Hypothesis.

*[2]* The restriction that the strategy name must end with an underscore is the same as in the test-macro case. In string form, the evalable string is evaluated in the callers context and variables are assigned into the workspace.

*[3]* D. R. MacIver,  Z. Hatfield-Dodds et. al., *Hypothesis: A new approach to property-based testing*, JOSS, 4 (43) 1891, (2019).

#### Multiple strategies
Multiple strategies can be used too.  As before we give three examples.  The presented tests will fail in general.
```matlab
GIVEN( 'sqm1', 'sqm2', 'EXPECT_NE( sqm1, sqm2 )' );
GIVEN( @(sqm1,sqm2) EXPECT_NE( sqm1, sqm2 ) );
GIVEN( sqmatrix, sqmatrix, @(x,y) EXPECT_NE( x, y ) );
```
If one strategy is used multiple times, it is necessary to postfix its shorthand with a number. Otherwise the test fails, e.g. the following call fails
```matlab
GIVEN( 'sqm', 'sqm', 'EXPECT_NE( sqm, sqm )' );
```


### Failed tests *(experimental)*
If a test run fails, the corresponding example is stored in the folder `tmp/fail` and executed again whenever the corresponding `GIVEN` test is executed. Whenever later in time the test succeeds 3 times on this example, the file is deleted from the folder again.

### Options for `GIVEN`
The given function accepts the following options as name-value pairs.
- `'maxexample',value` , integer, default = `200`, defines the maximal number of examples tried
- `'minexample',value` , integer, default = `5`. Controls the minimum number of examples which need to be generated in order that the test counts as succeeded. 
- `'maxtime',value`, integer, default = `30`,  defines the approximate maximum time in seconds.
- `'rng',value`, integer, default = `[]`, seeds the random number generator using the given value. If empty, the random number generator is not seeded. This does not guarantee that the generated examples are the same on a long time scale.

Note that, for all options various aliases are defined, in particular one can use those defined by Hypothesis.

### Notes
- **Shrinking** 
  Currently, shrinking of examples is not implemented. If there is need for this feature, it is very likely that it will be implemented.
- **Failed test** We actually do not store the examples of the failed test, but a function handle which has  the examples captured. This behaviour may change in a future release.

- **Ridiculous note**
  Since the string style of a `GIVEN`-test assigns variables and evaluates the given function in the callers workspace, it is possible to write self-modifying tests. We do not think that such tests should be written, because it makes repeating failed tests difficult or even impossible, but, maybe someone can prove us wrong.
  
- **Quality of strategies** Currently, most strategies are just a proof-of-concept and the quality of the returned examples has still room for optimization. If there is need for better strategies (which we hope that there is), we will focus our forces in improving the quality of the strategies and are willing to add new strategies. Currently we plan to add the strategies to generate the following objects (in order from important to unimportant)
sets of matrices, sparse matrices, vectors of bytes/integers/fractions/complex numbers, pictures, MRT scans, graphs, neural networks, unary functions,  midi Data, polytopes, times, dates, UUIDs, email addresses, fprintf strings, files, matlab programs.

- For more information about property based testing see:
  - [Property based testing in F#](https://fsharpforfunandprofit.com/posts/property-based-testing-2/)
  -  [FsCheck](https://fscheck.github.io/FsCheck/)
  - [Hypothesis - Documentation](https://hypothesis.readthedocs.io/en/latest/), [Hypothesis - Blog](https://hypothesis.works/)
  - [Quickcheck implementations](https://hypothesis.works/articles/quickcheck-in-every-language/):
  [Smallcheck](https://hackage.haskell.org/package/smallcheck), 
  [Quviq](http://www.quviq.com/), 
  [Scalacheck](https://www.scalacheck.org/)

### Strategies
Strategies are objects which generate random examples to be used for property based tests via `GIVEN`. All strategies are in the namespace `tt.*` *[0]*.

*[0]* See the footnote in the Matchers sections for informations about namespaces.

| Strategy    | Description            | Shorthand |
| ----------- | ---------------------- | --------- |
| `audio`     | Audio signals          | `'aud'`   | 
| `array`     | Arrays                 | `'arr'`   |
| `matrix`    | Matrices               | `'mat'`   |
| `sqmatrix`  | Square matrices        | `'sqm'`   |
| `bool`      | Vectors of booleans    | `'bool'`  |
| `float`     | Floating point numbers | `'flt'`   |
| `namevalue` | Name-value pairs       |  none     |

**`audio`** *(experimental)* returns vectors of double with represent audio signals. Additional options: `format`, `samplingrate`, `bitdepth`.

**`array`** returns arrays of arbitrary dimension.  The option `type` loosely describes what kind of array to generate. Possible values are:  `'zeros'`, `'ones'`, `'randn'`, `'rand'`, `'nrand'` (values in [-1 1]), `'irdand'` (integers), `'cyclic'` .

**`matrix`** returns matrices. The option `type` loosely describes what kind of matrices to generate. Possible values are:  `'zeros'`, `'ones'`, `'consecutive'`, `'array'`, `'log'`, `'hankel'`, `'chebvand'`, `'toeplitz'`, `'circulant'`, `'cycol'`.

**`sqmatrix`** returns square matrices. The option `type` loosely describes what kind of square matrices to generate. Possible values are: `'zeros'`, `'ones'`, `'randn'`, `'matrix'`, `'array'`, `'compan'`, `'hadamard'`, `'hilbert'`, `'inversehilbert'`, `'magic'`, `'pascal'`, `'rosser'`, `'wilkinson'`, `'binomial'`, `'chebspec'`, `'chebvand'`, `'clement'`, `'condex'`, `'dramadah'`, `'frank'`, `'gcdmat'`, `'gearmat'`, `'grcar'`, `'invol'`, `'ipjfact'`, `'kahan'`, `lauchli'`, `'lehmer'`, `'totallynonnegative'`, `'lesp'`, `'lotkin'`, `'minij'`, `'neumann'`, `'orthog'`, `'parter'`, `poisson'`, `'redheffer'`, `'riemann'`, `'ris'`, `'smoke'`, `'toeppen'`, `'tridiag'`, `'triw'`, `'ssetsqmatrix'`.

*(experimental)* This strategy additionally accepts the options `'symmetric'`, `'antisymmetric'` and the name-value pair `'invertible',value` (smallest allowed eigenvalue in modulus). **If these options are used, properties defined by other strategies or options may be violated!**

**`bool`**  returns a logical vector. The option `type` loosely describes what kind of vector to generate, possible values are:  `'true'`, `'mix'`, `'periodic'`, `'random'`, `'log'`, `'thue-morse'`, `'fibonacci'` .

**`float`** *(experimental)* returns scalar floating point values. In a future release this strategy most likely will return vectors of floating point values. This strategy additional accepts the options `'allowsingle'` (default=`false`) and `'allowdouble'` (default=`true`).

**`namevalue( name, value )`**, where `name` is a cell array of names and `value` is a cell array of values, returns a cell array of name-value pairs. `name` must consists of strings, the elements of `value` can be cell arrays, function handles or numeric types. If it is a cell array, than one element is chosen from the cell array. If it is a function handle, then the function handle is evaluated. If it is a scalar, then a fixed value is chosen. For example `namevalue( {'n1','n2'}, {@randn,{'A','B'}} )` may yield `{'n2','B'}`.
The strategy accepts the additional option `'allowmultiple'` in which case one name is allowed to be drawn multiple times. For example  `namevalue( {'n1','n2'}, {@randn,{'A','B'}}, 'allowmultiple' )` may yield `{'n2','B', 'n1',0.942, 'n2',A, 'n2','B'}`.

#### General options
Most strategies except the following options, whenever they make sense. If an option is passed which cannot be used, an error will be raised in future releases.

**`sze`/`minsze`/`maxsze`** Determines the `size` of the generated examples as returned by matlab, except for the fact that we report the size of column vectors with scalars. For the strategy `array` a trailing value of `-1` can be used which signifies that the dimension of the array is arbitrary. For example `array( 'minsze',[1 2 3], 'maxsze',[2 5 -1] )` will yield arrays of size between `[1 2]`  and `[2 5 5]`, `array( 'maxsze',[2 -1] )` will yields arrays of size between `[1]` and `[2 2 2 2 2 ... 2]`.

**`norm`/`minnorm`/`maxnorm`** This option is only supported by some strategies and determines the norm of the drawn examples.

**`val`/`minval`/`maxval`** determines the range of scalar values of the drawn examples. For example
`matrix( 'minval',2, 'maxval',4 );` will yield matrices whose entries are greater equal 2 and smaller equal 4.

**`'allownan'`/ `'allowinf'`/`'allownegative'`/`'allowcomplex'`** determine whether the drawn examples are allowed to contain such values. The defaults for `allownan`, `allowinf`, `allownegative`, are true, for `allowcomplex` the default is `false`. Note that just allowing such values is not a guarantee that such values are generated.

**`'type'`** determines the type of returned examples. Its exact meaning depends on the strategy.

### Adaptors
Adaptors are strategies which take other strategies as input and return a new strategy.

| Strategy       | Description                                    |
| -------------- | -----------------------------------------------|
| `map`          | Applies a function to drawn examples           | 
| `filter`       | Only returns good examples                     |
| `tuple`/`pair` | Returns a cell array of examples               |
| `recursive`    | Recursively calls an adaptor and a strategy    |

**map**
`map( m, st1, ..., stn, [options] )`, where `m` is an `n`-ary function and `sti` are strategies. `map` takes example drawn from `sti` and applies the function `m` to it. For example `map( @(x) round(x), float )` may draw `2`.

Currently `m` must be a function handle and a string from is not possible.
 
**filter**
`filter( f, st, [options] )`, where `f` is a unary function returning `true`/`false` and `st` is a strategies. `filter` draws from strategy `st` until an examples `ex` is found such that `f(ex)==true`.
For example `filter( @(x) x~=0, float )` may draw `-1e308`. Note that `filter` isn´t magic and if the filter is too restrictive the test will fail. It should only be used to filter out corner cases.

Currently `f` must be a function handle and a string from is not possible.
  
**tuple**/**pair**
`tuple( st1, ..., stn )`/`pair( st )` generates a cell array of elements drawn from strategies `sti`, respectively a cell array with two elements drawn from strategy `st`. For example `tuple( float, sqmatrix )` may draw `{1.4e-8, [1 0;1 0]}`.

**recursive**
`recursive( st, adpt )` takes a strategy and an adaptor and recursively either draws from `st` or from `adptr(st)`. 
For examples `bt = recursive( bool('sze'1), pair );` may draw `{ true, { false, { {true,false}, false} } }`.

**compose**
A very mighty adaptor is `compose`. All other adaptors can be implemented using the `compose` generator. It is also the easiest way write user defined strategies. `compose` takes a unary function `f`, which internally may use any logic, including drawing from strategies, and returns a new strategy. The passed function `f` must forward its only input argument to the strategies, whenever one wants to draw from a strategy. For example with the following function ordered pairs of floats are drawn
```matlab
[ raw ] = function f( tt );
    fl = float( 'allownan',0, 'allowinf',0 );
    f1 = fl.example( tt );
    f2 = fl.example( tt );
    raw = sort( [f1 f2] );
end
```

### Fixed size strategies

*(experimental)*  These strategies are experimental in the sense that the API of the strategies will not change in future releases, but the way which samples and how the samples are drawn will change. Furthermore, currently fixed size strategies do not work well together with strategy-adaptors.

These strategies return a fixed set of values and have no shorthand.

| Strategy   | Description         |
| ---------- | --------------------|
| `value`    | User defined values |     
| `range`    | Values in a range   |       
| `repeat`   | Repeats a value     |
| `type`     | Casts a value       |

**`value`** returns a fixed set of user defined examples. The values must be given as a cell array using curly braces `{}`. For example: `GIVEN( tt.value({1,2,5,10,20}), @(x) x>0 );`.

**`range( a, b, [s] )`** returns the values {`a`, `a+s`, `a+s+s`, ..., } and `b` is not included. If `s` is not given, it defaults to `1`. For example `tt.range( 1, 10, 3 )` yields {`1`,`4`,`7`}.

**`repeat( a, n )`**  yields a list of `n` repetitions of `a`. For example `tt.repeat( 3, 5 )` yields {`3`,`3`,`3`,`3`,`3`}. If option `'evaluate'`is given, then the argument `a` is evaluated and the result is appended to the to be generated list. For example `tt.repeat( @() randi(4), 5, 'evaluate' )` may yield {`3`,`2`,`2`,`1`,`5`}. It is also possible to pass strategies. For example `tt.repeat( bool, 2, 'evaluate' )`may yield {`[false]`,`[true false true false]`}.

*`type( a, t1, ..., tn )`* yields the casts of `a` to each type `ti`. User defined types are possible as long they implement either a `cast` operator or a unary constructor. For example  `tt.type( 2, 'int32', 'int64' )` yields {`int32(2)`,`int64(2)`}. There are also abbreviations possible. For example `tt.type( 2, 'symvpa' )` yields {`2`,`sym(2)`,`vpa(2)`}. The full list of abbreviations which cast to multiple types is:
- `'int'` casts to all integer types, , i.e. `'uint8'`, `'int8'`, `'uint16'`, `'int16'`, `'uint32'`, `'int32'`, `'uint64'`, `'int64'`
- `'sint'` casts to all signed integer types , i.e. `'int8'`, `'int16'`, `'int32'`, `'int64''`
- `'uint'` casts to all unsigned integer types, i.e. `'uint8'`, `'uint16'`, `'uint32'`, `'uint64'`
- `'float'` casts to all floating point types, i.e. `single` and `double`
- `'numeric'` casts to all numeric types, i.e. `uint8`, ..., `in64`, `single`, `double`
- `'text'` casts to `char` and `str`
- `'symvpa'` casts to `double`, `sym` and `vpa`.


### Defining strategies
To define a new strategy, one just uses pre defined strategies with suitable options and passes them to adaptors if necessary. To register a strategy one just uses the option `'name',name` . For example, the following defines a strategy which only returns positive numbers which are not integers and defines the shorthand `realfloat` for it. 
```matlab
tt.filter( ...
    tt.float( 'minval',0 ), ...
    @(x) x~=round(x) && isfinite(x), ...
    'name','realfloat' ...
);
``` 

Note that user defined shorthands should be at least 5 characters long and *must not end with a digit or an underscore*. If a user defined shorthand is the same as a predefined shorthand, the predefined shorthand may get overwritten silently.


## Disabling tests / Changing severity of tests
### Disabling/Enabling test macros
By passing the argument `DISABLED` to any test, the test is skipped and message is printed that the test was skipped. For example, the following tests are all skipped and, in particular, succeed,
```matlab
EXPECT_EQ( DISABLED, 2, 3 );
EXPECT_PRED( @asgfj, DISABLED, 'ti' );
EXPECT_THAT( DISABLED );
```
Additionally there is the severity `APPLIED`, which works as `ENABLED`, but never produces an output message.

The argument `ENABLED` is a convenience function which enables a test macro.

### Changing failure behaviour / Changing severity
By passing the arguments `TODOED`, `EXPECTED` or `ASSERTED` to any test macro, the tests severity is changed to `TODO`, `EXPECT` or `ASSERT`, respectively. For example, the following is a succeeding `TODO` test
```matlab
EXPECT_EQ( 2, TODOED, 2 )
```
In this form, the usefulness of the functions `DISABLED`, `ENABLED`,`TODOED`, etc... seems to be in question, but, these take all an additional argument to conditionally change the severity of the test macros.  One can pass:
- a scalar, representing truthy/falsy, e.g. the following test is disabled: `EXPECT_EQ( 2, 3, ENABLED(2==3) )`.
- a nullary function handle, e.g. the following test is a `TODO_` test: `EXPECT_EQ( 42, 666, TODOED( @() true ) );`.
- one of the following strings (case insensitive):  `'OnUnix'`, `'OnPc'`, `'OnMac'`, `'Onx86'`, `'Onx64'`, `'OnMatlab'`, `'OnOctave'` (or their convenience counterparts `'OnLinux'`, `'OnWindows'`, `'OnApple'`,`Onx32`). For example the following test is not executed under 32 bit systems: `EXPECT_EQ( ones(25000), ones(25000), ENABLED('Onx64') );`

### Disabling test files
- To disable a whole test file, the best way is to prepend the name  `DISABLED` to the filename. E.g. rename `test_1.m` to `DISABLEDtest_1.m`.  Note, this does only work with the *TTESTs* test runner `runttests`.


## Logging 
Additional messages can be logged using
- end of line comments `%`
- the functions `MESSAGE()` and `TRACE()`,
- using sections  via `TESTCASE`/`SECTION`/`SUBSECTION()` and
- comments `%%` and `%%%`

### End of line comments `%`
Since the testrunner prints out the line of the failed test, the easiest way to add messages to a test is to just add a comment
```matlab
EXPECT_FAIL()  % This comment here is printed in the runttests output
```
This of course does not support strings generated at runtime, nor does work with the Matlab testrunner `runtests`.

### Logging via `MESSAGE()` and `TRACE()`
These function capture some text and print it out when a test fails. Their difference lies in the scope on which they are working.
- `MESSAGE()` texts are only printed out by test macros which are in the same scope as where `MESSAGE()` was called. E.g.:
    ```matlab
    a = 2; 
    MESSAGE( a );
    EXPECT_EQ( a, 3 )
    ```
  will print out
    ```
    lhs ~= rhs (but they should be @isequal), where
    lhs = 2
    rhs = 3
    a : 2
    ```

- `TRACE()` *(Matlab only)* texts are printed out by test macros which are invoked using `TRACE()`. This helps to understand the context of an assertion failure when it comes from inside a subroutine.  `TRACE` accepts any number of function handles as additional inputs (unfortunately, no evalable strings so far). If no message is given, then the message from the last `MESSAGE` is used. E.g. 
    ```matlab
    a = 2; 
    TRACE( @() EXPECT_EQ(2,3), a );
    ```
  will print out
    ```
    lhs ~= rhs (but they should be @isequal), where
    lhs = 2
    rhs = 3
    Trace:
      a : 2
    ```

Both functions accept two input formats: *capture-style* and *printf-style*.
- *capture-style*: takes a variable and prints out that variable and its value at the time of capture. *Capture-style* can also print out expressions, but only its value, not the expression itself. E.g.: `a = 2; b = 1; MESSAGE( a, b, a>b );` will print out
    ```
    a : 2
    b : 1
    true
    ```

- *printf-style* prints out a printf formatted string. E.g.: 
    ```matlab
    a = 2; b = 1; 
    MESSAGE( 'a=%i and b=%i\n', a, b );
    ```
   will print out
    ```
    a=2 and b=1
    ```
Which style is used is determined in the following way: 
- capture-style is used when
  - the first argument of`MESSAGE`/`TRACE` is `'ttestcapture'` or `'ttest_capture'`
  - the first argument of`MESSAGE`/`TRACE` is not a string
  - all arguments are a variable whose name can be retrieved by `inputname()`
- printf-style is used when
   - the first argument of`MESSAGE`/`TRACE` is `'ttestprintf'` or `'ttest_printf'`
   - nothing else applies

We give some light shedding examples:
```matlab
a = 1; b = 2;
MESSAGE( a, b );  % capture-style
MESSAGE( '2', a );  % printf-style
MESSAGE( 'ttestcapture', '2', a  );  % capture-style
```

**Note:** 
- The `TRACE` function works only on Matlab.
- If *printf-style*  is used and the format string or the arguments is wrong, then Octave may not throw an error message but silently accept the wrong input.

### Logging via sections
If the section is named, i.e. a name is passed as first argument, then the name is reported in all failed test macros. For example the following produces
```
>> TESTCASE( 'TwoThree' );
>> SUBSECTION( 'Two' );
>> EXPECT_EQ( 2, 3 );

arg( 1 ) ~= arg( 2 ), ( but they should be @isequal ). Where
  arg( 1 ) =      2
  arg( 2 ) =      3
Testcase: TwoThree
Subsection: Two
=====================================
```

### Logging via comments
If a test fails, the text after the characters `%%` and `%%%` is printed, annoted with `'Test name'` and `'Subtest name'`. E.g. running the script
```matlab
%% test integers
%%% educated guess
EXPECT_EQ( 42, 666 )
```
will produce the output
```
lhs ~= rhs (but they should be @isequal), where
lhs = 42
rhs = 666
Test name:  test integers
Subtest name:  educated guess
``` 

## The `TTEST` function
This function is the frontend and backend for the *TTESTs*.

### Setting preferences
To change options call `TTEST` together with the option. We describe all stable options now:

**`'init'`** This option prepares the environment for a *TTEST* run. Whenever user defined options are necessary for different parts of the test suite, `TTEST init` must be called, prior to setting preferences. For example, the following sets only the verbosity level to 2 in that function
```matlab
function ret = test()
    TTEST init
    TTEST verbose 2
```
`TTEST init` uses `assignin`, and thus, does not work in static workspaces. In static workspaces one must the quite restrictive *output form* of `TTEST`.
```matlab
function ret = test()
    ttest_id = TTEST( 'init' );  % output variable must have name 'ttest_id'
    TTEST verbose 2
    function dummy()  % this function is only here 
    end               % to create a static workspace
end
```

**`'help'`** prints out a help text

**`'print'`** prints out all stored data inside of `TTEST`

**`'var'`** It is possible to store and values in `TTEST`. To store a value call `TTEST var varname varvalue`. To load a stored value call `TTEST var varname`. Is is an error to load a variable before it has been stored.

### Options controlling what happens when a test fails

**`'errorassert'`/ `'errorexpect'`/ `'errortodo'`/ `'successassert'`/  `'successexpect'`/ `'successtodo'`**    These options are used define what shall happen when a `TODO_`/`EXPECT_`/`ASSERT_` test fails or succeeds. The accompanying value can be
   - one of the strings `'ass'`, `'exp'`, `'todo'`,
   - a function handle, or
   - any other string evalable with `eval`

If the settings value is `'todo'`/`'exp'`/`'ass'` then the default `TODO_`, `EXPECT_` `ASSERT_`  error handler is called. Their behaviour is
 - `'todo'` If a test fails, then `TTEST_ERROR_TODO_WORKER()` is called, which prints out a summary of the failed test. `true` is returned. If a test succeeds, a message is printed that a `TODO_` test succeeded.
- `'exp'`If a test fails, then `TTEST_ERROR_EXPECT_WORKER()` is called, which prints out a summary of the failed test and the `TTEST` variable `errflg` is set to true. If a test succeeds, nothing happens.
 - `'ass'` If a test fails, then `TTEST_ERROR_ASSERT()` is called, which sets the global variable `TTEST_ERRORFLAG` to `true` and afterwards calls `assert(false)`, which is cached and rethrown (to have nicer Matlab output).  If a test succeeds, nothing happens.
    - If the queried variables value is a `function_handle`, it is evaluated, if the queried variables value is a `string` or `char`-array, it is passed to `feval`. This allows to define custom error handlers.

**`'errflg'`** This flag is set to true whenever a test macro fails. To query it call `TTEST errflg`, to reset it call `TTEST errflg false`.


## Utility functions 
The *TTESTs* come with some utility functions which will turn out to be useful and which reside in the folder `/utility/`. These functions are written by the *TTEST* team and are meant to be used. Some of them are very helpful (especially `allfunctionhandle`, `evallazy`, `variablename`), others are of very limited used. An in depth documentation is contained in the functions and can be retrieved with `help functionname`, where `functionname` is the name of the function. E.g. `help file2txt`.

### List of the `/utility/` functions

#### Very useful functions
- **allfunctionhandle**  `[ handle, ftype ] = allfunctionhandle( filename, [options] );`  *(experimental)* *(Matlab only)* This function retrieves handles to local and private functions of a file *[0]*. It works with functions and scripts, not yet with classes. For example:
    ```matlab
    h = allfunctionhandle( 'spy' );
    imagesc( h{1}() );
    ```
    Note that, for some functions, one has to set the proper options in order to make this work, for other functions this may not work at all. Nonetheless, the function only uses documented features of the language and it is used in *TTESTs* own unit test suite.
    The function currently works only in Matlab, due to severe Octave bugs.
        
    *[0]* Experimentally it also returns handles to nested functions, although these handles will be of no use since nested function rely on the workspace of he parent function which cannot be captured. I.e., executing these handles will most likely throw an error.
    
- **assign:** `ret = assign( name, value )` programmatically assigns a variable.

- **castws:** *(experimental)* `castws( type )` tries to cast all variables in the current workspace to the given type. Can be used to test implementations with different number types (e.g. `double` and `single`).

- **cwd:** `cwd( filename, [options] )` changes the **c**urrent **w**orking **d**irectory to the folder where `filename` is located. Does search on the path, and other folders.

- **dumpws:**  `dumpws( [overwriteall] )`saves all variable from the current workspace to the base workspace. Asks the user if variables would be overwritten.

- **evallazy:** `[ ret ] = evallazy( h, ws );` Evaluates a function handle using the variables currently in the workspace. For example: 
    ```matlab
    a = 1; 
    h = @(b) a + b; 
    a = 0; b = 1; 
    evallazy( h );  % yields 2
    ```

- **variablename:** `[ varname, allvarname, varidx ] = variablename( varargin )` Returns the variable names of a function handle. For example: `variablename( @(x,y) x+y, @(y,z) ); %yields varname=={{'x','y'},{'y','z'}}; allvarname=={'x','y'}; idx=={[1,2],[2,3]};`
#### The rest
- **badcall:** `ret = badcall( varargin )` yields an error.

- **complexity:** *(very experimental)* tries to asses the complexity of an algorithm


- **eval2nd**/**return2nd**/**evalfirstreturnlast**/**returnfalse:** *(experimental)* `ret = eval2nd( first, second )`, `ret = return2nd( first, second )`, `ret = evalfirstreturnlast( varargin )`, `ret = returnfalse( varargin )` These functions just evaluate or return one of the supplied arguments. It is very likely that the abundance of these very similar functions will be merged into one function in a future release.
- **errorat**: *(experimental)* This function injects an error into a `.m` file which can be used to early return from a function. Note: This function is quite unreliable.

-  **file2txt:** `[ ret ] = file2txt( in, out, newlineflag )` This function takes a filename or functionname as input and generates text output which gets parsed by Matlab to mimic the original input. For example: `file2txt EXPECT_EQ`.
- **filetype:** `[ t, numargin, numargout ] = filetype( filename )` tries to find out whether an `.m` file is a script, a function or a class.
- **heapoverflow**/**stackoverflow:** generates a heap or stackoverflow. The former most likely will crash Matlab.
- **infloop**/**secloop:** `[ t ] = secloop( sec )` is an infinite loop or a loop which takes `sec` seconds.
- **isequalproperty:** *(experimental)* returns false whenever the underlying properties of all inputs are not the same
- **limit:** *(experimental)* returns interesting values for types
- **noutput:** returns `n` outputs, each with the value `1`.
- **nullary:** comes from nothing and goes back to nothing.
- **subset:** `[ ret, Loca, Locb ] = subset( A, B );` Checks if `A` is a subset of `B`, where `A` and `B` can be vectors, matrices and cell arrays. 
- **terror:** (read: *"tommschs-error"*) `[ varargout ] = terror( varargin )` A wrapper function for `error`, accepting any number of inputs and outputs. For example: `a = terror( 'id:abc', 'msg' )` throws the exception with id `id:abc` and message `msg`, whereas `a = error( 'id:abc', 'msg' )` throws `'Error using error. Too many output arguments.'`.
- **thash:** *(experimental)* wraps various hashing functions.

## Compatibility
- Matlab compatibility will be for at least some versions.
- Octave compatibility is tested only for the *latest* version, and we mostly concentrate on the Linux version. Octaves frequent use of segfaults in the Windows version makes the life of an Octave developer indeed exciting. Free your minds. 
- Some things will not work on Octave. These are marked with *(Matlab only)*.

Currently, we regularly test the framework on the Matlab systems
- AMD Ryzen 3600, Windows 10, build 1809, Matlab R2020a *(flawless)*
- Intel Core i5-8500, Ubuntu 18.04, Matlab R2019a *(flawless)*
- AMD Ryzen 3600, Windows 10, build 1809, Matlab R2018a *(flawless)*

and the Octave systems
- AMD Ryzen 3600, Windows 10 build 1809, Octave 6.2 *(problems with symbolics and strategies)*
- Intel Core i7-8700, Ubuntu 20.04, Octave 6.3 *(problems with strategies)*

## Further Examples

A test framework itself not well tested is kind of weird. Thus, the folder `unittest` contains a lot of example one can have a look at how we test our own framework.

## Known "bugs"
- The warning settings are sometimes not restored fully, leading to a state where all warnings are disabled. This seems to be bug in *Matlab*. The overloaded *TTEST* `warning` function thus emits a warning whenever Matlab disables all warnings, and, it automatically resets the warning to a previous state after some time whenever `warning off` is called.
- Octave has problems with the function `allfunctionhandle`. It seems conditional breakpoints sometimes crash Octave.
- All bugs of *TTEST* are reported in *design.md*.

## Future work
The list of planned features can be found in *design.md*.

## Projects using *TTEST*
- [*ttoolboxes*](https://gitlab.com/tommsch/tjsr), University of Vienna
- [*LTFAT*](http://ltfat.org/), [*AMT*](http://amtoolbox.org/), OeAW/ARI

# State of the art of unit testing in Matlab
A quick survey of papers published on *JORS*, *JOSS* and *ACM TOMS* *[0]* between 2015 and 2020 for the Matlab language yields that only a small amount of submissions include automatic unit tests:

|  Journal  |  number of papers in survey | unit tests  | automatic tests|
| :-------: | :-------------------------: | :---------: | :------------: |
|    JORS   |              19             |      7      |        2       |
|    JOSS   |              18             |      7      |        3       | 
|  ACM TOMS |              18             |      8      |        2       | 

We suspect that this is mainly due to the following two reasons
1. People who use Matlab are, to a large extent, not developers but mathematicians or engineers *[1]*. Therefore, most Matlab users do not know about the importance of writing unit tests for a program.
2. It needs a lot of boilerplate code to write useful tests in Matlab.

*[0]* *JORS*: Journal of Open Research Software, *JOSS*: Journal of Open Source Software, *ACM TOMS*: ACM Transactions on Mathematical Software
*[1]* Conversely, most developers do not use Matlab.


The importance of writing tests on the other hand is undoubted. Unit testing
- saves time on the long term
- provides documentation
- helps to develop sensible interfaces (everything which cannot be unit tested has probably a bad interface)
- helps to find bugs
- prevents introducing new bugs
- prevents from reintroducing old bugs, i.e. regression
- simplifies the debugging process

While we do not know of a way to fix issue (1), we can work on issue (2) by making unit testing as easy as possible.

### Existing Matlab unit test frameworks
To our knowledge there are currently (2021) three test frameworks still in use: *MOxUnit*, *xunit4* and *Matlabs built in test framework*. Although, *xunit4* can be seen as a predecessor of Matlabs built in test framework, and thus, we do not discuss it here. Matlabs framework is very powerful, but also very verbose. For large projects Matlabs framework is well suited. But, given that most Matlab projects often consist only of a few `.m` functions, it is quite an overkill when the accompanying test suite needs more code, and more complex language constructs, than the project. On the other hand, *MOxUnit* and *xunit4* need much less code to write sensible unit tests, but lack a lot of important features. Therefore, the *TTESTs* are a perfect supplement for all three test frameworks.

For the following list we did an extensive search on *Wikipedia, Google, Gitlab, Github, Matlab File Exchange, Octave SourceForge*. We used various unit testing related keywords, in particular (list not exhaustive): *unit testing, framework, testing, test harness, assertion framework, testing, doctest, xUnit, mUnit, mutation testing, fault injection, pairwise testing, Jenkins, Travis, CI, test runner*.
We do not expect that there exist any other publicly available unit test frameworks for Matlab or Octave currently.

|    Name  | Last Update | Remarks |
| -------- | :---------: | ------- | 
| [Kit Ng unit testing tools](https://de.mathworks.com/matlabcentral/fileexchange/7404) | 2005 | very basic |
| [Kvasnica Cirka framework](https://www2.humusoft.cz/www/papers/tcp07/kvasnica.pdf) | 2005? | not available anymore |
| [Wall MATUnit](https://de.mathworks.com/matlabcentral/fileexchange/7979) | 2005 | very basic |
| Phelan MUnit| 2006 | not available anymore |
| [Lombardi MUnit](https://de.mathworks.com/matlabcentral/fileexchange/11306)| 2006 |    |
| [Kritzinger Unit test framework](https://de.mathworks.com/matlabcentral/fileexchange/10861) | 2006 | Does not look very promising |
| [Christopher mlunit_2008a](https://de.mathworks.com/matlabcentral/fileexchange/21888) | 2009 | Very verbose, Fork of MUnit |
| [Jay Test tools](https://de.mathworks.com/matlabcentral/fileexchange/346) | 2009 | only some helper functions |
| [Smith Doctest](https://de.mathworks.com/matlabcentral/fileexchange/28862) | 2010 |Predecessor of Sextons *xunit4* doctests |
| [Eddins xUnit](https://www.mathworks.com/matlabcentral/fileexchange/47302) | 2010 | Predecessor of xunit4 |
| [Brett matlabtesting](https://github.com/matthew-brett/matlabtesting) | 2011 | Very basic
| [Nievinski testit](https://de.mathworks.com/matlabcentral/fileexchange/30555-testit) | 2011 | Implementation of Octave`s unit tests for Matlab |
| [arka mUnit](https://github.com/arka/munit) | 2013 | |
| [Zyndric lute](https://github.com/Zyndric/lute) | 2013 | Very basic |
| [Hetu mlUnit](https://sourceforge.net/projects/mlunit/)                    | 2015 | Very verbose  |
| [Legland munit](https://de.mathworks.com/matlabcentral/fileexchange/7845)  | 2016 | Only two tests available: `assertEquals` and `assertTrue` |
| [Schiessl unitTAPsci](https://www.mathworks.com/matlabcentral/fileexchange/52179) | 2016 |  Jenkins support for Matlab built in framework | 
| [mwgeurts unit_harness](https://github.com/mwgeurts/unit_harness) | 2016 | 
| [Bergholz mUnittest](https://git.osuv.de/m/mUnittest) | 2017 |  Very basic, Jenkins support, Octave compatible |
| [Sexton xunit4](https://de.mathworks.com/matlabcentral/fileexchange/47302) | 2021 |  Predecessor of Matlabs built in framework, [doc tests](https://github.com/psexton/matlab-xunit-doctests)  |
| [Janke testify](https://github.com/apjanke/octave-testify) | 2020 | Octave BIST reimplementation |
| [Janke validoozy](https://de.mathworks.com/matlabcentral/fileexchange/71387) |2022| Only small set of test macros |
| [MOxUnit](https://github.com/MOxUnit/MOxUnit)                              | 2023 | popular, Only some tests available, Octave compatible, [doc tests](https://github.com/MOdox/MOdox), [coverage](https://github.com/MOcov/MOcov) |
| [Walter doctest](https://github.com/catch22/octave-doctest) | 2024 | Doctests for Octave |
| [Zimmermann MP-test](https://github.com/MATPOWER/mptest) | 2024 | Very specialized |
| [Matlabs built in framework](https://de.mathworks.com/help/matlab/matlab-unit-test-framework.html) | 2024 | Script based tests support only an `assert` test, other tests very verbose  |
| [*TTEST*](https://gitlab.com/tommsch/TTEST) | 2024 | Precise and mighty

### Usability
In this section we compare the usability of *TTESTs*, *MOxUnit* and Matlabs unit test framework. 
Matlabs unit tests further divide into *script based*, *function based* and *class based* unit tests. Also, *TTESTs* divide up into *script based* and *function based* unit tests.

**Boilerplate code**
To demotivate users to write unit test, its best to burden them with writing lots of boiler plate code. Thus, we compare the number of characters  needed to write a single unit test similar to `assert( 2==2 )`. 
- *TTEST Script* with *TTESTs* test runner `runttests`
    ```matlab
    TTEST init
    TESTCASE( 'test_2_2' );
        EXPECT_EQ( 2, 2 );
    ```
    or with Matlabs test runner `runtests`
    ```matlab
     %% test_2_2
        EXPECT_EQ( 2, 2 );
    ```    
- *TTEST Function* with *TTESTs* test runner `runttests`
    ```matlab
    function test_2_2
        EXPECT_EQ( 2, 2 );
    ```    
- *MOxUnit* with MOxUnits test runner `moxunit_run_tests`
    ```matlab
    function test_suite = test1Test
        test_functions = localfunctions();
        initTestSuite;
    
    function test_2_2
        assertEqual( 2, 2 );
    ```
- *Matlab Script* with Matlabs test runner `runtests`
    ```matlab
    %% test_2_2
        assert( 2 == 2 );
    ```  
- *Matlab Function* with Matlab test runner `runtests`
    ```matlab
    function tests = test2
        tests = functiontests( localfunctions );

    function test_2_2( testCase )
        verifyEqual( testCase, 2, 2 );
    ```  
- *Matlab Class* with Matlabs test runner `runtests`
    ```matlab
    classdef test2 < matlab.unittest.TestCase
        methods( Test )
            function test_2_2( testCase );
                testCase.verifyEqual( 2, 2 );
            end
        end
    end
    ```      
| **Characters needed** | *TTEST* <br/> *Script* | *TTEST* <br/> *Function* | *MOxUnit*  | *Matlab* <br/> *Script* | *Matlab* <br/> *Function* | *Matlab* <br/> *Class* |
|--------------------------|:-------:|:--:|:--:|:--:|:--:|:--:|
| **for boilerplate code** | 10 / 0   |  0 | 74 |  0 | 55 | 57 |
| **for one test**         | 35 / 25 | 32 | 32 | 23 | 52 | 56 |

Clearly, the Matlab framework has the most features, but also needs the most boiler plate code. Matlabs script based unit test needs the least code to write unit tests, but those lack nearly all necessary features of a unit test framework. All other frameworks need more code to write unit tests than what is needed to write unit tests with *TTESTs*.

For comparison reasons, we also assess the frameworks *GoogleTest*, *Catch2*, *PyTest*
- *GoogleTest*
    ```cpp
    #include <gtest.h>
    TEST( Test2, Test22 ) {
        EXPECT_EQ( 2, 2 );
    }
    ```
- *Catch2*
    ```cpp
    #include <catch2/catch_test_macros.hpp>
    TEST_CASE( "test_2_2" ) {
        REQUIRE( 2 == 2 );
    }
    ```
- *PyTest*
    ```python
    import unittest
    class test_2_2( unittest.TestCase ):
        def test_2_2( self )
            self.assertEqual( 2, 2 )
    ```
- *Julia*
    ```julia
    @testset "2_2" begin
        @test 2 == 2
    end;
    ```
- *R*
    ```r
    test_that( "test_2_2", {
        expect_equal( 2, 2 )
    } )
    ```
- *D*
    ```d
    /// test_2_2
    unittest {
        Assert.equal( 2, 2 );
    }
    ```
|   **Characters needed**  | *GoogleTest* <br/> ( C++ ) | *Catch2* <br/> ( C++ ) | *PyTest*  <br/> ( Python ) | *Julia* <br/> ( Julia )| *testthat* <br/> ( R ) | *D* <br/> ( D ) |
|--------------------------|:--:|:--:|:--:|:--:|:--:|:--:|
| **for boilerplate code** | 17 | 37 | 44 | 22 |  0 |  0 |
| **for one test**         | 35 | 37 | 38 |  9 | 41 | 40 |

**Performance**
Although not the most critical part of a unit test framework, performance is still important for two reasons: 
1. When the unit tests take too long to run, then the user will not do unit tests, or 
2. she will get bored while waiting for the unit tests to finish and start doing something else. 

Thus, from the frameworks design point of view,  the longest time in a test run must be spent in the software under test, not in setting up the unit test framework environment.

We measure the wall clock time for  three types of test suites, *(E)*, *(1024)* and *(1)*
- *(E)* is a folder with an empty test suite
- *(1024)* is a folder consisting of 1024 files each with one test suite with one test, similar to `assert( 2 == 2 );`
-  *(1)* is a folder consisting of 1file with 1024 test suites with one test, similar to `assert( 2 == 2 )`

System: AMD Ryzen 3600, 64 GB RAM, Matlab R2020a, TTEST version v3.2024.10.31
| Test <br/> Suite | *TTEST* | *TTEST* <br/> *no sections* | *MOxUnit* | *Matlab* <br/> *Script* | *Matlab* <br/> *Function* | *Matlab* <br/> *Class* |
| -------- |:--------:|:------:|:-------:|:-------:|:--------:|:--------:|
| *(1024)* |   8.7 s  |  7.1 s |  9.4 s  |  40 s   |  31 s    | 210 s    |
| *(1)*    |  10.5 s  |  4.6 s | 23 s    | 160 s   | 105 s    |  94 s    |
| *(E)*    |   0.7 s  |  0.7 s | 0.002 s |   0.8 s |   0.13 s |   0.14 s |

System: AMD Ryzen 3600, 64 GB RAM, Matlab R2023b, TTEST v3.2025.02.28
| Test <br/> Suite | *TTEST* <br/> *Script* | *TTEST* <br/> *Function* | *MOxUnit* | *Matlab* <br/> *Script* | *Matlab* <br/> *Function* | *Matlab* <br/> *Class* |
| -------- |:--------:|:------:|:-------:|:------:|:-------:|:--------:|
| *(1024)* |   35 s   | 16 s   | 18 s    | 48 s   | 58 s    | 125 s    |
| *(1)*    |    3 s   |  3 s   |  0.05 s |  9 s   | 22s     |   7s     |
| *(E)*    |    1.1 s |  0.1 s |  0.02 s |  0.2 s |  0.07 s |   0.06 s |


These numbers need some explanation before one can compare them.
The compared frameworks have a very different set of features. Clearly, Matlabs unit test framework has most features, but its overhead is ridiculously high *[0]*.
Also, the three frameworks reset different parts of the environement back to its original state between unit tests. Since for unit tests we want independent tests, the more state is restored the better. 
- By default, *MOxUnit* only restores the workspace (i.e. the variables) back for each unit test.
- Matlab additionally restores namespace imports. 
- *TTESTs* restore all global variables, the working directory, the warning state, debug break points, the matlab path and (optionally and experimentally) the base workspace and  the random number generator.  In order to restore this plethora in a fast way, *TTESTs* transparently overload the functions `addpath`, `dbclear`, `dbstop`, `path`, `restoredefaultpath`, `rmpath`and `userpath`.
 Future works includes restoring the state of figures and user defined random number generators (RNGs).
- If certain features are not needed, in particular sections (via `TESTCASE`, `SECTION`, `SUBSECTION`) and user defined options (via `TTEST init`), then *TTEST* is even faster, making it the fastest unit test framework for Matlab.

Summing up, when one compares the timings of the unit test frameworks, one needs to take into account that some frameworks restore more state than others. 

| What is restored             | *Matlab* |   *TTEST*   | *MOxUnit* | 
| -----------------------------|:--------:|:-----------:|:---------:|
| Workspace / Variables        | Yes      |  Yes        |    Yes    |
| Matlab path                  | No *[1]* |  Yes        |    No     |
| global variables             | No       |  Yes        |    No     |
| persistent variables         | No       |  No         |    No     |
| state of global RNG          | No       |  Opt. *[2]* |    No     |
| state of user-defined RNGs   | No       |  No         |    No     |
| figures                      | No       |  No         |    No     |
| base workspace               | No       |  Opt. *[2]* |    No     |
| working directory            | No *[1]* |  Yes        |    No     |
| namespace `import`           | Yes      |  No *[3]*   |    No     |
| debugger breakpoints         | No       |  Yes        |    No     |
| warning state                | No       |  Yes        |    No     |

*[0]* An interesting side note: The time Matlab reports for its test run is much less, for example 8 s for the test which took in reality 210 s.
*[1]* The path is only set back after the test suite is finished, it is not set back between tests.
*[2]* *(experimentally)* Currently, this only works it is enabled by the option `'rng',true` / `'base',true` at the first occurence of the opening of a section.
*[3]* *TTESTs* do not set back namespace imports, since (1) Octave does not support `import` yet and (2) the exact way how Matlab handles imports is undocumented. In particular, `import` statements are parsed before execution begins.


# License
Most of this work is copyright (c) 2020-2021 Thomas Mejstrik,
(tommsch@gmx.at), but it contains contributions by others. See
below for a full list of people who may hold copyright, and
consult the git log if you need to determine who owns an individual
contribution.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
## List of Contributors
The primary author for most of *TTEST* is Thomas Mejstrik. However the following people have also contributed work. As well as my thanks, they also have copyright over their individual contributions.
- **Clara Hollomey**


# Developer section

## Adding tests

**This section is slightly outdated**

In order to add tests, one has to add them to the file `TTEST_INSTALL.m` and afterwards reinstall `TTEST` using `TTEST install` . Each test consists of three part: *Preprocessing - Test - Postprocessing*. *Preprocessing* and *Postprocessing* are the same for all tests, the actual test logics resides in the *Test*-part. As an example we add a test which checks the class of an argument. We call the test `_ISA`.

1. At a proper place in `TTEST_INSTALL.m` we add the function `ISA`. A test function must only consist of uppercase letters and must not start with `TTEST`. This function takes no argument and must return two arguments. The first is a row character-array, the second an integer. In the test part  
  • the variable `ret` is already initialized as `true`, and  
  • all arguments passed to the test are stored in the cell array `varargin`.
  Thus, the test logic for our test could be something like
    ```matlab
    function ret = ISA
        assert( ischar(varargin{end}), 'TTEST:arg', 'Last argument must be a char-array defining the class.' );
        for i = 1:numel( varargin )-1;
           if( ~isa(varargin{i},varargin{end}) );
                ret = false;
                TTEST_FPRINTF( 'Argument has a wrong class, where\n Argument:' );
                TTEST_DISP( varargin{i} );
                TTEST_FPRINTF( 'has class: %s\n', class(varargin{i}) );
                TTEST_FPRINTF( 'but it should have class: %s\n', varargin{end} ); end; end;
    end
    ```        
2. The integer defines which arguments shall be expanded by default when using `inline` parameter generation *(already deprecated but still necessary)*. The rules defining the value are strange (and may be changed in a future release). For this function we need the value `1`.
4. Since we have to supply the code as a string, we have to add the following lines to `TTEST.m`.
    ```matlab
    function [ret,n] = ISA
        ret = [
              'assert( ischar(varargin{end}), ''TTEST:arg'', ''Last argument must be a char-array defining the class.'' );' newline ...
              'for i = 1:numel( varargin )-1;' newline ...
              '    if( ~isa(varargin{i},varargin{end}) );' newline ...
              '        ret = false;' newline ...
              '        TTEST_FPRINTF( ''Argument has a wrong class, where\n Argument:'' );' newline ...
              '        TTEST_DISP( varargin{i} );' newline ...
              '        TTEST_FPRINTF( ''has class: %s\n'', class(varargin{i}) );' newline ...
              '        TTEST_FPRINTF( ''but it should have class: %s\n'', varargin{end} ); end; end;' newline];
              n = 1;
    end
    ```
    Note the following changes to the code
    • Apostrophes `'` have to be escaped as `''`
    • Since the function must return a row-vector, we have to enter line breaks manually with `newline`, continue the lines with `...` and start and end  each line with an apostrophe `'`.
To simplify this set, one can also use the function `file2txt`. See the section about utility functions for more information.
		
3. To generate the test functions  `TODO_ISA`, `EXPECT_ISA` and`ASSERT_ISA` we have to call `TTEST.m`. For example, now we can test
    ```matlab
   >> EXPECT_ISA( 2, 'single' );
   Argument has a wrong class, where
   Argument:2
   has class: double
   but it should have class: single
    ```
4. Since this test turned out to be pretty useful, it is already contained in the *TTESTs*.
5. It is further possible to set the variable `errid` in the logic part, which is taken as *error id* in a failing default (i.e. no custom error handler defined) `ASSERT_` test. If this variable is not set, the id is set to `TTEST:NotTestname.`, i.e. in our example to `TTEST:NotIsa`.
  
## Adding matchers

As already noted, a *TTEST* matcher `H` is a struct, consisting of a unary function `H.m` and an explanation in words `H.t`. A matcher must accepts as inputs either 
- another matcher
- a function
-  or a number.

The *TTESTs* try to facilitate the creation of new matchers by providing the following means:

- **`Unary( h )`** returns a matcher where `h` is a unary function
- **`Matcher( 'name', handle )`** This command creates via `assignin` a variable with name `name` which is the matcher `Unary(handle)`
- **Defining a simple matcher in `TTEST.m`** 
- **Defining a general matcher in `TTEST.m`** 

### **Defining a simple matcher in `TTEST.m`**

**This section is slightly outdated**

Each matcher consists of three part: Preprocessing - Matcher - Postprocessing. Preprocessing and Postprocessing are the same for all tests, the actual test logics resides in the Matcher-part. As an example we add a test which checks the class of an argument. We call the matcher `Isa`.

1. At a proper place in `TTEST.m` we add the function `Isa` (e.g. right below the function `AnyOf`). A matcher function must contain at least on lowercase letter and must not start with `TTEST`. This function takes no argument and must return four arguments `ret`, `help`, `ass` `n`. `ret` is the actual matcher logic, `help` is the help text, `ass` can be set if one wants certain asserts before the matcher logic is executed and `n` is the number of arguments the matcher takes.
2. For our matcher `IsClass` takes one argument, the class name. Thus, `n` is 1. The help text for the matcher is `'is a `*`classname`*`'`. We do not want any prior assertions. The test logic for our test if `@(x) isa(x,classname)`. Thus we have to write

    ```matlab
    function [ret,help,ass,n] = IsClass
        ret = '@(x) isa( F(x), a )';
        help = ['is a '' a '''];
        ass = '';
        n = 1;
    end
    ```

    This code needs some further explanation. The free argument which can be passed to a constructed matcher, i.e. the `x` in the call `isa(x,'classname')`, must be named `F(x)`. All passed variables in the construction of the matcher, i.e. the string `classname` in the call `'isa(x,'classname')`, are lowercase letters starting with `a`. Thus, at most 26 arguments can be passed to a matcher using this approach. If one needs more, then one has to write the whole matcher logic by hand.

4. To generate the matcher `Isa.m`, we have to call `TTEST.m`. For example, now we can test
    ```matlab
    >> EXPECT_THAT( 2, IsClass('single') )
    Argument does not fulfil matcher, but it should, where
   matcher = is a single
   Argument 1 = 2
    ```
5. Since this matcher turned out to be pretty useful, it is already contained in the *TTESTs*.
   

### Defining a general matcher in `TTEST.m`
If one wants to autogenerate matchers without using the template, define a matcher-generator-function as above, but set `n = -1`. In this case, the returned value `ret` is directly written to a file, the output variables `help` and `ass` are ignored. For example, the `Description` function discussed above is generated (approximately) by
```matlab
function [ ret, help, ass,n ] = Description;
    ret = [
     'function H = Description( m, txt );' newline ...
     '    import tt.*' newline ...
     '    if( ~ismatcher(m) );' newline ...
     '        m = IsEq( m ); end;' newline ...
     '    H.m = m.m;' newline ...
     '    H.t = txt;' newline ...
     '    H = matcher_c( H.m, H.t );' newline ...
     'end'
     ];        
    help = [];
    ass = [];
    n = -1;
end
```


## Contributing to *TTESTs*

The following is a set of guidelines for contributing to *TTESTs*.

### Reporting Bugs

Before reporting, check if you can reproduce the problem in the latest version of TTESTs. A bug report shall contain

- a **clear and descriptive title** for the issue to identify the problem.
- an **exact description of the steps** to reproduce the problem in as many details as possible. Preferable together with an `m`-file. Remove everything which is not needed to reproduce the bug from the description and the `m`-file.
- a description of the **observed behaviour** after following your steps and point out the problem with that behaviour.
- an explanation of the **expected behaviour** and why it should be expected.
- details about your **configuration and environment**: 
  - version of *TTESTs* and 
  - version of *Matlab* and output of Matlabs `ver` / version of *Octave*
  - name and version of the OS in use
  - keyboard layout in use

### Suggesting Enhancements

Before creating enhancement suggestions, one shall check the file containing the design decisions. Maybe it is already on the list.

### Code Contribution

When making a first merge request, some active developer of the *TTESTs* shall be contacted.

## Style guide

### Matlab Style guide
#### Important rules
- All switch statements have an `otherwise` branch. In case the otherwise branch must not be executed, a harder error is to be generated. E.g. by calling a (non-existent) function with name `fatal_error`. The `otherwise` branch must not be a "do nothing"-branch. Whenever possible, the `otherwise` branch shall not be allowed to execute.
- There should be no warnings from the Matlab code checker, except for warnings which are different across Matlab version like `%#ok<DEFNU,NBRAK,NBRAK1,NBRAK2>`.
- Indentations are 4 spaces, no tabs shall ever be used.
- Python indentations are used (except for the closing `end` of a function)
    ```matlab
    for i = 1:10;
        i = i + 1; end;
    ```          
- All functions end with `end`. The line`function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.` is added at the end of each m-file, to avoid dangling `end`s.
- Each line, when possible, is finished with `;`.   
- Only one command for each line. E.g.
    ```matlab
    x = st( 1 );
    y = st( 2 );
    ```      
- Space after and before the first parentheses of a function, regardless whether in function notation,`plus( 2, 3 )`, or not, `2 + 3`.
    ```matlab
    prod( cumsum([1 3 2 5] + 1) ) + 7;
    ```
- Space after and before `=`.
    ```matlab
    i = 3;
    ```
- All variable/function/option/folder-names are in singular.
    ```matlab
    num_pair = 2;
    ```
- All `error` and `warning` commands shall provide an exception id and an exception message.
#### Not so important rules
- Using of short (but not too short) variable names and function names when possible.
- Spaces after commas, except when  separated by newlines, or in function calls in `name-value` pairs
    ```matlab
    b = {'a', 'b'; 'c', 'd'};
    gallery( 'circul',3 );
    ```
- No space after comma in idexing operations
    ```matlab
    a(2,3) = a(3,2);
    ```
- Usage of parentheses, whenever it improves code clarity
- No spaces at the begin and end of arrays
    ```matlab
    a = [1 2 3];
    ```
- Lines of whitespace between functions.

## Documentation Style guide

Each function which is intended to be called by the user is documented in source code and in `readme.md`.

### Source code documentation
Every `m-` file must be documented in the following format and order

1. Short description of the functions purpose
2. Function signature
3. Slightly longer description in more detail
4. Description of every input variable (type and meaning) in a section *Input*
5. Description of every output variable (type and meaning) in a section *Output*
6. Description of every possible option in a section *Options*
7. If necessary some *Remarks* in a section *Remarks* or *Notes*
8. Example usages
9. Person who and date/time when file was created 
10. Empty line 
11. changelog


#### Example
We give an example documentation of the the *(experimental)* `errorat` function which is included in *TTESTs*. Although its name it seems to be an *at* function, it is not, since the  API differs too wildly from the remaining *at* functions. Therefore, it is not included in the list of *at* functions in the documentation.

```matlab
function [ ret ] = errorat( funname, numline )
% This function tries to generate an error in the function funname at line numline 
% [] = errorat( funname )
% [] = errorat( funname, numline )
%
% Input: 
%   funname     char-array, the m-function in which to generate an error
%   numline     integer, the line where one shall try to inject the error
%               if not given, the "injected error" is deleted
%
% Note:
%   This function is just a proof-of-concept.
%   Do not call this function without arguments, since it will clear all variables in your workspace
%
% Example:
%   errorat( 'spy', 40 );  %inject error in 'spy.m' at line 40
%   spy; %run spy and see it fail somewhere after line 40
%   errorat( 'spy' ); %remove injected error
%
% Written by: tommsch, 2020-11-23
% Credits to:  Per Isakson’s tracer4m for the idea

% Changelog: tommsch, 2020-12-09:   Only error in specified line is generated

%TT EXPECT_NTHROW( @() errorat('spy',40) );
%TT a = 2;
%TT EXPECT_EQ( a, 2 );

    if( nargin == 0 );
        evalin( 'caller', 'clear' );
        ret = false;
    elseif( nargin == 1 );
        dbclear( 'in',funname );
    elseif( nargin == 2 );
        dbstop( 'in',funname, 'at', num2str(numline), 'if','errorat' ); end; 
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.
```

### *readme.md* documentation

The documentation in *readme.md* shall be a documentation for the user. It shall explain how things are done, and how things to do.

### *design.md* documentation
This file is 
-  a scratchpad for features currently in development 
-  issue tracker
- bug tracker
